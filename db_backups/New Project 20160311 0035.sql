-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.52-community


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema phone
--

CREATE DATABASE IF NOT EXISTS phone;
USE phone;

--
-- Definition of table `accesories_and_parts`
--

DROP TABLE IF EXISTS `accesories_and_parts`;
CREATE TABLE `accesories_and_parts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `Description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `accesories_and_parts`
--

/*!40000 ALTER TABLE `accesories_and_parts` DISABLE KEYS */;
INSERT INTO `accesories_and_parts` (`id`,`name`,`Description`) VALUES 
 (5,'','fgfgfg'),
 (6,'','10'),
 (8,'','10'),
 (9,'','10'),
 (10,'',''),
 (11,'','AAAA'),
 (12,'','AAAA'),
 (13,'','AAAA'),
 (14,'','akjsbnd ');
/*!40000 ALTER TABLE `accesories_and_parts` ENABLE KEYS */;


--
-- Definition of table `alert_types`
--

DROP TABLE IF EXISTS `alert_types`;
CREATE TABLE `alert_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alert` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `alert_types`
--

/*!40000 ALTER TABLE `alert_types` DISABLE KEYS */;
INSERT INTO `alert_types` (`id`,`alert`) VALUES 
 (1,'Vibration'),
 (2,'MP3'),
 (3,'WAV');
/*!40000 ALTER TABLE `alert_types` ENABLE KEYS */;


--
-- Definition of table `alert_types_has_sound`
--

DROP TABLE IF EXISTS `alert_types_has_sound`;
CREATE TABLE `alert_types_has_sound` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Alert_types_id` int(11) NOT NULL,
  `Sound_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Alert_types_has_Sound_Sound1_idx` (`Sound_id`),
  KEY `fk_Alert_types_has_Sound_Alert_types1_idx` (`Alert_types_id`),
  CONSTRAINT `fk_Alert_types_has_Sound_Alert_types1` FOREIGN KEY (`Alert_types_id`) REFERENCES `alert_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Alert_types_has_Sound_Sound1` FOREIGN KEY (`Sound_id`) REFERENCES `sound` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `alert_types_has_sound`
--

/*!40000 ALTER TABLE `alert_types_has_sound` DISABLE KEYS */;
INSERT INTO `alert_types_has_sound` (`id`,`Alert_types_id`,`Sound_id`) VALUES 
 (1,2,2),
 (2,3,2),
 (3,1,3),
 (4,2,3),
 (5,3,3),
 (6,1,4),
 (7,2,4),
 (8,3,4),
 (9,1,5),
 (10,2,5),
 (11,3,5),
 (12,1,6),
 (13,2,6),
 (14,3,6),
 (15,1,7),
 (16,2,7),
 (17,3,7),
 (18,1,8),
 (19,2,8),
 (20,3,8),
 (24,1,10),
 (25,2,10),
 (26,3,10),
 (27,1,11),
 (28,2,11),
 (29,3,11),
 (30,1,12),
 (31,2,12),
 (32,3,12),
 (33,1,13),
 (34,2,13),
 (35,3,13),
 (36,1,14),
 (37,2,14),
 (38,3,14),
 (39,1,15),
 (40,2,15),
 (41,1,16),
 (42,2,16),
 (43,1,17),
 (44,2,17),
 (45,1,18),
 (46,2,18);
/*!40000 ALTER TABLE `alert_types_has_sound` ENABLE KEYS */;


--
-- Definition of table `bands_has_network`
--

DROP TABLE IF EXISTS `bands_has_network`;
CREATE TABLE `bands_has_network` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Network_type_id` int(11) NOT NULL,
  `Network_id` int(11) NOT NULL,
  `Bands` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Bands_has_Network_Network1_idx` (`Network_id`),
  KEY `fk_Bands_has_Network_Bands1_idx` (`Network_type_id`),
  CONSTRAINT `fk_Bands_has_Network_Bands1` FOREIGN KEY (`Network_type_id`) REFERENCES `network_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Bands_has_Network_Network1` FOREIGN KEY (`Network_id`) REFERENCES `network` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bands_has_network`
--

/*!40000 ALTER TABLE `bands_has_network` DISABLE KEYS */;
INSERT INTO `bands_has_network` (`id`,`Network_type_id`,`Network_id`,`Bands`) VALUES 
 (1,1,3,'800/1000'),
 (2,2,3,'1000/2000'),
 (3,1,4,'sss'),
 (4,2,4,'sss'),
 (5,3,4,'sss'),
 (6,1,5,'sss'),
 (7,2,5,'sss'),
 (8,3,5,'sss'),
 (9,1,7,'sss'),
 (10,2,7,'sss'),
 (11,3,7,'sss'),
 (12,1,8,'sss'),
 (13,2,8,'sss'),
 (14,3,8,'sss'),
 (15,1,9,'GSM 850/900/1800/1900'),
 (16,2,9,'HSDPA 850/900/1900/2100-E6853'),
 (17,3,9,'1(2100),2(1900)'),
 (18,1,10,'GSM 850/900/1800/1900'),
 (19,2,10,'HSDPA 850/900/1900/2100-E6853'),
 (20,3,10,'1(2100),2(1900)'),
 (24,1,12,'GSM 850 / 900 / 1800 / 1900'),
 (25,2,12,'HSDPA 850 / 900 / 1900 / 2100   	HSDPA 850 / 1700(AWS) / 1900 / 2100 - H811'),
 (26,3,12,'LTE band 1(2100), 2(1900), 3(1800), 4(1700/2100), 5(850), 7(2600), 8(900), 17(700), 20(800), 28(700) - H815   	LTE band 1(2100), 3(1800), 4(1700/2100), 5(850), 7(2600), 8(900), 17(700), 20(800), 28(700), 40(2300) - H815T LTE band 4(1700/2100), 7(2600), 17(700) - H815P   	LTE band 2(1900), 3(1800), 4(1700/2100), 5(850), 7(2600), 12(700) - H811'),
 (27,1,13,'a'),
 (28,2,13,'a'),
 (29,1,14,'a'),
 (30,2,14,'a'),
 (31,1,15,'a'),
 (32,2,15,'a'),
 (33,1,17,'a'),
 (34,2,17,'a'),
 (35,1,18,'450'),
 (36,2,18,'300'),
 (37,3,18,'550'),
 (38,1,19,'450'),
 (39,2,19,'300'),
 (40,3,19,'550'),
 (41,1,20,'450'),
 (42,2,20,'300'),
 (43,3,20,'550'),
 (44,1,21,'450'),
 (45,2,21,'300'),
 (46,3,21,'550');
/*!40000 ALTER TABLE `bands_has_network` ENABLE KEYS */;


--
-- Definition of table `battery`
--

DROP TABLE IF EXISTS `battery`;
CREATE TABLE `battery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `size` varchar(45) DEFAULT NULL,
  `Removeble_id` int(11) NOT NULL,
  `Standy_time` varchar(45) DEFAULT NULL,
  `Call_time` varchar(45) DEFAULT NULL,
  `Fast_chrj_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Battery_Removeble1_idx` (`Removeble_id`),
  KEY `fk_Battery_Fast_chrj1_idx` (`Fast_chrj_id`),
  CONSTRAINT `fk_Battery_Fast_chrj1` FOREIGN KEY (`Fast_chrj_id`) REFERENCES `fast_chrj` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Battery_Removeble1` FOREIGN KEY (`Removeble_id`) REFERENCES `removeble` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `battery`
--

/*!40000 ALTER TABLE `battery` DISABLE KEYS */;
INSERT INTO `battery` (`id`,`size`,`Removeble_id`,`Standy_time`,`Call_time`,`Fast_chrj_id`) VALUES 
 (1,'2550',1,'10','11',1),
 (2,'4000MAH',1,'12','34',1),
 (3,'4000MAH',1,'12','34',1),
 (4,'4000MAH',1,'12','34',1),
 (5,'4000MAH',2,'12','34',1),
 (6,'Li-Ion 3430 mAh ',2,'0','0',1),
 (7,'Li-Ion 3430 mAh ',2,'0','0',1),
 (9,'3000',1,'360h','19h',1),
 (10,'qwqwqw',1,'qwqw','qwqw',2),
 (11,'qwqwqw',1,'qwqw','qwqw',2),
 (12,'qwqwqw',1,'qwqw','qwqw',2),
 (13,'qwqwqw',1,'qwqw','qwqw',1),
 (14,'1222',2,'21','21',1),
 (15,'1222',2,'21','21',1),
 (16,'1222',2,'21','21',1),
 (17,'1222',2,'21','21',1);
/*!40000 ALTER TABLE `battery` ENABLE KEYS */;


--
-- Definition of table `body`
--

DROP TABLE IF EXISTS `body`;
CREATE TABLE `body` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Dimension` varchar(45) DEFAULT NULL,
  `weight` varchar(45) DEFAULT NULL,
  `Sim_id` int(11) NOT NULL,
  `Sim_size_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Body_Sim1_idx` (`Sim_id`),
  KEY `fk_Body_Sim_size1_idx` (`Sim_size_id`),
  CONSTRAINT `fk_Body_Sim1` FOREIGN KEY (`Sim_id`) REFERENCES `sim` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Body_Sim_size1` FOREIGN KEY (`Sim_size_id`) REFERENCES `sim_size` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `body`
--

/*!40000 ALTER TABLE `body` DISABLE KEYS */;
INSERT INTO `body` (`id`,`Dimension`,`weight`,`Sim_id`,`Sim_size_id`) VALUES 
 (1,'10x20x30','150',1,1),
 (2,'10x20x30','150',2,2),
 (3,'10x20x30','150',2,3),
 (4,'10x20x30','150',2,3),
 (5,'12x12x12','10',1,2),
 (6,'12x12x12','10',1,2),
 (7,'12x12x12','10',1,2),
 (8,'12x12x12','10',1,3),
 (9,'154x75.8x7.8mm','180',2,3),
 (10,'154x75.8x7.8mm','180',2,3),
 (12,'148.9x76.1x6.3-9.8','155',1,2),
 (13,'axaxa','a',2,1),
 (14,'axaxa','a',2,1),
 (15,'axaxa','a',2,1),
 (16,'axaxa','a',2,2),
 (17,'1x2x3','123',1,3),
 (18,'1x2x3','123',1,3),
 (19,'1x2x3','123',1,3),
 (20,'1x2x3','123',1,3);
/*!40000 ALTER TABLE `body` ENABLE KEYS */;


--
-- Definition of table `camera`
--

DROP TABLE IF EXISTS `camera`;
CREATE TABLE `camera` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pixels` varchar(45) DEFAULT NULL,
  `Video` varchar(250) DEFAULT NULL,
  `Secondery` varchar(250) DEFAULT NULL,
  `resolution` varchar(45) DEFAULT NULL,
  `features` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `camera`
--

/*!40000 ALTER TABLE `camera` DISABLE KEYS */;
INSERT INTO `camera` (`id`,`pixels`,`Video`,`Secondery`,`resolution`,`features`) VALUES 
 (1,'21','asdab','5suer','4500 x 5000','tdgcbhjxzfu'),
 (2,'21','8','121','1024 x 1500','sdsdq'),
 (3,'21','8','121','1024 x 1500','sdsdq'),
 (4,'21','8','121','1024 x 1500','sdsdq'),
 (5,'21','8','121','1024 x 1500','sdsdq'),
 (6,'23','2160p@30fps, 1080p@60fps, 720p@120fps, HDR,','5.15.1 MP, f/2.4, 1080p, HDR','1024 x 1500','f/2.0, 24mm, phase detection autofocus, LED flash\n1/2.3\" sensor size, geo-tagging, touch focus, face detection, HDR, panorama'),
 (7,'23','2160p@30fps, 1080p@60fps, 720p@120fps, HDR,','5.15.1 MP, f/2.4, 1080p, HDR','1024 x 1500','f/2.0, 24mm, phase detection autofocus, LED flash\n1/2.3\" sensor size, geo-tagging, touch focus, face detection, HDR, panorama'),
 (9,'16','2160p@30fps, 1080p@60fps, HDR, stereo sound rec.,','8f/2.0, 1080p@30fps','1 x 1','f/1.8, 28 mm, laser autofocus, OIS, LED flash,1/2.6\" sensor size, 1.12 µm pixel size, geo-tagging, touch focus, face/smile detection, panorama, HDR'),
 (10,'qw','qw','qwq','qw x qw','qw'),
 (11,'qw','qw','qwq','qw x qw','qw'),
 (12,'qw','qw','qwq','qw x qw','qw'),
 (13,'qw','qw','qwq','qw x qw','qw'),
 (14,'12','12','112','12 x 21','12'),
 (15,'12','12','112','12 x 21','12'),
 (16,'12','12','112','12 x 21','12'),
 (17,'12','12','112','12 x 21','12');
/*!40000 ALTER TABLE `camera` ENABLE KEYS */;


--
-- Definition of table `camera_has_features`
--

DROP TABLE IF EXISTS `camera_has_features`;
CREATE TABLE `camera_has_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Camera_id` int(11) NOT NULL,
  `Features_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Camera_has_Features_Features1_idx` (`Features_id`),
  KEY `fk_Camera_has_Features_Camera1_idx` (`Camera_id`),
  CONSTRAINT `fk_Camera_has_Features_Camera1` FOREIGN KEY (`Camera_id`) REFERENCES `camera` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Camera_has_Features_Features1` FOREIGN KEY (`Features_id`) REFERENCES `features` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `camera_has_features`
--

/*!40000 ALTER TABLE `camera_has_features` DISABLE KEYS */;
/*!40000 ALTER TABLE `camera_has_features` ENABLE KEYS */;


--
-- Definition of table `card_slot`
--

DROP TABLE IF EXISTS `card_slot`;
CREATE TABLE `card_slot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slot` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `card_slot`
--

/*!40000 ALTER TABLE `card_slot` DISABLE KEYS */;
INSERT INTO `card_slot` (`id`,`slot`) VALUES 
 (1,'Yes'),
 (2,'No');
/*!40000 ALTER TABLE `card_slot` ENABLE KEYS */;


--
-- Definition of table `color`
--

DROP TABLE IF EXISTS `color`;
CREATE TABLE `color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `color`
--

/*!40000 ALTER TABLE `color` DISABLE KEYS */;
INSERT INTO `color` (`id`,`color`) VALUES 
 (1,'red'),
 (2,'any'),
 (3,'any'),
 (4,'any'),
 (5,'any'),
 (6,'Chrome, Black, Gold'),
 (7,'Chrome, Black, Gold'),
 (8,'Grey, White, Gold, Leather Black, Leather Brown, Leather Red, White/Gold'),
 (9,'qwqw'),
 (10,'qwqw'),
 (11,'qwqw'),
 (12,'qwqw'),
 (13,'12'),
 (14,'12'),
 (15,'12'),
 (16,'12');
/*!40000 ALTER TABLE `color` ENABLE KEYS */;


--
-- Definition of table `communication`
--

DROP TABLE IF EXISTS `communication`;
CREATE TABLE `communication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `WLAN` varchar(150) DEFAULT NULL,
  `Bluetooth` varchar(150) DEFAULT NULL,
  `GPS` varchar(150) DEFAULT NULL,
  `NFC_id` int(11) NOT NULL,
  `USB` varchar(150) DEFAULT NULL,
  `radio` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Communication_NFC1_idx` (`NFC_id`),
  CONSTRAINT `fk_Communication_NFC1` FOREIGN KEY (`NFC_id`) REFERENCES `nfc` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `communication`
--

/*!40000 ALTER TABLE `communication` DISABLE KEYS */;
INSERT INTO `communication` (`id`,`WLAN`,`Bluetooth`,`GPS`,`NFC_id`,`USB`,`radio`) VALUES 
 (1,'ffffs','ffff','0',2,'sssss','mono'),
 (2,'sss','yea','a gps',1,'','aksjLN'),
 (3,'sss','yea','a gps',1,'yes','aksjLN'),
 (4,'sss','yea','a gps',1,'yes','aksjLN'),
 (5,'sss','yea','A-gps',1,'yes','aksjLNs'),
 (6,'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, DLNA, hotspot','v4.1, A2DP, apt-X','A-GPS, GLONASS/ BDS (market dependant)',1,'microUSB v2.0 (MHL 3 TV-out), USB Host','RDS'),
 (7,'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, DLNA, hotspot','v4.1, A2DP, apt-X','A-GPS, GLONASS/ BDS (market dependant)',1,'microUSB v2.0 (MHL 3 TV-out), USB Host','RDS'),
 (9,'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, DLNA, hotspot','v4.1, A2DP, LE, apt-X','A-GPS, GLONASS',1,'microUSB v2.0 (SlimPort 4K), USB Host',' RDS'),
 (10,'qw','qw','qqq',1,'q','q'),
 (11,'qw','qw','qqq',1,'q','q'),
 (12,'qw','qw','qqq',1,'q','q'),
 (13,'qw','qw','a',1,'q','a'),
 (14,'222','12','1a',1,'no','a'),
 (15,'222','12','1a',1,'no','a'),
 (16,'222','12','1a',1,'no','a'),
 (17,'222','12','1a',1,'no','a');
/*!40000 ALTER TABLE `communication` ENABLE KEYS */;


--
-- Definition of table `delivery`
--

DROP TABLE IF EXISTS `delivery`;
CREATE TABLE `delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `phn` varchar(45) DEFAULT NULL,
  `ad1` varchar(45) DEFAULT NULL,
  `ad2` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `prov` varchar(45) DEFAULT NULL,
  `User_registration_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Delivery_User_registration1_idx` (`User_registration_id`),
  CONSTRAINT `fk_Delivery_User_registration1` FOREIGN KEY (`User_registration_id`) REFERENCES `user_registration` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `delivery`
--

/*!40000 ALTER TABLE `delivery` DISABLE KEYS */;
INSERT INTO `delivery` (`id`,`fname`,`lname`,`phn`,`ad1`,`ad2`,`city`,`prov`,`User_registration_id`) VALUES 
 (2,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (3,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (4,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (5,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (6,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (7,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (8,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (9,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (10,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (11,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (12,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (13,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (14,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (15,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (16,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (17,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (18,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (19,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (20,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (21,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (22,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (23,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (24,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (25,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (26,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (27,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (28,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (29,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (30,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (31,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (32,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (33,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (34,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (35,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (36,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (37,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (38,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (39,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (40,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (41,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (42,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (43,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (44,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (45,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (46,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (47,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92),
 (48,'mad','rangika','0715953316','rangiri','Millewa ','horana','NW',92);
/*!40000 ALTER TABLE `delivery` ENABLE KEYS */;


--
-- Definition of table `device_type`
--

DROP TABLE IF EXISTS `device_type`;
CREATE TABLE `device_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Smart phone, tab,accesories, normal phone\n';

--
-- Dumping data for table `device_type`
--

/*!40000 ALTER TABLE `device_type` DISABLE KEYS */;
INSERT INTO `device_type` (`id`,`type`) VALUES 
 (1,'Smart phones'),
 (2,'Tabs'),
 (3,'Accessories'),
 (4,'Smart watches');
/*!40000 ALTER TABLE `device_type` ENABLE KEYS */;


--
-- Definition of table `display`
--

DROP TABLE IF EXISTS `display`;
CREATE TABLE `display` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Size` varchar(45) DEFAULT NULL,
  `Resolution` varchar(45) DEFAULT NULL,
  `Protection` varchar(100) DEFAULT NULL,
  `Scrn_type_id` int(11) NOT NULL,
  `Scrn_body_ratio` varchar(45) DEFAULT NULL,
  `pixel_density` varchar(45) DEFAULT NULL,
  `Touch_type_id` int(11) NOT NULL,
  `Multi_touch_id` int(11) DEFAULT NULL,
  `Other` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Display_Scrn_type1_idx` (`Scrn_type_id`),
  KEY `fk_Display_Touch_type1_idx` (`Touch_type_id`),
  KEY `fk_Display_Multi_touch1_idx` (`Multi_touch_id`),
  CONSTRAINT `fk_Display_Multi_touch1` FOREIGN KEY (`Multi_touch_id`) REFERENCES `multi_touch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Display_Scrn_type1` FOREIGN KEY (`Scrn_type_id`) REFERENCES `scrn_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Display_Touch_type1` FOREIGN KEY (`Touch_type_id`) REFERENCES `touch_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `display`
--

/*!40000 ALTER TABLE `display` DISABLE KEYS */;
INSERT INTO `display` (`id`,`Size`,`Resolution`,`Protection`,`Scrn_type_id`,`Scrn_body_ratio`,`pixel_density`,`Touch_type_id`,`Multi_touch_id`,`Other`) VALUES 
 (1,'4.0','150x500','gorilla 3',4,'60','4.0',1,2,'Nothing bn'),
 (2,'4.0','150x500','gorilla 3',4,'60','156',3,2,'Nothing bn'),
 (3,'5.5','20x20','gorilla glass',6,'65.2','450',3,1,'aaaa'),
 (4,'5.5','20x20','gorilla glass',6,'65.2','450',3,1,'aaaa'),
 (5,'5.5','20x20','gorilla glass',6,'65.2','450',3,1,'aaaa'),
 (6,'5.0','20x20','gorilla glass',7,'78.0','450',3,1,'aaaa'),
 (7,'5.5','2160x3840','Scratch-resistant glass, oleophobic coating',3,'70.4','~806',3,1,'aaaa'),
 (8,'5.5','2160x3840','Scratch-resistant glass, oleophobic coating',3,'70.4','~806',3,1,'aaaa'),
 (10,'5.2','1440x2560','Corning Gorilla Glass 3',3,'72.5','538',3,1,'- LG Optimus UX 4.0 UI'),
 (11,'a','axa','a',7,'a','a',1,1,'a'),
 (12,'a','axa','a',7,'a','a',1,1,'a'),
 (13,'a','axa','a',7,'a','a',1,1,'a'),
 (14,'a','axa','a',7,'a','a',2,2,'a'),
 (15,'5','12x21','gorilla',7,'12','1212',3,1,'211'),
 (16,'5','12x21','gorilla',7,'12','1212',3,1,'211'),
 (17,'5','12x21','gorilla',7,'12','1212',3,1,'211'),
 (18,'5','12x21','gorilla',7,'12','1212',3,1,'211');
/*!40000 ALTER TABLE `display` ENABLE KEYS */;


--
-- Definition of table `fast_chrj`
--

DROP TABLE IF EXISTS `fast_chrj`;
CREATE TABLE `fast_chrj` (
  `id` int(11) NOT NULL,
  `stat` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fast_chrj`
--

/*!40000 ALTER TABLE `fast_chrj` DISABLE KEYS */;
INSERT INTO `fast_chrj` (`id`,`stat`) VALUES 
 (1,'Yes'),
 (2,'No');
/*!40000 ALTER TABLE `fast_chrj` ENABLE KEYS */;


--
-- Definition of table `features`
--

DROP TABLE IF EXISTS `features`;
CREATE TABLE `features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feature` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `features`
--

/*!40000 ALTER TABLE `features` DISABLE KEYS */;
/*!40000 ALTER TABLE `features` ENABLE KEYS */;


--
-- Definition of table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
CREATE TABLE `invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `User_registration_id` int(11) NOT NULL,
  `date` varchar(45) DEFAULT NULL,
  `Delivery_id` int(11) NOT NULL,
  `user_deleted` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Invoice_User_registration1_idx` (`User_registration_id`),
  KEY `fk_Invoice_Delivery1_idx` (`Delivery_id`),
  CONSTRAINT `fk_Invoice_Delivery1` FOREIGN KEY (`Delivery_id`) REFERENCES `delivery` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Invoice_User_registration1` FOREIGN KEY (`User_registration_id`) REFERENCES `user_registration` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice`
--

/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` (`id`,`User_registration_id`,`date`,`Delivery_id`,`user_deleted`) VALUES 
 (2,92,'2016-01-28_17:44:40',2,'1'),
 (3,92,'2016-01-28_18:17:59',3,'1'),
 (4,92,'2016-01-28_19:42:10',4,'1'),
 (5,92,'2016-01-28_19:42:31',5,'1'),
 (6,92,'2016-01-28_19:42:41',6,'1'),
 (7,92,'2016-01-28_19:43:50',7,'1'),
 (8,92,'2016-01-28_19:44:08',8,'1'),
 (9,92,'2016-01-28_19:44:20',9,'1'),
 (10,92,'2016-01-28_19:44:30',10,'1'),
 (11,92,'2016-01-28_19:45:05',11,'1'),
 (12,92,'2016-01-28_19:45:39',12,'1'),
 (13,92,'2016-01-28_19:45:41',13,'1'),
 (14,92,'2016-01-28_19:45:41',14,'1'),
 (15,92,'2016-01-28_19:45:41',15,'1'),
 (16,92,'2016-01-28_19:45:42',16,'1'),
 (17,92,'2016-01-28_19:45:42',17,'1'),
 (18,92,'2016-01-28_19:45:58',18,'1'),
 (19,92,'2016-01-28_19:46:08',19,'1'),
 (20,92,'2016-01-28_19:54:31',20,'1'),
 (21,92,'2016-01-28_19:56:33',21,'1'),
 (22,92,'2016-01-28_20:05:21',22,'1'),
 (23,92,'2016-01-28_20:06:48',23,'1'),
 (24,92,'2016-01-28_20:11:42',24,'1'),
 (25,92,'2016-01-28_20:15:30',25,'1'),
 (26,92,'2016-01-28_20:23:11',26,'1'),
 (27,92,'2016-01-28_20:43:15',27,'1'),
 (28,92,'2016-01-28_20:43:25',28,'1'),
 (29,92,'2016-01-28_20:43:46',29,'1'),
 (30,92,'2016-01-28_20:44:49',30,'1'),
 (31,92,'2016-01-28_21:21:18',31,'1'),
 (32,92,'2016-01-28_21:22:06',32,'1'),
 (33,92,'2016-01-28_21:27:42',33,'1'),
 (34,92,'2016-01-28_21:57:25',34,'1'),
 (35,92,'2016-01-28_21:57:40',35,'1'),
 (36,92,'2016-01-28_21:58:02',36,'1'),
 (37,92,'2016-01-28_21:58:26',37,'1'),
 (38,92,'2016-01-28_22:23:33',38,'1'),
 (39,92,'2016-01-28_22:24:39',39,'1'),
 (40,92,'2016-01-28_22:24:46',40,'1'),
 (41,92,'2016-01-29_14:50:21',41,'1'),
 (42,92,'2016-01-29_14:50:36',42,'1'),
 (43,92,'2016-02-12_18:10:31',43,'1'),
 (44,92,'2016-02-20_16:42:40',44,'1'),
 (45,92,'2016-03-01_15:26:10',45,'1'),
 (46,92,'2016-03-06_17:03:39',46,'1'),
 (47,92,'2016-03-07_14:13:58',47,'1'),
 (48,92,'2016-03-07_14:18:10',48,'1');
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;


--
-- Definition of table `invoice_registry`
--

DROP TABLE IF EXISTS `invoice_registry`;
CREATE TABLE `invoice_registry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Invoice_id` int(11) NOT NULL,
  `Products_id` int(11) NOT NULL,
  `Qty` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Invoice_has_Products_Products1_idx` (`Products_id`),
  KEY `fk_Invoice_has_Products_Invoice1_idx` (`Invoice_id`),
  CONSTRAINT `fk_Invoice_has_Products_Invoice1` FOREIGN KEY (`Invoice_id`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Invoice_has_Products_Products1` FOREIGN KEY (`Products_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_registry`
--

/*!40000 ALTER TABLE `invoice_registry` DISABLE KEYS */;
INSERT INTO `invoice_registry` (`id`,`Invoice_id`,`Products_id`,`Qty`) VALUES 
 (1,2,2,'1'),
 (2,2,1,'1'),
 (3,3,6,'1'),
 (4,3,2,'1'),
 (5,5,9,'1'),
 (6,6,9,'1'),
 (7,7,9,'1'),
 (8,31,2,'1'),
 (9,32,2,'1'),
 (10,33,2,'1'),
 (11,41,9,'1'),
 (12,43,6,'1'),
 (13,44,2,'1'),
 (14,45,6,'1'),
 (15,46,2,'1'),
 (16,46,1,'1'),
 (17,47,2,'1'),
 (18,48,2,'1'),
 (19,48,1,'1');
/*!40000 ALTER TABLE `invoice_registry` ENABLE KEYS */;


--
-- Definition of table `java_support`
--

DROP TABLE IF EXISTS `java_support`;
CREATE TABLE `java_support` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `java_support`
--

/*!40000 ALTER TABLE `java_support` DISABLE KEYS */;
INSERT INTO `java_support` (`id`,`status`) VALUES 
 (1,'Yes'),
 (2,'No');
/*!40000 ALTER TABLE `java_support` ENABLE KEYS */;


--
-- Definition of table `launch`
--

DROP TABLE IF EXISTS `launch`;
CREATE TABLE `launch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Announced_date` varchar(45) DEFAULT NULL,
  `Launch_status_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Launch_Launch_status1_idx` (`Launch_status_id`),
  CONSTRAINT `fk_Launch_Launch_status1` FOREIGN KEY (`Launch_status_id`) REFERENCES `launch_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `launch`
--

/*!40000 ALTER TABLE `launch` DISABLE KEYS */;
INSERT INTO `launch` (`id`,`Announced_date`,`Launch_status_id`) VALUES 
 (1,'2016-1-4',1),
 (2,'2015-8-6',2),
 (3,'2015-8-6',2),
 (4,'2015-8-6',2),
 (5,'2015-8-6',2),
 (6,'2015-9',2),
 (7,'2015-9',2),
 (9,'2015-4',2),
 (10,'2016-1-12',1),
 (11,'2016-1-12',1),
 (12,'2016-1-12',1),
 (13,'2016-1-12',1),
 (14,'2016-1-5',1),
 (15,'2016-1-5',1),
 (16,'2016-1-5',1),
 (17,'2016-1-5',1);
/*!40000 ALTER TABLE `launch` ENABLE KEYS */;


--
-- Definition of table `launch_status`
--

DROP TABLE IF EXISTS `launch_status`;
CREATE TABLE `launch_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stat` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `launch_status`
--

/*!40000 ALTER TABLE `launch_status` DISABLE KEYS */;
INSERT INTO `launch_status` (`id`,`stat`) VALUES 
 (1,'Coming soon'),
 (2,'Available');
/*!40000 ALTER TABLE `launch_status` ENABLE KEYS */;


--
-- Definition of table `loud_speaker`
--

DROP TABLE IF EXISTS `loud_speaker`;
CREATE TABLE `loud_speaker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `loud_speaker`
--

/*!40000 ALTER TABLE `loud_speaker` DISABLE KEYS */;
INSERT INTO `loud_speaker` (`id`,`type`) VALUES 
 (1,'Yes'),
 (2,'No');
/*!40000 ALTER TABLE `loud_speaker` ENABLE KEYS */;


--
-- Definition of table `manufacturers`
--

DROP TABLE IF EXISTS `manufacturers`;
CREATE TABLE `manufacturers` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manufacturers`
--

/*!40000 ALTER TABLE `manufacturers` DISABLE KEYS */;
INSERT INTO `manufacturers` (`Id`,`name`) VALUES 
 (1,'Lg'),
 (2,'Sony'),
 (3,'Samsung'),
 (4,'Microsoft'),
 (5,'Htc'),
 (6,'Apple'),
 (7,'Nokia'),
 (8,'Huawei'),
 (10,'OPPO'),
 (18,'Beats'),
 (19,'Ammam');
/*!40000 ALTER TABLE `manufacturers` ENABLE KEYS */;


--
-- Definition of table `memory`
--

DROP TABLE IF EXISTS `memory`;
CREATE TABLE `memory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Internel` varchar(45) DEFAULT NULL,
  `RAM` varchar(45) DEFAULT NULL,
  `Card_slot_id` int(11) NOT NULL,
  `card_slot_upto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Memory_Card_slot1_idx` (`Card_slot_id`),
  CONSTRAINT `fk_Memory_Card_slot1` FOREIGN KEY (`Card_slot_id`) REFERENCES `card_slot` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `memory`
--

/*!40000 ALTER TABLE `memory` DISABLE KEYS */;
INSERT INTO `memory` (`ID`,`Internel`,`RAM`,`Card_slot_id`,`card_slot_upto`) VALUES 
 (1,'32','2',1,'16'),
 (2,'32','2',2,'0'),
 (3,'16 or 32','3',1,'2tb'),
 (4,'16 or 32','3',1,'0'),
 (5,'16 or 32','3',1,'0'),
 (6,'16 or 32','1',1,'68'),
 (7,'32','3',1,'200'),
 (8,'32','3',1,'200'),
 (10,'32','3',1,'128'),
 (11,'qw','qw',1,'12'),
 (12,'qw','qw',1,'12'),
 (13,'qw','qw',1,'12'),
 (14,'qw','qw',2,'0'),
 (15,'16','1',1,'12'),
 (16,'16','1',1,'12'),
 (17,'16','1',1,'12'),
 (18,'16','1',1,'12');
/*!40000 ALTER TABLE `memory` ENABLE KEYS */;


--
-- Definition of table `model`
--

DROP TABLE IF EXISTS `model`;
CREATE TABLE `model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(45) DEFAULT NULL,
  `Manufacturers_Id` int(11) NOT NULL,
  `Specification_Id` int(11) DEFAULT NULL,
  `Device_type_id` int(11) NOT NULL,
  `img_src` varchar(1000) DEFAULT NULL,
  `Accesories_and_parts_id` int(11) DEFAULT NULL,
  `keywords` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Model_Manufacturers1_idx` (`Manufacturers_Id`),
  KEY `fk_Model_Specification1_idx` (`Specification_Id`),
  KEY `fk_Model_Device_type1_idx` (`Device_type_id`),
  KEY `fk_Model_Accesories_&_parts1_idx` (`Accesories_and_parts_id`),
  CONSTRAINT `fk_Model_Accesories_&_parts1` FOREIGN KEY (`Accesories_and_parts_id`) REFERENCES `accesories_and_parts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Model_Device_type1` FOREIGN KEY (`Device_type_id`) REFERENCES `device_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Model_Manufacturers1` FOREIGN KEY (`Manufacturers_Id`) REFERENCES `manufacturers` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Model_Specification1` FOREIGN KEY (`Specification_Id`) REFERENCES `specification` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `model`
--

/*!40000 ALTER TABLE `model` DISABLE KEYS */;
INSERT INTO `model` (`id`,`model`,`Manufacturers_Id`,`Specification_Id`,`Device_type_id`,`img_src`,`Accesories_and_parts_id`,`keywords`) VALUES 
 (1,'Xperia z5 premium',2,2,1,'uploads/z5.jpg',NULL,'sony z5,sonyz5,xperia z5'),
 (7,'Bello',1,7,1,'uploads/bello.jpg',NULL,'lg bello,lg l bello'),
 (9,'tab air',6,8,2,'uploads/appletab.jpg',NULL,'apple tab, apple tab air'),
 (10,'S6 edge',3,9,2,'uploads/s6edge.jpg',NULL,'s6 edge'),
 (11,'p6',8,10,2,'uploads/p6.jpg',NULL,'huawei'),
 (12,'p6',8,11,2,'uploads/p6.jpg',NULL,'huawei'),
 (14,'F12',1,NULL,3,'uploads/f12.jpg',8,'f12'),
 (15,'Handsfree',7,NULL,3,'uploads/hf.jpg',9,'handsree'),
 (16,'A2',8,NULL,3,'uploads/2109-flowers-1.jpg14538205428802109-flowers-1.jpg',12,'nullHuawei,A2,AAAA,Huawei A2A2 Huawei'),
 (17,'A2',1,NULL,3,'uploads/19321.jpg145382063264819321.jpg',13,'Lg,A2,AAAA,Lg A2A2 Lg'),
 (18,'1564',18,NULL,3,'uploads/2109-flowers-1.jpg14567339089722109-flowers-1.jpg',14,'Beats,1564,akjsbnd ,Beats 1564,1564 Beats,');
/*!40000 ALTER TABLE `model` ENABLE KEYS */;


--
-- Definition of table `multi_touch`
--

DROP TABLE IF EXISTS `multi_touch`;
CREATE TABLE `multi_touch` (
  `id` int(11) NOT NULL,
  `Multi_touch` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `multi_touch`
--

/*!40000 ALTER TABLE `multi_touch` DISABLE KEYS */;
INSERT INTO `multi_touch` (`id`,`Multi_touch`) VALUES 
 (1,'Yes'),
 (2,'No');
/*!40000 ALTER TABLE `multi_touch` ENABLE KEYS */;


--
-- Definition of table `network`
--

DROP TABLE IF EXISTS `network`;
CREATE TABLE `network` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Speed` varchar(100) DEFAULT NULL,
  `GPRS` varchar(100) DEFAULT NULL,
  `EDGE` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `network`
--

/*!40000 ALTER TABLE `network` DISABLE KEYS */;
INSERT INTO `network` (`id`,`Speed`,`GPRS`,`EDGE`) VALUES 
 (3,'140','2','1'),
 (4,'ssss','1','1'),
 (5,'ssss','1','1'),
 (7,'ssss','1','1'),
 (8,'ssss','1','1'),
 (9,'HSPA 42.2/5.76 Mbps, LTE Cat6 300/50 Mbps','1','1'),
 (10,'HSPA 42.2/5.76 Mbps, LTE Cat6 300/50 Mbps','1','1'),
 (12,'HSPA 42.2/5.76 Mbps, LTE Cat6 300/50 Mbps','1','1'),
 (13,'a','1','1'),
 (14,'a','1','1'),
 (15,'a','1','1'),
 (17,'a','2','2'),
 (18,'aaa','1','1'),
 (19,'aaa','1','1'),
 (20,'aaa','1','1'),
 (21,'aaa','1','1');
/*!40000 ALTER TABLE `network` ENABLE KEYS */;


--
-- Definition of table `network_types`
--

DROP TABLE IF EXISTS `network_types`;
CREATE TABLE `network_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Bands` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `network_types`
--

/*!40000 ALTER TABLE `network_types` DISABLE KEYS */;
INSERT INTO `network_types` (`id`,`Bands`) VALUES 
 (1,'2G'),
 (2,'3G'),
 (3,'4G');
/*!40000 ALTER TABLE `network_types` ENABLE KEYS */;


--
-- Definition of table `nfc`
--

DROP TABLE IF EXISTS `nfc`;
CREATE TABLE `nfc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nfc`
--

/*!40000 ALTER TABLE `nfc` DISABLE KEYS */;
INSERT INTO `nfc` (`id`,`status`) VALUES 
 (1,'Yes'),
 (2,'No');
/*!40000 ALTER TABLE `nfc` ENABLE KEYS */;


--
-- Definition of table `os_list`
--

DROP TABLE IF EXISTS `os_list`;
CREATE TABLE `os_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oses` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `os_list`
--

/*!40000 ALTER TABLE `os_list` DISABLE KEYS */;
INSERT INTO `os_list` (`id`,`oses`) VALUES 
 (1,'Other'),
 (2,'Android'),
 (3,'Windows'),
 (4,'iOS'),
 (5,'Firefox'),
 (6,'CynogenMod(Andriod based)'),
 (7,'Blackbery'),
 (42,'asss'),
 (43,''),
 (44,'sddsd'),
 (45,'sddsd'),
 (46,'asss');
/*!40000 ALTER TABLE `os_list` ENABLE KEYS */;


--
-- Definition of table `other_features`
--

DROP TABLE IF EXISTS `other_features`;
CREATE TABLE `other_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Browser` varchar(45) DEFAULT NULL,
  `Other` varchar(500) DEFAULT NULL,
  `Messeging` varchar(150) DEFAULT NULL,
  `Java_support_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Other_features_Java_support1_idx` (`Java_support_id`),
  CONSTRAINT `fk_Other_features_Java_support1` FOREIGN KEY (`Java_support_id`) REFERENCES `java_support` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `other_features`
--

/*!40000 ALTER TABLE `other_features` DISABLE KEYS */;
INSERT INTO `other_features` (`id`,`Browser`,`Other`,`Messeging`,`Java_support_id`) VALUES 
 (1,'html5','jnjkansd','msi',2),
 (2,'S','SSS','S',1),
 (3,'S','SSS','S',1),
 (4,'S','SSS','S',1),
 (5,'S','SSS','S',1),
 (6,'HTML5','- Fast battery charging: 60% in 30 min (Quick Charge 2.0) - ANT+ support','SMS (threaded view), MMS, Email, IM, Push Email',2),
 (7,'HTML5','- Fast battery charging: 60% in 30 min (Quick Charge 2.0) - ANT+ support','SMS (threaded view), MMS, Email, IM, Push Email',2),
 (9,'HTML5',' Fast battery charging: 60% in 30 min (Quick Charge 2.0) - Optional Wireless Charging (Qi-enabled) - Active noise cancellation with dedicated mic - MP4/DviX/XviD/H.264/WMV player - MP3/WAV/FLAC/eAAC+/WMA player - Photo/video editor - Document editor','SMS(threaded view), MMS, Email, Push Mail, IM',2),
 (10,'qw','qw','qw',1),
 (11,'qw','qw','qw',1),
 (12,'qw','qw','qw',1),
 (13,'qw','qw','qw',2),
 (14,'s','a','ars',2),
 (15,'s','a','ars',2),
 (16,'s','a','ars',2),
 (17,'s','a','ars',2);
/*!40000 ALTER TABLE `other_features` ENABLE KEYS */;


--
-- Definition of table `other_features_has_sensors`
--

DROP TABLE IF EXISTS `other_features_has_sensors`;
CREATE TABLE `other_features_has_sensors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Other_features_id` int(11) NOT NULL,
  `Sensors_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Other_features_has_Sensors_Sensors1_idx` (`Sensors_id`),
  KEY `fk_Other_features_has_Sensors_Other_features1_idx` (`Other_features_id`),
  CONSTRAINT `fk_Other_features_has_Sensors_Other_features1` FOREIGN KEY (`Other_features_id`) REFERENCES `other_features` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Other_features_has_Sensors_Sensors1` FOREIGN KEY (`Sensors_id`) REFERENCES `sensors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `other_features_has_sensors`
--

/*!40000 ALTER TABLE `other_features_has_sensors` DISABLE KEYS */;
INSERT INTO `other_features_has_sensors` (`id`,`Other_features_id`,`Sensors_id`) VALUES 
 (1,1,1),
 (2,1,5),
 (3,1,6),
 (4,1,7),
 (5,1,14),
 (6,2,1),
 (7,2,2),
 (8,2,3),
 (9,2,4),
 (10,2,5),
 (11,2,11),
 (12,2,12),
 (13,2,13),
 (14,3,1),
 (15,3,2),
 (16,3,3),
 (17,3,4),
 (18,3,5),
 (19,3,11),
 (20,3,12),
 (21,3,13),
 (22,4,1),
 (23,4,2),
 (24,4,3),
 (25,4,4),
 (26,4,5),
 (27,4,11),
 (28,4,12),
 (29,4,13),
 (30,5,1),
 (31,5,2),
 (32,5,3),
 (33,5,4),
 (34,5,5),
 (35,5,11),
 (36,5,12),
 (37,5,13),
 (38,6,1),
 (39,6,2),
 (40,6,4),
 (41,6,6),
 (42,6,11),
 (43,6,12),
 (44,6,1),
 (45,6,2),
 (46,6,4),
 (47,6,6),
 (48,6,11),
 (49,6,12),
 (50,7,1),
 (51,7,2),
 (52,7,4),
 (53,7,6),
 (54,7,11),
 (55,7,12),
 (56,7,1),
 (57,7,2),
 (58,7,4),
 (59,7,6),
 (60,7,11),
 (61,7,12),
 (62,7,1),
 (63,7,2),
 (64,7,4),
 (65,7,6),
 (66,7,11),
 (67,7,12),
 (86,9,1),
 (87,9,2),
 (88,9,4),
 (89,9,6),
 (90,9,12),
 (91,9,15),
 (92,9,1),
 (93,9,2),
 (94,9,4),
 (95,9,6),
 (96,9,12),
 (97,9,15),
 (98,9,1),
 (99,9,2),
 (100,9,4),
 (101,9,6),
 (102,9,12),
 (103,9,15),
 (104,9,1),
 (105,9,2),
 (106,9,4),
 (107,9,6),
 (108,9,12),
 (109,9,15),
 (110,10,8),
 (111,10,4),
 (112,10,13),
 (113,11,8),
 (114,11,4),
 (115,11,13),
 (116,11,8),
 (117,11,4),
 (118,11,13),
 (119,12,8),
 (120,12,4),
 (121,12,13),
 (122,12,8),
 (123,12,4),
 (124,12,13),
 (125,12,8),
 (126,12,4),
 (127,12,13),
 (128,13,8),
 (129,13,4),
 (130,13,13),
 (131,14,1),
 (132,14,9),
 (133,15,1),
 (134,15,9),
 (135,15,1),
 (136,15,9),
 (137,16,1),
 (138,16,9),
 (139,16,1),
 (140,16,9),
 (141,16,1),
 (142,16,9),
 (143,17,1),
 (144,17,9),
 (145,17,1),
 (146,17,9),
 (147,17,1),
 (148,17,9),
 (149,17,1),
 (150,17,9);
/*!40000 ALTER TABLE `other_features_has_sensors` ENABLE KEYS */;


--
-- Definition of table `page_registry`
--

DROP TABLE IF EXISTS `page_registry`;
CREATE TABLE `page_registry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Pages_id` int(11) NOT NULL,
  `User_types_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_User_types_has_Pages_Pages1_idx` (`Pages_id`),
  KEY `fk_Page_registry_User_types1_idx` (`User_types_id`),
  CONSTRAINT `fk_Page_registry_User_types1` FOREIGN KEY (`User_types_id`) REFERENCES `user_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_types_has_Pages_Pages1` FOREIGN KEY (`Pages_id`) REFERENCES `pages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page_registry`
--

/*!40000 ALTER TABLE `page_registry` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_registry` ENABLE KEYS */;


--
-- Definition of table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;


--
-- Definition of table `platforms_use`
--

DROP TABLE IF EXISTS `platforms_use`;
CREATE TABLE `platforms_use` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chipet_use` varchar(150) DEFAULT NULL,
  `CPU_use` varchar(150) DEFAULT NULL,
  `gpu_use` varchar(150) DEFAULT NULL,
  `Version_use_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Platforms_use_Version_use1_idx` (`Version_use_id`),
  CONSTRAINT `fk_Platforms_use_Version_use1` FOREIGN KEY (`Version_use_id`) REFERENCES `version_use` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `platforms_use`
--

/*!40000 ALTER TABLE `platforms_use` DISABLE KEYS */;
INSERT INTO `platforms_use` (`id`,`chipet_use`,`CPU_use`,`gpu_use`,`Version_use_id`) VALUES 
 (1,'gnomw','aaaa','sdSPJFK',4),
 (2,'gnomw','aaaa','sdSPJFK',10),
 (3,'q','a','m',13),
 (4,'q','a','m',10),
 (5,'q','a','m',10),
 (6,'q','a','m',24),
 (7,'Qualcomm MSM8994 Snapdragon 810','Quad-core 1.5 GHz Cortex-A53 & Quad-core 2 GHz Cortex-A57','Adreno 430',13),
 (8,'Qualcomm MSM8994 Snapdragon 810','Quad-core 1.5 GHz Cortex-A53 & Quad-core 2 GHz Cortex-A57','Adreno 430',13),
 (10,'Qualcomm MSM8992 Snapdragon 808','Quad-core 1.44 GHz Cortex-A53 & dual-core 1.82 GHz Cortex-A57','Adreno 418',13),
 (11,'a','a','a',13),
 (12,'a','a','a',13),
 (13,'a','a','a',13),
 (14,'a','a','a',10),
 (15,'aa','aa','aa',24),
 (16,'aa','aa','aa',24),
 (17,'aa','aa','aa',10),
 (18,'aa','aa','aa',10);
/*!40000 ALTER TABLE `platforms_use` ENABLE KEYS */;


--
-- Definition of table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Added_date` varchar(45) DEFAULT NULL,
  `price` varchar(45) DEFAULT NULL,
  `Qty` varchar(45) DEFAULT NULL,
  `Description` varchar(45) DEFAULT NULL,
  `Model_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Products_Model1_idx` (`Model_id`),
  CONSTRAINT `fk_Products_Model1` FOREIGN KEY (`Model_id`) REFERENCES `model` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`,`Added_date`,`price`,`Qty`,`Description`,`Model_id`) VALUES 
 (1,'date','36000','8','',7),
 (2,'2016-01-13_15:37:51','78500','16',NULL,1),
 (3,'date','24000','10','',9),
 (4,'date','30000','0','',10),
 (5,'20160113_153241','26000','10','',11),
 (6,'2016-01-13_15:33:15','24000','7','',12),
 (7,'2016-01-13_15:37:08','18500','12','',14),
 (8,'2016-01-13_15:37:51','13000','12','',15),
 (9,'2016-01-26_20:32:30','120','0','',16),
 (10,'2016-01-26_20:33:59','250','0','',17),
 (11,'2016-02-29_13:50:15','jasn','jnsjd','',18);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;


--
-- Definition of table `removeble`
--

DROP TABLE IF EXISTS `removeble`;
CREATE TABLE `removeble` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `removeble`
--

/*!40000 ALTER TABLE `removeble` DISABLE KEYS */;
INSERT INTO `removeble` (`id`,`status`) VALUES 
 (1,'Removeble'),
 (2,'Non-removeble');
/*!40000 ALTER TABLE `removeble` ENABLE KEYS */;


--
-- Definition of table `scrn_type`
--

DROP TABLE IF EXISTS `scrn_type`;
CREATE TABLE `scrn_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scrntype` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scrn_type`
--

/*!40000 ALTER TABLE `scrn_type` DISABLE KEYS */;
INSERT INTO `scrn_type` (`id`,`scrntype`) VALUES 
 (2,'TFT'),
 (3,'IPS'),
 (4,'OLED'),
 (5,'AMOLED'),
 (6,'Super AMOLED'),
 (7,'Retina'),
 (8,'LCD');
/*!40000 ALTER TABLE `scrn_type` ENABLE KEYS */;


--
-- Definition of table `sensors`
--

DROP TABLE IF EXISTS `sensors`;
CREATE TABLE `sensors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sensor` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sensors`
--

/*!40000 ALTER TABLE `sensors` DISABLE KEYS */;
INSERT INTO `sensors` (`id`,`sensor`) VALUES 
 (1,'Accelerometer'),
 (2,'Gyroscope '),
 (3,'Magnetometer'),
 (4,'Proximity '),
 (5,'Light'),
 (6,'Barometer '),
 (7,'Thermometer '),
 (8,'Air Humidity'),
 (9,'Pedometer '),
 (10,'Heart rate'),
 (11,'Fingerprint '),
 (12,'Compass'),
 (13,'SpO2'),
 (14,'UV'),
 (15,'Color Spectrum');
/*!40000 ALTER TABLE `sensors` ENABLE KEYS */;


--
-- Definition of table `sim`
--

DROP TABLE IF EXISTS `sim`;
CREATE TABLE `sim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sim_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim`
--

/*!40000 ALTER TABLE `sim` DISABLE KEYS */;
INSERT INTO `sim` (`id`,`sim_type`) VALUES 
 (1,'Single SIM'),
 (2,'Dual SIM');
/*!40000 ALTER TABLE `sim` ENABLE KEYS */;


--
-- Definition of table `sim_size`
--

DROP TABLE IF EXISTS `sim_size`;
CREATE TABLE `sim_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `size` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_size`
--

/*!40000 ALTER TABLE `sim_size` DISABLE KEYS */;
INSERT INTO `sim_size` (`id`,`size`) VALUES 
 (1,'Mini SIM'),
 (2,'Micro SIM'),
 (3,'Nano SIM');
/*!40000 ALTER TABLE `sim_size` ENABLE KEYS */;


--
-- Definition of table `sound`
--

DROP TABLE IF EXISTS `sound`;
CREATE TABLE `sound` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loud_speaker_id` int(11) NOT NULL,
  `threefivve_jack_id` int(11) NOT NULL,
  `other` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Sound_loud_speaker1_idx` (`loud_speaker_id`),
  KEY `fk_Sound_threefivve_jack1_idx` (`threefivve_jack_id`),
  CONSTRAINT `fk_Sound_loud_speaker1` FOREIGN KEY (`loud_speaker_id`) REFERENCES `loud_speaker` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Sound_threefivve_jack1` FOREIGN KEY (`threefivve_jack_id`) REFERENCES `threefivve_jack` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sound`
--

/*!40000 ALTER TABLE `sound` DISABLE KEYS */;
INSERT INTO `sound` (`id`,`loud_speaker_id`,`threefivve_jack_id`,`other`) VALUES 
 (2,2,1,'asdadsf'),
 (3,1,1,'sss'),
 (4,1,1,'sss'),
 (5,1,1,'sss'),
 (6,1,1,'sss'),
 (7,1,1,' High-Res audio,stereo speakers'),
 (8,1,1,' High-Res audio,stereo speakers'),
 (10,1,1,'no'),
 (11,1,2,'qw'),
 (12,1,2,'qw'),
 (13,1,2,'qw'),
 (14,2,2,'qw'),
 (15,1,1,'1212'),
 (16,1,1,'1212'),
 (17,1,1,'1212'),
 (18,1,1,'1212');
/*!40000 ALTER TABLE `sound` ENABLE KEYS */;


--
-- Definition of table `specification`
--

DROP TABLE IF EXISTS `specification`;
CREATE TABLE `specification` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Launch_id` int(11) NOT NULL,
  `Body_id` int(11) NOT NULL,
  `Display_id` int(11) NOT NULL,
  `Network_id` int(11) NOT NULL,
  `Memory_ID` int(11) NOT NULL,
  `Camera_id` int(11) NOT NULL,
  `Sound_id` int(11) NOT NULL,
  `Communication_id` int(11) NOT NULL,
  `Other_features_id` int(11) NOT NULL,
  `Battery_id` int(11) NOT NULL,
  `Platforms_use_id` int(11) NOT NULL,
  `Color_id` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_Specification_Launch1_idx` (`Launch_id`),
  KEY `fk_Specification_Body1_idx` (`Body_id`),
  KEY `fk_Specification_Display1_idx` (`Display_id`),
  KEY `fk_Specification_Network1_idx` (`Network_id`),
  KEY `fk_Specification_Memory1_idx` (`Memory_ID`),
  KEY `fk_Specification_Camera1_idx` (`Camera_id`),
  KEY `fk_Specification_Sound1_idx` (`Sound_id`),
  KEY `fk_Specification_Communication1_idx` (`Communication_id`),
  KEY `fk_Specification_Other_features1_idx` (`Other_features_id`),
  KEY `fk_Specification_Battery1_idx` (`Battery_id`),
  KEY `fk_Specification_Platforms_use1_idx` (`Platforms_use_id`),
  KEY `fk_Specification_Color1` (`Color_id`),
  CONSTRAINT `fk_Specification_Battery1` FOREIGN KEY (`Battery_id`) REFERENCES `battery` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Specification_Body1` FOREIGN KEY (`Body_id`) REFERENCES `body` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Specification_Camera1` FOREIGN KEY (`Camera_id`) REFERENCES `camera` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Specification_Color1` FOREIGN KEY (`Color_id`) REFERENCES `color` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Specification_Communication1` FOREIGN KEY (`Communication_id`) REFERENCES `communication` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Specification_Display1` FOREIGN KEY (`Display_id`) REFERENCES `display` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Specification_Launch1` FOREIGN KEY (`Launch_id`) REFERENCES `launch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Specification_Memory1` FOREIGN KEY (`Memory_ID`) REFERENCES `memory` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Specification_Network1` FOREIGN KEY (`Network_id`) REFERENCES `network` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Specification_Other_features1` FOREIGN KEY (`Other_features_id`) REFERENCES `other_features` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Specification_Platforms_use1` FOREIGN KEY (`Platforms_use_id`) REFERENCES `platforms_use` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Specification_Sound1` FOREIGN KEY (`Sound_id`) REFERENCES `sound` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `specification`
--

/*!40000 ALTER TABLE `specification` DISABLE KEYS */;
INSERT INTO `specification` (`Id`,`Launch_id`,`Body_id`,`Display_id`,`Network_id`,`Memory_ID`,`Camera_id`,`Sound_id`,`Communication_id`,`Other_features_id`,`Battery_id`,`Platforms_use_id`,`Color_id`) VALUES 
 (2,7,10,8,10,8,7,8,7,7,7,8,7),
 (3,9,12,10,12,10,9,10,9,9,9,10,8),
 (4,10,13,11,13,11,10,11,10,10,10,11,9),
 (5,11,14,12,14,12,11,12,11,11,11,12,10),
 (6,12,15,13,15,13,12,13,12,12,12,13,11),
 (7,13,16,14,17,14,13,14,13,13,13,14,12),
 (8,14,17,15,18,15,14,15,14,14,14,15,13),
 (9,15,18,16,19,16,15,16,15,15,15,16,14),
 (10,16,19,17,20,17,16,17,16,16,16,17,15),
 (11,17,20,18,21,18,17,18,17,17,17,18,16);
/*!40000 ALTER TABLE `specification` ENABLE KEYS */;


--
-- Definition of table `status`
--

DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `Id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` (`Id`,`status`) VALUES 
 (1,'Pending'),
 (2,'Active'),
 (3,'Disable');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;


--
-- Definition of table `threefivve_jack`
--

DROP TABLE IF EXISTS `threefivve_jack`;
CREATE TABLE `threefivve_jack` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `threefivve_jack`
--

/*!40000 ALTER TABLE `threefivve_jack` DISABLE KEYS */;
INSERT INTO `threefivve_jack` (`id`,`status`) VALUES 
 (1,'Yes'),
 (2,'No');
/*!40000 ALTER TABLE `threefivve_jack` ENABLE KEYS */;


--
-- Definition of table `touch_type`
--

DROP TABLE IF EXISTS `touch_type`;
CREATE TABLE `touch_type` (
  `id` int(11) NOT NULL,
  `Touch_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `touch_type`
--

/*!40000 ALTER TABLE `touch_type` DISABLE KEYS */;
INSERT INTO `touch_type` (`id`,`Touch_type`) VALUES 
 (1,'Non touch'),
 (2,'Resistive'),
 (3,'Capacitive');
/*!40000 ALTER TABLE `touch_type` ENABLE KEYS */;


--
-- Definition of table `user_details`
--

DROP TABLE IF EXISTS `user_details`;
CREATE TABLE `user_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `ad_line1` varchar(400) DEFAULT NULL,
  `ad_line2` varchar(45) DEFAULT NULL,
  `City` varchar(45) DEFAULT NULL,
  `Provice` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_details`
--

/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;
INSERT INTO `user_details` (`id`,`fname`,`lname`,`phone`,`ad_line1`,`ad_line2`,`City`,`Provice`) VALUES 
 (1,'a','a','a','a','a','a','a'),
 (3,'Ishara','Ruchiranga','nm','No 52','airport road','Anuradhapura','NCP'),
 (4,NULL,NULL,'1212',NULL,NULL,NULL,NULL),
 (5,NULL,NULL,'1212',NULL,NULL,NULL,NULL),
 (6,NULL,NULL,'1212',NULL,NULL,NULL,NULL),
 (7,NULL,NULL,'1212',NULL,NULL,NULL,NULL),
 (8,NULL,NULL,'1212',NULL,NULL,NULL,NULL),
 (9,NULL,NULL,'1212',NULL,NULL,NULL,NULL),
 (10,NULL,NULL,'1212','no 51','irr','annn','w'),
 (11,NULL,NULL,NULL,'nip','kjb','jhvb','jlbv'),
 (12,NULL,NULL,'1223','123','123','123','132');
/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;


--
-- Definition of table `user_registration`
--

DROP TABLE IF EXISTS `user_registration`;
CREATE TABLE `user_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) DEFAULT NULL,
  `user_name` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `Status_Id` int(11) NOT NULL,
  `User_details_id` int(11) DEFAULT NULL,
  `User_types_id` int(11) NOT NULL,
  `register_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_User_registration_User_details1_idx` (`User_details_id`),
  KEY `fk_User_registration_User_types1_idx` (`User_types_id`),
  KEY `fk_User_registration_Status1_idx` (`Status_Id`),
  CONSTRAINT `fk_User_registration_Status1` FOREIGN KEY (`Status_Id`) REFERENCES `status` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_registration_User_details1` FOREIGN KEY (`User_details_id`) REFERENCES `user_details` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_registration_User_types1` FOREIGN KEY (`User_types_id`) REFERENCES `user_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_registration`
--

/*!40000 ALTER TABLE `user_registration` DISABLE KEYS */;
INSERT INTO `user_registration` (`id`,`email`,`user_name`,`password`,`Status_Id`,`User_details_id`,`User_types_id`,`register_id`) VALUES 
 (92,'ishara.ira@gmail.com','ishara','aaaaa',2,3,1,'702a8d1f-811f-43c5-b814-337990514d57'),
 (93,'akash@gmail.com','madu','akash',3,NULL,1,'886fec62-56e9-4fda-8159-262ce3957fd8'),
 (94,'akash2@gmail.com','akash','123456',2,NULL,2,'3477c7c2-8236-427a-8f56-8fb30688fe4b'),
 (95,'oi@g.com','aaaa','qqqqqq',2,NULL,1,'a9e5f7bd-e125-4246-9336-c5085207bc7c'),
 (96,'aaa@gmail.com','is','aaaaa',2,NULL,2,'05b90339-3ebc-43d4-acd0-674b23567269'),
 (97,'akash@gmail.com.saa','a','asasas',1,NULL,1,'e7b40184-7513-4e1a-975e-689729edd2eb'),
 (98,'dj@ishara.com','ishara','asasas',2,NULL,1,'0'),
 (99,'testing.a@gmail.com','testing','123456',2,12,1,'0'),
 (100,'ishara@mail.com','asdhjkas','123456',1,NULL,1,'c280b13a-8ce3-41b6-b1aa-ca85f4e8aff1'),
 (101,'ishara@gmail.com','ish2','asasas',2,11,1,'6ebf2da7-570d-485a-91f4-17985d3b4022'),
 (102,'akshin@gmail.com','madhu','vvvvv',2,10,1,'a524537f-989b-4f59-aa55-3e1701756c84');
/*!40000 ALTER TABLE `user_registration` ENABLE KEYS */;


--
-- Definition of table `user_types`
--

DROP TABLE IF EXISTS `user_types`;
CREATE TABLE `user_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_types`
--

/*!40000 ALTER TABLE `user_types` DISABLE KEYS */;
INSERT INTO `user_types` (`id`,`type`) VALUES 
 (1,'Customer'),
 (2,'Admin');
/*!40000 ALTER TABLE `user_types` ENABLE KEYS */;


--
-- Definition of table `version_use`
--

DROP TABLE IF EXISTS `version_use`;
CREATE TABLE `version_use` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Versions_use` varchar(45) DEFAULT NULL,
  `Os_List_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Version_use_Os_List1_idx` (`Os_List_id`),
  CONSTRAINT `fk_Version_use_Os_List1` FOREIGN KEY (`Os_List_id`) REFERENCES `os_list` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `version_use`
--

/*!40000 ALTER TABLE `version_use` DISABLE KEYS */;
INSERT INTO `version_use` (`id`,`Versions_use`,`Os_List_id`) VALUES 
 (1,'Cupcake',2),
 (2,'Donut',2),
 (3,'Eclair',2),
 (4,'Froyo',2),
 (5,'Gingerbread',2),
 (6,'Honeycomb',2),
 (7,'Ice Cream Sandwich',2),
 (8,'Kitkat(4.4)',2),
 (9,'Kitkat(4.4.4)',2),
 (10,'Lollipop(5.0)',2),
 (11,'Lollipop(5.0.1)',2),
 (12,'Lollipop(5.0.2)',2),
 (13,'Lollipop(5.1)',2),
 (14,'Marshmallow(6.0)',2),
 (15,'Marshmallow(6.0.1)',2),
 (16,'iOS-1',4),
 (17,'iOS-2',4),
 (18,'iOS-3',4),
 (19,'iOS-4',4),
 (20,'iOS-5',4),
 (21,'iOS-6',4),
 (22,'iOS-7',4),
 (23,'iOS-8',4),
 (24,'iOS-9',4),
 (34,'asasasas',42),
 (35,'',3),
 (36,'',3),
 (37,'10',4),
 (38,'ASSSS',2),
 (39,'nutela',2),
 (40,'jhgvbkhjb',46);
/*!40000 ALTER TABLE `version_use` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
