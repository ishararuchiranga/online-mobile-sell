<%-- 
    Document   : Admin_shipping3
    Created on : Jun 13, 2016, 11:25:55 PM
    Author     : isharar
--%>

<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css" />
        <link href="css/font-awesome.min.css" rel="stylesheet">

        <script src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/calendar.js"></script>
        <script src="js/jquery.ajaxfileupload.js"></script>
    </head>
    <body>
        <style>
            .icolor{
                background: -webkit-linear-gradient(top, #8cc903 ,  #afe073); /* For Safari 5.1 to 6.0 */
                background: -o-linear-gradient(top, #8cc903 ,  #afe073); /* For Opera 11.1 to 12.0 */
                background: -moz-linear-gradient(top, #8cc903 ,  #afe073); /* For Firefox 3.6 to 15 */
                background: linear-gradient(top, #8cc903 ,  #afe073); /* Standard syntax (must be last) */
            }
            .red-colo{
                background: -webkit-linear-gradient(top, #fc1616 ,  #c20202);  /* For Safari 5.1 to 6.0 */
                background: -o-linear-gradient(top, #fc1616 , #b38709);  /* For Opera 11.1 to 12.0 */
                background: -moz-linear-gradient(top, #fc1616 ,  #c20202); /* For Firefox 3.6 to 15 */
                background: linear-gradient(top, #fc1616 ,  #c20202); /* Standard syntax (must be last) */
            }
            .bar1{
                background: -webkit-linear-gradient(top, #fc1616 ,  #c20202);  /* For Safari 5.1 to 6.0 */
                background: -o-linear-gradient(top, #fc1616 , #b38709);  /* For Opera 11.1 to 12.0 */
                background: -moz-linear-gradient(top, #fc1616 ,  #c20202); /* For Firefox 3.6 to 15 */
                background: linear-gradient(top, #fc1616 ,  #c20202); /* Standard syntax (must be last) */
                position: fixed;
                width: 44%;
                height: 14px;
                margin-top: 20px;
                margin-left: 40px;

            }
            .bar3{
                background: -webkit-linear-gradient(top, #8cc903 ,  #afe073); /* For Safari 5.1 to 6.0 */
                background: -o-linear-gradient(top, #8cc903 ,  #afe073); /* For Opera 11.1 to 12.0 */
                background: -moz-linear-gradient(top, #8cc903 ,  #afe073); /* For Firefox 3.6 to 15 */
                background: linear-gradient(top, #8cc903 ,  #afe073); /* Standard syntax (must be last) */
                position: fixed;
                width: 0%;
                height: 14px;
                margin-top: 20px;
                margin-left: 40px;
            }
            .bar2{
                background: -webkit-linear-gradient(top, #fc1616 ,  #c20202);  /* For Safari 5.1 to 6.0 */
                background: -o-linear-gradient(top, #fc1616 ,  #c20202);  /* For Opera 11.1 to 12.0 */
                background: -moz-linear-gradient(top, #fc1616 ,  #c20202); /* For Firefox 3.6 to 15 */
                background: linear-gradient(top, #fc1616 ,  #c20202); /* Standard syntax (must be last) */
                position: fixed;
                width: 44%;
                height: 14px;
                margin-top: 20px;
                margin-left: 420px;
            }

            .li1{
                position: fixed; 
                left: 31px; 
                top: 12px; 
                padding: 3px; 
                border-radius: 16px; 
                font-size: 25px; 
                color: white;
                z-index: 1;
            }
            .li2{

                position: fixed; 
                left: 47%;
                top: 12px;
                padding: 1px 3px 3px 6px; 
                border-radius: 16px; 
                font-size: 25px;
                color: white;
                width: 30px;
                z-index: 1;
            }
            .li3{
                background: -webkit-linear-gradient(top, #fc1616 ,  #c20202);  /* For Safari 5.1 to 6.0 */
                background: -o-linear-gradient(top, #fc1616 , #b38709);  /* For Opera 11.1 to 12.0 */
                background: -moz-linear-gradient(top, #fc1616 ,  #c20202); /* For Firefox 3.6 to 15 */
                background: linear-gradient(top, #fc1616 ,  #c20202); /* Standard syntax (must be last) */
                position: fixed; 
                left: 93%; top: 12px;
                padding: 1px 3px 3px 6px;
                border-radius: 16px; 
                font-size: 25px;
                color: white; 
                width: 30px;
                z-index: 1;
            }
            .span1{
                position: fixed;
                top: 40px;
                left: 29px;
            }
            .span2{
                position: fixed;
                top: 40px;
                left: 390px;
            }
            .span3{
                position: fixed;
                top: 40px;
                left: 782px;
            }

            #myDivId{background:red; color:white; padding:10px; overflow:hidden; position:relative;}
            .content{
                z-index:9;
                position:relative;
            }
            .progress{
                z-index:1 ;
                width:0px;
                height:100%;
                position:absolute;
                background:blue;
                left:0px;
                top:0px;
            }
            .tb-ship-td{
                border-top:  1px solid rgb(247, 247, 245);
                /*border-bottom:   1px solid rgb(247, 247, 245);*/
                color: rgb(95, 89, 89);
            }
            .tb-ship-td:hover{
                color: #7f7fec;
                border: 1px solid #7f7fec;
                background-color: #f1f1ff;
            }
            .Done-btn{
                border: 0;
                padding: 4px;
                background:  #8cc903 ; /* For Safari 5.1 to 6.0 */
                background: #8cc903 ;/* For Opera 11.1 to 12.0 */
                background: #8cc903 ; /* For Firefox 3.6 to 15 */
                background: #8cc903 ; /* Standard syntax (must be last) */
                color: white;

            }
            .btn-td{
                width: 80px;
                text-align: center;
            }
            .delover-btn{
                float: right;
                margin-right: 30px;
                width: 120px;
                height: 28px;
                margin-top: 15px;
                background-color: orange;
                border-radius: 14px;

            }
            .delover-btn:disabled{
                background-color: #efc882;;   
            }
            .done-btn{
                float: right;
                margin-right: 30px;
                width: 120px;
                height: 28px;
                margin-top: 15px;
                background-color: #8cc903;
                border: 0;
                border-radius: 14px;
                color: white;
            }


        </style>

        <%
            String id = request.getParameter("id");
            Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
            Criteria cr = ses.createCriteria(DB.Invoice.class);
            cr.add(Restrictions.eq("id", Integer.parseInt(id)));
            DB.Invoice inv = (DB.Invoice) cr.uniqueResult();

        %>
        <%    //
            if (inv.getDeleverStatus().getId() == 1) {
        %>
        <div>
            <i  class="fa fa-check li1 icolor"></i>
            <i id="li2co" class="fa fa-times li2 red-colo"></i>
            <i  class="fa fa-times li3"></i>
            <div  class="bar1">        </div>
            <div  id="progress" class="bar3">        </div>
            <div class="bar2">        </div>
            <span class="span1">Order</span>
            <span class="span2">Shipped</span>
            <span class="span3">Delever</span>
        </div>
        <%
        } else {

        %>
        <div>
            <i class="fa fa-check li1 icolor"></i>
            <i style="padding:3px" id="li2co" class="fa li2 icolor fa-check"></i>
            <i class="fa fa-times li3"></i>
            <div class="bar1">        </div>
            <div style="width: 44%;" id="progress" class="bar3">        </div>
            <div class="bar2">        </div>
            <span class="span1">Order</span>
            <span class="span2">Shipped</span>
            <span class="span3">Delever</span>
        </div>
        <%       //
            }
        %>

        <div style="margin-top: 80px; width: 50%; float: left; padding-left: 34px">
            TO:<br>
            <address style="font-size: 18px; padding-left: 40px;">
                <%=inv.getDelivery().getFname()%> <%=inv.getDelivery().getLname()%><br>
                <%=inv.getDelivery().getAd1()%><br>
                <%=inv.getDelivery().getAd2()%><br>
                <%=inv.getDelivery().getCity()%><br>
                <%=inv.getDelivery().getDistrict().getDistrict()%><br>
                <%=inv.getDelivery().getPhn()%>
            </address> 
        </div>
        <div style="margin-top: 90px; width: 50%;float: right;padding-right:  14px">
            <table style="width: 98%">
                <tr>
                    <th></th>
                    <th>Product ID</th>
                    <th>Name</th>
                    <th>Qty</th>
                    <th></th>
                </tr>

                <%
                    Criteria cr2 = ses.createCriteria(DB.InvoiceRegistry.class);
                    cr2.add(Restrictions.eq("invoice", inv));
                    List<DB.InvoiceRegistry> listin = cr2.list();
                    int cou = 0;
                    for (DB.InvoiceRegistry in : listin) {
                        ++cou;

                %>

                <tr class="tb-ship-td">
                    <td><%=cou%></td>
                    <td class="btn-td"><%=in.getProducts().getId()%></td>
                    <td><%=in.getProducts().getModel().getManufacturers().getName()%> <%=in.getProducts().getModel().getModel()%></td>
                    <td class="btn-td"><%=in.getQty()%></td>
                    <td class="btn-td" id="btnid<%=cou%>">
                        <%    //
                            if (inv.getDeleverStatus().getId() == 1) {

                        %>
                        <button class="Done-btn" onclick="check(<%=cou%>)">Check</button>
                        <%
                        } else {
                        %>
                        <i class="fa fa-check" style="color:#8cc903;"></i>
                        <%
                            }
                        %>

                    </td>
                </tr>
                <%
                    }
                %>

                <script>
                    var productsize =<%=cou%>;
                    var productsize2 = productsize;
                    function check(tdid) {
                        $('#btnid' + tdid + '').html('<i class="fa fa-check" style="color:#8cc903;"></i>');
                        productsize = productsize - 1;
                        if (productsize == 0) {
                            $('.delover-btn').prop('disabled', false);
                        }
                    }
                </script>
            </table>
            <%    //
                if (inv.getDeleverStatus().getId() == 1) {

            %>
            <button class="delover-btn" disabled="disabled">Ship&nbsp;&nbsp;<img src="index.png"></button>

            <%            } else {
            %>
            <button class="done-btn" disabled="disabled">Shiped&nbsp;&nbsp;<i class="fa fa-check" style="color:white;"></i></button>
            <%
                }
            %>
            <script>
                $(".delover-btn").click(function () {
                    $('#progress').animate({width: '44%'}, 990);
                    setTimeout(
                            function ()
                            {
                                $('#li2co').removeClass('red-colo');
                                $('#li2co').addClass('icolor');
                                $('#li2co').removeClass('fa-times');
                                $('#li2co').addClass('fa-check');
                                $('#li2co').attr('style', "padding:3px");
                                $(".delover-btn").replaceWith('<button class="done-btn" disabled="disabled">Shiped&nbsp;&nbsp;<i class="fa fa-check" style="color:white;"></i></button>');
                            }, 1000);

                    $.post(
                            "Admin_deliver_product",
                            {invid:<%=id%>},
                    function (result, status) {
                    });
                });
            </script>
        </div>
    </body>
</html>
