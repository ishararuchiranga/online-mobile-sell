<%-- 
Document   : User_management
Created on : Dec 27, 2015, 8:55:28 PM
Author     : Ishara
--%>

<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/main.js"></script>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/admin.css" rel="stylesheet">

        <style>
            body{
                background-color: rgb(248, 248, 248);
            }
            .nothingfound{
                text-align: center;
                padding: 20px 0px 20px 0px;
                background-color: rgb(143, 234, 143);
                margin-top: 40px;
                border-radius: 30px;
            }
        </style>

    </head>
    <body>



        <%
            int n = 100;

            String type = request.getParameter("type");
            String key = request.getParameter("key");
            boolean isnum = true;
            try {
                int a = Integer.parseInt(key);
            } catch (NumberFormatException e) {
                isnum = false;
            }

            if (type != null) {
                Session s = conn.NewHibernateUtil.getSessionFactory().openSession();
                Criteria criteria = s.createCriteria(DB.UserRegistration.class);
                DB.UserTypes u = (DB.UserTypes) s.load(DB.UserTypes.class, Integer.parseInt(type));
                
                if (key != null) {
                    if (isnum) {
                        criteria.add(Restrictions.eq("id", Integer.parseInt(key)));
                    } else {
                        criteria.add(Restrictions.like("email", "%" + key + "%"));
                    }
                }

                criteria.add(Restrictions.eq("userTypes", u));
                criteria.addOrder(Order.desc("id"));
                criteria.setMaxResults(n);

                List<DB.UserRegistration> list = criteria.list();

                if (!(list.isEmpty())) {


        %>
        <table cellspacing='0' id="Tableid"> <!-- cellspacing='0' is important, must stay -->
            <tr id="foc">
                <th>ID</th>
                <th>User name</th>
                <th>Email</th>
                <th>Status</th>
                <th>User Type</th>
            </tr><!-- Table Header -->

            <%                for (DB.UserRegistration ssr : list) {

            %>

            <tr class="xtable">
                <td class="xtable" ><%=ssr.getId()%></td>
                <td class="xtable"> <%=ssr.getUserName()%></td>
                <td class="xtable"><%=ssr.getEmail()%></td>
                <td class="xtable"><%=ssr.getStatus().getStatus()%></td>
                <td class="xtable"><%=ssr.getUserTypes().getType()%></td>

            </tr><!-- Table Row -->
            <%

                }
            %>
        </table>
        <%
        } else {

        %>
        <div class="nothingfound">
            <h3>Nothing Found....!</h3>
        </div>
        <%                }
        %> 

        <div id="Editcustomer" class="modalDialog3">
            <div>
                <form action="Logout" method="post" class="centerbtns">
                    <a href="#close" title="Close" onclick="hideelement()" class="close">X</a>
                    <div class="form-group ">

                        <p>Change user</p>
                        <div><h6 id="id"></h6>
                            <h6 id="uname"></h6>
                            <h6 id="email"></h6>
                            <h6 id="status"></h6>
                            <h6 id="utype"></h6>
                        </div>                        
                        <table>
                            <tr>
                                <th>Element</th>
                                <th>Chage</th>
                            </tr>
                            <tr>
                                <td>User type</td>
                                <td> 
                                    <button id="chngtypebtn" type="button" onclick="" class="btn btn-warning">
                                        <span class="glyphicon glyphicon-edit"></span>Edit
                                    </button>
                                    <select class="form-control hideitems" id="chngtype">
                                        <%
                                            s = conn.NewHibernateUtil.getSessionFactory().openSession();
                                            Criteria c2 = s.createCriteria(DB.UserTypes.class);

                                            List<DB.UserTypes> ut = c2.list();

                                        %>
                                        <option value="0">Nothing Change</option>

                                        <%                                            for (DB.UserTypes ust : ut) {
                                        %>
                                        <option value="<%=ust.getId()%>"><%=ust.getType()%></option>
                                        <%
                                            }
                                        %>    
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>User status</td>
                                <td> 
                                    <button id="chngstatbtn" type="button" onclick="" class="btn btn-warning">
                                        <span class="glyphicon glyphicon-edit"></span>Edit
                                    </button>
                                    <select class="form-control hideitems" id="chngstat">
                                        <%
                                            s = conn.NewHibernateUtil.getSessionFactory().openSession();
                                            Criteria c3 = s.createCriteria(DB.Status.class);

                                            List<DB.Status> usst = c3.list();
                                        %>
                                        <option value="0">Nothing Change</option>
                                        <%
                                            for (DB.Status st : usst) {

                                        %>
                                        <option value="<%=st.getId()%>"><%=st.getStatus()%></option>
                                        <%

                                            }
                                        %> 
                                    </select>
                                </td>
                            </tr>
                        </table>

                        <label id="Sameelements" class="hideitems"><h6 class="typcn typcn-times wrongformfield"> Nothing to change</h6></label>
                        <div class="search_bar_box ">
                            <button type="button" id="updateuserinfo" class="btn btn-info">
                                <span class="glyphicon glyphicon-floppy-disk"></span> Save Changes
                            </button>
                        </div>    
                    </div>
                </form>
            </div>
        </div>
        <%
            }
        %>


        <script>

            $(document).ready(function () {
                $("tr.xtable").click(function () {
                    var tableData = $(this).children("td").map(function () {
                        return $(this).text();
                    }).get();
                    var id = $.trim(tableData[0]);
                    alert(id);
                    document.getElementById("foc").focus();
                    window.location.replace("User_management2.jsp?id=" + id + "");
//                    var name = $.trim(tableData[1]);
//                    var email = $.trim(tableData[2]);
//                    var status = $.trim(tableData[3]);
//                    var utype = $.trim(tableData[4]);
//                    var uas = $.trim(tableData[5]);
//                    $('#id').text(id);
//                    $('#uname').text(name);
//                    $('#email').text(email);
//                    $('#status').text(status);
//                    $('#utype').text(utype);
//                    window.location = "#Editcustomer";
                });

//                var curtype = "nothing";
//                $("#chngtypebtn").click(function () {
//
//                    if ($('#utype').text() == "Customer") {
//                        curtype = 1;
//                    }
//                    if ($('#utype').text() == "Admin") {
//                        curtype = 2;
//                    }
//                    $("#chngtypebtn").hide();
//                    $("#chngtype").show();
//
//                    var selectobject = document.getElementById("chngtype");
//                    for (var i = 0; i < selectobject.length; i++) {
//                        if (selectobject.options[i].value == curtype) {
//                            selectobject.remove(i);
//                        }
//                    }
//                });
//                var curstate = "nothing";
//                $("#chngstatbtn").click(function () {
//                    if ($('#status').text() == "Pending") {
//                        curstate = 1;
//                    }
//                    if ($('#status').text() == "Active") {
//                        curstate = 2;
//                    }
//                    if ($('#status').text() == "Disable") {
//                        curstate = 3;
//                    }
//
//                    $("#chngstatbtn").hide();
//                    $("#chngstat").show();
//                    var selectobject = document.getElementById("chngstat");
//                    for (var i = 0; i < selectobject.length; i++) {
//                        if (selectobject.options[i].value == curstate) {
//                            selectobject.remove(i);
//                        }
//                    }
//                });
//
//                $("#updateuserinfo").click(function () {
//
//                    alert("clicked");
//
//                    if (($('#chngtype').val() === "0" & $('#chngstat').val() === "0")) {
//                        $('#Sameelements').show();
//                    } else if (!$("#chngtype").is(":visible") & !$("#chngstat").is(":visible")) {
//                        $('#Sameelements').show();
//                    } else if (!$("#chngtype").is(":visible") & $('#chngstat').val() === "0") {
//                        $('#Sameelements').show();
//                    } else if (!$("#chngstat").is(":visible") & $('#chngtype').val() === "0") {
//                        $('#Sameelements').show();
//                    } else {
//                        var sendstat;
//                        if (!$("#chngstat").is(":visible")) {
//                            sendstat = curstate;
//                        } else if ($('#chngstat').val() === "0") {
//                            sendstat = curstate;
//                        } else {
//                            sendstat = $('#chngstat').val();
//                        }
//
//                        var sendutype;
//                        if (!$("#chngtype").is(":visible")) {
//                            sendutype = curtype;
//                        } else if ($('#chngtype').val() === "0") {
//                            sendutype = curtype;
//                        } else {
//                            sendutype = $('#chngtype').val();
//                        }
//                        alert("else");
//                        var sendid = $('#id').text();
//                        $.post(
//                                "Admin_user_chnge",
//                                {Id: sendid, stid: sendstat, utid: sendutype}, //meaasge you want to send
//                        function (result) {
//                        });
//                    }
//                });
            });






            function hideelement() {
                $("#chngtype").hide();
                $("#chngstat").hide();

                $("#chngtypebtn").show();
                $("#chngstatbtn").show();


                $('#chngtype').children().remove();
                $('#chngstat').children().remove();

                $("#chngtype").append(new Option("Admin", '2'));
                $("#chngtype").append(new Option("Customer", '1'));
                $("#chngtype").append(new Option("Nothing Change", '0'));


                $("#chngstat").append(new Option("Disable", '3'));
                $("#chngstat").append(new Option("Active", '2'));
                $("#chngstat").append(new Option("Pending", '1'));
                $("#chngstat").append(new Option("Nothing Change", '0'));

            }
        </script>
    </body>
</html>
