<%-- 
    Document   : slidertexst
    Created on : Jul 5, 2016, 11:13:47 AM
    Author     : Ishara
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/js-image-slider.css" rel="stylesheet" type="text/css">
        <script src="js/js-image-slider.js" type="text/javascript"></script><style></style>
        <link href="css/generic.css" rel="stylesheet" type="text/css">

    </head>
    <body>
        <h1>Hello World!</h1>
        <div id="sliderFrame">
            <div id="slider">
                <a href="http://www.menucool.com/javascript-image-slider" target="_blank">
                    <img src="images/image-slider-1.jpg" alt="Welcome to Menucool.com" />
                </a>
                <img src="images/image-slider-2.jpg" />
                <img src="images/image-slider-3.jpg" alt="" />
                <img src="images/image-slider-4.jpg" alt="#htmlcaption" />
                <img src="images/image-slider-5.jpg" />
            </div>
            <div id="htmlcaption" style="display: none;">
                <em>HTML</em> caption. Link to <a href="http://www.google.com/">Google</a>.
            </div>
        </div>
    </body>
</html>
