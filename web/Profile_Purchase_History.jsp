<%-- 
    Document   : Profile_Purchase_History
    Created on : Mar 4, 2016, 3:48:13 PM
    Author     : Ishara
--%>

<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/typicons.min.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">

        <link href="css/price-range.css" rel="stylesheet">

        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/script.js"></script>
        <link href="css/navibar.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">

    </head>
    <body>

        <%@include file="Header2.jsp" %>


        <div  class="content_tabs" style="padding-top: 20px; padding-bottom: 20px; padding-right: 20px; ">
            <div id="tab-container">
                <ul>
                    <li class="selected"><a>Purchase History</a></li>
                    <li><a href="Profile_Account_details.jsp">Account</a></li>

                </ul>
            </div>
            <div id="main-container">
                <div class="prof_topBar">
                    <div style="float: left" >
                        <h4 class="prof_topBar_header">History</h4>
                    </div>
                    <div style="padding-left: 85%;float: left;">
                        <select id="optionselect" class="prof_topBar_select">
                            <option value="1">Products</option>
                            <option value="2">Invoices</option>
                        </select>
                    </div>
                </div>

                <div id="product_part">
                    <%                       //
                        //"userid"
                        HttpSession purhisSes = request.getSession();
                        int i = 0;
                        if (purhisSes.getAttribute("Login_object") != null) {

                            Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
                            DB.UserRegistration ur = (DB.UserRegistration) purhisSes.getAttribute("Login_object");

                            Criteria crprof = ses.createCriteria(DB.Invoice.class);
                            crprof.add(Restrictions.eq("userRegistration", ur));
                            crprof.addOrder(Order.desc("id"));
                            List<DB.Invoice> listpr = crprof.list();
                            int flag = 0;
                            for (DB.Invoice in : listpr) {
                                ++flag;
                                if (in.getUserDeleted().equals("0")) {
                                    Criteria crdev = ses.createCriteria(DB.InvoiceRegistry.class);
                                    crdev.add(Restrictions.eq("invoice", in));
                                    List<DB.InvoiceRegistry> listinvr = crdev.list();
                                    for (DB.InvoiceRegistry invr : listinvr) {

                    %>
                    <div class="product-image-wrapper">
                        <div class="col-sm-4">
                            <img src="<%=invr.getProducts().getModel().getImgSrc()%>" alt="" style="width: 200px;">
                        </div>
                        <div class="col-sm-8">
                            <div class="single-products">
                                <div class=" text-center">
                                    <ul class="nav nav-pills nav-justified">
                                        <li class="products-heading"><%=invr.getProducts().getModel().getManufacturers().getName()%>  <%=invr.getProducts().getModel().getModel()%></li>

                                        <%
                                            if (invr.getProducts().getModel().getDeviceType().getId() == 1) {
                                                //smartphone
                                        %>
                                        <li  class="products-price typcn typcn-device-phone">  <%=invr.getProducts().getModel().getDeviceType().getType()%></li>
                                            <%
                                            } else if (invr.getProducts().getModel().getDeviceType().getId() == 2) {
                                                //tabs
                                            %>
                                        <li  class="products-price typcn typcn-device-tablet">  <%=invr.getProducts().getModel().getDeviceType().getType()%></li>
                                            <%
                                            } else if (invr.getProducts().getModel().getDeviceType().getId() == 3) {
                                                //access
                                            %>
                                        <li  class="products-price typcn typcn-headphones">  <%=invr.getProducts().getModel().getDeviceType().getType()%></li>
                                            <%
                                            } else {
                                                //smartwatch
                                            %>
                                        <li  class="products-price typcn typcn-time">  <%=invr.getProducts().getModel().getDeviceType().getType()%></li>
                                            <%
                                                }
                                            %>
                                    </ul>
                                    <ul class="nav nav-pills nav-justified">
                                        <li class="products-date">Buy Date: <%=invr.getInvoice().getDate()%></li>
                                    </ul>
                                    <ul class="nav nav-pills nav-justified">
                                        <li class="products-date">Price :<%=invr.getProducts().getPrice()%> </li>
                                    </ul>
                                    <ul class="nav nav-pills nav-justified">
                                        <li class="products-date">QTY  :  <%=invr.getQty()%></li>
                                    </ul>

                                    <ul class="nav nav-pills nav-justified">
                                        <li class="products-type "> <a href="Product_details.jsp?id=2" style="text-align: right;"><button class="btn btn-success" value="100" onclick="viewproduct()"><label class="fa fa-clipboard"> View</label></button></a></li>
                                    </ul>

                                    <ul class="nav nav-pills nav-justified">
                                        <li class="products-type ">

                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%
                        }
                    } else {
                        if (flag == 1) {
                    %>
                    <div style="color: rgb(8, 83, 8);background-color: rgb(158, 230, 158);border-radius: 12px;">
                        <h4 style="padding: 30px;">Empty</h4>
                    </div>
                    <%
                                }
                            }
                        }


                    %>
                </div>

                <div style="display: none" id="invoice_part">
                    <div style="display: none" id="deletebar" class="prof_2nd_bar">
                        <input id="checkall"  type="checkbox" style="margin: 5px">
                        <input type="button" id="deletebtn" disabled="disabled"  class="prof_deleteBtn" value="Delete">
                    </div>
                    <%         //
                        Criteria invoCr = ses.createCriteria(DB.Invoice.class);
                        invoCr.add(Restrictions.eq("userRegistration", ur));
                        invoCr.addOrder(Order.desc("id"));
                        List<DB.Invoice> invliceList = invoCr.list();
                        int flag2 = 0;
                        for (DB.Invoice inv : invliceList) {
                            ++flag2;
                            if (inv.getUserDeleted().equals("0")) {
                                ++i;
                    %>

                    <div class="product-image-wrapper" style="border:1px solid #4E4E46;height: 276px;" id="invo<%=i%>">

                        <div class="col-sm-12">
                            <input id="inv<%=i%>" value="<%=inv.getId()%>" type="checkbox">
                            <hr style="margin: 0px">
                        </div>
                        <style>
                            .delever-ileft{
                                position: absolute;
                                left: 210px;
                                background: -webkit-linear-gradient(top, #8cc903 , #afe073);
                                background: -o-linear-gradient(top, #8cc903 , #afe073);
                                background: -moz-linear-gradient(top, #8cc903 , #afe073);
                                background: linear-gradient(top, #8cc903 , #afe073);
                                padding: 2px 5px 3px 3px;
                                border-radius: 16px;
                                font-size: 25px;
                                color: white;
                                width: 30px;
                                z-index: 1;
                            }
                            .delever-icenter{
                                position: absolute;
                                left: 480px;
                                background: -webkit-linear-gradient(top, #8cc903 , #afe073);
                                background: -o-linear-gradient(top, #8cc903 , #afe073);
                                background: -moz-linear-gradient(top, #8cc903 , #afe073);
                                background: linear-gradient(top, #8cc903 , #afe073);
                                padding: 2px 5px 3px 3px;
                                border-radius: 16px;
                                font-size: 25px;
                                color: white;
                                width: 30px;
                                z-index: 1;
                            }
                            .delever-iright{
                                position: absolute;
                                left: 750px;
                                background: -webkit-linear-gradient(top, #8cc903 , #afe073);
                                background: -o-linear-gradient(top, #8cc903 , #afe073);
                                background: -moz-linear-gradient(top, #8cc903 , #afe073);
                                background: linear-gradient(top, #8cc903 , #afe073);
                                padding: 2px 5px 3px 3px;
                                border-radius: 16px;
                                font-size: 25px;
                                color: white;
                                width: 30px;
                                z-index: 1;
                            }
                            .red-del{
                                background: -webkit-linear-gradient(top, #fc1616 , #c20202);
                                background: -o-linear-gradient(top, #fc1616 , #c20202);
                                background: -moz-linear-gradient(top, #fc1616 , #c20202);
                                background: linear-gradient(top, #fc1616 , #c20202);

                            }
                            .notyet{
                                padding: 2px 2px 2px 5px;
                            }
                            .prbar1{

                                width: 258px;
                                height: 20px;
                                position: absolute;
                                left: 236px;
                                top: 5px;
                            }
                            .prbar2{

                                width: 258px;
                                height: 20px;
                                position: absolute;
                                left: 504px;
                                top: 5px;
                            }
                            .delspan1{
                                position: absolute;
                                top: 27px;
                                left: 200px;
                            }
                            .delspan2{
                                position: absolute;
                                top: 27px;
                                left: 471px;
                            }
                            .delspan3{
                                position: absolute;
                                top: 27px;
                                left: 735px;    
                            }
                            .deldetailsarea{
                                margin-top: 1px;
                                margin-bottom: 52px;
                            }
                            .greendel{
                                background: -webkit-linear-gradient(top, #8cc903 , #afe073);
                                background: -o-linear-gradient(top, #8cc903 , #afe073);
                                background: -moz-linear-gradient(top, #8cc903 , #afe073);
                                background: linear-gradient(top, #8cc903 , #afe073);
                            }
                        </style>
                        <div class="col-sm-12 deldetailsarea">
                            <%
                                if (inv.getDeleverStatus().getId() == 1) {
                            %>
                            <div>
                                <i  class="fa fa-check delever-ileft "></i>
                                <div class="prbar1 red-del"></div>
                                <i class="fa fa-times delever-icenter red-del notyet"></i>
                                <div class="prbar2 red-del"></div>
                                <i class="fa fa-times delever-iright red-del notyet"></i>
                                <span class="delspan1">Ordered</span>
                                <span class="delspan2">Shipped</span>
                                <span class="delspan3">Delivered</span>
                            </div>
                            <%
                            } else if (inv.getDeleverStatus().getId() == 2) {
                            %>
                            <div>
                                <i  class="fa fa-check delever-ileft "></i>
                                <div class="prbar1 greendel"></div>
                                <i class="fa fa-check delever-icenter greendel"></i>
                                <div class="prbar2 red-del"></div>
                                <i class="fa fa-times delever-iright red-del notyet"></i>
                                <span class="delspan1">Ordered</span>
                                <span class="delspan2">Shipped</span>
                                <span class="delspan3">Delivered</span>
                            </div>

                            <%
                            } else {
                            %>
                            <div>
                                <i  class="fa fa-check delever-ileft "></i>
                                <div class="prbar1 greendel"></div>
                                <i class="fa fa-check delever-icenter greendel"></i>
                                <div class="prbar2 greendel"></div>
                                <i class="fa fa-check delever-iright greendel"></i>
                                <span class="delspan1">Ordered</span>
                                <span class="delspan2">Shipped</span>
                                <span class="delspan3">Delivered</span>
                            </div>
                            <%                                }
                            %>
                            <!--                            -->
                            <!--                           -->        
                            <!--                                                 -->


                        </div>

                        <div class="col-sm-12">
                            <hr style="margin: 0px">
                            <div class="col-sm-6">
                                <div>
                                    <h4> Bill date: <%=inv.getDate()%></h4>
                                    <h4>Address:</h4>
                                    <h5 style="padding-left: 60px"><%=inv.getDelivery().getFname()%> <%=inv.getDelivery().getLname()%></h5>
                                    <h5 style="padding-left: 60px"><%=inv.getDelivery().getAd1()%></h5>
                                    <h5 style="padding-left: 60px"><%=inv.getDelivery().getAd2()%></h5>
                                    <h5 style="padding-left: 60px"><%=inv.getDelivery().getCity()%></h5>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="col-sm-12">
                                    <%                                Criteria crInvreg = ses.createCriteria(DB.InvoiceRegistry.class);
                                        crInvreg.add(Restrictions.eq("invoice", inv));
                                        List<DB.InvoiceRegistry> listinvr = crInvreg.list();
                                        int tot = 0;
                                        int no = 0;
                                        for (DB.InvoiceRegistry invr : listinvr) {
                                            ++no;
                                            tot += invr.getProducts().getPrice() * Integer.parseInt(invr.getQty());
                                    %>
                                    <h5><%=no%>).<%=invr.getProducts().getModel().getManufacturers().getName()%>  <%=invr.getProducts().getModel().getModel()%>
                                        Rs.<%=invr.getProducts().getPrice()%>.00 x <%=invr.getQty()%></h5>                           
                                        <%
                                            }
                                        %>
                                </div>




                            </div>

                        </div>
                        <div class="col-sm-12">
                            <hr style="margin: 0"> 
                            <h5 style="float: right;">Total Rs.<%=tot%>.00</h5>
                        </div>

                    </div>

                    <%
                    } else {
                        if (flag2 == 1) {
                    %>
                    <div style="color: rgb(8, 83, 8);background-color: rgb(158, 230, 158);border-radius: 12px;">
                        <h4 style="padding: 30px;">Empty</h4>
                    </div>
                    <%
                                }
                            }
                        }
                    } else {
                        purhisSes.setAttribute("requestfrom", "Profile_Purchase_History.jsp");
                    %>

                    <script>
                        window.location.replace("index.jsp#opensignin");
                    </script>
                    <%                        }
                    %><input  type="hidden" id="maxids" value="<%=i%>"><%
                    %>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('#checkall').prop('checked', false);
                var max = $('#maxids').val();
                for (var i = 1; i <= max; ++i) {
                    $('#inv' + i + '').prop('checked', false);

                    $('#inv' + i + '').change(function () {
                        $('#deletebtn').prop('disabled', false);
                    });
                }
                $('#deletebtn').prop('disabled', true);
                $('#optionselect option[value="1"]').attr('selected', 'selected');
            });

            $('#optionselect').change(function () {
                var id = $('#optionselect').val();
                if (id == "1") {
                    $('#product_part').show();
                    $('#invoice_part').hide();
                    $('#deletebar').hide();
                } else {
                    $('#product_part').hide();
                    $('#invoice_part').show();
                    $('#deletebar').show();
                }
            });

            $('#checkall').change(function () {
                if ($('#checkall').is(':checked')) {
                    var max = $('#maxids').val();
                    for (var i = 1; i <= max; ++i) {
                        $('#inv' + i + '').prop('checked', true);
                    }
                    $('#deletebtn').prop('disabled', false);
                } else {
                    var max = $('#maxids').val();
                    for (var i = 1; i <= max; ++i) {
                        $('#inv' + i + '').prop('checked', false);
                    }
                    $('#deletebtn').prop('disabled', true);
                }
            });
            $('#deletebtn').click(function () {
                var ids = "";
                var maxid = $('#maxids').val();
                for (var i = 1; i <= maxid; ++i) {
                    if ($('#inv' + i + '').is(':checked')) {
                        ids += $('#inv' + i + '').val() + ",";
                        alert(ids);
                        $('#invo' + i + '').remove();
                    }
                }


                $.post(
                        "Profile_deletePurchaseHistory",
                        {id: ids}, //meaasge you want to send
                function (result) {
                });
            });
        </script>

        <%@include file="Footer.jsp" %>
    </body>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</html>
