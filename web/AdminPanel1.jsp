<%-- 
    Document   : AdminPanel1
    Created on : Mar 13, 2016, 10:54:06 AM
    Author     : Ishara
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.hibernate.criterion.Projections"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="shortcut icon" type="image/x-icon" href="images/home/Slider1.jpg" />
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="css/navibar.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/typicons.min.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/price-range.css" rel="stylesheet">


        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/script.js"></script>
        <script src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/calendar.js"></script>
        <script src="js/jquery.ajaxfileupload.js"></script>

    </head>
    <body>
        <div>

            <style>

                .dropbtn {
                    background-color: #ffffff;
                    color: black;
                    padding: 8px;
                    // margin:6px;
                    font-size: 16px;
                    border: none;
                    cursor: pointer;
                    border: none;
                    height: 50px;

                }

                .dropdown {
                    position: relative;
                    display: inline-block;
                    float: right;
                    margin-right: 30px;
                }

                .dropdown-content {
                    display: none;
                    position: absolute;
                    min-width: 160px;
                    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                }

                .dropdown-content a {
                    color: black;
                    padding: 12px 16px;
                    text-decoration: none;
                    display: block;
                }

                .dropdown-content a:hover {
                    background-color: #f1f1f1
                }

                .dropdown:hover .dropdown-content {
                    display: block;
                    z-index: 1;
                }

                .dropdown:hover .dropbtn {
                    background-color: #D8D7D7;
                }

                //
                .topbaradmin{
                    position: fixed;
                    z-index: 1;
                    background-color: #F8F8F8; 
                    margin: 0px;
                }
                .logoadmin{
                    display: inline-block
                }
                .logoadmin img{
                    margin: 6px;
                }
                .sidebaradmin li a{
                    margin:  0px;
                }
                .active-menu{
                    background-color: #E5580C;
                    color: white;
                }
                .well{
                    min-height: 20px;
                    padding: 19px;
                    margin-bottom: 20px;
                    background-color: #F5F5F5;
                    border: 1px solid #E3E3E3;
                    border-radius: 4px;
                    box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.05) inset;
                }
                .quick-btn {
                    position: relative;
                    display: inline-block;
                    width: 22%;
                    height: 90px;
                    padding-top: 16px;
                    margin: 10px;
                    color: white;
                    text-align: center;
                    text-decoration: none;
                    /*text-shadow: 0px 1px 0px rgba(255, 255, 255, 0.6);*/
                    box-shadow: 0px 0px 0px 1px #F8F8F8 inset, 0px 0px 0px 1px #CCC;
                    border-radius: 10px;
                    border-style: solid;
                    border-width: 4px;
                    border-color: #f0efef;
                }
                .quick-btn .label {
                    position: absolute;
                    top: -5px;
                    right: -5px;
                }
                .quick-btn span {
                    display: block;
                }
                .label-danger {
                    background-color: #D9534F;
                }
                .label {
                    display: inline;
                    padding: 0.2em 0.6em 0.3em;
                    font-size: 75%;
                    font-weight: bold;
                    line-height: 1;
                    color: #FFF;
                    text-align: center;
                    white-space: nowrap;
                    vertical-align: baseline;
                    border-radius: 0.25em;
                }
                .adminiconsize{
                    font-size: 35px;
                }
                .adminhome2{
                    margin-left: 5px;
                    margin-right: 5px;
                }
                .adminhome2:hover{
                    color: #E5580C;
                }
                .editbtn{
                    border: 0px none;
                    padding: 3px;
                    border-radius: 8px;
                    width: 45px;
                    background-color: rgb(166, 209, 236);
                    background: transparent linear-gradient(rgb(130, 167, 231), rgb(132, 160, 182)) repeat scroll 0% 0%;
                    background: -o-linear-gradient(rgb(130, 167, 231), rgb(132, 160, 182));
                    background: -moz-linear-gradient(rgb(130, 167, 231), rgb(132, 160, 182));
                    background: linear-gradient(rgb(130, 167, 231), rgb(132, 160, 182));
                    color: white;
                }
                .cancelbtn{
                    border: 0px none;
                    padding: 3px;
                    border-radius: 8px;
                    width: 45px;
                    background-color: rgb(166, 209, 236);
                    background: transparent linear-gradient(rgb(205, 215, 233), rgb(156, 156, 156)) repeat scroll 0% 0%;
                    background: -o-linear-gradient(rgb(205, 215, 233), rgb(156, 156, 156));
                    background: -moz-linear-gradient(rgb(205, 215, 233), rgb(156, 156, 156));
                    background: linear-gradient(rgb(205, 215, 233), rgb(156, 156, 156));
                    color: white;
                }
                .remoban{
                    border: 0px none;
                    padding: 3px;
                    border-radius: 8px;
                    width: 180px;
                    background-color: rgb(166, 209, 236);
                    background: transparent linear-gradient(rgb(231, 130, 130), rgb(177, 18, 40)) repeat scroll 0% 0%;
                    background: -o-linear-gradient(rgb(231, 130, 130), rgb(177, 18, 40));
                    background: -moz-linear-gradient(rgb(231, 130, 130), rgb(177, 18, 40));
                    background: linear-gradient(rgb(231, 130, 130), rgb(177, 18, 40));
                    color: white;
                }
            </style>
            <div class="topbaradmin">
                <div class="logoadmin">
                    <img src="images/home/Phone.PNG">
                </div>

                <div class="dropdown" >
                    <button class="dropbtn"><img style="height: 28px" src="images/orange.png"> Admin Ishara</button>
                    <div class="dropdown-content">
                        <a href="#">Logout</a>
                        <a href="#">Settings</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-2" style=" padding: 0px;position: relative; box-sizing: border-box; float: left; height:2500px; background-color: #F8F8F8 ;border: 1px solid #D8D7D7;">
                <nav class="navbar-default navbar-side" role="navigation">
                    <div class="sidebar-collapse">
                        <ul class="nav sidebaradmin" id="main-menu">
                            <li>
                                <a class="active-menu" href="AdminPanel1.jsp"><i class="fa fa-dashboard"></i> Dashboard</a>
                            </li>
                            <li>
                                <a  href="Admin_addProducts.jsp"><i class="fa fa-plus-square"></i> Addproduct</a>
                            </li>
                            <li>
                                <a href="Admin_product_details.jsp"><i class="fa fa-check-square-o"></i> Product details</a>
                            </li>
                            <li>
                                <a href="Admin_user_management.jsp"><i class="fa fa-users"></i> User management</a>

                            </li>

                            <li>
                                <a href="Admin_shipping.jsp"><i class="fa fa-truck"></i> Delivery</a>
                            </li>


                        </ul>


                    </div>

                </nav>

            </div>

            <%
                Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();

                String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());

                //today rating
                Criteria crtoday1 = ses.createCriteria(DB.Rating.class);
                crtoday1.add(Restrictions.eq("date", date));
                List<DB.Rating> ratelist = crtoday1.list();
                int todayratecou = ratelist.size();

                //today orders
                Criteria crtoday2 = ses.createCriteria(DB.Invoice.class);
                crtoday2.add(Restrictions.eq("date", date));
                List<DB.Invoice> orderlist = crtoday2.list();
                int todayordercou = orderlist.size();

                //
                Criteria crtoday3 = ses.createCriteria(DB.UserRegistration.class);
                crtoday3.add(Restrictions.eq("date", date));
                crtoday3.add(Restrictions.eq("registerId", "0"));
                List<DB.UserRegistration> urcoulist = crtoday3.list();
                int todayurcou = urcoulist.size();
                //
                Integer hitsCount
                        = (Integer) application.getAttribute("hitCounter");
                if(hitsCount == null || hitsCount == 0){
                    hitsCount=1;
                }
            %>
            <div class="col-sm-10"  style="float: left;  margin: 0px; box-sizing: border-box; height: 2500px; background-color: #F8F8F8;border: 1px solid #D8D7D7;">
                <div style="padding-left: 25px">
                    <h3 style="margin-top: 5px;margin-bottom: 5px">Dashboard</h3>
                    <p>
                        A true Sri Lankan shopping experience for mobile devices. Interesting offers & lowest price in the Sri Lanka with 
                        best customer service.
                    </p>
                </div>
                <div style="text-align: center;">
                    <a class="quick-btn" style="background-color: rgb(106, 165, 203);" href="#">
                        <i class="fa fa-check-square-o adminiconsize"></i>
                        <span> New Ratings Today</span>
                        <span class="label label-danger"><%=todayratecou%></span>
                    </a>
                    <a class="quick-btn" style="background-color: rgb(236, 182, 116);;" href="#">
                        <i class="fa fa-envelope adminiconsize"></i>
                        <span>New Orders Today</span>
                        <span class="label label-success"><%=todayordercou%></span>
                    </a>
                    <a class="quick-btn" style="background-color: rgb(71, 194, 124);" href="#">
                        <i class="fa fa-check-square-o adminiconsize"></i>
                        <span>New Users Today</span>
                        <span class="label label-danger"><%=todayurcou%></span>
                    </a>

                    <a class="quick-btn" style="background-color: rgb(219, 103, 103);;" href="#">
                        <i class="fa fa-envelope adminiconsize"></i>
                        <span>Total Hits Today</span>
                        <span class="label label-success"><%=hitsCount %></span>
                    </a>
                </div>
                <div style="text-align: center;">
                    <a href="" class="adminhome2">Add product</a>
                    <span>|</span>
                    <a href="" class="adminhome2" >Update product</a>
                    <span>|</span>
                    <a href="" class="adminhome2" >Product Details</a>
                    <span>|</span>
                    <a href="" class="adminhome2">User management</a>
                    <span>|</span>
                    <a href="" class="adminhome2">Delivery</a>
                </div>
                <style>
                    .bannerimg img{
                        margin: 10px;
                    }
                    .bannerimg button{
                        font-size: 37px;
                        color: rgb(129, 157, 207);
                        margin: 10px;
                        width: 100%;
                        height: 156.667px;
                        border: 1px solid rgb(188, 208, 245);
                    }
                    .bannerimg button:hover{
                        border: 1px solid rgb(53, 119, 242);
                        color: rgb(53, 119, 242);
                    }

                    .pos2{
                        cursor: pointer;
                        position: absolute; 
                        top: 5%;
                        left: 96%;
                        width: 100%; 
                    }
                </style>
                <hr style="margin-top: 10px;margin-bottom: 10px">
                <div id="banners" class="bannerimg">
                    <h3>Home page banners</h3>
                    <%                        Criteria crad1 = ses.createCriteria(DB.Banner.class);
                        crad1.addOrder(Order.asc("id"));
                        List<DB.Banner> ban = crad1.list();
                        int bannerno = 1;
                        if (!(ban.isEmpty())) {

                            for (DB.Banner b : ban) {
                    %>
                    <div id="banner<%=bannerno%>" class="col-lg-6">
                        <img style="max-width: 100%" src="<%=b.getImage()%>">
                        <a onclick="editbanner(<%=bannerno%>)"  class="pos2"><span class="fa fa-edit"></span></a>
                    </div>
                    <%
                                ++bannerno;
                            }
                        }
                    %>

                    <div id="addim" class="col-lg-6">
                        <button id="btnuplimg"><span class="fa fa-plus"></span>
                        </button>
                        <input type="file" accept="image/*" style="display: none" id="the-photo-file-field" name="datafile" onchange="handleFiles(this)" />
                    </div>


                </div>


            </div>
            <!--            <div class="col-sm-2"  style="display: block;padding: 10px;height: 2500px; background-color: #F8F8F8;float: left;border: 1px solid #D8D7D7;">
            
                            <div class="well well-small">
                                <ul class="list-unstyled">
            <%                //visitor count
                String visitcou = "0";
                if (application.getAttribute("visit") != null) {
                    visitcou = application.getAttribute("visit").toString();
                }
                //Users count
                Session sesadmin = conn.NewHibernateUtil.getSessionFactory().openSession();
                Criteria crusercou = sesadmin.createCriteria(DB.UserRegistration.class);
                DB.Status st = (DB.Status) sesadmin.load(DB.Status.class, 2);
                crusercou.add(Restrictions.eq("status", st));
                List<DB.UserRegistration> urlist = crusercou.list();
                int count = urlist.size();

                //hits
                Integer hits = (Integer) application.getAttribute("hitCounter");
                if (hits == null) {
                    hits = 1;
                }

                //registrations
                Criteria crreg = sesadmin.createCriteria(DB.UserRegistration.class);
                crreg.addOrder(Order.asc("id"));
                List<DB.Status> reglist = crreg.list();
                int reg = reglist.size();

            %>
            
            <table>
                <tr>
                    <td>Visitor: </td>
                    <td><span> <%=visitcou%></span></td>
                </tr>
                <tr>
                    <td>Users:</td>
                    <td><span> <%=count%></span></td>
                </tr>
                <tr>
                    <td>Hits:</td>
                    <td><span> <%=hits%></span></td>
                </tr>
               
            <tr>
                    <td>Registrations: </td>
                    <td><span> <%=reg%></span></td>
                </tr>
            </table>
        </ul>
    </div>
</div>-->
        </div>
        <div id="addurl" class="addqty-model">
            Modal content 
            <div class="addqty-content">
                <div class="addqty-header">
                    <span style="color: black; float: right;cursor: pointer" class="add-url-close">X</span>
                </div>
                <div class="">
                    <table>
                        <tr>
                            <td>&nbsp;&nbsp; Add url for the banner:&nbsp;</td>
                            <td> <input style="width: 360px" id="banerur" class="width-add-qty" placeholder="URL"></td>
                            <td><input type="button" onclick="addurl()" value="Add"></td>

                        </tr>
                        <tr>
                            <td style="text-align: center;display: none" id="inurl" colspan="2">
                                <p class="red-text">Invalid url</p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div id="editbanner" class="addqty-model">
            Modal content 
            <div class="addqty-content">
                <div class="addqty-header">
                    <span style="color: black; float: right;cursor: pointer" id="closechangebanner" class="add-url-close">X</span>
                </div>
                <div class="">
                    <table>
                        <tr>
                            <td>&nbsp;&nbsp;<p>URL:</p> &nbsp;&nbsp;</td>
                            <td style="width: 421px">
                                <a id="urlraw"></a>
                                <input id="urltxtbox"  style="width: 413px" class="hideitems" type="text" > 
                            </td>
                            <td style="width: 120px;text-align: center">
                                <input id="editurl" type="button" class="editbtn" value="edit">
                                <input id="saveurl" type="button" class="hideitems editbtn" value="save">
                                <input id="cancelurl" type="button" class="hideitems cancelbtn" value="cancel">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;display: none" id="inurl2" colspan="3">
                                <p class="red-text">Invalid url</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; text-align: center" id="inurl" colspan="3">
                                <button id="removebabtn" class="remoban"><span class="fa fa-times"></span>&nbsp;&nbsp;&nbsp;Remove Banner</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <script>
            var editbanid = 0;
            $('#removebabtn').click(function () {
                $.post(
                        "Admin_Banner_remove",
                        {id: editbanid},
                function (result) {
                    $('#banner' + editbanid + '').remove();
                    modalesitban.style.display = "none";
                    $('#editurl').show();
                    $('#urlraw').show();
                    $('#urltxtbox').hide();
                    $('#saveurl').hide();
                    $('#cancelurl').hide();
                });

            });
            $('#editurl').click(function () {
                $('#editurl').hide();
                $('#urlraw').hide();
                $('#urltxtbox').show();
                $('#saveurl').show();
                $('#cancelurl').show();
            });
            $('#cancelurl').click(function () {
                $('#editurl').show();
                $('#urlraw').show();
                $('#urltxtbox').hide();
                $('#saveurl').hide();
                $('#cancelurl').hide();
            });

            var modalesitban = document.getElementById('editbanner');
            function editbanner(id) {
                editbanid = id;
                $.post(
                        "Admin_Bannerurl_get",
                        {id: id},
                function (result) {
                    if (result.toString() == '#') {
                        $('#urlraw').html("NO URL");
                    } else {
                        $('#urlraw').html(result);
                    }

                    modalesitban.style.display = "block";
                });
            }
            $('#saveurl').click(function () {
                var newurl = $('#urltxtbox').val();
                var isv = isUrlValid(newurl);
                if (isv == 1 || newurl == "") {
                    $.post(
                            "Admin_Banner_updateurl",
                            {id: editbanid, url: newurl},
                    function (result) {
                        modalesitban.style.display = "none";
                        $('#editu rl').show();
                        $('#urlraw').show();
                        $('#urltxtbox').hide();
                        $('#saveurl').hide();
                        $('#cancelurl').hide();
                    });
                } else {
                    $('#inurl2').show();
                    setTimeout(function () {
                        $('#inurl2').hide();
                    },
                            2000);
                }
            });
            var banchclo = document.getElementById('closechangebanner');
            banchclo.onclick = function () {
                modalesitban.style.display = "none";
                $('#editurl').show();
                $('#urlraw').show();
                $('#urltxtbox').hide();
                $('#saveurl').hide();
                $('#cancelurl').hide();
            }
        </script>
        <script>
            var maxbannerno =<%=bannerno%>;
            var closeaddurl = document.getElementsByClassName("add-url-close")[0];
            var modaladdurl = document.getElementById('addurl');
            closeaddurl.onclick = function () {
                modaladdurl.style.display = "none";
                --maxbannerno;
                $('#banner' + maxbannerno + '').remove();
            }

            function isUrlValid(urls) {
                var res = urls.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
                if (res == null)
                    return 0;
                else
                    return 1;
            }
            function addurl() {


                var url = $('#banerur').val();
                var isvalu = isUrlValid(url);
                if (isvalu == 1 || url == "") {
                    modaladdurl.style.display = "none";
                    $.post(
                            "Admin_New_banner",
                            {url: url},
                    function (result) {

                    });
                } else {
                    $('#inurl').show();
                    setTimeout(
                            function () {
                                $('#inurl').hide();
                            },
                            2000);
                }
            }

            $(function () {
                $('#btnuplimg').click(function () {
                    $(this).next('input[type="file"]').trigger('click');
                    return false;
                });
            });
            $(document).ready(function () {
                // $('#up').click(function () {
                $('input[type="file"]').ajaxfileupload({
                    'action': 'UploadFile',
                    'onComplete': function (response) {
                        $('#upload').hide();
                    },
                    'onStart': function () {
                        $('#upload').show();
                    }
                });
                //                        });
            });
            function handleFiles(evt) {

                var files = evt.files; // FileList object

                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {

                    // Only process image files.
                    if (!f.type.match('image.*')) {
                        continue;
                    }
                    var reader = new FileReader();

                    // Closure to capture the file information.
                    reader.onload = (function (theFile) {
                        return function (e) {
                            var img = new Image();
                            var imgwidth;
                            var imgheight;
                            img.onload = function () {
                                //                                alert(this.width + 'x' + this.height);
                                if (this.width == 1120 && this.height == 340) {
                                    var div = document.createElement('div');
                                    div.innerHTML = ['<img id="loadedimage2" class="thumb" src="', e.target.result,
                                        '" title="', escape(theFile.name), '" style="max-width: 100%" />\n\
                                        <a  onclick="editbanner(' + maxbannerno + ')" class="pos2"><span class="fa fa-edit"></span></a>'].join('');
                                    div.className += "col-lg-6";
                                    $('#addim').before(div);
                                    div.setAttribute("id", "banner" + maxbannerno);

                                    $('#banerur').val('');

                                    modaladdurl.style.display = "block";
                                    ++maxbannerno;
                                } else {
                                    alert("Invalid image size");
                                }
                            }
                            img.src = e.target.result;

                        };
                    })(f);

                    reader.readAsDataURL(f);
                }

            }
        </script>
    </body>
</html>
