<%-- 
    Document   : Product_details
        Created on : Jan 24, 2016, 11:09:37 PM
    Author     : Ishara
--%>
<script>
    window.onpageshow = function (evt) {
        // If persisted then it is in the page cache, force a reload of the page.
        if (evt.persisted) {
            document.body.style.display = "none";
            location.reload();
        }
    };

    var url = window.location.href;
    var urlparts = url.split('#');
    var b;
    if (urlparts[1] == b) {
    } else {
        console.log("history back");
        history.back();
    }
</script>

<%@page import="java.text.DecimalFormat"%>
<%@page import="org.hibernate.criterion.Disjunction"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Set;"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache">

        <link rel="shortcut icon" type="image/x-icon" href="images/home/Slider1.jpg" />
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="css/navibar.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/typicons.min.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/price-range.css" rel="stylesheet">
        <link href="css/admin.css" rel="stylesheet">
        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/script.js"></script>
        <script src="js/main.js"></script>
    </head>
    <body>
        <style> 
            body{
                background-image: url("images/geometry2.png");
            }
        </style>

        <%@include file="Header2.jsp" %>

        <%         //
            String proid = request.getParameter("id");
            if (proid != null) {
                HttpSession sesprd = request.getSession();
                DecimalFormat df2 = new DecimalFormat("0.00");
                Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
                Criteria c = ses.createCriteria(DB.Products.class);
                c.add(Restrictions.eq("id", Integer.parseInt(proid)));
                DB.Products pr = (DB.Products) c.uniqueResult();
                if (pr != null) {
        %>
        <div class="container bodycont">
            <div class="row">
                <div class="col-sm-12 padding-right">
                    <div class="product-details"><!--product-details-->
                        <div class="col-sm-5">
                            <div class="view-product">
                                <img src="<%=pr.getModel().getImgSrc()%>" alt="<%=pr.getModel().getModel()%>">
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="product-information"><!--/product-information-->
                                <%--img src="images/product-details/new.jpg" class="newarrival" alt=""--%>
                                <h2><%=pr.getModel().getManufacturers().getName()%>  <%=pr.getModel().getModel()%></h2>
                                <%
                                    if (pr.getModel().getDeviceType().getId() == 1) {
                                %> <p class="typcn typcn-device-phone">Smart phone</p><%
                                } else if (pr.getModel().getDeviceType().getId() == 2) {
                                %><p class="typcn typcn-device-tablet">Tabs</p> <%
                                } else if (pr.getModel().getDeviceType().getId() == 3) {
                                %><p class="typcn typcn-headphones">Accesories</p> <%
                                } else {
                                %><p class="typcn typcn-time"> Smart watches</p> <%
                                    }
                                %>

                                <span>
                                    <span>Rs.<%
                                        DecimalFormat df = new DecimalFormat("0.00");
                                        out.print(df.format(pr.getPrice()));
                                        %></span>

                                    <%
                                        if (pr.getQty().equals("0")) {

                                        } else {
                                            out.print("<input  id='quantity'  type='text'>");
                                        }
                                    %>

                                    <%       //
                                        String qty = pr.getQty();
                                        if (qty.equals("0")) {

                                        } else {

                                            if (sesprd.getAttribute("mycart") != null) {
                                                Java_classes.Cart cartch = (Java_classes.Cart) sesprd.getAttribute("mycart");
                                                List<Java_classes.NewCartItems> n = cartch.getArrayData();
                                                int tot = 0;
                                                boolean flagcart = false;

                                                for (Java_classes.NewCartItems ni : n) {

                                                    if (Integer.parseInt(pr.getId() + "") == Integer.parseInt(ni.getProid() + "")) {
                                                        flagcart = true;
                                                    }

                                                }
                                                if (flagcart) {
                                                    out.print("<i class='typcn typcn-tick'  style='font-size: 23px; color:green;'> Already added</i>");
                                                } else {
                                                    out.print("<button type='button' id='addcrtbtn' onclick='addtocart()' class='btn btn-fefault cart'>"
                                                            + " <i class='fa fa-shopping-cart'></i>"
                                                            + "&nbsp;&nbsp;Add to cart"
                                                            + "</button>");
                                                }
                                            } else {
                                                out.print("<button type='button' id='addcrtbtn' onclick='addtocart()' class='btn btn-fefault cart'>"
                                                        + " <i class='fa fa-shopping-cart'></i>"
                                                        + "&nbsp;&nbsp;Add to cart"
                                                        + "</button>");
                                            }

                                        }

                                    %>

                                </span>
                                <script>
    var id =<%=request.getParameter("id")%>;
    var maxqty =<%=pr.getQty()%>;
    $('#quantity').val(1);
    function addtocart() {

        var pid = id;
        var qty = document.getElementById("quantity").value;
//        var er = 
//        alert(er);
        if (qty != "") {
            if (parseInt(qty)) {
                if (qty <= 4) {
                    $('#cantadmorethan').hide();
//                   
                    $.post(
                            "Add_to_cart",
                            {
                                id: pid, qty: qty
                            },
                    function (result) {
                        if (result == 1) {
                            $('#stocklow').show();
                            setTimeout(
                                    function () {
                                        $('#stocklow').hide();
                                    },
                                    2000);
                        } else if (result == 2) {
                            window.location.reload();
                        } else {
                            var avilcartcou = $('#cartcount').html();
                            if (avilcartcou == "" || avilcartcou == undefined) {
                                $('#cartcount').html("1");
                                $('#cartcount').addClass("show");
                            } else {
                                var newcartcou = Number(avilcartcou);
                                var lastcou = newcartcou + 1;
                                $('#cartcount').html(lastcou);
                            }
                            $('#addcrtbtn').hide();
                            $('#addcrtbtn').replaceWith('<i class="typcn typcn-tick"  style="font-size: 23px; color:green;"></i>');
                        }
                    });
                } else {
                    $('#cantadmorethan').show();
                    setTimeout(
                            function () {
                                $('#cantadmorethan').hide();
                            },
                            2000);
                }
            } else {
                window.location.reload();
            }
        } else {
            $('#giveqty').show();
            setTimeout(
                    function () {
                        $('#giveqty').hide();
                    },
                    2000);
        }

    }
                                </script>
                                <br>
                                <label id="cantadmorethan" class="color-red hideitems">Can't buy more than 4 items</label>
                                <label id="giveqty" class="color-red hideitems">Please give some qty</label>
                                <label id="stocklow" class="color-red hideitems">Stock is lower than requested quantity </label>
                                <%

                                    String available = null;
                                    if (qty.equals("0")) {
                                        available = "Out of Stock";
                                %>
                                <p><b>Availability:</b><label class='color-red'>Out of stock</label></p>
                                <%
                                } else {
                                %>
                                <p><b>Availability:</b>available in stock</p>
                                <%
                                    }
                                %>
                                <p><b>Brand:</b><%=pr.getModel().getManufacturers().getName()%></p>
                                <p><b>Model:</b><%=pr.getModel().getModel()%></p>
                                <a href="https://plus.google.com/share?url=http://localhost:8080/Phonehut.lk_2/Product_details.jsp?id=<%=pr.getId()%>"> <img src="images/gplus.png"></a>
                                <a href="http://www.facebook.com/sharer/sharer.php?u=http://localhost:8080/Phonehut.lk_2/Product_details.jsp?id=<%=pr.getId()%>"><img src="images/facebook.png"></a>
                            </div><!--/product-information-->
                        </div>
                    </div><!--/product-details-->

                    <div class="category-tab shop-details-tab"><!--category-tab-->
                        <div class="col-sm-12">
                            <ul class="nav nav-tabs" style="padding-left: 15px;">
                                <li class="active"><a href="#details" data-toggle="tab">Details</a></li>
                                <li class=""><a href="#reviews" data-toggle="tab">Reviews</a></li>

                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="details">

                                <%
                                    if (pr.getModel().getSpecification() != null) {
                                %>
                                <table class="" cellspacing="0">
                                    <tbody>

                                        <tr>
                                            <th scope="row" rowspan="4">Network</th>
                                            <td>Networks</td>
                                            <td>
                                                <%
                                                    Integer netid = pr.getModel().getSpecification().getNetwork().getId();
                                                    DB.Network netlo = (DB.Network) ses.load(DB.Network.class, netid);
                                                    Criteria crnet = ses.createCriteria(DB.BandsHasNetwork.class);
                                                    crnet.add(Restrictions.eq("network", netlo));
                                                    List<DB.BandsHasNetwork> b = crnet.list();
                                                    for (DB.BandsHasNetwork bn : b) {
                                                        DB.NetworkTypes nettylo = (DB.NetworkTypes) ses.load(DB.NetworkTypes.class, bn.getNetworkTypes().getId());
                                                        out.print(nettylo.getBands() + "-");
                                                        out.print(bn.getBands());
                                                        out.print("<br>");
                                                    }
                                                    /* Set<DB.BandsHasNetwork> bandsHasNetworks = pr.getModel().getSpecification().getNetwork().getBandsHasNetworks();
                                                     for (DB.BandsHasNetwork bandsHasNetwork : bandsHasNetworks) {
                                                        
                                                     DB.NetworkTypes nettylo = (DB.NetworkTypes) ses.load(DB.NetworkTypes.class, bandsHasNetwork.getNetworkTypes().getId());
                                                     out.print(nettylo.getBands() + "-");
                                                     out.print(bandsHasNetwork.getBands());
                                                     out.print("<br>");

                                                     }
                                                     */
                                                %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Speed</td>
                                            <td><p><%=pr.getModel().getSpecification().getNetwork().getSpeed()%></p></td>
                                        </tr>
                                        <tr>
                                            <td>GPRS</td>
                                            <td>
                                                <p><%
                                                    if (pr.getModel().getSpecification().getNetwork().getGprs().equals("1")) {
                                                        out.print("Yes");
                                                    } else {
                                                        out.print("no");
                                                    }
                                                    %></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>EDGE</td>
                                            <td>
                                                <p><%
                                                    if (pr.getModel().getSpecification().getNetwork().getEdge().equals("1")) {
                                                        out.print("Yes");
                                                    } else {
                                                        out.print("no");
                                                    }

                                                    %></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row" rowspan="2">Launch</th>
                                            <td >Announced</td>
                                            <td>
                                                <p><%=pr.getModel().getSpecification().getLaunch().getAnnouncedDate()%></p> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td>
                                                <p><%=pr.getModel().getSpecification().getLaunch().getLaunchStatus().getStat()%></p> 

                                            </td>
                                        </tr>

                                        <tr>
                                            <th scope="row" rowspan="3">Body</th>
                                            <td>Dimensions</td>
                                            <td>
                                                <p><%=pr.getModel().getSpecification().getBody().getDimension()%></p> 

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Weight</td>
                                            <td>
                                                <p><%=pr.getModel().getSpecification().getBody().getWeight()%>g</p> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>SIM</td>
                                            <td>
                                                <p><%=pr.getModel().getSpecification().getBody().getSim().getSimType()%>  
                                                    (<%=pr.getModel().getSpecification().getBody().getSimSize().getSize()%>)</p> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row" rowspan="7">Display</th>
                                            <td>Type</td>
                                            <td>
                                                <p><%=pr.getModel().getSpecification().getDisplay().getScrnType().getScrntype()%>
                                                    <%
                                                        if (pr.getModel().getSpecification().getDisplay().getTouchType().getTouchType().equals("Non touch")) {
                                                            out.print("Non touch");
                                                        } else {
                                                            out.print(pr.getModel().getSpecification().getDisplay().getTouchType().getTouchType() + " " + "Touch screen");
                                                        }
                                                        // 
%>
                                                </p> 
                                            </td>
                                        </tr><tr>
                                            <td>Size</td>
                                            <td>

                                                <p><%=pr.getModel().getSpecification().getDisplay().getSize()%> 
                                                    (<%=pr.getModel().getSpecification().getDisplay().getScrnBodyRatio()%>)</p> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Resolution</td>
                                            <td>
                                                <p><%=pr.getModel().getSpecification().getDisplay().getResolution()%></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >Pixel density</td>
                                            <td>
                                                <p>(<%=pr.getModel().getSpecification().getDisplay().getPixelDensity()%>ppi)</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Multi touch</td>
                                            <td>
                                                <p><%=pr.getModel().getSpecification().getDisplay().getMultiTouch().getMultiTouch()%></p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Protection</td>
                                            <td>
                                                <p><%=pr.getModel().getSpecification().getDisplay().getProtection()%></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Other</td>
                                            <td>
                                                <p><%=pr.getModel().getSpecification().getDisplay().getOther()%></p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th scope="row" rowspan="4">Platform</th>
                                            <td class="ttl">OS</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getPlatformsUse().getVersionUse().getOsList().getOses()%>
                                                    <%=pr.getModel().getSpecification().getPlatformsUse().getVersionUse().getVersionsUse()%>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ttl">Chipset</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getPlatformsUse().getChipetUse()%></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ttl">CPU</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getPlatformsUse().getCpuUse()%></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ttl">GPU</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getPlatformsUse().getGpuUse()%></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row" rowspan="3">Memory</th>
                                            <td>Card slot</td>
                                            <td>
                                                <%

                                                    if (pr.getModel().getSpecification().getMemory().getCardSlot().getId().equals(1)) {
                                                        out.print(pr.getModel().getSpecification().getMemory().getCardSlot().getSlot() + " (Up to "
                                                                + pr.getModel().getSpecification().getMemory().getCardSlotUpto() + "GB)");
                                                    } else {
                                                        out.print("No");
                                                    }
                                                %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>RAM</td>
                                            <td>
                                                <p><%=pr.getModel().getSpecification().getMemory().getRam()%>GB</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internal memory</td>
                                            <td>
                                                <p><%=pr.getModel().getSpecification().getMemory().getInternel()%>GB</p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th scope="row" rowspan="3">Camera</th>
                                            <td class="ttl">Primary</td>
                                            <td class="ttl">
                                                <p><%=pr.getModel().getSpecification().getCamera().getPixels()%>mp</p>
                                                <p>(<%=pr.getModel().getSpecification().getCamera().getFeatures()%>)</p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="ttl">Video</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getCamera().getVideo()%></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ttl">Secondary</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getCamera().getSecondery()%></p>
                                            </td>

                                        </tr>   


                                        <tr>
                                            <th scope="row" rowspan="4">Sound</th>
                                            <td class="ttl">Alert types</td>
                                            <td class="nfo">
                                                <%

                                                    Integer souid = pr.getModel().getSpecification().getSound().getId();
                                                    DB.Sound so = (DB.Sound) ses.load(DB.Sound.class, souid);

                                                    Criteria cso1 = ses.createCriteria(DB.AlertTypesHasSound.class);
                                                    cso1.add(Restrictions.eq("sound", so));

                                                    List<DB.AlertTypesHasSound> alttypelsit = cso1.list();

                                                    for (DB.AlertTypesHasSound alertTypesHasSound : alttypelsit) {
                                                        DB.AlertTypes alt = (DB.AlertTypes) ses.load(DB.AlertTypes.class, alertTypesHasSound.getAlertTypes().getId());
                                                        out.print(alt.getAlert());
                                                        out.print("<br>");
                                                    }
                                                %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ttl">Loudspeaker </td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getSound().getLoudSpeaker().getType()%></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ttl">3.5mm jack </td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getSound().getThreefivveJack().getStatus()%></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ttl">Other </td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getSound().getOther()%></p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th scope="row" rowspan="7">COMMS</th>  
                                            <td class="ttl">WLAN</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getCommunication().getWlan()%></p>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="ttl">Bluetooth</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getCommunication().getBluetooth()%></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ttl">GPS</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getCommunication().getGps()%></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ttl">NFC</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getCommunication().getNfc().getStatus()%></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ttl">Radio</td>
                                            <td class="nfo">

                                                <%
                                                    if (pr.getModel().getSpecification().getCommunication().getRadio().equals("0")) {
                                                        out.print("np");
                                                    } else {
                                                        out.print(pr.getModel().getSpecification().getCommunication().getRadio());
                                                    }
                                                %>

                                            </td>
                                        </tr>   
                                        <tr>
                                            <td class="ttl">USB</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getCommunication().getUsb()%></p>
                                            </td>
                                        </tr>
                                        <tr>
                                        </tr>


                                        <tr>
                                            <th  rowspan="5">Features</th>
                                            <td>Sensors</td>
                                            <td>
                                                <%

                                                    Integer othf = pr.getModel().getSpecification().getOtherFeatures().getId();
                                                    DB.OtherFeatures ot = (DB.OtherFeatures) ses.load(DB.OtherFeatures.class, othf);

                                                    Criteria cothfe = ses.createCriteria(DB.OtherFeaturesHasSensors.class);
                                                    cothfe.add(Restrictions.eq("otherFeatures", ot));

                                                    List<DB.OtherFeaturesHasSensors> otherflist = cothfe.list();

                                                    for (DB.OtherFeaturesHasSensors o : otherflist) {
                                                        DB.Sensors sen = (DB.Sensors) ses.load(DB.Sensors.class, o.getSensors().getId());
                                                        out.print(sen.getSensor() + ",");

                                                    }
                                                %>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ttl">Messaging</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getOtherFeatures().getMesseging()%></p>
                                            </td>
                                        </tr><tr>
                                            <td class="ttl">Browser</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getOtherFeatures().getBrowser()%></p>
                                            </td>
                                        </tr>	
                                        <tr>
                                            <td class="ttl">Java</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getOtherFeatures().getJavaSupport().getStatus()%></p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="ttl">Other</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getOtherFeatures().getOther()%></p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th scope="row" rowspan="4">Battery</th>
                                            <td class="ttl">Main</td>
                                            <td class="nfo"> 
                                                <p><%=pr.getModel().getSpecification().getBattery().getSize()%>(
                                                    <%=pr.getModel().getSpecification().getBattery().getRemoveble().getStatus()%>)
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ttl">Fast charge</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getBattery().getFastChrj().getStat()%></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ttl">Stand-by</td>
                                            <td class="nfo"> 
                                                <p><%=pr.getModel().getSpecification().getBattery().getStandyTime()%></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ttl">Call time</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getBattery().getCallTime()%></p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th scope="row" rowspan="1">Misc</th>
                                            <td class="ttl">Colors</td>
                                            <td class="nfo">
                                                <p><%=pr.getModel().getSpecification().getColor().getColor()%></p>

                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                                <%                                   //
                                } else {

                                %>

                                <p><%=pr.getModel().getAccesoriesAndParts().getDescription()%></p>
                                <%
                                    }
                                    System.gc();
                                %>



                            </div>


                            <div class="tab-pane fade " id="reviews">
                                <%
                                    Criteria crrate = ses.createCriteria(DB.Rating.class);
                                    crrate.add(Restrictions.eq("products", pr));
                                    List<DB.Rating> listRating = crrate.list();
                                    if (!(listRating.isEmpty())) {

                                        for (DB.Rating r : listRating) {
                                            if (r.getNonregistercomment() == null) {
                                                int stars = Integer.parseInt(r.getStars());
                                %>
                                <div class="col-sm-12">
                                    <ul>
                                        <li><a href=""><i class="fa fa-user"></i><%=r.getUserRegistration().getUserName()%></a></li>
                                        <li><a href=""><i class="fa fa-clock-o"></i><%=r.getTime()%></a></li>
                                        <li><a href=""><i class="fa fa-calendar-o"></i><%=r.getDate()%></a></li>
                                        <li><a href="">
                                                <%
                                                    for (int i = 1; i <= stars; ++i) {
                                                %>
                                                <i style="padding: 0" class="fa fa-star"></i>
                                                <%
                                                    }
                                                    if (stars < 5) {
                                                        int nonstar = 5 - stars;
                                                        for (int i = 1; i <= nonstar; ++i) {
                                                %>
                                                <i style="padding: 0" class="fa fa-star-o"></i>
                                                <%
                                                        }
                                                    }

                                                %>
                                            </a>
                                        </li>
                                    </ul>
                                    <p><%=r.getComments()%></p>
                                </div>

                                <%
                                } else {
                                    int stars = Integer.parseInt(r.getStars());
                                %>
                                <div class="col-sm-12">
                                    <ul>
                                        <li><a href=""><i class="fa fa-user"></i><%=r.getNonregistercomment().getName()%></a></li>
                                        <li><a href=""><i class="fa fa-clock-o"></i><%=r.getTime()%></a></li>
                                        <li><a href=""><i class="fa fa-calendar-o"></i><%=r.getDate()%></a></li>
                                        <li><a href="">
                                                <%
                                                    for (int i = 1; i <= stars; ++i) {
                                                %>
                                                <i style="padding: 0" class="fa fa-star"></i>
                                                <%
                                                    }
                                                    if (stars < 5) {
                                                        int nonstar = 5 - stars;
                                                        for (int i = 1; i <= nonstar; ++i) {
                                                %>
                                                <i style="padding: 0" class="fa fa-star-o"></i>
                                                <%
                                                        }
                                                    }

                                                %>
                                            </a>
                                        </li>
                                    </ul>
                                    <p><%=r.getComments()%></p>
                                </div>

                                <%
                                        }
                                    }
                                %>

                                <%
                                    }
                                %>


                                <%
                                    if (sesprd.getAttribute("Login_object") != null) {
                                        DB.UserRegistration ur = (DB.UserRegistration) sesprd.getAttribute("Login_object");
                                %>
                                <div id="addrating1" class="col-sm-12">
                                    <b>Hey <%=ur.getUserName()%> give us your review</b>
                                    <input type="hidden" id="proidrate" value="<%=pr.getId()%>">
                                    <textarea maxlength="450" id="review-text"></textarea>
                                    <div class="star-div">
                                        <b>Rating: </b> 
                                        <a id="star1" class="rating-stars fa fa-star-o"></a>
                                        <a id="star2" class="rating-stars fa fa-star-o "></a>
                                        <a id="star3" class="rating-stars fa fa-star-o"></a>
                                        <a id="star4" class="rating-stars fa fa-star-o"></a>
                                        <a id="star5" class="rating-stars fa fa-star-o"></a>
                                    </div>
                                    <h6 id="mustrate" class="color-red paddin_this nomargin hideitems">You must rate</h6>
                                    <button type="button" id="rate-btn" class="btn btn-default pull-right">
                                        Rate
                                    </button>

                                </div>
                                <%
                                } else {
                                %>
                                <div id="addrating2" class="col-sm-12">
                                    <b>Hey give us your review</b>
                                    <input type="hidden" id="proidrate" value="<%=pr.getId()%>">
                                    <span>
                                        <input id="Unknownname" placeholder="Your Name" type="text">
                                        <input id="Unknownemail" placeholder="Email Address" type="text">
                                    </span>
                                    <textarea maxlength="450" id="review-text"></textarea>

                                    <div class="star-div">
                                        <b>Rating: </b> 
                                        <a id="star1" class="rating-stars fa fa-star-o"></a>
                                        <a id="star2" class="rating-stars fa fa-star-o "></a>
                                        <a id="star3" class="rating-stars fa fa-star-o"></a>
                                        <a id="star4" class="rating-stars fa fa-star-o"></a>
                                        <a id="star5" class="rating-stars fa fa-star-o"></a>
                                    </div>
                                    <h6 id="mustrate" class="color-red paddin_this nomargin hideitems">You must rate</h6>
                                    <button type="button" id="rate-btn2" class="btn btn-default pull-right">
                                        Rate
                                    </button>


                                </div>
                                <%
                                    }
                                %>

                            </div>

                        </div>
                    </div>

                    <div class="recommended_items"><!--recommended_items-->
                        <h2 class="title text-center">recommended items</h2>

                        <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="item active">	

                                    <%//
                                        int s = 3;
                                        Criteria cp = ses.createCriteria(DB.Products.class);
                                        cp.addOrder(Order.desc("id"));
                                        //cp.setFirstResult(s);
                                        cp.setMaxResults(4);
                                        List<DB.Products> pro = cp.list();
                                        for (DB.Products p : pro) {

                                    %>
                                    <div class="col-sm-3">
                                        <div class="product-image-wrapper">
                                            <div class="single-products">
                                                <div class="productinfo text-center">
                                                    <div style="height: 180px;width: 260px">
                                                        <img style="max-width: 100%; max-height: 100%" src="<%=p.getModel().getImgSrc()%>" alt="">
                                                    </div>
                                                    <h2>Rs.<%
                                                        out.print(df.format(p.getPrice()));
                                                        %></h2>
                                                    <p><%=p.getModel().getManufacturers().getName()%> <%=p.getModel().getModel()%></p>
                                                    <a href="Product_details.jsp?id=<%=p.getId()%>"><button type="button" class="btn btn-default add-to-cart"><i class="fa fa-clipboard"></i>View product</button></a>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <% }%>

                                </div>
                                <div class="item">	
                                    <%//

                                        Criteria cp2 = ses.createCriteria(DB.Products.class);
                                        cp2.addOrder(Order.desc("id"));
                                        //

                                        Criteria criteriatest = ses.createCriteria(DB.Model.class);

                                        criteriatest.add(Restrictions.eq("manufacturers", pr.getModel().getManufacturers()));
                                        List<DB.Model> modlist = criteriatest.list();

                                        cp2.add(Restrictions.in("model", modlist));
                                        //cp.setFirstResult(s);
                                        cp2.setMaxResults(4);
                                        List<DB.Products> pro2 = cp2.list();
                                        for (DB.Products p : pro2) {

                                    %>
                                    <div class="col-sm-3">
                                        <div class="product-image-wrapper">
                                            <div class="single-products">
                                                <div class="productinfo text-center">
                                                    <div style="height: 180px;width: 260px">
                                                        <img style="max-width: 100%; max-height: 100%" src="<%=p.getModel().getImgSrc()%>" alt="">
                                                    </div>
                                                    <h2>Rs.<%
                                                        out.print(df.format(p.getPrice()));
                                                        %></h2>
                                                    <p><%=p.getModel().getManufacturers().getName()%> <%=p.getModel().getModel()%></p>
                                                    <a href="Product_details.jsp?id=<%=p.getId()%>"><button type="button" class="btn btn-default add-to-cart"><i class="fa fa-clipboard"></i>View product</button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <% }%>

                                </div>
                            </div>
                            <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>			
                        </div>
                    </div><!--/recommended_items-->

                </div>
            </div>
        </div>
        <%
        } else {
        %>
        <div class="nothingfound">
            <h3>Can't find the product</h3>
        </div>
        <%
            }
        } else {
        %>
        <script>
            window.location.replace('products.jsp');</script>
            <%
                }
            %>
            <%@include file="Footer.jsp" %>
    </body>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script> 
</html>
