


<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="javax.script.ScriptEngine"%>
<%@page import="javax.script.ScriptEngineManager"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>






<header>

    <script type="text/javascript">
        function as() {
            $(document).ready(function () {
                Setuname();
            });
        }
        function wrongpassword() {
            $(document).ready(function () {
                wrngp();
            });
        }
        function confirmyouremal() {
            $(document).ready(function () {
                confirmyouremal2();
            });
        }
        function accdisabled() {
            $(document).ready(function () {
                accdisabled2();
            });
        }
    </script>

    <%      //
        Session sesheader = conn.NewHibernateUtil.getSessionFactory().openSession();
        HttpSession hs = request.getSession();
        String loginusername = "";
        String email = "";
        String utype = "";
        String status = "";
        String login_status = "";

        int utid = 0;

        if (hs.getAttribute("Login_object") != null) {
            DB.UserRegistration ur = (DB.UserRegistration) hs.getAttribute("Login_object");
            if (ur.getStatus().getId() == 2) {
//                loginusername = (String) hs.getAttribute("uname");
//                email = (String) hs.getAttribute("email");
//                utype = (String) hs.getAttribute("utype").toString();
//                status = (String) hs.getAttribute("urstatus").toString();
                loginusername = ur.getUserName();
                email = ur.getEmail();
                utype = ur.getUserTypes().getId().toString();
                status = ur.getStatus().getId() + "";


    %>
    <script type="text/javascript">
        as();</script>

    <%       } else if (ur.getStatus().getId() == 1) {
    %>
    <script type="text/javascript">
        confirmyouremal();</script>
        <%
        } else if (ur.getStatus().getId() == 3) {

        %>
    <script>
        accdisabled();
    </script>    
    <%    }


    %>

    <div id="Logout" class="modalDialog2">
        <div>
            <form action="Logout" method="post">
                <a href="#close" title="Close" onclick="hide()" class="close">X</a>
                <p>
                    Sure you want to log out?
                </p>
                <div class="centerbtns"> 
                    <button type="submit" onclick="$('#logout').prop('style', 'display:none')"  id="subtn" class="btn btn-danger alertbt">YES</button>

                    <a href="#close" title="Close" onclick="hide()">
                        <button type="button"  id="subtn" onclick="hide()" class="btn btn-success alertbt">No</button>
                    </a>

                </div>
            </form>
        </div>
    </div>
    <%    } else {
        String uuid = Java_classes.cookie.getCookieValue(request, "phonehutloginquthentication");
        if (uuid != null) {
            DB.CookieTable cook = (DB.CookieTable) sesheader.load(DB.CookieTable.class, uuid);
            if (cook != null) {
                DB.UserRegistration ur = (DB.UserRegistration) sesheader.load(DB.UserRegistration.class, cook.getUserRegistration().getId());
                hs.setAttribute("Login_object", ur);
                response.sendRedirect("index.jsp");
            }
        } else {
    %>
    <div id="opensignin" class="modalDialog">
        <div>
            <a href="#close" onclick="hideloginwrongelements()" title="Close" class="close">X</a>
            <div class="login-form" id="loginfoem"><!--login form-->
                <h2>Login to your account</h2>
                <form id="signupform" action="Login" method="post">

                    <%
                        if (hs.getAttribute("requestfrom") != null) {

                    %>
                    <label id="invaliduser" ><p class="typcn typcn-times wrongformfield">Please login first</p></label>

                    <%                        }

                    %>

                    <label id="invaliduser" class="hideitems"><p class="typcn typcn-times wrongformfield">Wrong username or pssword</p></label>

                    <label  id="wrngemaillogin" class="hideitems"><p class="typcn typcn-times wrongformfield"> invalid email</p></label>
                    <input name="loginemailad" id="loginemail" class="form-control" type="email" placeholder="E-mail or User name" />

                    <label id="wrngepwrdlogin" class="hideitems"><p class="typcn typcn-times wrongformfield"> Length must be more than 4 characters</p></label>
                    <input name="loginfrmpwrd" id="loginpwrd" class="form-control" type="password" placeholder="Password" />
                    <span>
                        <input name="keepme" type="checkbox" class="checkbox" value="true"> 
                        Keep me signed in
                    </span>
                    <button type="button" onclick="loginformvalidate()" class="btn btn-default">Login</button>
                    <a href="#SignUpForm" class="btn btn-link addblackcolor">Sign up</a>
                    OR
                    <a href="#fgtpwrd" class="btn btn-link addRedcolor">Forgot Password</a>
                </form>
            </div>
        </div>	
    </div>
    <%            }

        }

        if (hs.getAttribute("login_status") != null) {
            if (hs.getAttribute("login_status").equals("2")) {
    %>
    <script type="text/javascript">
        wrongpassword();</script>
        <%
                }
            }
        %>


    <script type="text/javascript">
        function Setuname() {
            //$("#username").html("<a id='username' style='color: black; ' href='#'><i class='fa fa-male'></i> Welcome <%--=loginusername--%></a>");

            $('.super-top-ul li:eq(0)').after("<li class='toolt'><a  href='Profile_Account_details.jsp'><i class='fa fa-gear'></i> <div></div><span class='tooltiptext'>Account settings</span></a></li>");
            $("#Loginbtn").html("<a  href='#Logout'>\n\
                <i class='fa fa-sign-out'></i> <span class='tooltiptext'>Signout</span></a>");
        }
        function wrngp() {

            $("#invaliduser").show(200);
            $("#loginemail").addClass("loginform-wrong-email");
            $("#loginpwrd").addClass("loginform-wrong-email");
        }
        function hideloginwrongelements() {
            $("#invaliduser").hide();
            $("#loginemail").removeClass("loginform-wrong-email");
            $("#loginpwrd").removeClass("loginform-wrong-email");
            $("#wrngemaillogin").hide();
            $("#loginpwrd").removeClass("loginform-wrong-email");
            $("#wrnguname").hide(500);
            $("#unametf").removeClass("loginform-wrong-email");
            $("#emailad").removeClass("loginform-wrong-email");
            $("#wrngemail").hide();
            $("#pwrd1").removeClass("loginform-wrong-email");
            $("#pwrd2").removeClass("loginform-wrong-email");
            $("#wrngpasslength").hide();
        }
        function confirmyouremal2() {
            window.location.replace("index.jsp#pleasecheckurmail");
        }
        function accdisabled2() {
            window.location.replace("index.jsp#asssas");
        }
    </script>

    <div class="top_bar_1_2">
        <div class="row">
            <div class="col-md-12 ">
                <div class="container">
                    <div class="super-top">
                        <div class="col-sm-4">
                            <a href="index.jsp"><img src="images/home/Phone.PNG" alt="" /></a>
                        </div>
                        <div class="col-sm-8">
                            <div class="shop-menu pull-right">
                                <ul class="nav navbar-nav super-top-ul">
                                    <%
                                        if (hs.getAttribute("Login_object") != null) {
                                    %>
                                    <li class="toolt">
                                        <a class='dropbtn' id='username' style='color: black; ' href='Profile_Purchase_History.jsp'>
                                            <i class='fa fa-user'></i> Welcome <%=loginusername%><span class='tooltiptext'>Account</span></a>
                                    </li>
                                    <%
                                    } else {
                                    %>
                                    <li class="toolt"><a id="username" href="#opensignin"><i class="fa fa-user"></i> <div>Account</div><span class="tooltiptext">Profile</span></a>  </li>
                                        <%
                                            }
                                        %>

                                    <li class="toolt"><a id="Loginbtn" href="#opensignin"><i class="fa fa-sign-in"></i><span class="tooltiptext">Signin</span></a></li>

                                    <li class="toolt"><a href="#"><i class="fa fa-bookmark-o"></i><span class="tooltiptext">Wishlist</span></a></li>

                                    <li class="toolt"><a href="Checkout.jsp"><i class="fa fa-check-square-o"></i> <span class="tooltiptext">Checkout</span></a></li>
                                        <%
                                            if (hs.getAttribute("mycart") != null) {

                                                Java_classes.Cart c = (Java_classes.Cart) hs.getAttribute("mycart");
                                                List<Java_classes.NewCartItems> n = c.getArrayData();
                                                if (!(n.isEmpty())) {

                                        %>
                                    <li class="toolt"><a href="Cart.jsp"><i class="fa fa-shopping-cart"></i><div class="cart-count" id="cartcount"><%=n.size()%></div> <span class="tooltiptext">cart</span></a></li>

                                    <%
                                    } else {
                                    %>
                                    <li class="toolt"><a href="Cart.jsp"><i class="fa fa-shopping-cart"></i><div class="cart-count hideitems" id="cartcount"></div><span class="tooltiptext">cart</span></a></li>

                                    <%
                                        }
                                    } else {
                                    %>
                                    <li class="toolt"><a href="Cart.jsp"><i class="fa fa-shopping-cart"></i><div class="cart-count hideitems" id="cartcount"></div><span class="tooltiptext">cart</span></a></li>

                                    <%
                                        }
                                    %>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="logo" style="display: none">
            <div class="row">
                <div class="col-md-12 ">

                    <div class="container top_bar">
                        <div class="row paddin_this">

                            <div class="col-sm-8">
                                <div class="shop-menu pull-left">
                                    <ul class="nav navbar-nav">
                                        <li><a id="username" href="Profile_Purchase_History.jsp"><i class="fa fa-male"></i> Account</a></li>
                                        <li><a href="#"><i class="fa fa-star-half-full"></i> Wishlist</a></li>

                                        <li><a href="Checkout.jsp"><i class="fa fa-check-square-o"></i> Checkout</a></li>
                                        <li><a href="Cart.jsp"><i class="fa fa-shopping-cart"></i> Cart</a></li>
                                        <li><a id="Loginbtn" href="#opensignin"><i class="fa fa-sign-in"></i> Login</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="logo pull-right">
                                    <a href="index.jsp"><img src="images/home/Phone.PNG" alt="" /></a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <div class="menu">
            <div class="row">
                <div class="col-md-12">

                    <div id="cssmenu" class="align-center">

                        <ul class="secmenu-border" >
                            <li class='active'><a href='index.jsp'>Home</a></li>
                            <li><a href="products.jsp">products</a></li>
                            <li><a href="products.jsp?devtypeid=1">Smart phones</a></li>
                            <li><a href='products.jsp?devtypeid=2'>Tabs</a></li>
                            <li><a href='products.jsp?devtypeid=4'>Smart watches</a></li>
                            <li><a href='products.jsp?devtypeid=3'>Accessories</a></li>
                            <li><a href='#'>About us</a></li>
                            <li style="
                                animation : none;
                                animation-delay : 0;
                                animation-direction : normal;
                                animation-duration : 0;
                                animation-fill-mode : none;
                                animation-iteration-count : 1;
                                animation-name : none;
                                animation-play-state : running;
                                animation-timing-function : ease;
                                backface-visibility : visible;
                                background : 0;
                                background-attachment : scroll;
                                background-clip : border-box;
                                background-color : transparent;
                                background-image : none;
                                background-origin : padding-box;
                                background-position : 0 0;
                                background-position-x : 0;
                                background-position-y : 0;
                                background-repeat : repeat;
                                background-size : auto auto;
                                border : 0;
                                border-style : none;
                                border-width : medium;
                                border-color : inherit;
                                border-bottom : 0;
                                border-bottom-color : inherit;
                                border-bottom-left-radius : 0;
                                border-bottom-right-radius : 0;
                                border-bottom-style : none;
                                border-bottom-width : medium;
                                border-collapse : separate;
                                border-image : none;
                                border-left : 0;
                                border-left-color : inherit;
                                border-left-style : none;
                                border-left-width : medium;
                                border-radius : 0;
                                border-right : 0;
                                border-right-color : inherit;
                                border-right-style : none;
                                border-right-width : medium;
                                border-spacing : 0;
                                border-top : 0;
                                border-top-color : inherit;
                                border-top-left-radius : 0;
                                border-top-right-radius : 0;
                                border-top-style : none;
                                border-top-width : medium;
                                bottom : auto;
                                box-shadow : none;
                                box-sizing : content-box;
                                caption-side : top;
                                clear : none;
                                clip : auto;
                                color : inherit;
                                columns : auto;
                                column-count : auto;
                                column-fill : balance;
                                column-gap : normal;
                                column-rule : medium none currentColor;
                                column-rule-color : currentColor;
                                column-rule-style : none;
                                column-rule-width : none;
                                column-span : 1;
                                column-width : auto;
                                content : normal;
                                counter-increment : none;
                                counter-reset : none;
                                cursor : auto;
                                direction : ltr;
                                display : inline;
                                empty-cells : show;
                                float : none;
                                font : normal;
                                font-family : inherit;
                                font-size : medium;
                                font-style : normal;
                                font-variant : normal;
                                font-weight : normal;
                                height : auto;
                                hyphens : none;
                                left : auto;
                                letter-spacing : normal;
                                line-height : normal;
                                list-style : none;
                                list-style-image : none;
                                list-style-position : outside;
                                list-style-type : disc;
                                margin : 0;
                                margin-bottom : 0;
                                margin-left : 0;
                                margin-right : 0;
                                margin-top : 0;
                                max-height : none;
                                max-width : none;
                                min-height : 0;
                                min-width : 0;
                                opacity : 1;
                                orphans : 0;
                                outline : 0;
                                outline-color : invert;
                                outline-style : none;
                                outline-width : medium;
                                overflow : visible;
                                overflow-x : visible;
                                overflow-y : visible;
                                padding : 0;
                                padding-bottom : 0;
                                padding-left : 0;
                                padding-right : 0;
                                padding-top : 0;
                                page-break-after : auto;
                                page-break-before : auto;
                                page-break-inside : auto;
                                perspective : none;
                                perspective-origin : 50% 50%;
                                position : static;
                                /* May need to alter quotes for different locales (e.g fr) */
                                quotes : '\201C' '\201D' '\2018' '\2019';
                                right : auto;
                                tab-size : 8;
                                table-layout : auto;
                                text-align : inherit;
                                text-align-last : auto;
                                text-decoration : none;
                                text-decoration-color : inherit;
                                text-decoration-line : none;
                                text-decoration-style : solid;
                                text-indent : 0;
                                text-shadow : none;
                                text-transform : none;
                                top : auto;
                                transform : none;
                                transform-style : flat;
                                transition : none;
                                transition-delay : 0s;
                                transition-duration : 0s;
                                transition-property : none;
                                transition-timing-function : ease;
                                unicode-bidi : normal;
                                vertical-align : baseline;
                                visibility : visible;
                                white-space : normal;
                                widows : 0;
                                width : auto;
                                word-spacing : normal;
                                z-index : auto;
                                "><a style="
                                 animation : none;
                                 animation-delay : 0;
                                 animation-direction : normal;
                                 animation-duration : 0;
                                 animation-fill-mode : none;
                                 animation-iteration-count : 1;
                                 animation-name : none;
                                 animation-play-state : running;
                                 animation-timing-function : ease;
                                 backface-visibility : visible;
                                 background : 0;
                                 background-attachment : scroll;
                                 background-clip : border-box;
                                 background-color : transparent;
                                 background-image : none;
                                 background-origin : padding-box;
                                 background-position : 0 0;
                                 background-position-x : 0;
                                 background-position-y : 0;
                                 background-repeat : repeat;
                                 background-size : auto auto;
                                 border : 0;
                                 border-style : none;
                                 border-width : medium;
                                 border-color : inherit;
                                 border-bottom : 0;
                                 border-bottom-color : inherit;
                                 border-bottom-left-radius : 0;
                                 border-bottom-right-radius : 0;
                                 border-bottom-style : none;
                                 border-bottom-width : medium;
                                 border-collapse : separate;
                                 border-image : none;
                                 border-left : 0;
                                 border-left-color : inherit;
                                 border-left-style : none;
                                 border-left-width : medium;
                                 border-radius : 0;
                                 border-right : 0;
                                 border-right-color : inherit;
                                 border-right-style : none;
                                 border-right-width : medium;
                                 border-spacing : 0;
                                 border-top : 0;
                                 border-top-color : inherit;
                                 border-top-left-radius : 0;
                                 border-top-right-radius : 0;
                                 border-top-style : none;
                                 border-top-width : medium;
                                 bottom : auto;
                                 box-shadow : none;
                                 box-sizing : content-box;
                                 caption-side : top;
                                 clear : none;
                                 clip : auto;
                                 color : inherit;
                                 columns : auto;
                                 column-count : auto;
                                 column-fill : balance;
                                 column-gap : normal;
                                 column-rule : medium none currentColor;
                                 column-rule-color : currentColor;
                                 column-rule-style : none;
                                 column-rule-width : none;
                                 column-span : 1;
                                 column-width : auto;
                                 content : normal;
                                 counter-increment : none;
                                 counter-reset : none;
                                 cursor : auto;
                                 direction : ltr;
                                 display : inline;
                                 empty-cells : show;
                                 float : none;
                                 font : normal;
                                 font-family : inherit;
                                 font-size : medium;
                                 font-style : normal;
                                 font-variant : normal;
                                 font-weight : normal;
                                 height : auto;
                                 hyphens : none;
                                 left : auto;
                                 letter-spacing : normal;
                                 line-height : normal;
                                 list-style : none;
                                 list-style-image : none;
                                 list-style-position : outside;
                                 list-style-type : disc;
                                 margin : 0;
                                 margin-bottom : 0;
                                 margin-left : 0;
                                 margin-right : 0;
                                 margin-top : 0;
                                 max-height : none;
                                 max-width : none;
                                 min-height : 0;
                                 min-width : 0;
                                 opacity : 1;
                                 orphans : 0;
                                 outline : 0;
                                 outline-color : invert;
                                 outline-style : none;
                                 outline-width : medium;
                                 overflow : visible;
                                 overflow-x : visible;
                                 overflow-y : visible;
                                 padding : 0;
                                 padding-bottom : 0;
                                 padding-left : 0;
                                 padding-right : 0;
                                 padding-top : 0;
                                 page-break-after : auto;
                                 page-break-before : auto;
                                 page-break-inside : auto;
                                 perspective : none;
                                 perspective-origin : 50% 50%;
                                 position : static;
                                 /* May need to alter quotes for different locales (e.g fr) */
                                 quotes : '\201C' '\201D' '\2018' '\2019';
                                 right : auto;
                                 tab-size : 8;
                                 table-layout : auto;
                                 text-align : inherit;
                                 text-align-last : auto;
                                 text-decoration : none;
                                 text-decoration-color : inherit;
                                 text-decoration-line : none;
                                 text-decoration-style : solid;
                                 text-indent : 0;
                                 text-shadow : none;
                                 text-transform : none;
                                 top : auto;
                                 transform : none;
                                 transform-style : flat;
                                 transition : none;
                                 transition-delay : 0s;
                                 transition-duration : 0s;
                                 transition-property : none;
                                 transition-timing-function : ease;
                                 unicode-bidi : normal;
                                 vertical-align : baseline;
                                 visibility : visible;
                                 white-space : normal;
                                 widows : 0;
                                 width : auto;
                                 word-spacing : normal;
                                 z-index : auto;

                                 "><div class="search_bar_box1" >                        
                                        <div class="form-group block">
                                            <input type="text" id="searchtxt" class="form-control" style="display: inline-block">
                                        </div>

                                    </div>
                                    <div class="search_bar_box1" >                        
                                        <button type="button" class="search-btn" onclick="search()" id="search-btn">
                                            <span class="glyphicon glyphicon-search"></span> Search
                                        </button>
                                    </div></a></li>
                        </ul>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>
        var loginemail = false;
        var loginpwrdle = false;
        function loginformvalidate() {
            var email = document.getElementById("loginemail");
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!filter.test(email.value)) {
                loginemail = false;
                $("#loginemail").addClass("loginform-wrong-email");
                $("#wrngemaillogin").show(500);
                document.getElementById("emailad").focus();
            } else {
                loginemail = true;
                $("#loginemail").removeClass("loginform-wrong-email");
                $("#wrngemaillogin").hide();
            }

            var pwrd = document.getElementById("loginpwrd").value;
            if (pwrd.length <= 4) {
                $("#loginpwrd").addClass("loginform-wrong-email");
                $("#wrngepwrdlogin").show(500);
                loginpwrdle = false;
            } else {
                $("#loginpwrd").removeClass("loginform-wrong-email");
                $("#wrngepwrdlogin").hide();
                loginpwrdle = true;
            }
            console.log(loginemail);
            console.log(loginpwrdle);
            if (loginemail === true && loginpwrdle === true) {
                $("#opensignin").prop("style", "display:none");
                document.getElementById("signupform").submit();
            }

        }

        function search() {
            var searchtxt = document.getElementById("searchtxt").value;
            var url = "products.jsp";
            window.location.replace(url + "?searchtxt=" + searchtxt);
        }
        $('#searchtxt').keypress(function (e) {
            if (e.which == 13) {
                var searchtxt = document.getElementById("searchtxt").value;
                var url = "products.jsp";
                window.location.replace(url + "?searchtxt=" + searchtxt);
            }
        });




    </script>



    <script type="text/javascript">
        /*Form validation*/

        function regformvalidate() {
            var emailch = false;
            var pwrdle = false;
            var pwrdma = false;
            var username = false;
            var uname = document.getElementById("unametf").value;
            if (uname === "" || uname === null) {

                var username = false;
                $("#unametf").addClass("loginform-wrong-email");
                $("#wrnguname").show(500);
            } else {

                var username = true;
                $("#wrnguname").hide(500);
                $("#unametf").removeClass("loginform-wrong-email");
            }


            var email = document.getElementById("emailad");
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!filter.test(email.value)) {

                emailch = false;
                $("#emailad").addClass("loginform-wrong-email");
                $("#wrngemail").show(500);
            } else {
                emailch = true;
                $("#emailad").removeClass("loginform-wrong-email");
                $("#wrngemail").hide();
            }

            var pwrd = document.getElementById("pwrd1").value;
            if (pwrd.length <= 4) {
                $("#pwrd1").addClass("loginform-wrong-email");
                $("#pwrd2").addClass("loginform-wrong-email");
                $("#wrngpasslength").show(500);
                pwrdle = false;
            } else {
                $("#pwrd1").removeClass("loginform-wrong-email");
                $("#wrngpasslength").hide();
                pwrdle = true;
            }

            var p1 = document.getElementById("pwrd1").value;
            var p2 = document.getElementById("pwrd2").value;
            if (p1 === p2) {
                if (!(p1 === "" || p2 === "")) {
                    pwrdma = true;
                    if (emailch === true && pwrdle === true && username === true) {

                        servletCall();
                    }
                } else {
                    pwrdma = false;
                    $("#pwrd1").addClass("loginform-wrong-email");
                    $("#pwrd2").addClass("loginform-wrong-email");
                    $("#passworddoesntmatch").html("<p class='typcn typcn-times wrongformfield'> Please fill password fields</p>");
                    $("#passworddoesntmatch").show();
                }
            } else {
                pwrdma = false;
                $("#pwrd1").addClass("loginform-wrong-email");
                $("#pwrd2").addClass("loginform-wrong-email");
                $("#pwrd1").val("");
                $("#pwrd2").val("");
                $("#passworddoesntmatch").show();
            }

        }



        function hide() {
            $("#wrngemail").hide();
            $("#wrngpasslength").hide();
            $("#passworddoesntmatch").hide();
            ("#emailad").removeClass("loginform-wrong-email");
            $("#pwrd1").removeClass("loginform-wrong-email");
            $("#pwrd2").removeClass()("loginform-wrong-email");
        }

        /*ajax request send*/
        function servletCall() {
            var a = $("#emailad").val();
            var b = $("#pwrd1").val();
            var c = $("#unametf").val();
            $('#signupformbegin').html('<p class="emailsentfont">Please check your email for complete the registration...!</p>');
            $('#SignUpFormInside').addClass(".emailsentbackground");
            $('#signupformbegin').show();
            $.post(
                    "User_registration",
                    {email: a, pwrd: b, uname: c}, //meaasge you want to send

            function (result) {

            });
        }

    </script>
    <div id="SignUpForm" class="modalDialog">
        <div id="SignUpFormInside">
            <a href="#close" title="Close" onclick="hideloginwrongelements()" class="close">X</a>
            <div id="signupformbegin" class="signup-form"><!--sign up form-->
                <h2>New User Signup!</h2>
                <form id="signupform" action="NewServlet" method="post">
                    <label id="wrnguname" class="hideitems"><p class="typcn typcn-times wrongformfield">Please Give a user name</p></label>
                    <%--input name="loginuname" id="unametf" class="form-control" type="text" placeholder="User name"/--%>
                    <input type="text" placeholder="user name" name="loginusername" class="form-control" id="unametf"/>

                    <label id="wrngemail" class="hideitems"><p class="typcn typcn-times wrongformfield"> invalid email</p></label>
                    <input name="email"  id="emailad" class="form-control" type="email" placeholder="Email Address"/>

                    <label id="wrngpasslength" class="hideitems"><p class="typcn typcn-times wrongformfield"> password length is must more than 5 characters</p></label>
                    <label id="passworddoesntmatch" class="hideitems"><p class="typcn typcn-times wrongformfield"> password doesn't match</p></label>
                    <label id="enterpassword" class="hideitems"><p class="typcn typcn-times wrongformfield"> enter password</p></label>
                    <input name="pwrd" id="pwrd1" onblur="" class="form-control" type="password" placeholder="Password"/>
                    <input  id="pwrd2" onfocus="" type="password"  class="form-control" placeholder="Re type Password"/>
                    <label class="pwrdreset">(Password must have more than 5 characters)</label>
                    <div><button type="button" id="subtn" onclick="regformvalidate()" class="btn btn-default">Signup</button></div>
                    <p id="equal"></p>
                </form>
            </div>	
        </div>
    </div>



    <div id="fgtpwrd" class="modalDialog">
        <div id="forgotpwrdinside">
            <a href="#close" title="Close" onclick="hideloginwrongelements()" class="close">X</a>
            <div id="forgotpwrdfrm" class="signup-form"><!--sign up form-->
                <h2>Forgot password</h2>
                <div id="loading" class="hideitems"><i class='fa fa-repeat fa-spin '></i> Please wait..</div>
                <form id="signupform" action="NewServlet" method="post">
                    <label id="invalidfrgtemail" class="hideitems"><p class="typcn typcn-times wrongformfield">Invalid email</p></label>
                    <label id="invalidfrgtemail2" class="hideitems"><p class="typcn typcn-times wrongformfield">Can't found you in system</p></label>
                    <label id="wrnguname" class="hideitems"><p class="typcn typcn-times wrongformfield">your email</p></label>
                    <input type="text" placeholder="Your email address" name="loginusername" class="form-control" id="frgtemail"/>
                    <div id="Removebtnsetwait"><button type="button" id="frgtpwrdbtn"  class="btn btn-default">Submit request</button></div>
                </form>
            </div>	
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#frgtpwrdbtn').click(function () {
                $("#invalidfrgtemail2").hide();
                var frgtmail = document.getElementById("frgtemail").value;
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!filter.test(frgtmail)) {

                    var status = '';
                    $("#frgtemail").addClass("loginform-wrong-email");
                    $("#invalidfrgtemail").show();
                } else {

                    $("#loading").show();
                    $.post(
                            "Forgot_password",
                            {email: frgtmail}, //meaasge you want to send

                    function (result) {

                        if (result == 1) {
                            $('#forgotpwrdfrm').html('<p class="emailsentfont">Please check your email reset your password</p>');
                        }
                        if (result == 2) {
                            $("#loading").hide();
                            $('#invalidfrgtemail2').show();
                        }

                    });
                }
            });
        });</script>

    <!--finish-->
    <div id="pleasecheckurmail" class="modalDialog">
        <div id="forgotpwrdinside">
            <a href="#close" title="Close" onclick="hideloginwrongelements()" class="close">X</a>
            <div id="forgotpwrdfrm" class="signup-form"><!--sign up form-->
                <p class="emailsentfont">Please confirm your email address</p>
            </div>	
        </div>
    </div>

    <!--finish-->
    <!--compare-->
    <script>
        var comp_id_list = [];

        <%//
            if (hs.getAttribute("mycompare") != null) {

                Java_classes.Compare com8 = (Java_classes.Compare) hs.getAttribute("mycompare");
                List<Java_classes.NewCompareItems> n2 = com8.getArrayData();
                String id = "";
                String details = "";

                if (n2.size() != 0) {
                    for (Java_classes.NewCompareItems ni : n2) {
                        id += ni.getProid() + ",";
                        Criteria c = sesheader.createCriteria(DB.Products.class);
                        c.add(Restrictions.eq("id", Integer.parseInt(ni.getProid())));
                        DB.Products pr = (DB.Products) c.uniqueResult();
                        details += pr.getModel().getManufacturers().getName() + " " + pr.getModel().getModel() + ",";
                    }
                    details = details.substring(0, details.length() - 1);
                    id = id.substring(0, id.length() - 1);
                }
        %>
        var ids = "<%=id%>";
        var details = "<%=details%>";
        var countids = 0;



        function loadcompare() {
            if (ids != "" && details != "") {
                var id = ids.split(",");
                var detai = details.split(",");
                countids = id.length;
                for (var i = 0; i < id.length; ++i) {
                    $('#test2').append('<tr id="row' + id[i] + '"><td>' + detai[i] + '</td><td><a style="cursor: pointer;" onclick="removefromcompare(' + id[i] + ')">X</a></td></tr>');
                    $('#test1').show(300);
                    if (id.length > 1) {
                        $('#combtnrow').show(300);
                    }
                    comp_id_list.push(id[i]);
                }
            }
        }
        <%//          
            }
        %>
        var compidsarr;
        var countids = 0;
        function compare(b) {

            ++countids;
            var c = b.split(",");
            compidsarr = c;

            if (countids < 3) {
                if (comp_id_list.length < 1) {

                    comp_id_list.push(c[0]);
                    $('#test2').append('<tr id="row' + c[0] + '"><td>' + c[1] + '</td><td><a style="cursor: pointer;" onclick="removefromcompare(' + c[0] + ')">X</a></td></tr>');
                    $('#test1').show(300);
                    var cou = document.getElementById('test2').rows.length;
                    if (cou > 1) {
                        $('#combtnrow').show(300);
                    }
                    $.post(
                            "Add_to_compare",
                            {id: c[0]}, //meaasge you want to send
                    function (result) {
                    });
                } else {
                    --countids;
                    forloop:for (var i = 0; i < comp_id_list.length; ++i) {
                        if (comp_id_list[i] == c[0]) {
                            alert("already added");
                            break forloop;
                        } else {
                            if (comp_id_list.length == i + 1) {
                                comp_id_list.push(c[0]);
                                $('#test2').append('<tr id="row' + c[0] + '"><td>' + c[1] + '</td><td><a style="cursor: pointer;" onclick="removefromcompare(' + c[0] + ')">X</a></td></tr>');
                                $('#test1').show(300);
                                var cou = document.getElementById('test2').rows.length;
                                if (cou > 1) {
                                    $('#combtnrow').show(300);
                                }
                                $.post(
                                        "Add_to_compare",
                                        {id: c[0]}, //meaasge you want to send
                                function (result) {
                                });
                                break forloop;
                            }
                        }
                    }
                }
            } else {
                alert("Can't compare more than 2 devices");
                --countids;
            }
        }
        function removefromcompare(b) {
            --countids;
            for (var i = 0; i < comp_id_list.length; ++i) {
                if (comp_id_list[i] == b) {
                    comp_id_list.splice(i, 1);
                }
            }
            $('#row' + b + '').remove();
            var cou = document.getElementById('test2').rows.length;
            if (cou == "") {
                $('#test1').hide(300);
            }
            if (cou == "1") {
                $('#combtnrow').hide();
            }


            $.post(
                    "Remove_from_compare",
                    {id: b}, //meaasge you want to send
            function (result) {
            });

        }
        function gotocompare() {
            window.location.replace("Compare.jsp");
        }
    </script>
    <!--finish-->
    <div id="test1"   class="watermark hideitems" >
        <table id="test3" border="0" cellspacing="3" i>
            <tbody id="test2">
            </tbody>
            <tfoot>
                <tr id="combtnrow" class="hideitems">               
                    <td   style="text-align: center"><button onclick="gotocompare()" class="btn btn-danger products-compare-btn"><label class="typcn typcn-export"> Compare</label></button></td>
                </tr>
            </tfoot>
        </table>
    </div>
    <!--finish-->

    `<%        Integer hitsCount = (Integer) application.getAttribute("hitCounter");
        if (hitsCount == null) {
            /* First visit */
            hitsCount = 1;
        } else {
            /* return visit */
            ++hitsCount;
        }

        application.setAttribute("hitCounter", hitsCount);

        String a = session.getId();
        HttpSession hssesid = request.getSession();
        Integer visitorcount = 1;
        if (hssesid.getAttribute("sesid") == null) {
            hssesid.setAttribute("sesid", a);

            visitorcount = (Integer) application.getAttribute("visit");
            if (visitorcount == null) {
                /* First visit */
                visitorcount = 1;
            } else {
                /* return visit */
                ++visitorcount;
            }

            application.setAttribute("visit", visitorcount);
        }
    %>

    <!--finish-->
    <div id="asssas" class="modalDialog">
        <div id="forgotpwrdinside">
            <a href="#close" title="Close" onclick="hideloginwrongelements()" class="close">X</a>
            <div id="forgotpwrdfrm" class="signup-form"><!--sign up form-->
                <h3>Your accc is disabled. please conact admin.(ishara.ira@gmail.com)</h3>
            </div>	
        </div>
    </div>
</header>
<!--finish-->
<script>
    // Create a clone of the menu, right next to original.
    $('.menu').addClass('original').clone().insertAfter('.menu').addClass('cloned').css('position', 'fixed').css('top', '0').css('margin-top', '0').css('z-index', '500').removeClass('original').hide();

    scrollIntervalID = setInterval(stickIt, 10);


    function stickIt() {

        var orgElementPos = $('.original').offset();
        orgElementTop = orgElementPos.top;

        if ($(window).scrollTop() >= (orgElementTop)) {
            // scrolled past the original position; now only show the cloned, sticky element.

            // Cloned element should always have same left position and width as original element.     
            orgElement = $('.original');
            coordsOrgElement = orgElement.offset();
            leftOrgElement = coordsOrgElement.left;
            widthOrgElement = orgElement.css('width');
            $('.cloned').css('left', leftOrgElement + 'px').css('top', 0).css('width', widthOrgElement).show();
            $('.original').css('visibility', 'hidden');
        } else {
            // not scrolled past the menu; only show the original menu.
            $('.cloned').hide();
            $('.original').css('visibility', 'visible');
        }
    }
</script>
<script type="text/javascript">
    window.onpageshow = function (event) {
        if (event.persisted) {
            window.location.reload()
        }
    };
</script>
