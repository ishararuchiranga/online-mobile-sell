<%-- 
    Document   : newjsp
    Created on : Dec 9, 2015, 11:26:12 AM
    Author     : Ishara
--%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.hibernate.criterion.Disjunction"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<script>
    window.onpageshow = function (evt) {
        // If persisted then it is in the page cache, force a reload of the page.
        if (evt.persisted) {
            document.body.style.display = "none";
            location.reload();

        }
    };

    var url = window.location.href;
    var urlparts = url.split('#');
    var b;
    if (urlparts[1] == b) {
    } else {
        console.log("history back");
        history.back();
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META HTTP-EQUIV="Expires" CONTENT="-1">

        <title>Home_page</title>
        <link rel="shortcut icon" type="image/x-icon" href="images/home/Slider1.jpg" />
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="css/navibar.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/typicons.min.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/price-range.css" rel="stylesheet">

        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/script.js"></script>


    </head >
    <body  id="fontchnge">
        <style>
            body{
                background-image: url("images/geometry2.png");
            }
            .active{
                position: relative;
            }
            .carousel-indicators li{
                border-radius: 5px;

                width: 50px;
            }
        </style>

        <div class="container-fluid fontchnge">         
            <%@include file="Header2.jsp" %>


            <div class="container bodycont">
                <div class="col-sm-12 ">
                    <%                             //
                        Session sesind1 = conn.NewHibernateUtil.getSessionFactory().openSession();
                        Criteria bancr = sesind1.createCriteria(DB.Banner.class);
                        List<DB.Banner> banlis = bancr.list();
                        if (!(banlis.isEmpty())) {


                    %>
                    <div id="slider-carousel" class="carousel slide" style="margin-top: 10px" data-ride="carousel">
                        <ol style="bottom: -32px" class="carousel-indicators">
                            <%  //
                                for (int i = 0; i <banlis.size(); ++i) {
                                    if (i == 0) {
                            %>
                            <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                                <%
                                } else {
                                %>
                            <li class="" data-target="#slider-carousel" data-slide-to="<%=i%>"></li>
                                <%
                                    }
                                %>

                            <%
                                }
                            %>
                        </ol>


                        <div class="carousel-inner imagepadding">
                            <%
                                int bansi = 1;
                                for (DB.Banner b : banlis) {
                                    if (bansi == 1) {
                            %>
                            <div class="item active">
                                <div class="col-sm-12">

                                    <a href="<%=b.getUrl()%>"><img src="<%=b.getImage()%>" class="girl img-responsive" alt="">

                                    </a>
                                </div>
                            </div>
                            <%
                            } else {

                            %>
                            <div class="item">
                                <div class="col-sm-12">
                                    <a href="<%=b.getUrl()%>"><img src="<%=b.getImage() %>" class="girl img-responsive" alt="">
                                    </a>
                                </div>
                            </div>

                            <%                              //
                                    }
                                    ++bansi;
                                }

                            %>

                            <!--                            <div class="item active">
                                                            <div class="col-sm-12">
                            
                                                                <a href="#"><img src="images/1.jpg" class="girl img-responsive" alt="">
                            
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="col-sm-12">
                                                                <a href="products.jsp"><img src="images/2.jpg" class="girl img-responsive" alt="">
                            
                                                                </a>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="item">
                            
                                                            <div class="col-sm-12">
                            
                                                                <img src="images/3.JPG" class="girl img-responsive" alt="">
                            
                                                            </div>
                                                        </div>-->
                        </div>                     
                    </div>
                    <%    }
                    %>
                    <!--                    <div id="slider-carousel" class=" carousel slide" data-ride="carousel">
                                            <ol class="carousel-indicators">
                                                <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                                                <li class="" data-target="#slider-carousel" data-slide-to="1"></li>
                                                <li class="" data-target="#slider-carousel" data-slide-to="2"></li>
                                            </ol>
                    
                                            <div class="carousel-inner imagepadding" >
                                                <div class="item active">
                    
                    
                                                    <div class="col-sm-12" style="padding: 0px">
                                                        <img src="images/924be7c0-f5e8-41b8-84cc-117d13d94f28.jpg" class="girl img-responsive" alt="">
                    
                                                    </div>
                                                </div>
                                                <div class="item">
                    
                                                    <div class="col-sm-12" style="padding: 0px">
                                                        <img src="images/vl2pkxxlgbgapa0c5vlt.png" class="girl img-responsive" alt="">
                    
                                                    </div>
                                                </div>
                    
                                                <div class="item">
                    
                                                    <div class="col-sm-12" style="padding: 0px">
                                                        <img src="images/924be7c0-f5e8-41b8-84cc-117d13d94f28.jpg" class="girl img-responsive" alt="">
                    
                                                    </div>
                                                </div>
                                            </div>
                    
                    
                    
                    
                                        </div>-->
                </div>
                <div class="row">
                    <div class="col-sm-12">


                        <div class="col-sm-3" style="padding-top: 30px;">

                            <div class="left-sidebar">

                                <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                                    <h3 class="sider_bar_headings">Brands</h3>
                                    <%//
                                        Session sesindex = conn.NewHibernateUtil.getSessionFactory().openSession();
                                        Criteria crindex = sesindex.createCriteria(DB.Manufacturers.class);
                                        crindex.addOrder(Order.desc("name"));
                                        List<DB.Manufacturers> lman = crindex.list();
                                        for (DB.Manufacturers manget : lman) {
                                    %>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="products.jsp?brandid=<%=manget.getId()%>">
                                                    <%= manget.getName()%>
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                    <%
                                        }
                                    %>

                                </div><!--/category-products-->
                            </div>
                            <div >
                                <div class="brands_products"><!--brands_products-->

                                    <div class="brands-name">
                                        <h3 class="sider_bar_headings">Products</h3>
                                        <ul class="nav nav-pills nav-stacked">
                                            <%
                                                Criteria crindex2 = sesindex.createCriteria(DB.DeviceType.class);
                                                crindex2.addOrder(Order.asc("type"));

                                                List<DB.DeviceType> listdev = crindex2.list();
                                                for (DB.DeviceType devt : listdev) {
                                            %>
                                            <li><a href="products.jsp?devtypeid=<%=devt.getId()%>"> <span class="pull-right"></span><%=devt.getType()%></a></li>
                                                    <%}%>
                                        </ul>
                                    </div>
                                </div>
                                <div class="shipping text-center"><!--shipping-->
                                    <img src="images/home/shipping.jpg" alt="">
                                </div><!--/shipping-->

                            </div>
                        </div><%--Side bar--%>

                        <div class="col-sm-9">
                            <!--                            <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                                                            <ol class="carousel-indicators">
                                                                <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                                                                <li class="" data-target="#slider-carousel" data-slide-to="1"></li>
                                                                <li class="" data-target="#slider-carousel" data-slide-to="2"></li>
                                                            </ol>
                            
                                                            <div class="carousel-inner imagepadding">
                                                                <div class="item active">
                                                                    <div class="col-sm-6">
                                                                        <h1><span>Smart phones</span></h1>
                                                                        <h2>Buy or sell</h2>
                                                                        <p>Come to us. Sell your old one. Buy your dream phone. Compare prices, specs and etc. Rating system can rate Phones and advertisements</p>
                                                                        <button type="button" class="btn btn-default get">Find it now</button>
                                                                    </div>
                            
                                                                    <div class="col-sm-6">
                                                                        <img src="images/home/Slider1.jpg" class="girl img-responsive" alt="">
                            
                                                                    </div>
                                                                </div>
                                                                <div class="item">
                                                                    <div class="col-sm-6">
                                                                        <h1><span>Fix Your Phone</span></h1>
                                                                        <h2>Locate Repair shop</h2>
                                                                        <p>Locate your nearest repair center.Rate the place with us. Add your business place. Promote your business place. </p>
                                                                        <button type="button" class="btn btn-default get">Locate it now</button>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <img src="images/home/Slider2.jpg" class="girl img-responsive" alt="">
                            
                                                                    </div>
                                                                </div>
                            
                                                                <div class="item">
                                                                    <div class="col-sm-6">
                                                                        <h1><span>Find phone store</span></h1>
                                                                        <h2>Locate near phone shop</h2>
                                                                        <p>Locate your nearest mobile phone selling centers. feel the expirienc. share it with us. rate it with us. Add your business place. promote your business.</p>
                                                                        <button type="submit" class="btn btn-default get">Locate it now</button>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <img src="images/home/Slider3.jpg" class="girl img-responsive" alt="">
                            
                                                                    </div>
                                                                </div>
                                                            </div>
                            
                            
                            
                            
                                                        </div>-->

                            <%--Slider--%>

                            <div class="row">
                                <div class="col-sm-12">


                                    <div class="col-sm-12">
                                        <div class="row">


                                            <div class="col-sm-12 padding-right">
                                                <div class="features_items"><!--features_items-->
                                                    <h3 class="features_item_bar_headings">Newly Added product</h3>

                                                    <%//
                                                        DB.ProStatus prost = (DB.ProStatus) sesindex.load(DB.ProStatus.class, Integer.parseInt("1"));

                                                        Criteria crnewpr = sesindex.createCriteria(DB.Products.class);
                                                        crnewpr.addOrder(Order.desc("id"));
                                                        crnewpr.add(Restrictions.eq("proStatus", prost));
                                                        crnewpr.setMaxResults(8);
                                                        List<DB.Products> newprlist = crnewpr.list();
                                                        for (DB.Products pr : newprlist) {
                                                    %>
                                                    <div class="col-sm-3">
                                                        <div class="product-image-wrapper">
                                                            <div class="single-products">
                                                                <div class="productinfo text-center">
                                                                    <p class="Home_brand"><%=pr.getModel().getManufacturers().getName()%></p>
                                                                    <p><%=pr.getModel().getModel()%></p>
                                                                    <div style="height: 170px;">
                                                                        <img src="<%=pr.getModel().getImgSrc()%>" alt="">                                                                        
                                                                    </div>
                                                                    <h2>Rs.<%
                                                                        DecimalFormat df = new DecimalFormat("0.00");
                                                                        out.print(df.format(pr.getPrice()));
                                                                        %></h2>
                                                                    <a href="Product_details.jsp?id=<%=pr.getId()%>" class="btn btn-default add-to-cart"><i class="fa fa-clipboard"></i>View product</a>
                                                                </div> 
                                                            </div>
                                                            <div class="choose">
                                                                <ul class="nav nav-pills nav-justified">
                                                                    <li><a href="#"><i class="typcn typcn-bookmark"></i>Add to wishlist</a></li>
                                                                    <li><a href="#"><i class="typcn typcn-export-outline"></i>Add to compare</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%           //
                                                        }
                                                    %>



                                                </div><!--features_items-->

                                                <div class="category-tab"><!--category-tab-->
                                                    <div class="col-sm-12">
                                                        <ul class="nav nav-tabs">
                                                            <li><a href="#phones" data-toggle="tab">Smart phones</a></li>
                                                            <li><a href="#Tabs" data-toggle="tab">Tabs</a></li>
                                                            <li><a href="#smartwatches" data-toggle="tab">Smart Watches</a></li>
                                                            <li><a href="#accesories" data-toggle="tab">Accessories</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-content">
                                                        <div class="tab-pane fade active in" id="phones">
                                                            <%
                                                                Criteria criteriatest = sesindex.createCriteria(DB.Model.class);
                                                                Disjunction dis = Restrictions.disjunction();
                                                                DB.DeviceType de = (DB.DeviceType) sesindex.load(DB.DeviceType.class, Integer.parseInt("1"));
                                                                dis.add(Restrictions.eq("deviceType", de));
                                                                criteriatest.add(dis);
                                                                List<DB.Model> phnlist1 = criteriatest.list();

                                                                Criteria crphnes = sesindex.createCriteria(DB.Products.class);
                                                                crphnes.addOrder(Order.desc("id"));
                                                                crnewpr.add(Restrictions.eq("proStatus", prost));
                                                                crphnes.setMaxResults(4);
                                                                crphnes.add(Restrictions.in("model", phnlist1));
                                                                List<DB.Products> phnlist2 = crphnes.list();
                                                                for (DB.Products pr : phnlist2) {

                                                            %> 
                                                            <div class="col-sm-3">
                                                                <div class="product-image-wrapper">
                                                                    <div class="single-products">
                                                                        <div class="productinfo text-center">
                                                                            <div style="height: 140px">
                                                                                <img style="max-height: 140px" src="<%=pr.getModel().getImgSrc()%>" alt="">                                                                                
                                                                            </div>
                                                                            <h2>RS.<%
                                                                                DecimalFormat df = new DecimalFormat("0.00");
                                                                                out.print(df.format(pr.getPrice()));
                                                                                %></h2>
                                                                            <p><%=pr.getModel().getManufacturers().getName()%> <%=pr.getModel().getModel()%> </p>
                                                                            <a href="Product_details.jsp?id=<%=pr.getId()%>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <%                                               //
                                                                }
                                                            %>


                                                        </div>

                                                        <div class="tab-pane fade" id="Tabs">

                                                            <%
                                                                Criteria criteriatest2 = sesindex.createCriteria(DB.Model.class);
                                                                Disjunction dis2 = Restrictions.disjunction();
                                                                DB.DeviceType de2 = (DB.DeviceType) sesindex.load(DB.DeviceType.class, Integer.parseInt("2"));
                                                                dis2.add(Restrictions.eq("deviceType", de2));
                                                                criteriatest2.add(dis2);
                                                                List<DB.Model> tablist1 = criteriatest2.list();

                                                                Criteria crtabs = sesindex.createCriteria(DB.Products.class);
                                                                crtabs.addOrder(Order.desc("id"));
                                                                crnewpr.add(Restrictions.eq("proStatus", prost));
                                                                crtabs.setMaxResults(4);
                                                                crtabs.add(Restrictions.in("model", tablist1));
                                                                List<DB.Products> tabList2 = crtabs.list();
                                                                for (DB.Products pr2 : tabList2) {


                                                            %> 

                                                            <div class="col-sm-3">
                                                                <div class="product-image-wrapper">
                                                                    <div class="single-products">
                                                                        <div class="productinfo text-center">
                                                                            <div style="height: 140px">
                                                                                <img style="max-height: 140px" src="<%=pr2.getModel().getImgSrc()%>" alt="">                                                                                
                                                                            </div>
                                                                            <h2>RS.<%=pr2.getPrice()%></h2>
                                                                            <p><%=pr2.getModel().getManufacturers().getName()%> <%=pr2.getModel().getModel()%> </p>
                                                                            <a href="Product_details.jsp?id=<%=pr2.getId()%>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <%
                                                                }
                                                            %>
                                                        </div>

                                                        <div class="tab-pane fade" id="accesories">
                                                            <%
                                                                Criteria criteriatest3 = sesindex.createCriteria(DB.Model.class);
                                                                Disjunction dis3 = Restrictions.disjunction();
                                                                DB.DeviceType de3 = (DB.DeviceType) sesindex.load(DB.DeviceType.class, Integer.parseInt("3"));
                                                                dis3.add(Restrictions.eq("deviceType", de3));
                                                                criteriatest3.add(dis3);
                                                                List<DB.Model> acclist1 = criteriatest3.list();

                                                                Criteria cracc = sesindex.createCriteria(DB.Products.class);
                                                                cracc.addOrder(Order.desc("id"));
                                                                crnewpr.add(Restrictions.eq("proStatus", prost));
                                                                cracc.setMaxResults(4);
                                                                cracc.add(Restrictions.in("model", acclist1));
                                                                List<DB.Products> acclist2 = cracc.list();
                                                                for (DB.Products pr3 : acclist2) {


                                                            %> 

                                                            <div class="col-sm-3">
                                                                <div class="product-image-wrapper">
                                                                    <div class="single-products">
                                                                        <div class="productinfo text-center">
                                                                            <div style="height: 140px">
                                                                                <img style="max-height: 140px" src="<%=pr3.getModel().getImgSrc()%>" alt="">                                                                                
                                                                            </div>
                                                                            <h2>RS.<%=pr3.getPrice()%></h2>
                                                                            <p><%=pr3.getModel().getManufacturers().getName()%> <%=pr3.getModel().getModel()%> </p>
                                                                            <a href="Product_details.jsp?id=<%=pr3.getId()%>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <%
                                                                }
                                                            %>
                                                        </div>

                                                        <div class="tab-pane fade" id="smartwatches">
                                                            <%
                                                                Criteria criteriatest4 = sesindex.createCriteria(DB.Model.class);
                                                                Disjunction dis4 = Restrictions.disjunction();
                                                                DB.DeviceType de4 = (DB.DeviceType) sesindex.load(DB.DeviceType.class, Integer.parseInt("4"));
                                                                dis4.add(Restrictions.eq("deviceType", de4));
                                                                criteriatest4.add(dis4);
                                                                List<DB.Model> smwatchlsit1 = criteriatest4.list();
                                                                if (!(smwatchlsit1.isEmpty())) {
                                                                    Criteria crsmwatch = sesindex.createCriteria(DB.Products.class);
                                                                    crnewpr.add(Restrictions.eq("proStatus", prost));
                                                                    crsmwatch.addOrder(Order.desc("id"));
                                                                    crsmwatch.setMaxResults(4);
                                                                    crsmwatch.add(Restrictions.in("model", smwatchlsit1));
                                                                    List<DB.Products> smwatchlsit2 = crsmwatch.list();

                                                                    for (DB.Products pr4 : smwatchlsit2) {


                                                            %> 

                                                            <div class="col-sm-3">
                                                                <div class="product-image-wrapper">
                                                                    <div class="single-products">
                                                                        <div class="productinfo text-center">
                                                                            <div style="height: 140px">
                                                                                <img style="max-height: 140px" src="<%=pr4.getModel().getImgSrc()%>" alt="">                                                                                
                                                                            </div>
                                                                            <h2>RS.<%=pr4.getPrice()%></h2>
                                                                            <p><%=pr4.getModel().getManufacturers().getName()%> <%=pr4.getModel().getModel()%> </p>
                                                                            <a href="Product_details.jsp?id=<%=pr4.getId()%>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <%
                                                                    }
                                                                } else {
                                                                    out.print("empty");
                                                                }
                                                            %>
                                                        </div>

                                                        <div class="tab-pane fade" id="poloshirt">
                                                            <div class="col-sm-3">
                                                                <div class="product-image-wrapper">
                                                                    <div class="single-products">
                                                                        <div class="productinfo text-center">
                                                                            <img src="images/home/gallery2.jpg" alt="">
                                                                            <h2>$56</h2>
                                                                            <p>Easy Polo Black Edition</p>
                                                                            <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="product-image-wrapper">
                                                                    <div class="single-products">
                                                                        <div class="productinfo text-center">
                                                                            <img src="images/home/gallery4.jpg" alt="">
                                                                            <h2>$56</h2>
                                                                            <p>Easy Polo Black Edition</p>
                                                                            <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="product-image-wrapper">
                                                                    <div class="single-products">
                                                                        <div class="productinfo text-center">
                                                                            <img src="images/home/gallery3.jpg" alt="">
                                                                            <h2>$56</h2>
                                                                            <p>Easy Polo Black Edition</p>
                                                                            <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="product-image-wrapper">
                                                                    <div class="single-products">
                                                                        <div class="productinfo text-center">
                                                                            <img src="images/home/gallery1.jpg" alt="">
                                                                            <h2>$56</h2>
                                                                            <p>Easy Polo Black Edition</p>
                                                                            <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!--/category-tab-->

                                                <div class="recommended_items"><!--recommended_items-->


                                                    <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                                                        <h3 class="recomonded_item_bar_headings">recommended items</h3>
                                                        <div class="carousel-inner">
                                                            <div class="item">	
                                                                <div class="col-sm-4">
                                                                    <div class="product-image-wrapper">
                                                                        <div class="single-products">
                                                                            <div class="productinfo text-center">
                                                                                <img src="images/home/recommend1.jpg" alt="">
                                                                                <h2>$56</h2>
                                                                                <p>Easy Polo Black Edition</p>
                                                                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="product-image-wrapper">
                                                                        <div class="single-products">
                                                                            <div class="productinfo text-center">
                                                                                <img src="images/home/recommend2.jpg" alt="">
                                                                                <h2>$56</h2>
                                                                                <p>Easy Polo Black Edition</p>
                                                                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="product-image-wrapper">
                                                                        <div class="single-products">
                                                                            <div class="productinfo text-center">
                                                                                <img src="images/home/recommend3.jpg" alt="">
                                                                                <h2>$56</h2>
                                                                                <p>Easy Polo Black Edition</p>
                                                                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item active">	
                                                                <div class="col-sm-4">
                                                                    <div class="product-image-wrapper">
                                                                        <div class="single-products">
                                                                            <div class="productinfo text-center">
                                                                                <img src="images/home/recommend1.jpg" alt="">
                                                                                <h2>$56</h2>
                                                                                <p>Easy Polo Black Edition</p>
                                                                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="product-image-wrapper">
                                                                        <div class="single-products">
                                                                            <div class="productinfo text-center">
                                                                                <img src="images/home/recommend2.jpg" alt="">
                                                                                <h2>$56</h2>
                                                                                <p>Easy Polo Black Edition</p>
                                                                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="product-image-wrapper">
                                                                        <div class="single-products">
                                                                            <div class="productinfo text-center">
                                                                                <img src="images/home/recommend3.jpg" alt="">
                                                                                <h2>$56</h2>
                                                                                <p>Easy Polo Black Edition</p>
                                                                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                                            <i class="fa fa-angle-left"></i>
                                                        </a>
                                                        <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                                            <i class="fa fa-angle-right"></i>
                                                        </a>			
                                                    </div>
                                                </div><!--/recommended_items-->
                                            </div>
                                        </div>
                                        <%--Products--%>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>


        <%@include file="Footer.jsp" %>
    </body>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/price-range.js"></script>
    <!--<script src="js/jquery.prettyPhoto.js"></script>-->
    <script src="js/main.js"></script>

    <script>

    $('.menu').addClass('original').clone().insertAfter('.menu').addClass('cloned').css('position', 'fixed').css('top', '0').css('margin-top', '0').css('z-index', '500').removeClass('original').hide();

    scrollIntervalID = setInterval(stickIt, 10);


    function stickIt() {

        var orgElementPos = $('.original').offset();
        orgElementTop = orgElementPos.top;

        if ($(window).scrollTop() >= (orgElementTop)) {
            // scrolled past the original position; now only show the cloned, sticky element.

            // Cloned element should always have same left position and width as original element.     
            orgElement = $('.original');
            coordsOrgElement = orgElement.offset();
            leftOrgElement = coordsOrgElement.left;
            widthOrgElement = orgElement.css('width');
            $('.cloned').css('left', leftOrgElement + 'px').css('top', 0).css('width', widthOrgElement).show();
            $('.original').css('visibility', 'hidden');
        } else {
            // not scrolled past the menu; only show the original menu.
            $('.cloned').hide();
            $('.original').css('visibility', 'visible');
        }
    }


    </script>
</html>

