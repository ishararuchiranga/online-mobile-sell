<%-- 
    Document   : AdminPanel1
    Created on : Mar 13, 2016, 10:54:06 AM
    Author     : Ishara
--%>

<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.hibernate.criterion.Projections"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="shortcut icon" type="image/x-icon" href="images/home/Slider1.jpg" />
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="css/navibar.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/typicons.min.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/price-range.css" rel="stylesheet">

        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/script.js"></script>

    </head>
    <body onload="loadfirst()" >

        <style>

            .dropbtn {
                background-color: #ffffff;
                color: black;
                padding: 8px;
                // margin:6px;
                font-size: 16px;
                border: none;
                cursor: pointer;
                border: none;
                height: 50px;

            }

            .dropdown {
                position: relative;
                display: inline-block;
                float: right;
                margin-right: 30px;
            }

            .dropdown-content {
                display: none;
                position: absolute;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            }

            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }

            .dropdown-content a:hover {
                background-color: #f1f1f1
            }

            .dropdown:hover .dropdown-content {
                display: block;
                z-index: 1;
            }

            .dropdown:hover .dropbtn {
                background-color: #D8D7D7;
            }

            //
            .topbaradmin{
                position: fixed;
                z-index: 1;
                background-color: #F8F8F8; 
                margin: 0px;
            }
            .logoadmin{
                display: inline-block
            }
            .logoadmin img{
                margin: 6px;
            }
            .sidebaradmin li a{
                margin:  0px;
            }
            .active-menu{
                background-color: #E5580C;
                color: white;
            }
            .well{
                min-height: 20px;
                padding: 19px;
                margin-bottom: 20px;
                background-color: #F5F5F5;
                border: 1px solid #E3E3E3;
                border-radius: 4px;
                box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.05) inset;
            }
            .quick-btn {
                position: relative;
                display: inline-block;
                width: 90px;
                height: 80px;
                padding-top: 16px;
                margin: 10px;
                color: #444;
                text-align: center;
                text-decoration: none;
                text-shadow: 0px 1px 0px rgba(255, 255, 255, 0.6);
                box-shadow: 0px 0px 0px 1px #F8F8F8 inset, 0px 0px 0px 1px #CCC;
            }
            .quick-btn .label {
                position: absolute;
                top: -5px;
                right: -5px;
            }
            .quick-btn span {
                display: block;
            }
            .label-danger {
                background-color: #D9534F;
            }
            .label {
                display: inline;
                padding: 0.2em 0.6em 0.3em;
                font-size: 75%;
                font-weight: bold;
                line-height: 1;
                color: #FFF;
                text-align: center;
                white-space: nowrap;
                vertical-align: baseline;
                border-radius: 0.25em;
            }
            .adminiconsize{
                font-size: 35px;
            }
        </style>
        <div class="topbaradmin">
            <div class="logoadmin">
                <img src="images/home/Phone.PNG">
            </div>

            <div class="dropdown" >
                <button class="dropbtn"><img style="height: 28px" src="images/orange.png"> Admin Ishara</button>
                <div class="dropdown-content">
                    <a href="#">Logout</a>
                    <a href="#">Settings</a>
                </div>
            </div>
        </div>
        <div class="col-sm-2" style=" padding: 0px;position: relative; box-sizing: border-box; float: left; height:2500px; background-color: #F8F8F8 ;border: 1px solid #D8D7D7;">
            <nav class="navbar-default navbar-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav sidebaradmin" id="main-menu">
                        <li>
                            <a   href="AdminPanel1.jsp"><i class="fa fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="Admin_addProducts.jsp"><i class="fa fa-plus-square"></i> Add product</a>
                        </li>
                        <li>
                            <a class="active-menu" href="Admin_product_details.jsp"><i class="fa fa-check-square-o"></i> Product details</a>
                        </li>
                        <li>
                            <a href="Admin_user_management.jsp"><i class="fa fa-users"></i> User management</a>

                        </li>

                        <li>
                            <a href="Admin_shipping.jsp"><i class="fa fa-truck"></i> Delivery</a>
                        </li>
                    </ul>

                </div>

            </nav>

        </div>
        <div class="col-sm-10"  style="float: left;  margin: 0px; box-sizing: border-box; height: 2500px; background-color: #F8F8F8;border: 1px solid #D8D7D7;">

            <h3>Sales details</h3>
            <select style="width: 10%" id="op" onchange="loadIframe()">
                <option  value="a">Daily</option>
                <option  value="b">Weekly</option>
                <option  value="c">Monthly</option>
                <option  value="d">Yearly</option>
            </select>
            <iframe id="if" width="100%" height="100%" scrolling="no" border="0" src="Selling_summery.jsp" frameborder="no"></iframe>
        </div>
        <script>
            function loadIframe() {

                if ($('#op').find(":selected").val() == "a") {
                    var $iframe = $('#if');
                    if ($iframe.length) {
                        $iframe.attr('src', "Selling_summery.jsp?type=1");
                        //  return false;
                    }
                    //return true;
                } else {
                    var $iframe = $('#if');
                    if ($iframe.length) {
                        $iframe.attr('src', "Selling_summery.jsp?type=2");
                        //return false;
                    }
                    //return true;
                }
//                
            }
            function loadfirst() {
                var $iframe = $('#if');
                if ($iframe.length) {
                    $iframe.attr('src', "Selling_summery.jsp?type=1");
                    //  return false;
                }

            }

        </script>


    </body>
</html>
