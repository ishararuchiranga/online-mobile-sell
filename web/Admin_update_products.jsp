<%-- 
    Document   : Admin_addProducts
    Created on : Mar 16, 2016, 6:29:26 PM
    Author     : Ishara
--%>

<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css" />
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/admin.css" rel="stylesheet">
        <script src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/calendar.js"></script>
        <script src="js/jquery.ajaxfileupload.js"></script>


        <%
            Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        %>
    </head>
    <body>
        <style>

            .dropbtn {
                background-color: #ffffff;
                color: black;
                padding: 8px;
                // margin:6px;
                font-size: 16px;
                border: none;
                cursor: pointer;
                border: none;
                height: 50px;

            }

            .dropdown {
                position: relative;
                display: inline-block;
                float: right;
                margin-right: 30px;
            }

            .dropdown-content {
                display: none;
                position: absolute;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            }

            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }

            .dropdown-content a:hover {
                background-color: #f1f1f1
            }

            .dropdown:hover .dropdown-content {
                display: block;
                z-index: 1;
            }

            .dropdown:hover .dropbtn {
                background-color: #D8D7D7;
            }

            //
            .topbaradmin{
                position: fixed;
                z-index: 1;
                background-color: #F8F8F8; 
                margin: 0px;
            }
            .logoadmin{
                display: inline-block
            }
            .logoadmin img{
                margin: 6px;
            }
            .sidebaradmin li a{
                margin:  0px;
            }
            .active-menu{
                background-color: #E5580C;
                color: white;
            }
            .well{
                min-height: 20px;
                padding: 19px;
                margin-bottom: 20px;
                background-color: #F5F5F5;
                border: 1px solid #E3E3E3;
                border-radius: 4px;
                box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.05) inset;
            }
            .quick-btn {
                position: relative;
                display: inline-block;
                width: 90px;
                height: 80px;
                padding-top: 16px;
                margin: 10px;
                color: #444;
                text-align: center;
                text-decoration: none;
                text-shadow: 0px 1px 0px rgba(255, 255, 255, 0.6);
                box-shadow: 0px 0px 0px 1px #F8F8F8 inset, 0px 0px 0px 1px #CCC;
            }
            .quick-btn .label {
                position: absolute;
                top: -5px;
                right: -5px;
            }
            .quick-btn span {
                display: block;
            }
            .label-danger {
                background-color: #D9534F;
            }
            .label {
                display: inline;
                padding: 0.2em 0.6em 0.3em;
                font-size: 75%;
                font-weight: bold;
                line-height: 1;
                color: #FFF;
                text-align: center;
                white-space: nowrap;
                vertical-align: baseline;
                border-radius: 0.25em;
            }
            .adminiconsize{
                font-size: 35px;
            }
        </style>
        <div class="topbaradmin">
            <div class="logoadmin">
                <img src="images/home/Phone.PNG">
            </div>

            <div class="dropdown" >
                <button class="dropbtn"><img style="height: 28px" src="images/orange.png"> Admin Ishara</button>
                <div class="dropdown-content">
                    <a href="#">Logout</a>
                    <a href="#">Settings</a>
                </div>
            </div>
        </div>
        <div class="col-sm-2" style=" padding: 0px;position: relative; box-sizing: border-box; float: left; height:2500px; background-color: #F8F8F8 ;border: 1px solid #D8D7D7;">
            <nav class="navbar-default navbar-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav sidebaradmin" id="main-menu">
                        <li>
                            <a  href="AdminPanel1.jsp"><i class="fa fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a class="active-menu" href="Admin_addProducts.jsp"><i class="fa fa-plus-square"></i> Addproduct</a>
                        </li>
                        <li>
                            <a href="Admin_product_details.jsp"><i class="fa fa-check-square-o"></i> Product details</a>
                        </li>
                        <li>
                            <a href="Admin_user_management.jsp"><i class="fa fa-users"></i> User management</a>

                        </li>

                        <li>
                            <a href=""><i class="fa fa-table"></i> Responsive Tables</a>
                        </li>
                        <li>
                            <a href=""><i class="fa fa-edit"></i> Forms </a>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-sitemap"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li>
                                    <a href="#">Second Level Link</a>
                                </li>
                                <li>
                                    <a href="#">Second Level Link</a>
                                </li>
                                <li>
                                    <a href="#">Second Level Link<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level collapse">
                                        <li>
                                            <a href="#">Third Level Link</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Link</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Link</a>
                                        </li>

                                    </ul>

                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="empty.html"><i class="fa fa-fw fa-file"></i> Empty Page</a>
                        </li>
                    </ul>

                </div>

            </nav>

        </div>
        <div class="col-sm-10"  style="padding: 5px;float: left;  margin: 0px; box-sizing: border-box; height: 2500px; background-color: #F8F8F8;border: 1px solid #D8D7D7;">
            <style>
                .tabs-ec{
                    margin-bottom: 60px;
                }
                #oth{
                    color: black;
                    background-color: rgb(236, 236, 236);
                    margin-bottom: 20px;
                }
                #oth a{
                    color: #98989f;
                }
                #oth a:hover{
                    color: #a5bce0;
                }
                .tabs-ec h2{
                    text-align: center;
                    margin: 0px;
                    float: left;
                    width: 50%;
                    border: 1px solid #D5D5BC;
                    height: 40px;
                    padding-left: 10px;
                    color: wheat;
                    padding-top: 2px;
                    cursor: pointer;
                }
                .first{
                    border-top-left-radius: 15px;
                    border-bottom-left-radius: 15px;
                }
                .final{
                    border-top-right-radius:  15px;
                    border-bottom-right-radius: 15px;
                }
                #act{
                    background-color: #dcdcf6;
                    color: #8f8f8f;
                }


            </style>
            <div class="tabs-ec">
                <h2 class="first" id="oth"><a href="Admin_addProducts.jsp">Add Product</a></h2>
                <h2  class="final"  id="act"><a href="Admin_update_products.jsp">Update Product</a></h2>
            </div>


            <div class="updatepro-bar">
                <input type="text" id="searckeuy" placeholder="Product ID or Name">
                &nbsp;&nbsp;&nbsp;
                Product type:
                <select style="width: 15%" id="type">
                    <option  value="0">All</option>
                    <option  value="1">Smart Phones</option>
                    <option  value="2">Tabs</option>
                    <option  value="4">Smart Watches</option>
                    <option  value="3">Accessories</option>
                </select>
                &nbsp;&nbsp;&nbsp;
                Stock Availability:
                <select style="width: 10%" id="stockav">
                    <option  value="0">All</option>
                    <option  value="1">In stock</option>
                    <option  value="2">Out of stock</option>
                </select>
                &nbsp;&nbsp;&nbsp;
                Status:
                <select style="width: 10%" id="status">
                    <option  value="0">All</option>
                    <option  value="1">Active</option>
                    <option  value="2">Inactive</option>
                </select>
                <input type="button" value="Search" id="loadifrm" onclick="loadIframe()">
            </div>

            <iframe id="if" name="ifr" width="100%" height="100%" scrolling="no" border="0" src="Admin_update_product2.jsp?keyword=0x&type=0&stock=0&status=0" frameborder="no">Your browser not support this feature</iframe>
        </div>
        <script>
            function loadIframe() {
                var searchkey = $('#searckeuy').val();
                var type = $('#type').find(":selected").val();
                var stock = $('#stockav').find(":selected").val();
                var status = $('#status').find(":selected").val();
                var $iframe = $('#if');
                if (searchkey == "") {
                    $iframe.attr('src', "Admin_update_product2.jsp?keyword=0x&type=" + type + "&stock=" + stock + "&status=" + status + "");
                } else {
                    $iframe.attr('src', "Admin_update_product2.jsp?keyword=" + searchkey + "&type=" + type + "&stock=" + stock + "&status=" + status + "");
                }
            }
            
            $('#searckeuy').keypress(function (e) {
                if (e.which == 13) {

                    var searchkey = $('#searckeuy').val();
                    var type = $('#type').find(":selected").val();
                    var stock = $('#stockav').find(":selected").val();
                    var status = $('#status').find(":selected").val();
                    var $iframe = $('#if');
                    if (searchkey == "") {
                        $iframe.attr('src', "Admin_update_product2.jsp?keyword=0x&type=" + type + "&stock=" + stock + "&status=" + status + "");
                    } else {
                        $iframe.attr('src', "Admin_update_product2.jsp?keyword=" + searchkey + "&type=" + type + "&stock=" + stock + "&status=" + status + "");
                    }
                }
            });


            $(document).ready(function () {
                $('#type option[value="0"]').attr('selected', 'selected');
                $('#stockav option[value="0"]').attr('selected', 'selected');
                $('#status option[value="0"]').attr('selected', 'selected');
                $('#searckeuy').val("");
            });
        </script>




    </body>
</html>
