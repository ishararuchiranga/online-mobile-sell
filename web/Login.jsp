<%-- 
    Document   : Login
    Created on : Dec 15, 2015, 7:53:43 PM
    Author     : Ishara
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="shortcut icon" type="image/x-icon" href="images/home/Slider1.jpg" />
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="css/navibar.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/typicons.min.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/price-range.css" rel="stylesheet">

        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/script.js"></script>
    </head>
    <body>
        <%@include file="Header.jsp" %>
        <%  //
            HttpSession httpses = request.getSession();
            if (httpses.getAttribute("requestfrom") != null) {
                out.print(httpses.getAttribute("requestfrom"));
            }
        %>

        <div class="container login_page">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-5">
                        <div class="login-form"><!--login form-->
                            <h2>Login to your account</h2>
                            <form action="#">
                                <input type="text" placeholder="Name" />
                                <input type="email" placeholder="Email Address" />
                                <span>
                                    <input type="checkbox" class="checkbox"> 
                                    Keep me signed in
                                </span>
                                <button type="submit" class="btn btn-default">Login</button>
                            </form>
                        </div><!--/login form-->
                    </div>

                    <div class="col-sm-1">
                        <h2 class="login_or">OR</h2>
                    </div>

                    <div class="col-sm-5">
                        <div class="signup-form"><!--sign up form-->
                            <h2>New User Signup!</h2>
                            <form action="#">
                                <input type="text" placeholder="Full Name"/>
                                <input type="email" placeholder="Email Address"/>
                                <input type="password" placeholder="Password"/>
                                <input type="password" placeholder="Re type Password"/>
                                <button type="submit" class="btn btn-default">Signup</button>
                            </form>
                        </div><!--/sign up form-->
                    </div>

                </div>
            </div>
        </div>
        <%@include file="Footer.jsp" %>
    </body>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</html>
