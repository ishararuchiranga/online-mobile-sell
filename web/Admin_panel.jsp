<%-- 
    Document   : Admin_panel
    Created on : Dec 27, 2015, 11:09:40 AM
    Author     : Ishara
--%>

<%@page import="java.util.List"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <link href="css/main.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/admin.css" rel="stylesheet">
        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/main.js"></script>


        <script type="text/javascript">

            $(document).ready(function () {

                // assuming the controls you want to attach the plugin to 
                // have the "datepicker" class set

            });


        </script>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('.tabs .tab-links a').on('click', function (e) {
                    var currentAttrValue = jQuery(this).attr('href');
                    // Show/Hide Tabs
                    //jQuery('.tabs ' + currentAttrValue).show().siblings().hide();
                    jQuery('.tabs ' + currentAttrValue).slideDown(400).siblings().slideUp(400);
                    // Change/remove current tab to active
                    jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
                    e.preventDefault();
                });

            });

            $(function () {

                var iFrames = $('iframe');

                function iResize() {

                    for (var i = 0, j = iFrames.length; i < j; i++) {
                        iFrames[i].style.height = iFrames[i].contentWindow.document.body.offsetHeight + 'px';
                    }
                }

                if ($.browser.safari || $.browser.opera) {

                    iFrames.load(function () {
                        setTimeout(iResize, 0);
                    });

                    for (var i = 0, j = iFrames.length; i < j; i++) {
                        var iSource = iFrames[i].src;
                        iFrames[i].src = '';
                        iFrames[i].src = iSource;
                    }
                } else {
                    iFrames.load(function () {
                        this.style.height = this.contentWindow.document.body.offsetHeight + 'px';
                    });
                }

            });

        </script>

    </head>

    <body style="background: transparent radial-gradient(at 40% 30% , rgb(74, 152, 211), rgb(0, 89, 143))repeat scroll 0% 0%;">

        <div class="container">
            <div class="col-sm-12">
                <h1>Admin panel</h1>
            </div>
            <div class="tabs">

                <ul class="tab-links">
                    <li class="active"><a href="#Welcome">Welcome </a></li>
                    <li><a href="#Usermanagement" class="removedecoration">User Management</a></li>
                    <li><a href="#Addproduct">Add Products</a></li>
                    <li><a href="#Devices">Add device</a></li>
                    <li><a href="#tab2">Tab #2</a></li>
                    <li><a href="#tab3">Tab #3</a></li>
                    <li><a href="#tab4">Tab #4</a></li>
                </ul>

                <div class="tab-content">
                    <div id="Welcome" class="tab active">
                        <h2>Wel come</h2><div class="row">
                        </div>
                    </div>
                    <div id="Usermanagement" class="tab">
                        <iframe  seamless="seamless" src="User_management.jsp" style="overflow: hidden" width="100%" height="1000px" frameborder="0">
                        </iframe><br />
                    </div>

                    <div id="Addproduct" class="tab">
                        <iframe  src="Add_product.jsp" style="overflow: hidden" width="100%" height="1000px" frameborder="0">
                        </iframe><br />
                    </div>
                    <div id="Devices" class="tab">
                        <iframe seamless="seamless"  src="Devices.jsp" width="99%" height="4500px" frameborder="0">
                        </iframe>
                        <br />
                    </div>  


                    <div id="tab2" class="tab">
                        <iframe  src="index.jsp" width="100%" height="1000px" frameborder="0">
                        </iframe><br />
                    </div>

                    <div id="tab3" class="tab">
                        <iframe  src="User_management.jsp" width="100%" height="1000px" frameborder="0">
                        </iframe>
                    </div>

                    <div id="tab4" class="tab">
                        <p>Tab #4 content goes here!</p>
                        <p>Donec pulvinar neque sed semper lacinia. Curabitur lacinia ullamcorper nibh; quis imperdiet velit eleifend ac. Donec blandit mauris eget aliquet lacinia! Donec pulvinar massa interdum risus ornare mollis. In hac habitasse platea dictumst. Ut euismod tempus hendrerit. Morbi ut adipiscing nisi. Etiam rutrum sodales gravida! Aliquam tellus orci, iaculis vel.</p>

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
