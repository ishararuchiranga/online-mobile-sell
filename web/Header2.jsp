
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="javax.script.ScriptEngine"%>
<%@page import="javax.script.ScriptEngineManager"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>



<div class="top_bar_1_2">

    <%
        boolean notlogged = false;
        boolean conem = false;
        boolean blocku = false;

        HttpSession seshead = request.getSession();

        if (seshead.getAttribute("Login_object") != null) {
            DB.UserRegistration ur = (DB.UserRegistration) seshead.getAttribute("Login_object");
            if (ur.getStatus().getId() == 2) {
    %>
    <!-- logged top header-->
    <div class="row">
        <div class="col-md-12 ">
            <div class="container">
                <div class="super-top">
                    <div class="col-sm-4">
                        <a href="index.jsp"><img src="images/home/Phone.PNG" alt="" /></a>
                    </div>
                    <div class="col-sm-8">
                        <div class="shop-menu pull-right">
                            <ul class="nav navbar-nav super-top-ul">

                                <li class="toolt">
                                    <a class="dropbtn" id="username" style="color: black; " href="Profile_Purchase_History.jsp">
                                        <i class="fa fa-user"></i> Welcome <%=ur.getUserName()%><span class="tooltiptext">Account</span>
                                    </a>
                                </li>
                                <li class="toolt"><a href="Profile_Account_details.jsp"><i class="fa fa-gear"></i> <div></div><span class="tooltiptext">Account settings</span></a></li>
                                <li class="toolt">
                                    <a style="cursor: pointer" onclick=" document.getElementById('sourt').submit();" >
                                        <i class="fa fa-sign-out"></i> <span class="tooltiptext">Signout</span>
                                    </a>
                                </li>
                                <li class="toolt"><a href="#"><i class="fa fa-bookmark-o"></i><span class="tooltiptext">Wishlist</span></a></li>
                                <li class="toolt"><a href="Checkout.jsp"><i class="fa fa-check-square-o"></i> <span class="tooltiptext">Checkout</span></a></li>
                                    <%
                                        if (seshead.getAttribute("mycart") != null) {

                                            Java_classes.Cart c = (Java_classes.Cart) seshead.getAttribute("mycart");
                                            List<Java_classes.NewCartItems> n = c.getArrayData();
                                            if (!(n.isEmpty())) {

                                    %>
                                <li class="toolt"><a href="Cart.jsp"><i class="fa fa-shopping-cart"></i><div class="cart-count" id="cartcount"><%=n.size()%></div> <span class="tooltiptext">cart</span></a></li>

                                <%
                                } else {
                                %>
                                <li class="toolt"><a href="Cart.jsp"><i class="fa fa-shopping-cart"></i><div class="cart-count hideitems" id="cartcount"></div><span class="tooltiptext">cart</span></a></li>

                                <%
                                    }
                                } else {
                                %>
                                <li class="toolt"><a href="Cart.jsp"><i class="fa fa-shopping-cart"></i><div class="cart-count hideitems" id="cartcount"></div><span class="tooltiptext">cart</span></a></li>

                                <%
                                    }
                                %>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <form action="Logout" class="hideitems" id="sourt" method="post">
    </form>
    <%
    } else if (ur.getStatus().getId() == 1) {
        seshead.removeAttribute("Login_object");

        conem = true;
        notlogged = true;
    %>
    <!--non log top header-->
    <div class="row">
        <div class="col-md-12 ">
            <div class="container">
                <div class="super-top">
                    <div class="col-sm-4">
                        <a href="index.jsp"><img src="images/home/Phone.PNG" alt="" /></a>
                    </div>
                    <div class="col-sm-8">
                        <div class="shop-menu pull-right">
                            <ul class="nav navbar-nav super-top-ul">
                                <li class="toolt"><a id="username" href="#opensignin"><i class="fa fa-user"></i> <div>Account</div><span class="tooltiptext">Profile</span></a>  </li>
                                <li class="toolt"><a id="Loginbtn" href="#opensignin"><i class="fa fa-sign-in"></i><span class="tooltiptext">Signin</span></a></li>
                                <li class="toolt"><a href="#"><i class="fa fa-bookmark-o"></i><span class="tooltiptext">Wishlist</span></a></li>
                                <li class="toolt"><a href="Checkout.jsp"><i class="fa fa-check-square-o"></i> <span class="tooltiptext">Checkout</span></a></li>
                                    <%
                                        if (seshead.getAttribute("mycart") != null) {

                                            Java_classes.Cart c = (Java_classes.Cart) seshead.getAttribute("mycart");
                                            List<Java_classes.NewCartItems> n = c.getArrayData();
                                            if (!(n.isEmpty())) {

                                    %>
                                <li class="toolt"><a href="Cart.jsp"><i class="fa fa-shopping-cart"></i><div class="cart-count" id="cartcount"><%=n.size()%></div> <span class="tooltiptext">cart</span></a></li>

                                <%
                                } else {
                                %>
                                <li class="toolt"><a href="Cart.jsp"><i class="fa fa-shopping-cart"></i><div class="cart-count hideitems" id="cartcount"></div><span class="tooltiptext">cart</span></a></li>

                                <%
                                    }
                                } else {
                                %>
                                <li class="toolt"><a href="Cart.jsp"><i class="fa fa-shopping-cart"></i><div class="cart-count hideitems" id="cartcount"></div><span class="tooltiptext">cart</span></a></li>

                                <%
                                    }
                                %>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <%
    } else {
        seshead.removeAttribute("Login_object");
        blocku = true;
        notlogged = true;
        Java_classes.cookie.removeCookie(response, "phonehutloginquthentication");
    %>
    <!--non log top header-->
    <div class="row">
        <div class="col-md-12 ">
            <div class="container">
                <div class="super-top">
                    <div class="col-sm-4">
                        <a href="index.jsp"><img src="images/home/Phone.PNG" alt="" /></a>
                    </div>
                    <div class="col-sm-8">
                        <div class="shop-menu pull-right">
                            <ul class="nav navbar-nav super-top-ul">
                                <li class="toolt"><a id="username" href="#opensignin"><i class="fa fa-user"></i> <div>Account</div><span class="tooltiptext">Profile</span></a>  </li>
                                <li class="toolt"><a id="Loginbtn" href="#opensignin"><i class="fa fa-sign-in"></i><span class="tooltiptext">Signin</span></a></li>
                                <li class="toolt"><a href="#"><i class="fa fa-bookmark-o"></i><span class="tooltiptext">Wishlist</span></a></li>
                                <li class="toolt"><a href="Checkout.jsp"><i class="fa fa-check-square-o"></i> <span class="tooltiptext">Checkout</span></a></li>
                                    <%
                                        if (seshead.getAttribute("mycart") != null) {

                                            Java_classes.Cart c = (Java_classes.Cart) seshead.getAttribute("mycart");
                                            List<Java_classes.NewCartItems> n = c.getArrayData();
                                            if (!(n.isEmpty())) {

                                    %>
                                <li class="toolt"><a href="Cart.jsp"><i class="fa fa-shopping-cart"></i><div class="cart-count" id="cartcount"><%=n.size()%></div> <span class="tooltiptext">cart</span></a></li>

                                <%
                                } else {
                                %>
                                <li class="toolt"><a href="Cart.jsp"><i class="fa fa-shopping-cart"></i><div class="cart-count hideitems" id="cartcount"></div><span class="tooltiptext">cart</span></a></li>

                                <%
                                    }
                                } else {
                                %>
                                <li class="toolt"><a href="Cart.jsp"><i class="fa fa-shopping-cart"></i><div class="cart-count hideitems" id="cartcount"></div><span class="tooltiptext">cart</span></a></li>

                                <%
                                    }
                                %>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <%
        }
    } else {
        String uuid = Java_classes.cookie.getCookieValue(request, "phonehutloginquthentication");
        if (uuid != null) {
            Session seshd = conn.NewHibernateUtil.getSessionFactory().openSession();
            DB.CookieTable cook = (DB.CookieTable) seshd.load(DB.CookieTable.class, uuid);
            if (cook != null) {
                String requestURL = request.getRequestURL().toString();
                String appPath2 = request.getQueryString();
                String comefrom = null;
                if (appPath2 == null) {
                    comefrom = requestURL;
                } else {
                    comefrom = requestURL + "?" + appPath2;
                }
                if (comefrom == null) {
                    comefrom = "index.jsp";
                }

                DB.UserRegistration ur = (DB.UserRegistration) seshd.load(DB.UserRegistration.class, cook.getUserRegistration().getId());
                seshead.setAttribute("Login_object", ur);
                response.sendRedirect(comefrom);
            }
        } else {
            notlogged = true;
    %>
    <!--non log top header-->
    <div class="row">
        <div class="col-md-12 ">
            <div class="container">
                <div class="super-top">
                    <div class="col-sm-4">
                        <a href="index.jsp"><img src="images/home/Phone.PNG" alt="" /></a>
                    </div>
                    <div class="col-sm-8">
                        <div class="shop-menu pull-right">
                            <ul class="nav navbar-nav super-top-ul">
                                <li class="toolt"><a id="accou" href="#opensignin"><i class="fa fa-user"></i> <div>Account</div><span class="tooltiptext">Profile</span></a>  </li>
                                <li class="toolt"><a id="Loginbtn" href="#opensignin"><i class="fa fa-sign-in"></i><span class="tooltiptext">Signin</span></a></li>
                                <li class="toolt"><a href="#"><i class="fa fa-bookmark-o"></i><span class="tooltiptext">Wishlist</span></a></li>

                                <li class="toolt"><a id="checko" href="#opensignin"><i class="fa fa-check-square-o"></i> <span class="tooltiptext">Checkout</span></a></li>
                                    <%
                                        if (seshead.getAttribute("mycart") != null) {

                                            Java_classes.Cart c = (Java_classes.Cart) seshead.getAttribute("mycart");
                                            List<Java_classes.NewCartItems> n = c.getArrayData();
                                            if (!(n.isEmpty())) {

                                    %>
                                <li class="toolt"><a href="Cart.jsp"><i class="fa fa-shopping-cart"></i><div class="cart-count" id="cartcount"><%=n.size()%></div> <span class="tooltiptext">cart</span></a></li>

                                <%
                                } else {
                                %>
                                <li class="toolt"><a href="Cart.jsp"><i class="fa fa-shopping-cart"></i><div class="cart-count hideitems" id="cartcount"></div><span class="tooltiptext">cart</span></a></li>

                                <%
                                    }
                                } else {
                                %>
                                <li class="toolt"><a href="Cart.jsp"><i class="fa fa-shopping-cart"></i><div class="cart-count hideitems" id="cartcount"></div><span class="tooltiptext">cart</span></a></li>

                                <%
                                    }
                                %>  
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <%            }
        }
    %>
    <!--bottom heder-->
    <div class="menu">
        <div class="row">
            <div class="col-md-12">

                <div id="cssmenu" class="align-center">

                    <ul class="secmenu-border" >
                        <li class='active'><a href='index.jsp'>Home</a></li>
                        <li><a href="products.jsp">products</a></li>
                        <li><a href="products.jsp?devtypeid=1">Smart phones</a></li>
                        <li><a href='products.jsp?devtypeid=2'>Tabs</a></li>
                        <li><a href='products.jsp?devtypeid=4'>Smart watches</a></li>
                        <li><a href='products.jsp?devtypeid=3'>Accessories</a></li>
                        <li><a href='#'>About us</a></li>
                        <li  style="display : inline;float : none;padding : 0;padding-bottom : 0;padding-left : 0;padding-right : 0;padding-top : 0;">
                            <a style="display : inline;float : none;padding : 0;padding-bottom : 0;padding-left : 0;padding-right : 0;padding-top : 0;"><div class="search_bar_box1" >                        
                                    <div class="form-group block">
                                        <input type="text" id="searchtxt" class="form-control" style="display: inline-block">
                                    </div>
                                </div>
                                <div class="search_bar_box1" >                        
                                    <button type="button" class="search-btn" onclick="search()" id="search-btn">
                                        <span class="glyphicon glyphicon-search"></span> Search
                                    </button>
                                </div>
                            </a>
                        </li>
                    </ul>

                </div>

            </div>
        </div>
    </div>
</div>


<%     //
    if (notlogged) {
%>
<!--Login and log out-->
<div id="opensignin" class="modalDialog">
    <div>
        <a href="#" id="closelog" title="Close" class="close">X</a><br>
        <!--log-->
        <div class="login-form" id="loginfoem"><!--login form-->
            <h2>Login to your account</h2>
            <div id="pleaseconfm" class="pleaseconfm hideitems" > 
                <label>
                    please confirm your email address
                    <a href="https://mail.google.com"><img src="images/gmail.png"/></a>
                    <a href="https://mail.yahoo.com"><img src="images/yahoo.gif"/></a> 
                </label>
            </div>
            <div id="userblocked" class="blocked-user hideitems" > 
                <label>
                    Your account was blocked by admin. Please contact admin.
                    ishara.ira@gmal.com
                </label>
            </div>
            <form id="signinform" action="Login" method="post">

                <label class="hideitems"  id="pleaselog" ><p class="typcn typcn-times wrongformfield">Please login first</p></label>
                <label id="invaliduser" class="hideitems"><p class="typcn typcn-times wrongformfield">Wrong username and/or pssword</p></label>
                <label  id="wrngemaillogin" class="hideitems"><p class="typcn typcn-times wrongformfield"> invalid email</p></label>
                <label  id="emptyemail" class="hideitems"><p class="typcn typcn-times wrongformfield">Please fill email</p></label>
                <input name="loginemailad" id="loginemail" class="form-control" type="email" placeholder="E-mail or User name" />
                <label id="wrngepwrdlogin" class="hideitems"><p class="typcn typcn-times wrongformfield"> Length must be more than 4 characters</p></label>
                <label id="fillpwrd" class="hideitems"><p class="typcn typcn-times wrongformfield"> Please fill password</p></label>
                <input name="loginfrmpwrd" id="loginpwrd" class="form-control" type="password" placeholder="Password" />
                <span>
                    <input name="keepme" type="checkbox" class="checkbox" value="true"> 
                    Keep me signed in
                </span><br>
                <button type="button" onclick="loginformvalidate()" class="btn btn-default">Login</button>

                <div class="loglastdiv">
                    <a id="opensigup" class="btn btn-link addblackcolor">Sign up</a>
                    <label class="logorlabel">Or</label>
                    <a id="frgtpwrd" class="btn btn-link addRedcolor">Forgot Password</a>
                </div>
                <div id="frgtsubmit" class="alert3 hideitems">
                    <span class="closebtn" onclick="this.parentElement.style.display = 'none';">&times;</span> 
                    <strong>Send rest link</strong> 
                    <input type="text" id="frgtmil" placeholder="E-mail">
                    <input type="button" id="frgtbtn" value="Submit">
                    <div class="pleasewait hideitems" id="pleasewaitfrgtpwrd" >
                        <i style="text-align:center" class="fa fa-repeat fa-spin"></i> Please wait..
                    </div>
                    <div class="pleasewait hideitems" id="frgtpwdone" >
                        Please Check Your  E-Mails<i style="text-align:center" class="typcn typcn-tick"></i>
                    </div>
                </div>
            </form>
        </div>
        <!--up-->
        <div id="signupformbegin" class="signup-form hideitems"><!--sign up form-->
            <h2>New User Signup!</h2>
            <form id="signupform" action="NewServlet" method="post">
                <div id="emalr"  class="alertsup hideitems">
                    <span class="closebtn" onclick="this.parentElement.style.display = 'none';">&times;</span> 
                    <strong>E-mail already registered...!</strong>
                </div>
                <div id="erroc" class="erroroc hideitems">
                    <span class="closebtn" onclick="this.parentElement.style.display = 'none';">&times;</span> 
                    <strong>Error occurred. Please try again after few seconds.!!!</strong>
                </div>
                <label id="wrnguname" class="hideitems"><p class="typcn typcn-times wrongformfield">Please Give a user name</p></label>
                <%--input name="loginuname" id="unametf" class="form-control" type="text" placeholder="User name"/--%>
                <input type="text" autocomplete="off" placeholder="user name" name="loginusername" class="form-control" id="unametf"/>

                <label id="wrngemail" class="hideitems"><p class="typcn typcn-times wrongformfield"> invalid email</p></label>
                <input name="email" autocomplete="off"  id="emailad" class="form-control" type="email" placeholder="Email Address"/>

                <label id="wrngpasslength" class="hideitems"><p class="typcn typcn-times wrongformfield"> password length is must more than 5 characters</p></label>
                <label id="passworddoesntmatch" class="hideitems"><p class="typcn typcn-times wrongformfield"> password doesn't match</p></label>
                <label id="enterpassword" class="hideitems"><p class="typcn typcn-times wrongformfield"> enter password</p></label>
                <input style="display:none" type="text" name="fakeusernameremembered"/>
                <input style="display:none" type="password" name="fakepasswordremembered"/>
                <input name="pwrd" autocomplete="off" id="pwrd1" onblur="" class="form-control" type="password" placeholder="Password"/>

                <input  id="pwrd2" onfocus="" type="password"  class="form-control" placeholder="Re type Password"/>
                <label id="pwrdmst" class="pwrdreset">(Password must have more than 5 characters)</label>
                <div><button type="button" id="subtn" onclick="signup()" class="btn btn-default">Signup</button></div>
                <p id="equal"></p>
                <div class="mailsent hideitems" id="mailsent" >
                    Confirmatin E-Mail was sent.. <i style="text-align:center" class="typcn typcn-tick"></i>
                </div>
                <div class="mailsent hideitems" id="plwmas" >
                    <i style="text-align:center" class="fa fa-repeat fa-spin"></i> Please wait..
                </div>
                <div class="loglastdiv">
                    <a id="backtosignin" class="btn btn-link addblackcolor"><i class="fa fa-arrow-left"></i> Back To Signin</a>
                </div>

            </form>
        </div>      
    </div>	
</div>

<%    }

    if (conem) {
%>
<div class="alert">
    <span class="closebtn" onclick="this.parentElement.style.display = 'none';">&times;</span> 
    <strong></strong> Please confirm your email address before login
</div>

<%
    }
    if (blocku) {
%>
<div class="alert2">
    <span class="closebtn" onclick="this.parentElement.style.display = 'none';">&times;</span> 
    <strong>Contact ishara.ira@gmail.com!!!</strong> Your account was blocked by admin.
</div>
<%
    }
    if (seshead.getAttribute("confem") != null) {

        seshead.removeAttribute("confem");
%>
<div class="emainsuc">
    <span class="closebtn" onclick="this.parentElement.style.display = 'none';">&times;</span> 
    <strong>E-Mail successfully confirmed.</strong> <a href="#opensignin">Please login</a>
</div>
<%
    }
%>

<!--compare-->
<div id="test1"   class="watermark hideitems" >
    <table id="test3" border="0" cellspacing="3" i>
        <tbody id="test2">
        </tbody>
        <tfoot>
            <tr id="combtnrow" class="hideitems">               
                <td   style="text-align: center"><button onclick="gotocompare()" class="btn btn-danger products-compare-btn"><label class="typcn typcn-export"> Compare</label></button></td>
            </tr>
        </tfoot>
    </table>
</div>
<!--compare-->
<script>
    var comp_id_list = [];

    <%//
        if (seshead.getAttribute("mycompare") != null) {
            Session sesht = conn.NewHibernateUtil.getSessionFactory().openSession();

            Java_classes.Compare com8 = (Java_classes.Compare) seshead.getAttribute("mycompare");
            List<Java_classes.NewCompareItems> n2 = com8.getArrayData();
            String id = "";
            String details = "";

            if (n2.size() != 0) {
                for (Java_classes.NewCompareItems ni : n2) {
                    id += ni.getProid() + ",";
                    Criteria c = sesht.createCriteria(DB.Products.class);
                    c.add(Restrictions.eq("id", Integer.parseInt(ni.getProid())));
                    DB.Products pr = (DB.Products) c.uniqueResult();
                    details += pr.getModel().getManufacturers().getName() + " " + pr.getModel().getModel() + ",";
                }
                details = details.substring(0, details.length() - 1);
                id = id.substring(0, id.length() - 1);
            }
    %>
    var ids = "<%=id%>";
    var details = "<%=details%>";
    var countids = 0;



    function loadcompare() {
        if (ids != "" && details != "") {
            var id = ids.split(",");
            var detai = details.split(",");
            countids = id.length;
            for (var i = 0; i < id.length; ++i) {
                $('#test2').append('<tr id="row' + id[i] + '"><td>' + detai[i] + '</td><td><a style="cursor: pointer;" onclick="removefromcompare(' + id[i] + ')">X</a></td></tr>');
                $('#test1').show(300);
                if (id.length > 1) {
                    $('#combtnrow').show(300);
                }
                comp_id_list.push(id[i]);
            }
        }
    }
    <%//          
        }
    %>
    var compidsarr;
    var countids = 0;
    function compare(b) {

        ++countids;
        var c = b.split(",");
        compidsarr = c;

        if (countids < 3) {
            if (comp_id_list.length < 1) {

                comp_id_list.push(c[0]);
                $('#test2').append('<tr id="row' + c[0] + '"><td>' + c[1] + '</td><td><a style="cursor: pointer;" onclick="removefromcompare(' + c[0] + ')">X</a></td></tr>');
                $('#test1').show(300);
                var cou = document.getElementById('test2').rows.length;
                if (cou > 1) {
                    $('#combtnrow').show(300);
                }
                $.post(
                        "Add_to_compare",
                        {id: c[0]}, //meaasge you want to send
                function (result) {
                });
            } else {
                --countids;
                forloop:for (var i = 0; i < comp_id_list.length; ++i) {
                    if (comp_id_list[i] == c[0]) {
                        alert("already added");
                        break forloop;
                    } else {
                        if (comp_id_list.length == i + 1) {
                            comp_id_list.push(c[0]);
                            $('#test2').append('<tr id="row' + c[0] + '"><td>' + c[1] + '</td><td><a style="cursor: pointer;" onclick="removefromcompare(' + c[0] + ')">X</a></td></tr>');
                            $('#test1').show(300);
                            var cou = document.getElementById('test2').rows.length;
                            if (cou > 1) {
                                $('#combtnrow').show(300);
                            }
                            $.post(
                                    "Add_to_compare",
                                    {id: c[0]}, //meaasge you want to send
                            function (result) {
                            });
                            break forloop;
                        }
                    }
                }
            }
        } else {
            alert("Can't compare more than 2 devices");
            --countids;
        }
    }
    function removefromcompare(b) {
        --countids;
        for (var i = 0; i < comp_id_list.length; ++i) {
            if (comp_id_list[i] == b) {
                comp_id_list.splice(i, 1);
            }
        }
        $('#row' + b + '').remove();
        var cou = document.getElementById('test2').rows.length;
        if (cou == "") {
            $('#test1').hide(300);
        }
        if (cou == "1") {
            $('#combtnrow').hide();
        }


        $.post(
                "Remove_from_compare",
                {id: b}, //meaasge you want to send
        function (result) {
        });

    }
    function gotocompare() {
        window.location.replace("Compare.jsp");
    }
</script>
<%
    Integer hitsCount
            = (Integer) application.getAttribute("hitCounter");
    if (hitsCount == null || hitsCount == 0) {
        hitsCount = 1;
    } else {
        hitsCount += 1;
    }
    application.setAttribute("hitCounter", hitsCount);
%>