<%-- 
Document   : products
Created on : Dec 15, 2015, 11:33:33 PM
Author     : Ishara
--%>
<script>
    window.onpageshow = function (evt) {
        // If persisted then it is in the page cache, force a reload of the page.
        if (evt.persisted) {
            document.body.style.display = "none";
            location.reload();
        }
    };

    var url = window.location.href;
    var urlparts = url.split('#');
    var b;
    if (urlparts[1] == b) {
    } else {
        console.log("history back");
        history.back();
    }
</script>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.mysql.jdbc.exceptions.MySQLSyntaxErrorException"%>
<%@page import="org.hibernate.criterion.Disjunction"%>
<%@page import="org.hibernate.criterion.Conjunction"%>
<%@page import="org.hibernate.criterion.Junction"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.criterion.Projections"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache">

        <script>
//            var url = window.location.href;
//            var urlparts = url.split('#');
//            var b;
//            if (urlparts[1] == b) {
//            } else {
//                window.location.href = urlparts[0];
//            }
        </script>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/typicons.min.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">

        <link href="css/main.css" rel="stylesheet">
        <link href="css/price-range.css" rel="stylesheet">

        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/script.js"></script>
        <link href="css/navibar.css" rel="stylesheet">
    </head>
    <body onload="ch(), loadcompare()">



        <%@include file="Header2.jsp" %>
        <style>
            body{
                background-image: url("images/geometry2.png");
            }
            .bodycont{
                background-color: white;
                box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.16), 0px 2px 10px 0px rgba(0, 0, 0, 0.12) !important;
            }
        </style>
        <div class="container bodycont" >
            <div class="row">
                <div class="Productpage">
                    <div class="col-sm-3 side-bar-effects">
                        <div class="left-sidebar">

                            <div class="col-sm-12 refinebox">
                                <h3 class="product-refine-header">Refine <i class="fa fa-cogs"></i></h3>
                            </div>

                            <div class="col-sm-12 refinebox" >
                                <button type="button" class="Brand btn btn-success refines" data-toggle="collapse" data-target="#Brand_items">
                                    <span class="typcn typcn-minus"></span> Brand
                                </button>
                                <div id="Brand_items" class="collapse in">
                                    <%//
                                        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
                                        Criteria cbrand = ses.createCriteria(DB.Manufacturers.class);
                                        cbrand.addOrder(Order.asc("id"));

                                        List<DB.Manufacturers> manulist = cbrand.list();
                                        for (DB.Manufacturers man : manulist) {
                                    %>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="<%=man.getId()%>" name="brand"><%=man.getName()%></label>
                                    </div>
                                    <%
                                        }

                                    %>
                                </div>
                            </div>

                            <div class="col-sm-12 refinebox" >
                                <button type="button" class="catogory btn btn-success refines" data-toggle="collapse" data-target="#Catogory_items">
                                    <span class="typcn typcn-minus"></span> Catogory
                                </button>
                                <div id="Catogory_items" class="collapse in">
                                    <%//
                                        Criteria ccat = ses.createCriteria(DB.DeviceType.class);
                                        cbrand.addOrder(Order.asc("id"));

                                        List<DB.DeviceType> cate = ccat.list();
                                        for (DB.DeviceType cat : cate) {
                                    %>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="<%=cat.getId()%>" name="cate"><%=cat.getType()%></label>
                                    </div>
                                    <%
                                        }
                                    %>

                                </div>
                            </div>

                            <div class="col-sm-12 refinebox" >
                                <button type="button" class="productprice btn btn-success refines" data-toggle="collapse" data-target="#productprice_items">
                                    <span class="typcn typcn-minus"></span> Price range
                                </button>
                                <div id="productprice_items" class="collapse in">
                                    <div class="checkbox">
                                        <label><input type="text" id="stprice" placeholder="Min Price" value="" class="form-control price-range-tf"></label>

                                    </div>
                                    <div class="checkbox">
                                        <label><input type="text" id="endprice" placeholder="Max value" value="" class="form-control price-range-tf"></label>
                                    </div>

                                </div>
                            </div>

                            <div class="col-sm-12" >
                                <button onclick="applyrefine()" class="refine-btn">
                                    <span><i class="fa fa-refresh"></i> Refine</span>
                                </button>
                            </div>

                            <script>

                                function applyrefine() {
                                    var url = "products.jsp";
                                    var urladditionbrand = "";
                                    var urladditioncat = "";
                                    var b = false;
                                    var c = false;
                                    var p = false;
                                    var k = false;
//brand
                                    var brand = [];
                                    $.each($("input[name='brand']:checked"), function () {
                                        brand.push($(this).val());
                                    });
                                    if (brand[0] != null) {

                                        b = true;
                                        for (var i = 0; i < brand.length; ++i) {
                                            urladditionbrand += brand[i] + ",";
                                        }
                                        urladditionbrand = urladditionbrand.slice(0, -1);
                                        if (url == "products.jsp") {
                                            url += "?" + "brandid=" + urladditionbrand;
                                        }
                                    }

//catogory
                                    var catogory = [];
                                    $.each($("input[name='cate']:checked"), function () {
                                        catogory.push($(this).val());
                                    });
                                    if (catogory[0] != null) {
                                        c = true;
                                        for (var i = 0; i < catogory.length; ++i) {
                                            urladditioncat += catogory[i] + ",";
                                        }
                                        urladditioncat = urladditioncat.slice(0, -1);
                                        if (url == "products.jsp") {
                                            url += "?" + "devtypeid=" + urladditioncat;
                                        } else {
                                            url += "&" + "devtypeid=" + urladditioncat;
                                        }
                                    }
//price
                                    var pricestart = document.getElementById("stprice").value;
                                    var priceend = document.getElementById("endprice").value;
                                    if (pricestart != "" && priceend != "") {
                                        p = true;
                                        if (url == "products.jsp") {
                                            url += "?" + "pricerange=" + pricestart + "," + priceend;
                                        } else {
                                            url += "&" + "pricerange=" + +pricestart + "," + priceend
                                        }
                                    }

//keyword

                                    var keyword = document.getElementById("searchtxt").value;
                                    if (keyword != "") {
                                        if (url == "products.jsp") {
                                            url += "?" + "searchtxt=" + keyword;
                                        } else {
                                            url += "&" + "searchtxt=" + keyword;
                                        }
                                    }
                                    window.location.replace(url);

                                }
                                var b1;
                                var d1;
                                var p1;
                                var s1;
                                function access() {
                                <% String str = request.getParameter("brandid");
                                    String dev = request.getParameter("devtypeid");
                                    String pr = request.getParameter("pricerange");
                                    String pri = "not";
                                    if (pr != null) {
                                        pri = pr;
                                    }
                                    String se = request.getParameter("searchtxt");
                                    String ser = "not";
                                    if (se != null) {
                                        ser = se;
                                    }
                                %>
                                    b1 = "<%=str%>";
                                    d1 = "<%=dev%>";
                                    p1 = "<%=pri%>";
                                    s1 = "<%=ser%>";
                                }

                                function ch() {
                                    access();
//brand
                                    var br = b1;
                                    var brArray = br.split(',');
                                    brArray.forEach(function (item) {

                                        $('input[type="checkbox"][name=brand][value="' + item + '"]').attr('checked', true);
                                    });
//dev type
                                    var dev = d1;
                                    var devArray = dev.split(',');
                                    devArray.forEach(function (item) {

                                        $('input[type="checkbox"][name=cate][value="' + item + '"]').attr('checked', true);
                                    });
//price range


                                    var pri = p1;



                                    if (pri != "not") {
                                        var prarray = pri.split(',');
                                        $('#stprice').val(prarray[0]);
                                        $('#endprice').val(prarray[1]);
                                    } else {

                                    }


//Keyword   
                                    var keyw = s1;
                                    if (keyw != "not") {
                                        if (keyw != 0) {
                                            //alert("awa");
                                            $("#searchtxt").val(keyw);
                                        }
                                    }
                                }

                            </script>

                        </div>
                    </div>
                </div>

                <div class="col-sm-9">
                    <div class="features_items"><!--features_items-->
                        <h2 class="title text-center">
                            <%
                                if (request.getParameter("devtypeid") != null) {
                                    String protypehead = request.getParameter("devtypeid");
                                    if (protypehead.equals("1")) {
                                        out.print("Smart phone");
                                    } else if (protypehead.equals("2")) {
                                        out.print("Tabs");
                                    } else if (protypehead.equals("3")) {
                                        out.print("Accesories");
                                    } else if (protypehead.equals("4")) {
                                        out.print("Smart watches");
                                    } else {
                                        out.print("Products");
                                    }
                                } else {
                                    out.print("Products");
                                }

                            %>

                        </h2>
                        <div class="col-sm-12">

                            <% //                                
                                int resultperpage = 10;
                                int startcount = 0;
                                String pageno;
                                int totalresultscou = 0;
                                boolean flagempty = true;
                                boolean notempty = true;

                                if (request.getParameter("pageIndex") != null) {
                                    pageno = request.getParameter("pageIndex");
                                } else {
                                    pageno = "1";
                                }
                                startcount = resultperpage * (Integer.parseInt(pageno) - 1);

                                Criteria criteria = ses.createCriteria(DB.Products.class);
                                criteria.addOrder(Order.desc("id"));
                                DB.ProStatus prost = (DB.ProStatus) ses.load(DB.ProStatus.class, Integer.parseInt("1"));
                                criteria.add(Restrictions.eq("proStatus", prost));

//                                
                                //keywordd
                                String keyword = request.getParameter("searchtxt");
                                if (keyword != null && !(keyword.equals("0"))) {
                                    Criteria criteriatest = ses.createCriteria(DB.Model.class);
                                    criteriatest.add(Restrictions.like("keywords", "%" + keyword + "%"));
                                    List<DB.Model> modlist = criteriatest.list();

                                    if (!(modlist.isEmpty())) {
                                        criteria.add(Restrictions.in("model", modlist));
                                    } else {
                                        flagempty = false;
                                    }
                                } else {
                                    keyword = "0";
                                }

                                //devtype
                                String getdevtype = request.getParameter("devtypeid");
                                if (getdevtype != null && !(getdevtype.equals("0"))) {
                                    String devtype[] = getdevtype.split(",");
                                    Criteria criteriatest = ses.createCriteria(DB.Model.class);

                                    Disjunction dis = Restrictions.disjunction();
                                    for (int i = 0; i < devtype.length; i++) {
                                        DB.DeviceType de = (DB.DeviceType) ses.load(DB.DeviceType.class, Integer.parseInt(devtype[i]));
                                        dis.add(Restrictions.eq("deviceType", de));
                                    }
                                    criteriatest.add(dis);
                                    List<DB.Model> modlist = criteriatest.list();

                                    if (!(modlist.isEmpty())) {
                                        criteria.add(Restrictions.in("model", modlist));
                                    } else {
                                        flagempty = false;
                                    }
                                } else {
                                    getdevtype = "0";
                                }
                                //brand
                                String getBrand = request.getParameter("brandid");
                                if (getBrand != null && !(getBrand.equals("0"))) {

                                    String brand[] = getBrand.split(",");
                                    Criteria criteriatest2 = ses.createCriteria(DB.Model.class);
                                    Disjunction dis = Restrictions.disjunction();
                                    for (int i = 0; i < brand.length; i++) {
                                        DB.Manufacturers ma = (DB.Manufacturers) ses.load(DB.Manufacturers.class, Integer.parseInt(brand[i]));
                                        dis.add(Restrictions.eq("manufacturers", ma));
                                    }
                                    criteriatest2.add(dis);
                                    List<DB.Model> manlist = criteriatest2.list();

                                    if (!(manlist.isEmpty())) {
                                        criteria.add(Restrictions.in("model", manlist));
                                    } else {
                                        flagempty = false;
                                    }

                                } else {
                                    getBrand = "0";
                                }
                                //price range
                                String prange = request.getParameter("pricerange");
                                if (prange != null && !(prange.equals("0"))) {
                                    String prices[] = prange.split(",");
                                    out.print(prices[0]);
                                    out.print("-");
                                    out.print(prices[1]);
//                                    criteria.add(Restrictions.between("price", prices[0] , prices[1] ));
                                    criteria.add(Restrictions.ge("price", Integer.parseInt(prices[0])));
                                    criteria.add(Restrictions.le("price", Integer.parseInt(prices[1])));
                                } else {
                                    prange = "0";
                                }

                                List<DB.Products> totalresultlist = criteria.list();
                                totalresultscou = totalresultlist.size();

                                criteria.setFirstResult(startcount);
                                criteria.setMaxResults(resultperpage);

                                List<DB.Products> listpr = criteria.list();

                                int cos = 0;

                                if (flagempty) {
                                    if (!(listpr.isEmpty())) {

                                        for (DB.Products ssr : listpr) {

                                            ++cos;
                                            String mod = ssr.getModel().getModel();
                            %>
                            <div class="product-image-wrapper" >
                                <div class="col-sm-4">
                                    <img src="<%=ssr.getModel().getImgSrc()%>" alt="" style="width: 200px;">
                                </div>
                                <div class="col-sm-8" >
                                    <div class="single-products">
                                        <div class=" text-center">
                                            <ul class="nav nav-pills nav-justified">
                                                <li class="products-heading"><%=ssr.getModel().getManufacturers().getName()%>   <%=mod%></li>
                                                <li class="products-price">Rs.<%
                                                    DecimalFormat df = new DecimalFormat("0.00");
                                                    out.print(df.format(ssr.getPrice()));
                                                    %></li>
                                            </ul>
                                            <ul class="nav nav-pills nav-justified">
                                                <li class="products-date">Last Updated: <%=ssr.getAddedDate()%></li>
                                                    <%
                                                        if (ssr.getModel().getDeviceType().getId() == 1) {
                                                            //smartphone
%>
                                                <li  class="products-type typcn typcn-device-phone">  <%=ssr.getModel().getDeviceType().getType()%></li>
                                                    <%
                                                    } else if (ssr.getModel().getDeviceType().getId() == 2) {
                                                        //tabs
%>
                                                <li  class="products-type typcn typcn-device-tablet">  <%=ssr.getModel().getDeviceType().getType()%></li>
                                                    <%
                                                    } else if (ssr.getModel().getDeviceType().getId() == 3) {
                                                        //access
%>
                                                <li  class="products-type typcn typcn-headphones">  <%=ssr.getModel().getDeviceType().getType()%></li>
                                                    <%
                                                    } else {
                                                        //smartwatch
%>
                                                <li  class="products-type typcn typcn-time">  <%=ssr.getModel().getDeviceType().getType()%></li>
                                                    <%
                                                        }
                                                    %>
                                            </ul >
                                            <ul class="nav nav-pills nav-justified">
                                                <!--Rating-->
                                                <%
                                                    if (ssr.getQty().equals("0")) {
                                                        out.print("<li class='product-status-outofstock'>Out of stock</li>");

                                                    } else {
                                                        out.print("<li class='product-status-available'>Available</li>");
                                                    }

                                                    Criteria crate = ses.createCriteria(DB.Rating.class);
                                                    crate.add(Restrictions.eq("products", ssr));
                                                    List<DB.Rating> ratelist = crate.list();

                                                    if (!(ratelist.isEmpty())) {
                                                %>
                                                <li class='product-status-available' >
                                                    <div class="product-rate-show">
                                                        <%
                                                            double starcount = 0;
                                                            int totstars = 0;
                                                            for (DB.Rating rat : ratelist) {
                                                                starcount += Double.parseDouble(rat.getStars());
                                                            }
                                                            int totfives = 5 * ratelist.size();
                                                            double stars = (starcount / totfives) * 5;

                                                            double half = (10 * stars - 10 * (int) stars) / 10;

                                                            for (int i = 1; i <= (int) stars; ++i) {
                                                                ++totstars;
                                                        %>
                                                        <i style="padding: 0;color: rgb(100, 169, 228)" class="fa fa-star"></i>
                                                        <%
                                                            }
                                                            if (half <= 0.7 && half >= 0.3) {
                                                                ++totstars;
                                                        %>
                                                        <i style="padding: 0;color: rgb(100, 169, 228)" class="fa fa-star-half-empty"></i>

                                                        <%
                                                        } else if (half > 0.7) {
                                                            ++totstars;
                                                        %>
                                                        <i style="padding: 0;color: rgb(100, 169, 228)" class="fa fa-star"></i>
                                                        <%
                                                            }
                                                            for (int i = 1; i <= (5 - totstars); ++i) {
                                                        %>
                                                        <i style="padding: 0;color: rgb(100, 169, 228)" class="fa fa-star-o"></i>

                                                        <%
                                                            }

                                                        %>


                                                    </div>
                                                </li>
                                                <%                                                } else {
                                                %>
                                                <li class='product-status-available' >
                                                    <a style="text-align: right;padding: 0px">
                                                        Not Rated

                                                    </a>
                                                </li>
                                                <%
                                                    }

                                                %>
                                                <!--Rating end-->
                                            </ul>
                                            <p></p>
                                            <p class="products-details">
                                                <%                                                    if (ssr.getModel().getDeviceType().getId() == 3) {
                                                %>
                                                <%=ssr.getModel().getAccesoriesAndParts().getName()%>
                                                <% } else {
                                                %>
                                                <%=ssr.getModel().getSpecification().getPlatformsUse().getVersionUse().getOsList().getOses()%>-  <%=ssr.getModel().getSpecification().getPlatformsUse().getVersionUse().getVersionsUse()%>,
                                                <%=ssr.getModel().getSpecification().getCamera().getPixels()%>px camera,
                                                <%=ssr.getModel().getSpecification().getBody().getSim().getSimType()%>-
                                                <%=ssr.getModel().getSpecification().getBody().getSimSize().getSize()%>,
                                                <%=ssr.getModel().getSpecification().getDisplay().getSize()%>inches screen size,
                                                <%=ssr.getModel().getSpecification().getMemory().getInternel()%> internal memory,
                                                <%=ssr.getModel().getSpecification().getBattery().getSize()%> internal memory.......<a href="Product_details.jsp?id=<%=ssr.getId()%>">More</a>
                                                <%
                                                    }
                                                %>
                                            </p>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">
                                            <%
                                                if (ssr.getModel().getDeviceType().getId() == 1 || ssr.getModel().getDeviceType().getId() == 2 || ssr.getModel().getDeviceType().getId() == 4) {
                                            %>
                                            <li>
                                                <button onclick="compare('<%=ssr.getId()%>,<%=ssr.getModel().getManufacturers().getName()%> <%=mod%>')" class="product-uti-btn products-compare-btn" ><label class="typcn typcn-export"> Compare</label></button>
                                                <button type="button" class="product-uti-btn products-wishlist-btn"><label class="typcn typcn-bookmark"> Wish list</label></button>
                                            </li>
                                            <%
                                                }
                                            %>
                                            <li>
                                                <a href="Product_details.jsp?id=<%=ssr.getId()%>" style="text-align: right;">
                                                    <button class="view-ad-btn">
                                                        <span class="fa fa-clipboard"><label>&nbsp;&nbsp; View Ad</label></span>
                                                    </button>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <%
                                    System.gc();
                                }
                            } else {
                                notempty = false;
                            %>
                            <div class="nothingfound">
                                <h3>Nothing Found....!</h3>
                            </div>
                            <%                                }
                            } else {
                                notempty = false;
                            %>
                            <div class="nothingfound">
                                <h3>Nothing Found....!</h3>
                            </div>
                        </div>
                        <%
                            }
                            if (notempty) {
                                int pagescount = totalresultscou / resultperpage;
                                if (totalresultscou > (pagescount * resultperpage)) {
                                    pagescount = pagescount + 1;
                                }
                        %>
                        <%                                ses.close();
                        %>
                        <div class="pagination-product-update">
                            <ul class="pagination">
                                <li><a id="pagination-pagecount">Page <%=pageno%> of <%=pagescount%></a></li>
                                    <%
                                        if (Integer.parseInt(pageno) != 1) {
                                            String url = "products.jsp?pageIndex=" + 1 + "&searchtxt=" + keyword + "&brandid=" + getBrand + "&devtypeid=" + getdevtype + "&pricerange=" + prange + "";
                                            url += "&pageno=1";
                                    %>
                                <li><a href="<%=url%>"><<</a></li>
                                    <%
                                    } else {
                                    %>
                                <li><a id="disabled-pagination"><<</a></li>
                                    <%
                                        }

                                        if (Integer.parseInt(pageno) != 1) {
                                            String url = "products.jsp?pageIndex=" + (Integer.parseInt(pageno) - 1) + "&searchtxt=" + keyword + "&brandid=" + getBrand + "&devtypeid=" + getdevtype + "&pricerange=" + prange + "";
                                            url += "&pageno=" + (Integer.parseInt(pageno) - 1);
                                    %>
                                <li><a href="<%=url%>">Prev</a></li>
                                    <%

                                    } else {
                                    %>
                                <li><a id="disabled-pagination">Prev</a></li>

                                <%
                                    }

                                    if (Integer.parseInt(pageno) < 7) {

                                        int maxpagination = 11;
                                        if (pagescount < maxpagination) {
                                            maxpagination = pagescount;
                                        }

                                        for (int c = 1; c <= maxpagination; ++c) {
                                            String url = "products.jsp?pageIndex=" + c + "&searchtxt=" + keyword + "&brandid=" + getBrand + "&devtypeid=" + getdevtype + "&pricerange=" + prange + "";

                                            if (c == Integer.parseInt(pageno)) {

                                %>
                                <li><a id="active-page"><%=c%></a></li>
                                    <%
                                    } else {
                                    %>
                                <li><a href="<%=url%>"><%=c%></a></li>
                                    <%        }
                                        }
                                    } else {
                                        int curpage = Integer.parseInt(pageno);
                                        int start = curpage - 5;
                                        int endpage = curpage + 5;
                                        if (endpage > pagescount) {

                                            int overcou = endpage - pagescount;
                                            start = start - overcou;
                                            if (start < 1) {
                                                start = 1;
                                            }

                                            endpage = pagescount;
                                        }
                                        for (int c = start; c <= endpage; ++c) {
                                            String url = "products.jsp?pageIndex=" + c + "&searchtxt=" + keyword + "&brandid=" + getBrand + "&devtypeid=" + getdevtype + "&pricerange=" + prange + "";

                                            if (c == Integer.parseInt(pageno)) {

                                    %>
                                <li><a id="active-page"><%=c%></a></li>
                                    <%
                                    } else {
                                    %>
                                <li><a href="<%=url%>"><%=c%></a></li>
                                    <%        }
                                            }
                                        }
                                        if (pagescount == Integer.parseInt(pageno)) {
                                    %>
                                <li><a id="disabled-pagination">Next</a></li>
                                    <%
                                    } else {
                                        String url = "products.jsp?pageIndex=" + (Integer.parseInt(pageno) + 1) + "&searchtxt=" + keyword + "&brandid=" + getBrand + "&devtypeid=" + getdevtype + "&pricerange=" + prange + "";


                                    %>
                                <li><a href="<%=url%>">Next</a></li>
                                    <%                    }

                                        if (pagescount != Integer.parseInt(pageno)) {
                                            String url = "products.jsp?pageIndex=" + pagescount + "&searchtxt=" + keyword + "&brandid=" + getBrand + "&devtypeid=" + getdevtype + "&pricerange=" + prange + "";

                                    %>
                                <li><a href="<%=url%>">>></a></li>
                                    <%                    } else {

                                    %>
                                <li><a id="disabled-pagination">>></a></li>    
                                    <%                    }
                                    %>
                            </ul>
                        </div>
                        <%
                            }
                        %>

                        <%--ul class="pagination">
                            <li class="active"><a href="">1</a></li>
                            <li><a href="">2</a></li>
                            <li><a href="">3</a></li>
                            <li><a href="">»</a></li>
                        </ul--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%@include file="Footer.jsp" %>
</body>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/price-range.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/main.js"></script>
</html>
