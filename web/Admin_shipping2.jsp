<%-- 
    Document   : Admin_shipping2
    Created on : Jun 13, 2016, 7:55:46 PM
    Author     : isharar
--%>

<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css" />
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/admin.css" rel="stylesheet">
        <script src="js/jquery-2.1.4.min.js"></script>
        <!--<script type="text/javascript" src="js/calendar.js"></script>-->
        <script src="js/jquery.ajaxfileupload.js"></script>
    </head>
    <body>
        <style>
            body{
                background-color: rgb(248, 248, 248);

            }
            td{
                text-align: center;
            }
            .xtable1{
                width: 35px;
            }
            table{
                width: 97%;
            }
            .view-ad-btn{
                color: black;
                text-decoration: none;
                font-weight: bold;
                padding: 9px;
                cursor: pointer;
                text-shadow: none;
            }
            .view-ad-btn:hover{
                color: black;
                text-decoration: none;
            }
            #opendeldetail{
                color: black;
            }
            th{
                text-align: center;
            }
        </style>
        <%
            int id = Integer.parseInt(request.getParameter("type"));

            int resultperpage = 25;
            int startcount = 0;
            String pageno;
            int totalresultscou = 0;

            boolean flagnothingfound = true;

            System.out.print("pagenois- " + request.getParameter("pageno"));

            if (request.getParameter("pageno") != null) {
                pageno = request.getParameter("pageno");
            } else {
                pageno = "1";
            }

            startcount = resultperpage * (Integer.parseInt(pageno) - 1);

            Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
            Criteria crship = ses.createCriteria(DB.Invoice.class);

            DB.DeleverStatus dels = (DB.DeleverStatus) ses.load(DB.DeleverStatus.class, id);

            crship.add(Restrictions.eq("deleverStatus", dels));
            crship.addOrder(Order.asc("id"));

            List<DB.Invoice> totalresultlist = crship.list();
            totalresultscou = totalresultlist.size();

            crship.setFirstResult(startcount);
            crship.setMaxResults(resultperpage);

            List<DB.Invoice> liin = crship.list();

            if (liin.isEmpty()) {
                flagnothingfound = false;
            }

            if (!(liin.isEmpty())) {


        %>
        <table cellspacing='0' id="Tableid"> 
            <tr id="foc">
                <th></th>
                <th>Id</th>
                <th>Item count</th>
                <th>Products</th>
                <th>Order date</th>
                <th></th>
            </tr> 
            <%                      //
                int cou = 1;

                for (DB.Invoice in : liin) {

                    Criteria invcr = ses.createCriteria(DB.InvoiceRegistry.class);
                    invcr.add(Restrictions.eq("invoice", in));
                    List<DB.InvoiceRegistry> listinvr = invcr.list();

                    int itemcou = 0;
                    for (DB.InvoiceRegistry ir : listinvr) {

                        itemcou += Integer.parseInt(ir.getQty());
                    }
            %>
            <tr class="xtable">
                <th><%=cou%></th>
                <td class="xtable" ><%=in.getId()%></td>
                <td class="xtable" ><%=itemcou%></td>
                <td class="xtable" ><%=listinvr.size()%></td>
                <td class="xtable" ><%=in.getDate()%></td>
                <td class="xtable" ><a href="#deldetails" onclick="loadIframe(<%=in.getId()%>)" id="opendeldetail" class="view-ad-btn" >Proceed To Deliver <img src="index.png"></a></td>
            </tr>
            <%                   ++cou;
                }
            %>
        </table>
        <%
            }
        %>
        <div id="deldetails" class="delDialog">
            <div id="forgotpwrdinside">
                <a href="#close" title="Close" onclick="reloadupdatepr2()"  class="close">X</a>
                <div id="forgotpwrdfrm" class="delovery-form">
                    <iframe class="pro-edit-iframe" id="prodetail-if" scrolling="no" border="0" src="" frameborder="no" height="100%" width="100%">Your browser not support this feature</iframe>
                </div>	
            </div>
        </div>
        <%
            if (flagnothingfound) {
                int pagescount = totalresultscou / resultperpage;
                if (totalresultscou > (pagescount * resultperpage)) {
                    pagescount = pagescount + 1;
                }
        %>

        <div class="pagination-product-update">
            <ul class="pagination">
                <li><a id="pagination-pagecount">Page <%=pageno%> of <%=pagescount%></a></li>

                <%

                    if (Integer.parseInt(pageno) != 1) {

                        String url = "Admin_shipping2.jsp?pageno=1";

                %>
                <li><a href="<%=url%>"><<</a></li>
                    <%
                    } else {

                    %>
                <li><a id="disabled-pagination"><<</a></li>
                    <%                        }

                        if (Integer.parseInt(pageno) != 1) {
                            String url = "Admin_shipping2.jsp?";
                            url += "pageno=" + (Integer.parseInt(pageno) - 1);
                    %>
                <li><a href="<%=url%>">Prev</a></li>
                    <%

                    } else {

                    %>
                <li><a id="disabled-pagination">Prev</a></li>

                <%                    }
                    //
                    if (Integer.parseInt(pageno) < 7) {

                        int maxpagination = 11;
                        if (pagescount < maxpagination) {
                            maxpagination = pagescount;
                        }
                        for (int a = 1; a <= maxpagination; ++a) {
                            System.out.println(pageno + "as");
                            String url = "Admin_shipping2.jsp?";
                            url += "type=" + id;
                            url += "&pageno=" + a;

                            if (a == Integer.parseInt(pageno)) {
                %>
                <li><a id="active-page"><%=a%></a></li>
                    <%

                    } else {
                    %>
                <li><a href="<%=url%>"><%=a%></a></li>
                    <%
                            }

                        }
                    } else {

                        int curpage = Integer.parseInt(pageno);
                        int start = curpage - 5;
                        int endpage = curpage + 5;
                        if (endpage > pagescount) {

                            int overcou = endpage - pagescount;
                            start = start - overcou;
                            if (start < 1) {
                                start = 1;
                            }

                            endpage = pagescount;

                        }
                        for (int a = start; a <= endpage; ++a) {
                            String url = "Admin_shipping2.jsp?";

                            url += "pageno=" + a;

                            if (a == Integer.parseInt(pageno)) {
                    %>
                <li><a id="active-page"><%=a%></a></li>
                    <%

                    } else {
                    %>
                <li><a href="<%=url%>"><%=a%></a></li>
                    <%
                                }

                            }

                        }
                        if (pagescount == Integer.parseInt(pageno)) {
                    %>
                <li><a id="disabled-pagination">Next</a></li>
                    <%
                    } else {
                        String url = "Admin_shipping2.jsp?";
                        url += "pageno=" + (Integer.parseInt(pageno) + 1);

                    %>
                <li><a href="<%=url%>">Next</a></li>
                    <%                    }

                        if (pagescount != Integer.parseInt(pageno)) {
                            String url = "Admin_shipping2.jsp?";
                            url += "pageno=" + pagescount;
                    %>
                <li><a href="<%=url%>">>></a></li>
                    <%                    } else {

                    %>
                <li><a id="disabled-pagination">>></a></li>    
                    <%                    }
                    %>
            </ul>
        </div>
        <%
        } else {

        %>
        <div class="nothingfound">
            <h3>Nothing Found....!</h3>
        </div>
        <%            }
        %>
        <script>
            function loadIframe(id) {
                window.parent.parent.scrollTo(0, 0);
                var $iframe = $('#prodetail-if');
                $iframe.attr('src', "Admin_shipping3.jsp?id=" + id + "");
            }

            var url = window.location.href;
            function reloadupdatepr2() {
                window.location.replace(url);
            }
        </script>

    </body>
</html>
