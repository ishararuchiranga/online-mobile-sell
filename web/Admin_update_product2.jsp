<%-- 
    Document   : Admin_update_product2
    Created on : May 12, 2016, 6:48:42 PM
    Author     : Ishara
--%>

<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/main.js"></script>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/admin.css" rel="stylesheet">
        <style>
            body{
                background-color: rgb(248, 248, 248);
            }
        </style>
    </head>
    <body>


        <%
//            String keyw = request.getParameter("keyword");
            String keyw = request.getParameter("keyword");
            String type = request.getParameter("type");
            String stock = request.getParameter("stock");
            String status = request.getParameter("status");

            boolean flagkeyword = true;
            boolean flagmodlist = true;
            boolean flagnothingfound = true;

            int resultperpage = 20;
            int startcount = 0;
            String pageno;
            int totalresultscou = 0;

            if (request.getParameter("pageno") != null) {
                pageno = request.getParameter("pageno");
            } else {
                pageno = "1";
            }

            startcount = resultperpage * (Integer.parseInt(pageno) - 1);

            if (keyw != null) {
                Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
                Criteria criadup2 = ses.createCriteria(DB.Products.class);

                //keyword
                if (!(keyw.equals("0x"))) {
                    boolean isnum = true;
                    try {
                        int testnum = Integer.parseInt(keyw);
                    } catch (NumberFormatException e) {
                        isnum = false;
                    }

                    if (isnum) {
                        criadup2.add(Restrictions.eq("id", Integer.parseInt(keyw)));
                    } else {
                        Criteria criadup2_2 = ses.createCriteria(DB.Model.class);
                        criadup2_2.add(Restrictions.like("keywords", "%" + keyw + "%"));
                        List<DB.Model> modlist = criadup2_2.list();

                        if (!(modlist.isEmpty())) {
                            criadup2.add(Restrictions.in("model", modlist));
                        } else {
                            flagkeyword = false;
                        }
                    }
                }
                //type
                if (!(type.equals("0"))) {
                    DB.DeviceType dtype = (DB.DeviceType) ses.load(DB.DeviceType.class, Integer.parseInt(type));

                    Criteria criadup2_3 = ses.createCriteria(DB.Model.class);
                    criadup2_3.add(Restrictions.eq("deviceType", dtype));
                    List<DB.Model> modlist = criadup2_3.list();

                    if (!(modlist.isEmpty())) {
                        criadup2.add(Restrictions.in("model", modlist));
                    } else {
                        flagmodlist = false;
                    }

                }
                //stock
                if (!(stock.equals("0"))) {
                    if (stock.equals("1")) {
                        criadup2.add(Restrictions.ne("qty", "0"));
                    } else {
                        criadup2.add(Restrictions.eq("qty", "0"));
                    }
                }
                //status
                if (!(status.equals("0"))) {
                    DB.ProStatus prs = null;
                    if (status.equals("1")) {
                        prs = (DB.ProStatus) ses.load(DB.ProStatus.class, Integer.parseInt(status));
                    } else {
                        prs = (DB.ProStatus) ses.load(DB.ProStatus.class, Integer.parseInt(status));
                    }
                    criadup2.add(Restrictions.eq("proStatus", prs));
                }

                List<DB.Products> totalresultlist = criadup2.list();
                totalresultscou = totalresultlist.size();

                criadup2.setFirstResult(startcount);
                criadup2.setMaxResults(resultperpage);

                List<DB.Products> prolist = criadup2.list();

                if (!(totalresultlist.isEmpty()) && flagkeyword && flagmodlist) {

                    if (!(prolist.isEmpty())) {


        %>

        <table cellspacing='0' id="Tableid"> 

            <tr id="foc">
                <th>ID</th>
                <th>Name</th>
                <th>Added Date</th>
                <th>Brand</th>
                <th>Available Qty</th>
                <th>price</th>
                <th></th>
            </tr><!-- Table Header -->
            <%            for (DB.Products pr : prolist) {
            %>

            <tr class="xtable">
                <td class="xtable " ><%=pr.getId()%></td>
                <td class="xtable" ><%=pr.getModel().getManufacturers().getName()%> <%=pr.getModel().getModel()%></td>
                <td class="xtable" ><%=pr.getAddedDate()%> <%=pr.getAddedTime()%></td>
                <td class="xtable" ><%=pr.getModel().getManufacturers().getName()%></td>
                <td class="xtable" ><%=pr.getQty()%></td>
                <td class="xtable" >Rs.<%=pr.getPrice()%>.00</td>
                <td class="xtable"><a href="#prodetauls" onclick="opendetails('<%=pr.getId()%>')">open</a></td>
            </tr>
            <%
                    }

                } else {

                    String url = "Admin_update_product2.jsp?keyword=" + keyw + "&type=" + type + "&stock=" + stock + "&status=" + status + "";
                    url += "&pageno=" + (Integer.parseInt(pageno) - 1);
                    response.sendRedirect(url);
                }

            } else {

                flagnothingfound = false;

            %>
            <div class="nothingfound">
                <h3>Nothing Found....!</h3>
            </div>
            <%                }

                    ses.close();
                }
            %>


        </table>

        <script>
            function opendetails(id) {
                window.parent.parent.scrollTo(0, 0);
                var $iframe = $('#prodetail-if');
                $iframe.attr('src', "Admin_update_product3.jsp?pid=" + id + "");

            }

            var url = window.location.href;
            function reloadupdatepr2() {
                window.location = url;
            }
        </script>

        <%
            if (flagnothingfound) {
                int pagescount = totalresultscou / resultperpage;
                if (totalresultscou > (pagescount * resultperpage)) {
                    pagescount = pagescount + 1;
                }
        %>

        <div class="pagination-product-update">
            <ul class="pagination">
                <li><a id="pagination-pagecount">Page <%=pageno%> of <%=pagescount%></a></li>

                <%
                    if (Integer.parseInt(pageno) != 1) {
                        String url = "Admin_update_product2.jsp?keyword=" + keyw + "&type=" + type + "&stock=" + stock + "&status=" + status + "";
                        url += "&pageno=1";
                %>
                <li><a href="<%=url%>"><<</a></li>
                    <%
                    } else {
                    %>
                <li><a id="disabled-pagination"><<</a></li>
                    <%
                        }

                        if (Integer.parseInt(pageno) != 1) {
                            String url = "Admin_update_product2.jsp?keyword=" + keyw + "&type=" + type + "&stock=" + stock + "&status=" + status + "";
                            url += "&pageno=" + (Integer.parseInt(pageno) - 1);
                    %>
                <li><a href="<%=url%>">Prev</a></li>
                    <%

                    } else {
                    %>
                <li><a id="disabled-pagination">Prev</a></li>

                <%
                    }
                    //
                    if (Integer.parseInt(pageno) < 7) {

                        int maxpagination = 11;
                        if (pagescount < maxpagination) {
                            maxpagination = pagescount;
                        }
                        for (int a = 1; a <= maxpagination; ++a) {
                            String url = "Admin_update_product2.jsp?keyword=" + keyw + "&type=" + type + "&stock=" + stock + "&status=" + status + "";

                            url += "&pageno=" + a;

                            if (a == Integer.parseInt(pageno)) {
                %>
                <li><a id="active-page"><%=a%></a></li>
                    <%

                    } else {
                    %>
                <li><a href="<%=url%>"><%=a%></a></li>
                    <%
                            }

                        }
                    } else {

                        int curpage = Integer.parseInt(pageno);
                        int start = curpage - 5;
                        int endpage = curpage + 5;
                        if (endpage > pagescount) {

                            int overcou = endpage - pagescount;
                            start = start - overcou;
                            if (start < 1) {
                                start = 1;
                            }

                            endpage = pagescount;
                        }
                        for (int a = start; a <= endpage; ++a) {
                            String url = "Admin_update_product2.jsp?keyword=" + keyw + "&type=" + type + "&stock=" + stock + "&status=" + status + "";

                            url += "&pageno=" + a;

                            if (a == Integer.parseInt(pageno)) {
                    %>
                <li><a id="active-page"><%=a%></a></li>
                    <%

                    } else {
                    %>
                <li><a href="<%=url%>"><%=a%></a></li>
                    <%
                                }

                            }

                        }
                        if (pagescount == Integer.parseInt(pageno)) {
                    %>
                <li><a id="disabled-pagination">Next</a></li>
                    <%
                    } else {
                        String url = "Admin_update_product2.jsp?keyword=" + keyw + "&type=" + type + "&stock=" + stock + "&status=" + status + "";
                        url += "&pageno=" + (Integer.parseInt(pageno) + 1);

                    %>
                <li><a href="<%=url%>">Next</a></li>
                    <%                    }

                        if (pagescount != Integer.parseInt(pageno)) {
                            String url = "Admin_update_product2.jsp?keyword=" + keyw + "&type=" + type + "&stock=" + stock + "&status=" + status + "";
                            url += "&pageno=" + pagescount;
                    %>
                <li><a href="<%=url%>">>></a></li>
                    <%                    } else {

                    %>
                <li><a id="disabled-pagination">>></a></li>    
                    <%                    }
                    %>
            </ul>
        </div>
        <%
            }
        %>

        <div id="prodetauls" class="modalDialogpr">
            <div>
                <a href="#close" onclick="reloadupdatepr2()" title="Close" class="close">X</a>
                <div class="edit-pr" id="loginfoem"><!--login form-->
                    <form class="modalDialogprform">
                        <iframe class="pro-edit-iframe" id="prodetail-if" width="100%" height="100%" scrolling="no" border="0" src="Admin_update_product3.jsp?pid=1" frameborder="no">Your browser not support this feature</iframe>
                    </form>
                </div>
            </div>	
        </div>
    </body>
</html>
