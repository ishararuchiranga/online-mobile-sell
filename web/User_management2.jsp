<%-- 
    Document   : User_management2
    Created on : May 7, 2016, 8:48:41 PM
    Author     : Ishara
--%>

<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/main.js"></script>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/typicons.min.css" rel="stylesheet">
        <style>
            body{
                background-color: rgb(248, 248, 248);
            }
            .um2-div1{
                padding-right: 1%;
                padding-left: 1%;
                background-color: rgb(66, 139, 202);
                border-bottom-left-radius: 10px;
                border-bottom-right-radius: 10px;
                color: white;
                margin-bottom: 40px;
                font-size: 16px;
            }
            .um2-table1{
                width: 100%;
            }
            .um2-table1 td{
                padding-left: 50px;
            }
            .um2-table1 td a{
                font-size: 12px;
                color: black;
            }
            select{
                width: 100px;
            }

            //
            .dropbtn {
                /* background-color: #ffffff;*/

                padding: 8px;
                // margin:6px;
                font-size: 16px;
                border: none;
                cursor: pointer;
                border: none;
                height: 50px;

            }
            span{
                color: #ba0505;
            }
            span:hover{
                background-color: rgb(66, 139, 202);
            }
            span:last-child{
                background-color: rgb(66, 139, 202);
            }

            .divchng{
                position: relative;
                display: inline-block;
            }
            .dropdown {

                margin: 0px 0px 0px -25px;
                margin-left: -25px;
                position: relative;
                display: inline-block;
                float: right;
                margin-right: 30px;
            }

            .dropdown-content {
                display: none;
                position: absolute;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            }

            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }

            .dropdown-content a:hover {
                background-color: #f1f1f1;
            }


            .dropdown:hover .dropdown-content {
                background-color: white;
                display: block;
                z-index: 2;
            }

            .dropdown:hover .dropbtn {
                background-color: #D8D7D7;
            }
            .nothingfound{
                text-align: center;
                padding: 20px 0px 20px 0px;
                background-color: rgb(143, 234, 143);
                margin-top: 40px;
                border-radius: 30px;
            }
        </style>
    </head>
    <body>
        <!--<input type="button" value="back" onclick="window.history.back()">-->
        <%
            //String uid = request.getParameter("id");

        %>
        <div class="container um2">


            <%//
                int i = 0;
                HttpSession htsesum2 = request.getSession();

                if (htsesum2.getAttribute("Login_object") != null) {
                    DB.UserRegistration urs = (DB.UserRegistration) htsesum2.getAttribute("Login_object");
                    String utype =(urs.getUserTypes().getId()).toString();

                    if (request.getParameter("id") != null) {

                        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();

                        DB.UserRegistration ur = (DB.UserRegistration) ses.load(DB.UserRegistration.class, Integer.parseInt(request.getParameter("id")));


            %>

            <div class="um2-div1">
                <table class="um2-table1" >

                    <tbody>
                        <tr>
                            <td> User Name: <%=ur.getUserName()%></td>
                            <td>Email: <%=ur.getEmail()%></td>
                            <td id="tdst">Status: <div class="divchng" id="stch"><%=ur.getStatus().getStatus()%></div>
                                <%
                                    if (ur.getUserTypes().getId() != 3) {
                                %>
                                <div class="dropdown">
                                    <a class="dropbtn drp" ><span class="typcn typcn-edit"></span></a>
                                    <div id="stdr" class="dropdown-content">
                                        <%
                                            if (ur.getStatus().getId() == 1) {
                                        %>
                                        <a onclick="chng('st,2')">Active</a>
                                        <a onclick="chng('st,3')">Disable</a>
                                        <%
                                        } else if (ur.getStatus().getId() == 2) {
                                        %>
                                        <a onclick="chng('st,1')">Pending</a>
                                        <a onclick="chng('st,3')">Disable</a> 
                                        <%
                                        } else {
                                        %>
                                        <a onclick="chng('st,2')">Active</a>
                                        <a onclick="chng('st,1')">Pending</a>
                                        <%
                                            }
                                        %>
                                    </div>
                                </div>
                                <%    //
                                    }
                                %>

                            </td>
                            <td>User type: <div class="divchng" id="utype"> <%=ur.getUserTypes().getType()%> </div>
                                <%
                                

                                    if (utype.equals("3")) {
                                        if (ur.getUserTypes().getId() != 3) {

                                %>
                                <div class="dropdown">
                                    <a class="dropbtn drp"><span class="typcn typcn-edit"></span></a>
                                    <div id="utdr" class="dropdown-content">
                                        <%                                   //
                                            if (ur.getUserTypes().getId() == 1) {
                                        %>
                                        <a onclick="chng2('ut,2')">Admin</a>
                                        <%
                                        } else if (ur.getUserTypes().getId() == 2) {
                                        %>
                                        <a onclick="chng2('ut,1')">Customer</a>
                                        <%
                                            }
                                        %>

                                    </div>
                                </div>
                                <%
                                        }
                                    }
                                %>

                            </td>
                        </tr>
                        <tr>
                            <%                          //
                                if (ur.getUserDetails() != null) {
                                    if (ur.getUserDetails().getFname() != null) {
                                        if (ur.getUserDetails().getPhone() != null) {

                            %>
                            <td>Name: <%=ur.getUserDetails().getFname()%> <%=ur.getUserDetails().getLname()%></td>
                            <td>Phone: <%=ur.getUserDetails().getPhone()%></td>
                            <%
                            } else {
                            %>
                            <td>Name: <%=ur.getUserDetails().getFname()%> <%=ur.getUserDetails().getLname()%></td>
                            <%
                                }

                            } else {
                                if (ur.getUserDetails().getPhone() != null) {
                            %>
                            <td>Phone: <%=ur.getUserDetails().getPhone()%></td>
                            <%                                      }
                                    }
                                }
                            %>
                        </tr>
                    </tbody>
                </table>


                <script>

                    function chng(iid) {

                        var sp = iid.split(",");
                        var st = sp[1] + "";
                        var pen = "'st,1'";
                        var act = "'st,2'";
                        var dis = "'st,3'";
                        alert("a");
                        if (st == "1") {
                            $('#stch').replaceWith('<div class="divchng" id="stch">Pending</div>');
                            $('#stdr').replaceWith('<div id="stdr" class="dropdown-content"><a onclick="chng(' + act + ')">Active</a><a onclick="chng(' + dis + ')">Disable</a></div>');
                            edituserst("1");
                        } else if (st == "2") {
                            $('#stch').replaceWith('<div class="divchng" id="stch">Active</div>');
                            $('#stdr').replaceWith('<div id="stdr" class="dropdown-content"><a onclick="chng(' + pen + ')">Pending</a><a onclick="chng(' + dis + ')">Disable</a></div>');
                            edituserst("2");
                        } else {
                            $('#stch').replaceWith('<div class="divchng" id="stch">Disable</div>');
                            $('#stdr').replaceWith('<div id="stdr" class="dropdown-content"><a onclick="chng(' + pen + ')">Pending</a><a onclick="chng(' + act + ')">Active</a></div>');
                            edituserst("3");
                        }
                    }
                    function edituserst(stid) {
                        var id = '<%=ur.getId()%>';
                        $.post(
                                "Admin_update_user_status",
                                {uid: id, stid: stid}, //meaasge you want to send
                        function (result) {
                        });
                    }

                    function chng2(iid2) {
                        var sp = iid2.split(",");
                        var ut = sp[1] + "";
                        alert(ut);
                        var cus = "'ut,1'";
                        var adm = "'ut,2'";
                        if (ut == "1") {
                            $('#utype').replaceWith('<div class="divchng" id="utype">Customer</div>');
                            $('#utdr').replaceWith('<div id="utdr" class="dropdown-content"><a onclick="chng2(' + adm + ')">Admin</a></div>');
                            editutype("1");
                        } else {
                            $('#utype').replaceWith('<div class="divchng" id="utype">Admin</div>');
                            $('#utdr').replaceWith('<div id="utdr" class="dropdown-content"><a onclick="chng2(' + cus + ')">Customer</a></div>');
                            editutype("2");
                        }

                    }
                    function editutype(utid) {
                        var id = '<%=ur.getId()%>';
                        $.post(
                                "Admin_update_user_type",
                                {uid: id, utid: utid}, //meaasge you want to send
                        function (result) {

                        });

                    }


                    //                    function editstatus(a) {
                    //                        var sp = a.split(",");
                    //                        var st = sp[0] + "";
                    //                        alert(st);
                    //                        var id = sp[1];
                    //                        var tdstclone = $('#tdst').clone();
                    //                        if (st == "1") {
                    //                            $('#tdst').replaceWith('Status: <select>\n\
                    //                                           <option selected="selected">Pending</option>\n\
                    //                                           <option>Active</option>\n\
                    //                                           <option>Disabled</option>\n\
                    //                                           </select>');
                    //
                    //                        } else if (st == "2") {
                    //                            $('#tdst').replaceWith('Status:<select>\n\
                    //                                           <option>Pending</option>\n\
                    //                                           <option selected="selected">Active</option>\n\
                    //                                           <option>Disabled</option>\n\
                    //                                           </select>');
                    //                        } else {
                    //                            $('#tdst').replaceWith('Status: <select>\n\
                    //                                           <option>Pending</option>\n\
                    //                                           <option>Active</option>\n\
                    //                                           <option selected="selected">Disabled</option>\n\
                    //                                           </select>');
                    //                        }
                    //
                    //                    }

                </script>
            </div>

            <div>
                <div  id="invoice_part">
                    <div style="display: none" id="deletebar" class="prof_2nd_bar">
                        <input id="checkall"  type="checkbox" style="margin: 5px">
                        <input type="button" id="deletebtn" disabled="disabled"  class="prof_deleteBtn" value="Delete">
                    </div>
                    <%         //
                        Criteria invoCr = ses.createCriteria(DB.Invoice.class);
                        invoCr.add(Restrictions.eq("userRegistration", ur));
                        invoCr.addOrder(Order.desc("id"));
                        List<DB.Invoice> invliceList = invoCr.list();
                        if (!(invliceList.isEmpty())) {
                            int flag2 = 0;
                            for (DB.Invoice inv : invliceList) {
                                ++flag2;
                                if (inv.getUserDeleted().equals("0")) {
                                    ++i;
                    %>

                    <div class="product-image-wrapper" style="border:1px solid #4E4E46;" id="invo<%=i%>">
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <div>
                                    <h4> Bill date: <%=inv.getDate()%></h4>
                                    <h4>Address:</h4>
                                    <h5 style="padding-left: 60px"><%=inv.getDelivery().getFname()%> <%=inv.getDelivery().getLname()%></h5>
                                    <h5 style="padding-left: 60px"><%=inv.getDelivery().getAd1()%></h5>
                                    <h5 style="padding-left: 60px"><%=inv.getDelivery().getAd2()%></h5>
                                    <h5 style="padding-left: 60px"><%=inv.getDelivery().getCity()%></h5>
                                </div>
                            </div>
                            <div class="col-sm-6">

                                <%
                                    Criteria crInvreg = ses.createCriteria(DB.InvoiceRegistry.class);
                                    crInvreg.add(Restrictions.eq("invoice", inv));
                                    List<DB.InvoiceRegistry> listinvr = crInvreg.list();
                                    int tot = 0;
                                    int no = 0;

                                    for (DB.InvoiceRegistry invr : listinvr) {
                                        ++no;
                                        tot += invr.getProducts().getPrice() * Integer.parseInt(invr.getQty());
                                %>
                                <h5><%=no%>).<%=invr.getProducts().getModel().getManufacturers().getName()%>  <%=invr.getProducts().getModel().getModel()%>
                                    Rs.<%=invr.getProducts().getPrice()%>.00 x <%=invr.getQty()%></h5>                           
                                    <%
                                        }
                                    %>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <hr style="margin: 0"> 
                            <h5 style="float: right;">Total Rs.<%=tot%>.00</h5>
                        </div>
                    </div>
                    <%
                    } else {
                        if (flag2 == 1) {
                    %>
                    <div style="color: rgb(8, 83, 8);background-color: rgb(158, 230, 158);border-radius: 12px;">
                        <h4 style="padding: 30px;">Empty</h4>
                    </div>
                    <%
                                }
                            }
                        }
                    } else {
                    %>
                    <div class="nothingfound">
                        <h3>No Invoice Found From This User.</h3>
                    </div>
                    <%
                                }
                                ses.close();
                            }
                        } else {
                   out.print("login plz");
                        }
                    %>
                </div>
            </div>


    </body>
</html>
