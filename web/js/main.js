/*price range*/

$('#sl2').slider();
var RGBChange = function () {
    $('#RGB').css('background', 'rgb(' + r.getValue() + ',' + g.getValue() + ',' + b.getValue() + ')')
};
/*scroll to top*/

$(document).ready(function () {
    $(function () {
        $.scrollUp({
            scrollName: 'scrollUp', // Element ID
            scrollDistance: 300, // Distance from top/bottom before showing element (px)
            scrollFrom: 'top', // 'top' or 'bottom'
            scrollSpeed: 300, // Speed back to top (ms)
            easingType: 'linear', // Scroll to top easing (see http://easings.net/)
            animation: 'fade', // Fade, slide, none
            animationSpeed: 500, // Animation in speed (ms)
            scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
            //scrollTarget: false, // Set a custom target element for scrolling to the top
            scrollText: '<i class="typcn typcn-arrow-up-outline"></i>', // Text for element, can contain HTML
            scrollTitle: false, // Set a custom <a> title if required.
            scrollImg: false, // Set true to use image
            activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
            zIndex: 2147483647 // Z-Index for the overlay
        });
    });
});
//Product page

$(document).ready(function () {
    $("#condition_items").on("hide.bs.collapse", function () {
        $(".conditon").html('<span class="typcn typcn-plus"></span> Condition');
    });
    $("#condition_items").on("show.bs.collapse", function () {
        $(".conditon").html('<span class="typcn typcn-minus"></span> Condition');
    });
}); //
$(document).ready(function () {
    $("#Brand_items").on("hide.bs.collapse", function () {
        $(".Brand").html('<span class="typcn typcn-plus"></span> Brand');
    });
    $("#Brand_items").on("show.bs.collapse", function () {
        $(".Brand").html('<span class="typcn typcn-minus"></span> Brand');
    });
});
$(document).ready(function () {
    $("#Catogory_items").on("hide.bs.collapse", function () {
        $(".catogory").html('<span class="typcn typcn-plus"></span> Catogory');
    });
    $("#Catogory_items").on("show.bs.collapse", function () {
        $(".catogory").html('<span class="typcn typcn-minus"></span> Catogory');
    });
});
$(document).ready(function () {
    $("#Seller_type_items").on("hide.bs.collapse", function () {
        $(".Sellertype").html('<span class="typcn typcn-plus"></span> Seller type');
    });
    $("#Seller_type_items").on("hide.bs.collapse", function () {
        $(".Sellertype").html('<span class="typcn typcn-minus"></span> Seller type');
    });
});
$(document).ready(function () {
    $("#productprice_items").on("hide.bs.collapse", function () {
        $(".productprice").html('<span class="typcn typcn-plus"></span> Price range');
    });
    $("#productprice_items").on("show.bs.collapse", function () {
        $(".productprice").html('<span class="typcn typcn-minus"></span> Price range');
    });
});
$(document).ready(function () {
    $("#Location_items").on("hide.bs.collapse", function () {
        $(".Location").html('<span class="typcn typcn-plus"></span> Location');
    });
    $("#Location_items").on("show.bs.collapse", function () {
        $(".Location").html('<span class="typcn typcn-minus"></span> Location');
    });
});
/*produc details jsp*/

var star1 = false;
var star2 = false;
var star3 = false;
var star4 = false;
var star5 = false;
$('#star1').mouseover(function () {
    colorStar1();
    uncolorStar2();
    uncolorStar3();
    uncolorStar4();
    uncolorStar5();
});
$('#star1').mouseout(function () {
    if (!star1) {
        uncolorStar1();
    } else {
        colorStar1();
    }
    if (!star2) {
        uncolorStar2();
    } else {
        colorStar2();
    }
    if (!star3) {
        uncolorStar3();
    } else {
        colorStar3();
    }
    if (!star4) {
        uncolorStar4();
    } else {
        colorStar4();
    }
    if (!star5) {
        uncolorStar5();
    } else {
        colorStar5();
    }

});
$('#star2').mouseover(function () {
    colorStar1();
    colorStar2();
    uncolorStar3();
    uncolorStar4();
    uncolorStar5();
});
$('#star2').mouseout(function () {

    if (!star1) {
        uncolorStar1();
    } else {
        colorStar1();
    }
    if (!star2) {
        uncolorStar2();
    } else {
        colorStar2();
    }
    if (!star3) {
        uncolorStar3();
    } else {
        colorStar3();
    }
    if (!star4) {
        uncolorStar4();
    } else {
        colorStar4();
    }
    if (!star5) {
        uncolorStar5();
    } else {
        colorStar5();
    }
});
$('#star3').mouseover(function () {
    colorStar1();
    colorStar2();
    colorStar3();
    uncolorStar4();
    uncolorStar5();
});
$('#star3').mouseout(function () {

    if (!star1) {
        uncolorStar1();
    } else {
        colorStar1();
    }
    if (!star2) {
        uncolorStar2();
    } else {
        colorStar2();
    }
    if (!star3) {
        uncolorStar3();
    } else {
        colorStar3();
    }
    if (!star4) {
        uncolorStar4();
    } else {
        colorStar4();
    }
    if (!star5) {
        uncolorStar5();
    } else {
        colorStar5();
    }

});
$('#star4').mouseover(function () {
    colorStar1();
    colorStar2();
    colorStar3();
    colorStar4();
    uncolorStar5();
});
$('#star4').mouseout(function () {

    if (!star1) {
        uncolorStar1();
    } else {
        colorStar1();
    }
    if (!star2) {
        uncolorStar2();
    } else {
        colorStar2();
    }
    if (!star3) {
        uncolorStar3();
    } else {
        colorStar3();
    }
    if (!star4) {
        uncolorStar4();
    } else {
        colorStar4();
    }
    if (!star5) {
        uncolorStar5();
    } else {
        colorStar5();
    }

});
$('#star5').mouseover(function () {
    colorStar1();
    colorStar2();
    colorStar3();
    colorStar4();
    colorStar5();
});
$('#star5').mouseout(function () {

    if (!star1) {
        uncolorStar1();
    } else {
        colorStar1();
    }
    if (!star2) {
        uncolorStar2();
    } else {
        colorStar2();
    }
    if (!star3) {
        uncolorStar3();
    } else {
        colorStar3();
    }
    if (!star4) {
        uncolorStar4();
    } else {
        colorStar4();
    }
    if (!star5) {
        uncolorStar5();
    } else {
        colorStar5();
    }
});
function colorStar1() {
    $('#star1').removeClass('fa-star-o');
    $('#star1').addClass('fa-star');
}
function colorStar2() {
    $('#star2').removeClass('fa-star-o');
    $('#star2').addClass('fa-star');
}
function colorStar3() {
    $('#star3').removeClass('fa-star-o');
    $('#star3').addClass('fa-star');
}
function colorStar4() {
    $('#star4').removeClass('fa-star-o');
    $('#star4').addClass('fa-star');
}
function colorStar5() {
    $('#star5').removeClass('fa-star-o');
    $('#star5').addClass('fa-star');
}
function uncolorStar1() {
    $('#star1').removeClass('fa-star');
    $('#star1').addClass('fa-star-o');
}
function uncolorStar2() {
    $('#star2').removeClass('fa-star');
    $('#star2').addClass('fa-star-o');
}
function uncolorStar3() {
    $('#star3').removeClass(' fa-star');
    $('#star3').addClass('fa-star-o');
}
function uncolorStar4() {
    $('#star4').removeClass('fa-star');
    $('#star4').addClass('fa-star-o');
}
function uncolorStar5() {
    $('#star5').removeClass('fa-star');
    $('#star5').addClass('fa-star-o');
}

//
$('#star1').click(function () {
    star1 = true;
    if (star2) {
        star2 = false;
    }
    if (star3) {
        star3 = false;
    }
    if (star4) {
        star4 = false;
    }
    if (star4) {
        star4 = false;
    }
    if (star5) {
        star5 = false;
    }
});
$('#star2').click(function () {
    star1 = true;
    star2 = true;
    if (star3) {
        star3 = false;
    }
    if (star4) {
        star4 = false;
    }
    if (star5) {
        star5 = false;
    }
});
$('#star3').click(function () {
    star1 = true;
    star2 = true;
    star3 = true;
    if (star4) {
        star4 = false;
    }
    if (star5) {
        star5 = false;
    }

});
$('#star4').click(function () {
    star1 = true;
    star2 = true;
    star3 = true;
    star4 = true;
    if (star5) {
        star5 = false;
    }

});
$('#star5').click(function () {
    star1 = true;
    star2 = true;
    star3 = true;
    star4 = true;
    star5 = true;
});
$('#rate-btn').click(function () {
    var proidrate = $("#proidrate").val();
    var review = $('#review-text').val();
    var rate = 0;
    if (star1) {
        rate = 1;
    }
    if (star2) {
        rate = 2;
    }
    if (star3) {
        rate = 3;
    }
    if (star4) {
        rate = 4;
    }
    if (star5) {
        rate = 5;
    }

    if (rate == 0) {
        $('#mustrate').show();
    } else {

        var dt = new Date();
        var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        var date = dt.getFullYear() + "-" + dt.getMonth() + 1 + "-" + dt.getDate();

        var fullstars = "";
        for (var i = 1; i <= rate; ++i) {
            fullstars += ' <i style="padding: 0" class="fa fa-star"> </i> ';
        }
        var halfstars = "";
        if (rate < 5) {
            var nonstar = 5 - rate;
            for (var i = 1; i <= nonstar; ++i) {
                halfstars += ' <i style="padding: 0" class="fa fa-star-o"></i> ';
            }
        }

        $('#reviews').append('<div id="appendpart" class="hideitems col-sm-12">\n\
                                     <ul>\n\
            <li><a href=""><i class="fa fa-user"></i>You</a></li>\n\
             <li><a href=""><i class="fa fa-clock-o"></i>' + time + '</a></li>\n\
            <li><a href=""><i class="fa fa-calendar-o"></i>' + date + '</a></li>\n\
             <li><a href="">' + fullstars + '' + halfstars + '\n\
                        <li><a href=""><h6 id="mustrate" class="color-red paddin_this nomargin ">New</h6></li>\n\
                </a></li></ul>\n\
            <p>' + review + '</p>\n\
            </div>');

        $('#addrating1').hide('2000');
        $('#appendpart').show('2000');



        $.post(
                "Rate_product",
                {
                    id: proidrate, review: review, rate: rate
                },
        function (result, status) {

        });
    }

});
$('#rate-btn2').click(function () {
    var proidrate = $("#proidrate").val();
    var ratename = $("#Unknownname").val();
    var rateemail = $("#Unknownemail").val();
    var review = $('#review-text').val();
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var flagname = false;
    var flagemail = false;
    if (ratename == "" || ratename == null) {
        $("#Unknownname").addClass("loginform-wrong-email");
        $("#Unknownname").prop("placeholder", "Please fill name");
        $("#Unknownname").prop("style", "color:red");
    } else {
        flagname = true;
    }

    if (rateemail == "" || rateemail == null) {
        $("#Unknownemail").addClass("loginform-wrong-email");
        $("#Unknownemail").prop("placeholder", "Please fill email");
        $("#Unknownemail").prop("style", "color:red");
    } else if (!filter.test(rateemail)) {
        $("#Unknownemail").val("");
        $("#Unknownemail").addClass("loginform-wrong-email");
        $("#Unknownemail").prop("placeholder", "Enter valid email address");
        $("#Unknownemail").prop("style", "color:red");
    } else {
        flagemail = true;
    }

    var rate = 0;
    if (star1) {
        rate = 1;
    }
    if (star2) {
        rate = 2;
    }
    if (star3) {
        rate = 3;
    }
    if (star4) {
        rate = 4;
    }
    if (star5) {
        rate = 5;
    }

    if (rate == 0) {
        $('#mustrate').show();
    } else {
        if (flagemail && flagname) {
            var dt = new Date();
            var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
            var date = dt.getFullYear() + "-" + dt.getMonth() + 1 + "-" + dt.getDate();

            var fullstars = "";
            for (var i = 1; i <= rate; ++i) {
                fullstars += ' <i style="padding: 0" class="fa fa-star"> </i> ';
            }
            var halfstars = "";
            if (rate < 5) {
                var nonstar = 5 - rate;
                for (var i = 1; i <= nonstar; ++i) {
                    halfstars += ' <i style="padding: 0" class="fa fa-star-o"></i> ';
                }
            }

            $('#reviews').append('<div id="appendpart" class="hideitems col-sm-12">\n\
                                     <ul>\n\
            <li><a href=""><i class="fa fa-user"></i>' + ratename + '</a></li>\n\
             <li><a href=""><i class="fa fa-clock-o"></i>' + time + '</a></li>\n\
            <li><a href=""><i class="fa fa-calendar-o"></i>' + date + '</a></li>\n\
             <li><a href="">' + fullstars + '' + halfstars + '\n\
                        <li><a href=""><h6 id="mustrate" class="color-red paddin_this nomargin ">New</h6></li>\n\
                </a></li></ul>\n\
            <p>' + review + '</p>\n\
            </div>');
            $('#addrating2').hide('2000');
            $('#appendpart').show('2000');


            $.post(
                    "Rate_product2",
                    {
                        id: proidrate, review: review, rate: rate, name: ratename, email: rateemail
                    },
            function (result, status) {

            });
        }
    }

});
$("#Unknownname").keydown(function () {
    $("#Unknownname").removeClass("loginform-wrong-email");
    $("#Unknownname").prop("placeholder", "Name");
    $("#Unknownname").removeProp("style");
});
$("#Unknownemail").keydown(function () {
    $("#Unknownemail").removeClass("loginform-wrong-email");
    $("#Unknownemail").prop("placeholder", "E-mail");
    $("#Unknownemail").removeProp("style");
});
$(".star-div").mouseenter(function () {
    $('#mustrate').hide();
});



/*Header*/
$('.menu').addClass('original').clone().insertAfter('.menu').addClass('cloned').css('position', 'fixed').css('top', '0').css('margin-top', '0').css('z-index', '500').removeClass('original').hide();

scrollIntervalID = setInterval(stickIt, 10);


function stickIt() {

    var orgElementPos = $('.original').offset();
    orgElementTop = orgElementPos.top;

    if ($(window).scrollTop() >= (orgElementTop)) {
        // scrolled past the original position; now only show the cloned, sticky element.

        // Cloned element should always have same left position and width as original element.     
        orgElement = $('.original');
        coordsOrgElement = orgElement.offset();
        leftOrgElement = coordsOrgElement.left;
        widthOrgElement = orgElement.css('width');
        $('.cloned').css('left', leftOrgElement + 'px').css('top', 0).css('width', widthOrgElement).show();
        $('.original').css('visibility', 'hidden');
    } else {
        // not scrolled past the menu; only show the original menu.
        $('.cloned').hide();
        $('.original').css('visibility', 'visible');
    }
}
var loginemail = false;
var loginpwrdle = false;
function loginformvalidate() {


    var email = document.getElementById("loginemail");

    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(email.value)) {
        loginemail = false;
        $("#loginemail").addClass("loginform-wrong-email");
        document.getElementById("emailad").focus();
        if (email.value == "" || email.value == null) {
            $("#emptyemail").animate({"opacity": "show", top: "100"}, 0);
        } else {
            $("#wrngemaillogin").animate({"opacity": "show", left: "100"}, 0);
        }
    } else {
        loginemail = true;
    }

    var pwrd = document.getElementById("loginpwrd").value;
    var und;
    if (pwrd.length <= 4) {
        $("#loginpwrd").addClass("loginform-wrong-email");
        if (pwrd == "") {
            $("#fillpwrd").animate({"opacity": "show", left: "100"}, 0);
        } else {
            $("#wrngepwrdlogin").animate({"opacity": "show", left: "100"}, 0);
        }
        loginpwrdle = false;
    } else {
        loginpwrdle = true;
    }

    if (loginemail === true && loginpwrdle === true) {

        $.post(
                "Check_before_login",
                {
                    email: email.value, pword: pwrd
                },
        function (result) {

            if (result.toString() == "1") {
                wronguorp();
            } else if (result.toString() == "2") {
                pleaseconfm();
            } else if (result.toString() == "3") {
                userblocked();
            } else if (result.toString() == "4") {
                $("#opensignin").prop("style", "display:none");
                $("#opensignin").hide();
                document.getElementById("signinform").submit();
            }
        });
    }

}


$("#loginemail").keydown(function () {
    $("#emptyemail").hide();
    $("#loginemail").removeClass("loginform-wrong-email");
    $("#wrngemaillogin").hide();
    $("#pleaseconfm").hide(1000);
    $("#invaliduser").hide();
    $("#userblocked").hide(800);
});

$("#loginpwrd").keydown(function () {
    $("#loginpwrd").removeClass("loginform-wrong-email");
    $("#wrngepwrdlogin").hide();
    $("#fillpwrd").hide();
    $("#pleaseconfm").hide(1000);
    $("#invaliduser").hide(800);
    $("#userblocked").hide(800);
});
function wronguorp() {
    $("#invaliduser").animate({"opacity": "show", left: "100"}, 1000);
}
function pleaseconfm() {
    $("#pleaseconfm").animate({"opacity": "show", left: "100"}, 1000);
}
function userblocked() {
    $("#userblocked").animate({"opacity": "show", left: "100"}, 1000);
}
//search
function search() {
    var searchtxt = document.getElementById("searchtxt").value;
    var url = "products.jsp";
    window.location.replace(url + "?searchtxt=" + searchtxt);
}
$('#searchtxt').keypress(function (e) {
    if (e.which == 13) {
        var searchtxt = document.getElementById("searchtxt").value;
        var url = "products.jsp";
        window.location.replace(url + "?searchtxt=" + searchtxt);
    }
});

$('#frgtpwrd').click(function () {
    $('#frgtsubmit').show(500);
});
$('#frgtbtn').click(function () {
    var mailad = $('#frgtmil').val();
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(mailad)) {
        $('#frgtmil').val("");
        $('#frgtmil').addClass("loginform-wrong-email");
        $('#frgtmil').prop("placeholder", "Invalid email");
        $('#frgtmil').prop("style", "color:red");
    } else {
        $('#frgtbtn').hide();
        $('#pleasewaitfrgtpwrd').show();
        $.post(
                "Forgot_password",
                {email: mailad}, //meaasge you want to send

        function (result) {
            if (result == 1) {
                $('#pleasewaitfrgtpwrd').hide();
                $('#frgtpwdone').show();
            }
            if (result == 2) {
                $('#frgtbtn').show();
                $('#pleasewaitfrgtpwrd').hide();
                $('#frgtmil').val("");
                $('#frgtmil').addClass("loginform-wrong-email");
                $('#frgtmil').prop("placeholder", "Wrong email");
                $('#frgtmil').prop("style", "color:red");
            }

        });
    }
});
$('#frgtmil').keydown(function () {
    $('#frgtmil').removeClass("loginform-wrong-email");
    $('#frgtmil').prop("placeholder", "E-mail");
    $('#frgtmil').removeProp("style");
});
$('#opensigup').click(function () {
    $('#loginfoem').hide();
    $('#signupformbegin').animate({"opacity": "show", left: "100"}, 1000);
});
$('#backtosignin').click(function () {
    $('#signupformbegin').hide();
    $('#loginfoem').animate({"opacity": "show", left: "100"}, 1000);

});
$('#closelog').click(function () {
    $('#signupformbegin').hide();
    $("#pleaselog").hide();
    $('#loginfoem').animate({"opacity": "show", left: "100"}, 1000);

    $.post(
            "removefrom",
            {
                a: "Profile_Purchase_History.jsp"
            },
    function (result) {
        $("#pleaselog").hide();
    });

});
// $('#frgtbtn').removeProp("disabled");

//signup
function signup() {
    $("#erroc").hide();
    var emailch = false;
    var pwrdle = false;
    var pwrdma = false;
    var username = false;
    var uname = document.getElementById("unametf").value;
    if (uname === "" || uname === null) {
        var username = false;
        $("#unametf").addClass("loginform-wrong-email");
        $("#wrnguname").show();
    } else {
        var username = true;
        $("#wrnguname").hide();
        $("#unametf").removeClass("loginform-wrong-email");
    }
    var email = document.getElementById("emailad");
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email.value)) {
        emailch = false;
        $("#emailad").addClass("loginform-wrong-email");
        $("#wrngemail").show();
    } else {
        emailch = true;
        $("#emailad").removeClass("loginform-wrong-email");
        $("#wrngemail").hide();
    }

    var pwrd = document.getElementById("pwrd1").value;
    if (pwrd.length <= 4) {
        $("#pwrd1").addClass("loginform-wrong-email");
        $("#pwrd2").addClass("loginform-wrong-email");
        $("#wrngpasslength").show();
        pwrdle = false;
    } else {
        $("#pwrd1").removeClass("loginform-wrong-email");
        $("#wrngpasslength").hide();
        pwrdle = true;
    }

    var p1 = document.getElementById("pwrd1").value;
    var p2 = document.getElementById("pwrd2").value;
    if (p1 === p2) {
        if (!(p1 === "" || p2 === "")) {

            if (emailch === true && pwrdle === true && username === true) {
                $("#subtn").hide();
                $("#pwrdmst").hide();
                $("#plwmas").show();


                $.post(
                        "User_registration",
                        {
                            email: email.value, pwrd: pwrd, uname: uname
                        },
                function (result) {

                    if (result == 1) {
                        $("#plwmas").hide();
                        $("#mailsent").show();
                    } else if (result == 2) {
                        $("#plwmas").hide()
                        $("#emalr").show();
                        $("#subtn").show();
                        $("#pwrdmst").show();
                    } else {
                        $("#erroc").show();
                        $("#subtn").show();
                        $("#pwrdmst").show();
                        $("#plwmas").hide();
                    }



                });

//                $.post("User_registration",
//                        {email: email, pwrd: pwrd, uname: uname}, //meaasge you want to send
//
//                function (result) {
//                    alert("apahu");
////                    $("#mailsent").show();
////                    $("#subtn").hide();
//
//                });
            }
        } else {
            pwrdma = false;
            $("#pwrd1").addClass("loginform-wrong-email");
            $("#pwrd2").addClass("loginform-wrong-email");
            $("#passworddoesntmatch").html("<p class='typcn typcn-times wrongformfield'> Please fill password fields</p>");
            $("#passworddoesntmatch").show();
        }
    } else {
        pwrdma = false;
        $("#pwrd1").addClass("loginform-wrong-email");
        $("#pwrd2").addClass("loginform-wrong-email");
        $("#pwrd1").val("");
        $("#pwrd2").val("");
        $("#passworddoesntmatch").show();
    }
}
$("#emailad").keydown(function () {
    hidesignuperrors();
});
$("#unametf").keydown(function () {
    hidesignuperrors();
});
$("#pwrd1").keydown(function () {
    hidesignuperrors();
});
$("#pwrd2").keydown(function () {
    hidesignuperrors();
});
function hidesignuperrors() {
    $("#unametf").removeClass("loginform-wrong-email");
    $("#wrnguname").hide();
    $("#wrngemail").hide();
    $("#emailad").removeClass("loginform-wrong-email");
    $("#wrngpasslength").hide();
    $("#passworddoesntmatch").hide();
    $("#pwrd1").removeClass("loginform-wrong-email");
    $("#pwrd2").removeClass("loginform-wrong-email");
    $("#emalr").hide();
    $("#erroc").hide();
}
$("#pwrd2").keypress(function (e) {
    if (e.which == 13) {
        signup();
    }
});
$("#loginpwrd").keypress(function (e) {
    if (e.which == 13) {
        loginformvalidate();
    }
});
$("#checko").click(function (e) {

    $.post(
            "setfrom",
            {
                a: "Checkout.jsp"
            },
    function (result) {
        $("#pleaselog").show();
    });
});
$("#accou").click(function (e) {

    $.post(
            "setfrom",
            {
                a: "Profile_Purchase_History.jsp"
            },
    function (result) {
        $("#pleaselog").show();
    });
});
$("#checkobtn").click(function (e) {
    $.post(
            "setfrom",
            {
                a: "Checkout.jsp"
            },
    function (result) {
        $("#pleaselog").show();
    });
});


//product details

$("#quantity").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and 
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
            // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: Ctrl+C
                            (e.keyCode == 67 && e.ctrlKey === true) ||
                            // Allow: Ctrl+X
                                    (e.keyCode == 88 && e.ctrlKey === true) ||
                                    // Allow: home, end, left, right
                                            (e.keyCode >= 35 && e.keyCode <= 39)
                                            )
                            {
                                // let it happen, don't do anything
                                return;
                            }
                            // Ensure that it is a number and stop the keypress
                            if ((e.shiftKey || (e.keyCode < 49 || e.keyCode > 57)) && (e.keyCode < 97 || e.keyCode > 105)) {

                                e.preventDefault();
                            }
                            $('#cantadmorethan').hide();

                        });


          