<%-- 
    Document   : Invoice
    Created on : May 2, 2016, 10:22:53 AM
    Author     : Ishara
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Invoice</title>
        <script src="js/jquery-2.1.4.min.js"></script>
        <link href="css/main.css" rel="stylesheet" media="all">
        <style>
            body{
                padding-right: 15px;
                padding-left: 15px;
                margin-right: auto;
                margin-left: auto;
                width: 842px;
            }
        </style>
    </head>
    <body id="a" onload="setval()">


        <script>
            $(document).ready(function () {
                PrintElem('#a');
            });
            function PrintElem(elem)
            {
//                Popup($('<div/>').append($(elem).clone()).html());
                Popup($(elem).html());
            }

            function Popup(data)
            {
//                var mywindow = window.open('aa', 'Invoice', 'height=700,width=842');
//                mywindow.document.write('<html><head><title>my div</title>');
//                /*optional stylesheet*/
//                mywindow.document.write('<link rel="stylesheet" href="css/main.css" type="text/css" />');
//                mywindow.document.write('</head><body >');
//                mywindow.document.write(data);
//                mywindow.document.write('</body></html>');
//
//                mywindow.document.close(); // necessary for IE >= 10
//                mywindow.focus(); // necessary for IE >= 10
//
//                mywindow.print();
//                // mywindow.close();
//                window.location.replace("index.jsp");
//
//                return true;

                var headstr = "<html><head><title>Booking Details</title></head><body>";
                var footstr = "</body></html>";
                var newstr = document.getElementById('a').innerHTML;
                var oldstr = document.body.innerHTML;
                document.body.innerHTML = headstr + newstr + footstr;
                window.print();
                document.body.innerHTML = oldstr;
                return false;
            }
        </script>

        <%
//            out.print("awa");
            HttpSession hsinv = request.getSession();
            DecimalFormat df = new DecimalFormat("0.00");
//
            if (hsinv.getAttribute("Invoice") != null && hsinv.getAttribute("Login_object") != null) {
                Session se = conn.NewHibernateUtil.getSessionFactory().openSession();

                DB.UserRegistration ure = (DB.UserRegistration) hsinv.getAttribute("Login_object");
                DB.UserRegistration ur = (DB.UserRegistration) se.load(DB.UserRegistration.class, ure.getId());

//                out.print("awa");
                String fname = hsinv.getAttribute("ship_fname").toString();
                String lname = hsinv.getAttribute("ship_lname").toString();
                String phn = hsinv.getAttribute("ship_phn").toString();
                String adl1 = hsinv.getAttribute("ship_adl1").toString();
                String adl2 = hsinv.getAttribute("ship_adl2").toString();
                String city = hsinv.getAttribute("ship_city").toString();

                String dist = hsinv.getAttribute("dist").toString();

                String date = hsinv.getAttribute("inv_date").toString();
                String time = hsinv.getAttribute("inv_time").toString();
                String invid = hsinv.getAttribute("inv_id").toString();

                String Subtot = hsinv.getAttribute("subtot").toString();
                String shipping = hsinv.getAttribute("ship").toString();
                if (shipping == "x" || shipping.equals("x")) {
                    shipping = "Free";
                } else {
                    shipping = "Rs." + shipping;
                }
                String total = hsinv.getAttribute("tot").toString();


        %>
        <!--1st line-->
        <div class="top-line">

            <div class="inv-top-left">
                <h3 class="invoiceheader">INVOICE</h3>
                <table>
                    <tr>
                        <td>Date</td>
                        <td class="tdcenter inv-top-left-table-header"><%=date%>&nbsp;(<%=time%>)</td>
                    </tr>
                    <tr>
                        <td>Invoice id </td>
                        <td class="tdcenter inv-top-left-table-outline"><%=invid%></td>
                    </tr>
                    <tr>
                        <td>Customer id &nbsp;&nbsp;</td>
                        <td class="tdcenter inv-top-left-table-outline"><%=ur.getId()%></td>
                    </tr>
                </table>
            </div>
            <div class="inv-top-right">
                <h3>Phonehut.lk</h3>
                <pre>
No 011,
Colombo 07,
Sri Lanka.
Phone: +9411222222
                </pre>
            </div>
        </div>
        <!--2nd line-->
        <div class="sec-line">
            <ul>
                <li>
                    <h4>Bill to</h4>
                    <address class="addre">

                        <%=ur.getUserDetails().getFname()%> <%=ur.getUserDetails().getLname()%><br>
                        <%=ur.getUserDetails().getAdLine1()%>,<br>
                        <%=ur.getUserDetails().getAdLine2()%>,<br>
                        <%=ur.getUserDetails().getCity()%>,<br>
                        <%=ur.getUserDetails().getDistrict().getDistrict()%>.<br>
                        <%=ur.getUserDetails().getPhone()%>
                    </address>
                </li>
                <li>
                    <h4>Ship to</h4>
                    <address class="addre">

                        <%=fname%> <%=lname%><br>
                        <%=adl1%>,<br>
                        <%=adl2%>,<br>
                        <%=city%>,<br>
                        <%=dist%>.<br>
                        <%=phn%>
                    </address>
                </li>
                <li>
                    <h4>Due Date</h4>
                    <address class="addre">
                        <%=date%>
                    </address>
                </li>
            </ul>
        </div>
        <!--table 1-->
        <div>
            <table id="tb1" class="table1">
                <thead>
                    <tr>
                        <th></th>
                        <th>ITEM</th>
                        <th>DESCRIPTION</th>
                        <th>QTY</th>
                        <th>UNIT PRICE</th>
                        <th>TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                    <%

                        Java_classes.Cart c = (Java_classes.Cart) hsinv.getAttribute("Invoice");
                        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
                        List<Java_classes.NewCartItems> n = c.getArrayData();
                        int cou = 0;

                        for (Java_classes.NewCartItems ni : n) {
                            ++cou;
                            DB.Products pr = (DB.Products) ses.get(DB.Products.class, ni.getProid());
                            double tot = ni.getProQty() * pr.getPrice();

                    %>
                    <tr>
                        <td><%=cou%></td>
                        <td><%=pr.getId()%></td>
                        <td><%=pr.getModel().getManufacturers().getName()%> <%=pr.getModel().getModel()%> </td>
                        <td><%=ni.getProQty()%></td>
                        <td><% out.print(df.format(pr.getPrice())); %></td>
                        <td class="table-tot"><% out.print(df.format(tot)); %></td>
                    </tr>
                    <%
                        }

                    %>


                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>-</td>
                        <td class="table-tot">-</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>-</td>
                        <td class="table-tot">-</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>-</td>
                        <td class="table-tot">-</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!--bottom-->
        <div class="bottom-line">
            <!--            <div class="notes">
                            <h5>Notes</h5>
                            <p>sdhgj adjb adjfh</p>
                        </div>-->
            <div class="equations">
                <table id="tbl2" class="table2">
                    <tr>
                        <td class="tb2td1">SUB TOTAL</td>
                        <td id="subt" class="tb2td2 txtalignright">Rs.<%=Subtot%></td>
                    </tr>
                    <tr class="table2middle">
                        <td class="tb2td1">SHIPPING</td>
                        <td class="tb2td22 txtalignright"><%=shipping%></td>
                    </tr>
                    <tr>
                        <td class="tb2td1">TOTAL</td>
                        <td class="tb2td23 txtalignright">Rs.<%=total%></td>
                    </tr>

                </table>

            </div>
        </div>
        <!--last-->
        <div>
            <p class="p2">If any problem please contact ishara.ira@gmail.com</p>
        </div>
        <script>
            function setval() {
                var tbl = document.getElementById('tbl2');
                var td = tbl.rows[0].cells[0];
//                alert(document.getElementById('tb1').rows[0].cells[5].offsetWidth);
                document.getElementById("subt").style.width = (document.getElementById('tb1').rows[0].cells[5].offsetWidth)+"px";
            }
        </script>
        <% //            
            }
//            hsinv.removeAttribute("Invoice");

            //else {
            // response.sendRedirect("index.jsp");
            // }
        %>
    </body>
</html>
