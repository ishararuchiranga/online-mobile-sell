<%-- 
    Document   : newjsp
    Created on : Jul 6, 2016, 6:02:21 PM
    Author     : Ishara
--%>

<%@page import="org.w3c.dom.Element"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.io.File"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            try {
                 String file = application.getRealPath("/");
                File fXmlFile = new File(file+"..\\..\\src\\java\\XML\\websitedata.xml");
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(fXmlFile);

         //optional, but recommended
                //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
                doc.getDocumentElement().normalize();

                System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

                NodeList nList = doc.getElementsByTagName("Banner");

                System.out.println("----------------------------");
 //
                for (int temp = 0; temp < nList.getLength(); temp++) {

                    Node nNode = nList.item(temp);

                    System.out.println("\nCurrent Element :" + nNode.getNodeName());
 //				
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
 //
                        Element eElement = (Element) nNode;
 //

                        System.out.println("First Name : " + eElement.getElementsByTagName("image").item(0).getTextContent());
 //			System.out.println("Last Name : " + eElement.getElementsByTagName("lastname").item(0).getTextContent());
 //			System.out.println("Nick Name : " + eElement.getElementsByTagName("nickname").item(0).getTextContent());
 //			System.out.println("Salary : " + eElement.getElementsByTagName("salary").item(0).getTextContent());
 //
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        %>
    </body>
</html>
