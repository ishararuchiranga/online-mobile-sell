<%-- 
    Document   : Admin_addProducts
    Created on : Mar 16, 2016, 6:29:26 PM
    Author     : Ishara
--%>

<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css" />
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/admin.css" rel="stylesheet">
        <script src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/calendar.js"></script>
        <script src="js/jquery.ajaxfileupload.js"></script>
        <script type="text/javascript">

            var qty;
            var price;
            var selectbrand;
            var devtype;
            var phnmodel;
            //
            //
            var networks = [];
            var tgdetails = [];
            var speed;
            var gprs;
            var edge;
            //
            //
            var anounced_date;
            var status;
            //
            //
            var dimension;
            var weight;
            var simsize;
            var sim;
            //
            //
            var scrntype;
            var touchtype;
            var scrnsize;
            var scrnbratio;
            var resolution;
            var ppi;
            var multitouch;
            var scrnprotection;
            var otherscrnde;
            //
            //
            var osversion = "10";
            var chipset;
            var cpu;
            var gpu;
            //
            //
            var cardslot = 1;
            var cardupto = 16;
            var ram;
            var intmemory;
            //
            //
            var Pixels = 0;
            var camresolution = 0;
            var features = 0;
            var video = 0;
            var seccampixel = 0;
            var seccaminfo = 0;
            //
            //
            var alerttypes = [];
            var loudspeaker;
            var threefivejack;
            var soundotherspec;
            //
            //
            var WLAN;
            var Bluetooth;
            var gps;
            var nfc;
            var radio;
            var USB;
            //
            //
            var sensors = [];
            var Messaging;
            var Browser;
            var java;
            var Other;
            //
            var btrymovrble;
            var btrysize;
            var fstchrj;
            var Standby;
            var calltime;
            var colors;
            function Devicedetails() {
                //price

                price = document.getElementById("Price").value;
                alert("call");
                qty = document.getElementById("qty").value;

                //brand
                var selectbrand1 = document.getElementById('selectbrand');
                selectbrand = selectbrand1.options[selectbrand1.selectedIndex].value;
                //device type
                var devtype1 = document.getElementById('devtype');
                devtype = devtype1.options[devtype1.selectedIndex].value;
                //model
                phnmodel = document.getElementById("phnmodel").value;
                network();
            }
            function network() {


                //newtworks
                networks = [];
                tgdetails = [];
                $.each($("input[name='network']:checked"), function () {
                    networks.push($(this).val());
                });
                if ($('#2gdetais').is(":visible")) {
                    tgdetails.push($('#2gdetais').val());
                }
                if ($('#3gdetais').is(":visible")) {
                    tgdetails.push($('#3gdetais').val());
                }
                if ($('#4gdetais').is(":visible")) {
                    tgdetails.push($('#4gdetais').val());
                }
                //speed
                speed = document.getElementById("newtworkspeed").value;
                //gprs
                var gprsrates = document.getElementsByName('gprs');
                for (var i = 0; i < gprsrates.length; i++) {
                    if (gprsrates[i].checked) {
                        gprs = gprsrates[i].value;
                    }
                }
                //edge
                var edgerates = document.getElementsByName('edge');
                for (var i = 0; i < edgerates.length; i++) {
                    if (edgerates[i].checked) {
                        edge = edgerates[i].value;
                    }
                }

                launch();
            }

            function launch() {

                //annnounced date
                anounced_date = document.getElementById("date").value;
                //status

                var statusrates = document.getElementsByName('status');
                for (var i = 0; i < statusrates.length; i++) {
                    if (statusrates[i].checked) {
                        status = statusrates[i].value;
                    }
                }

                body();
            }

            function body() {
                //dimension               

                var width = document.getElementById("width").value;
                var height = document.getElementById("height").value;
                var depth = document.getElementById("depth").value;
                dimension = width + "x" + height + "x" + depth;
                //weight
                weight = document.getElementById("weight").value;
                //sim

                var simsizerates = document.getElementsByName('simsize');
                for (var i = 0; i < simsizerates.length; i++) {
                    if (simsizerates[i].checked) {
                        simsize = simsizerates[i].value;
                    }
                }


                var simrates = document.getElementsByName('sim');
                for (var i = 0; i < simrates.length; i++) {
                    if (simrates[i].checked) {
                        sim = simrates[i].value;
                    }
                }

                display();
            }
            function display() {
                //type

                var scrntype1 = document.getElementById('displaytype');
                scrntype = scrntype1.options[scrntype1.selectedIndex].value;
                var touchtypera = document.getElementsByName('touchtypera');
                for (var i = 0; i < touchtypera.length; i++) {
                    if (touchtypera[i].checked) {
                        touchtype = touchtypera[i].value;
                    }
                }

                //size
                scrnsize = document.getElementById("scrnsize").value;
                scrnbratio = document.getElementById("scrnbratio").value;
                //resolution
                var reswidth = document.getElementById("reswidth").value;
                var resheight = document.getElementById("resheight").value;
                resolution = reswidth + "x" + resheight;
                //pixel
                ppi = document.getElementById("ppi").value;
                //multitouch

                var multch = document.getElementsByName('multouch');
                for (var i = 0; i < multch.length; i++) {
                    if (multch[i].checked) {
                        multitouch = multch[i].value;
                    }
                }

                //protection
                scrnprotection = document.getElementById("Protection").value;
                //other
                otherscrnde = document.getElementById("otherscrndetails").value;
                platform();
            }

            function platform() {
                //os

                if ($('#jsonselect').is(":visible")) {
                    var osversion1 = document.getElementById('jsonselect');
                    osversion = osversion1.options[osversion1.selectedIndex].value;
                } else {
                }
                //chipset
                chipset = document.getElementById("chipset").value;
                //cpu
                cpu = document.getElementById("cpu").value;
                //gpu
                gpu = document.getElementById("gpu").value;
                memory();
            }

            function memory() {
                //card slot
                var cardslotrates = document.getElementsByName('cardslot');
                for (var i = 0; i < cardslotrates.length; i++) {
                    if (cardslotrates[i].checked) {
                        cardslot = cardslotrates[i].value;
                    }
                }
                if ($('#card_up_to').is(":visible")) {
                    cardupto = document.getElementById("card_up_to").value;
                } else {
                    cardupto = 0;
                }

                //ram
                ram = document.getElementById("ram").value;
                //internal
                intmemory = document.getElementById("intmemory").value;

                camera();
            }
            function camera() {
                //primary   

                Pixels = document.getElementById("Pixels").value;
                var camresheight = document.getElementById("camwidth").value;
                var camreswidth = document.getElementById("camheight").value;
                camresolution = camresheight + " x " + camreswidth;
                //features
                features = document.getElementById("camfeatures").value;

                //video
                video = document.getElementById("video").value;
                //secondary
                seccampixel = document.getElementById("seccampixel").value;
                seccaminfo = document.getElementById("seccaminfo").value;

                sound();
            }

            function sound() {

                //alert type
                alerttypes = [];
                $.each($("input[name='alerttypes']:checked"), function () {
                    alerttypes.push($(this).val());
                });
                //loud speaker

                var loudspeakerrates = document.getElementsByName('loudspeaker');
                for (var i = 0; i < loudspeakerrates.length; i++) {
                    if (loudspeakerrates[i].checked) {
                        loudspeaker = loudspeakerrates[i].value;
                    }
                }
                //3.5mm

                var threefivejackrates = document.getElementsByName('threefivejack');
                for (var i = 0; i < threefivejackrates.length; i++) {
                    if (threefivejackrates[i].checked) {
                        threefivejack = threefivejackrates[i].value;
                    }
                }
                //other
                soundotherspec = document.getElementById("soundotherspec").value;

                comms();
            }
            function comms() {
                //wlan

                WLAN = document.getElementById("WLAN").value;
                //bluetooth
                Bluetooth = document.getElementById("Bluetooth").value;
                //gps

                var gpsrates = document.getElementsByName('gps');
                for (var i = 0; i < gpsrates.length; i++) {
                    if (gpsrates[i].checked) {
                        gps = gpsrates[i].value;
                    }
                }
                if (gps == "1") {
                    gps = document.getElementById("gps_details").value;
                } else {
                    gps = "0";
                }

                //nfc

                var nfcrates = document.getElementsByName('nfc');
                for (var i = 0; i < nfcrates.length; i++) {
                    if (nfcrates[i].checked) {
                        nfc = nfcrates[i].value;
                    }
                }
                //radio

                var radiorates = document.getElementsByName('radi');
                for (var i = 0; i < radiorates.length; i++) {
                    if (radiorates[i].checked) {
                        radio = radiorates[i].value;
                    }
                }
                if (radio == "1") {
                    radio = document.getElementById("radio_details").value;
                } else {
                    radio = "0";
                }
                radio_details = document.getElementById("radio_details").value;
                //usb
                USB = document.getElementById("USB").value;

                fatures();
            }

            function fatures() {

                //Sensor
                $.each($("input[name='sensor']:checked"), function () {
                    sensors.push($(this).val());
                });
                //messeging
                Messaging = document.getElementById("Messaging").value;
                //browser
                Browser = document.getElementById("Browser").value;
                //java

                var javarates = document.getElementsByName('java');
                for (var i = 0; i < javarates.length; i++) {
                    if (javarates[i].checked) {
                        java = javarates[i].value;
                    }
                }
                //other
                Other = document.getElementById("Other").value;

                battery();
            }

            function battery() {

                //name
                var btrymovrblerates = document.getElementsByName('btrymovrble');
                for (var i = 0; i < btrymovrblerates.length; i++) {
                    if (btrymovrblerates[i].checked) {
                        btrymovrble = btrymovrblerates[i].value;
                    }
                }

                btrysize = document.getElementById("btrysize").value;
                //fastchrj
                var fstchrj1 = document.getElementsByName('fstchrj');
                for (var i = 0; i < fstchrj1.length; i++) {
                    if (fstchrj1[i].checked) {
                        fstchrj = fstchrj1[i].value;
                    }
                }

                //standby
                Standby = document.getElementById("Standby").value;
                //call-time
                calltime = document.getElementById("calltime").value;
                //colors

                colors = document.getElementById("colors").value;

                servletCal();
            }

            function servletCal() {

                alert(qty);
                alert(price);


                if (phnmodel == "") {
                    $("#phnmodel").addClass("loginform-wrong-email");
                    $('html, body').animate({
                        scrollTop: $("#phnmodel").offset().top
                    }, 300);
                } else if (osversion == "45666") {

                } else {
                    alert(selectbrand);

                    $.post(
                            "Admin_save_Device",
                            {
                                brand: selectbrand, devicetyp: devtype, model: phnmodel,
                                ntworks: networks, netbands: tgdetails, spd: speed, gprs: gprs, ed: edge,
                                an_date: anounced_date, stat: status,
                                dimension: dimension, weight: weight, simsize: simsize, sim: sim,
                                scrntye: scrntype, touchtype: touchtype, scrnsize: scrnsize, scrnratio: scrnbratio, res: resolution, ppi: ppi, multitouch: multitouch, scrnprotection: scrnprotection, otherscrnde: otherscrnde,
                                os: osversion, chip: chipset, cpu: cpu, gpu: gpu,
                                cardslot: cardslot, cardup: cardupto, ram: ram, intmem: intmemory,
                                pixels: Pixels, camres: camresolution, camfeat: features, video: video, seccampix: seccampixel, seccaminf: seccaminfo,
                                alerttypes: alerttypes, loudspeaker: loudspeaker, threefive: threefivejack, souther: soundotherspec,
                                wl: WLAN, bluet: Bluetooth, gps: gps, nfc: nfc, radio: radio, usb: USB,
                                sensor: sensors, msg: Messaging, browser: Browser, ja: java, other: Other,
                                btrymo: btrymovrble, btrysize: btrysize, fstchrj: fstchrj, standb: Standby, calltime: calltime, color: colors,
                                price: price, qty: qty
                            },
                    function (result, status) {
                    });
                }
            }

        </script>

        <%
            Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        %>
    </head>
    <body>
        <style>

            .dropbtn {
                background-color: #ffffff;
                color: black;
                padding: 8px;
                // margin:6px;
                font-size: 16px;
                border: none;
                cursor: pointer;
                border: none;
                height: 50px;

            }

            .dropdown {
                position: relative;
                display: inline-block;
                float: right;
                margin-right: 30px;
            }

            .dropdown-content {
                display: none;
                position: absolute;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            }

            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }

            .dropdown-content a:hover {
                background-color: #f1f1f1
            }

            .dropdown:hover .dropdown-content {
                display: block;
                z-index: 1;
            }

            .dropdown:hover .dropbtn {
                background-color: #D8D7D7;
            }

            //
            .topbaradmin{
                position: fixed;
                z-index: 1;
                background-color: #F8F8F8; 
                margin: 0px;
            }
            .logoadmin{
                display: inline-block
            }
            .logoadmin img{
                margin: 6px;
            }
            .sidebaradmin li a{
                margin:  0px;
            }
            .active-menu{
                background-color: #E5580C;
                color: white;
            }
            .well{
                min-height: 20px;
                padding: 19px;
                margin-bottom: 20px;
                background-color: #F5F5F5;
                border: 1px solid #E3E3E3;
                border-radius: 4px;
                box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.05) inset;
            }
            .quick-btn {
                position: relative;
                display: inline-block;
                width: 90px;
                height: 80px;
                padding-top: 16px;
                margin: 10px;
                color: #444;
                text-align: center;
                text-decoration: none;
                text-shadow: 0px 1px 0px rgba(255, 255, 255, 0.6);
                box-shadow: 0px 0px 0px 1px #F8F8F8 inset, 0px 0px 0px 1px #CCC;
            }
            .quick-btn .label {
                position: absolute;
                top: -5px;
                right: -5px;
            }
            .quick-btn span {
                display: block;
            }
            .label-danger {
                background-color: #D9534F;
            }
            .label {
                display: inline;
                padding: 0.2em 0.6em 0.3em;
                font-size: 75%;
                font-weight: bold;
                line-height: 1;
                color: #FFF;
                text-align: center;
                white-space: nowrap;
                vertical-align: baseline;
                border-radius: 0.25em;
            }
            .adminiconsize{
                font-size: 35px;
            }
        </style>
        <div class="topbaradmin">
            <div class="logoadmin">
                <img src="images/home/Phone.PNG">
            </div>

            <div class="dropdown" >
                <button class="dropbtn"><img style="height: 28px" src="images/orange.png"> Admin Ishara</button>
                <div class="dropdown-content">
                    <a href="#">Logout</a>
                    <a href="#">Settings</a>
                </div>
            </div>
        </div>
        <div class="col-sm-2" style=" padding: 0px;position: relative; box-sizing: border-box; float: left; height:2500px; background-color: #F8F8F8 ;border: 1px solid #D8D7D7;">
            <nav class="navbar-default navbar-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav sidebaradmin" id="main-menu">
                        <li>
                            <a  href="AdminPanel1.jsp"><i class="fa fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a class="active-menu" href="Admin_addProducts.jsp"><i class="fa fa-plus-square"></i> Addproduct</a>
                        </li>
                        <li>
                            <a href="Admin_product_details.jsp"><i class="fa fa-check-square-o"></i> Product details</a>
                        </li>
                        <li>
                            <a href="Admin_user_management.jsp"><i class="fa fa-users"></i> User management</a>

                        </li>

                        <li>
                            <a href="Admin_shipping.jsp"><i class="fa fa-truck"></i> Delivery</a>
                        </li>
                    </ul>

                </div>

            </nav>

        </div>
        <div class="col-sm-10"  style="padding: 5px;float: left;  margin: 0px; box-sizing: border-box; height: 2500px; background-color: #F8F8F8;border: 1px solid #D8D7D7;">
            <style>
                .tabs-ec{

                }
                #oth{
                    color: black;
                    background-color: rgb(236, 236, 236);
                    margin-bottom: 20px;
                }
                #oth a{
                    color: #98989f;
                }
                #oth a:hover{
                    color: #a5bce0;
                }
                .tabs-ec h2{
                    text-align: center;
                    margin: 0px;
                    float: left;
                    width: 50%;
                    border: 1px solid #D5D5BC;
                    height: 40px;
                    padding-left: 10px;
                    color: wheat;
                    padding-top: 2px;
                    cursor: pointer;
                }
                .first{
                    border-top-left-radius: 15px;
                    border-bottom-left-radius: 15px;
                }
                .final{
                    border-top-right-radius:  15px;
                    border-bottom-right-radius: 15px;
                }
                #act{
                    background-color: #dcdcf6;
                    color: #8f8f8f;
                }

            </style>
            <div class="tabs-ec">
                <h2 class="first" id="act"><a href="Admin_addProducts.jsp">Add Product</a></h2>
                <h2  class="final" id="oth" ><a href="Admin_update_products.jsp">Update Product</a></h2>
            </div>




            <div class="row">
                <div class="col-sm-12" >
                    <div class="col-sm-6" style="margin-left: 30px; padding-top: 20px;">
                        Select a product:
                        <div class="search_bar_box">
                            <select class="" onchange="loadtables()" id="devtype">
                                <option value="0">Select a Type</option>
                                <%
                                    Criteria criteria2 = ses.createCriteria(DB.DeviceType.class);
                                    criteria2.addOrder(Order.asc("id"));
                                    List<DB.DeviceType> list2 = criteria2.list();
                                    for (DB.DeviceType dev : list2) {
                                %>
                                <option value="<%=dev.getId()%>"><%=dev.getType()%></option>
                                <%
                                    }
                                %>
                            </select>

                        </div>
                        <div class="search_bar_box hideitems" id="pricebox">
                            <input class="form-control" id="Price" style="display: inline-block" placeholder="Price"  type="text">
                        </div>
                        <div class="search_bar_box hideitems" id="qtybox">
                            <input class="form-control" id="qty" style="display: inline-block" placeholder="qty" type="text">
                        </div>
                        <div class="search_bar_box hideitems" id="imgbrowsebox">

                            <input placeholder="Image" class=" btn btn-danger" id="the-photo-file-field" type="file" name="datafile" /> 
                        </div>
                    </div>
                    <div class=" col-sm-6 imgpadding" id="preview" >




                    </div>‪

                </div>              

                <script>
                    //file upload
                    $(document).ready(function () {
                        $('input[type="file"]').ajaxfileupload({
                            'action': 'UploadFile',
                            'onComplete': function (response) {
                                $('#upload').hide();
                            },
                            'onStart': function () {
                                $('#upload').show();
                            }
                        });
                    });
                    //image show
                    if (window.File && window.FileReader && window.FileList && window.Blob) {
                        function humanFileSize(bytes, si) {
                            var thresh = si ? 1000 : 1024;
                            if (bytes < thresh)
                                return bytes + ' B';
                            var units = si ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
                            var u = -1;
                            do {
                                bytes /= thresh;
                                ++u;
                            } while (bytes >= thresh);
                            return bytes.toFixed(1) + ' ' + units[u];
                        }

                        function renderImage(file) {
                            var reader = new FileReader();
                            reader.onload = function (event) {
                                the_url = event.target.result
                                //of course using a template library like handlebars.js is a better solution than just inserting a string
                                $('#preview').html("<img class='' style='margin-left: auto; margin-right: auto; height: 100%; display: block;' src='" + the_url + "' />");
                                $('#preview').addClass("prev-border");
                            }
                            //when the file is read it triggers the onload event above.
                            reader.readAsDataURL(file);
                        }
                        //watch for change on the 
                        $("#the-photo-file-field").change(function () {
                            console.log("photo file has been chosen")
                            //grab the first image in the fileList
                            //in this example we are only loading one file.
                            console.log(this.files[0].size)
                            renderImage(this.files[0])
                        });
                    } else {
                        alert('The File APIs are not fully supported in this browser.');
                    }




                    function loadtables() {

                        var val1 = document.getElementById("devtype");
                        var val2 = val1[val1.selectedIndex].value;


                        if (val2 == "3") {
                            $('#Devicespec').hide(400);
                            $('#Accesoriespart').show(400);

                            $('#pricebox').show(400);
                            $('#qtybox').show(400);
                            $('#imgbrowsebox').show(400);
                            $('#image').show(400);

                        } else if (val2 == "0") {
                            $('#Accesoriespart').hide(400);

                            $('#Devicespec').hide(400);
                            $('#pricebox').hide(400);
                            $('#qtybox').hide(400);
                            $('#imgbrowsebox').hide(400);
                        } else {
                            $('#Accesoriespart').hide(400);
                            $('#Devicespec').show(400);
                            $('#pricebox').show(400);
                            $('#qtybox').show(400);
                            $('#imgbrowsebox').show(400);
                        }
                    }


                </script>
            </div>
            <div id="Devicespec" class="col-sm-12 hideitems">
                <div class="col-sm-12">
                    <form>
                        <table class="" cellspacing="0">
                            <tbody>
                                <tr>
                                    <th scope="row" rowspan="2">Device details</th>
                                    <td>Brand</td>
                                    <td>
                                        <select class=" form-control " onchange="addbrand1()" id="selectbrand">
                                            <%
                                                Criteria criteria = ses.createCriteria(DB.Manufacturers.class);
                                                criteria.addOrder(Order.asc("id"));
                                                List<DB.Manufacturers> list = criteria.list();

                                                for (DB.Manufacturers man : list) {
                                            %>
                                            <option value="<%=man.getId()%>"><%=man.getName()%></option>
                                            <%
                                                }
                                            %>
                                            <option class="addnew-select" value="15000">Add new</option>
                                        </select>
                                        <div id="addbrndarea" class="hideitems">
                                            <div class="search_bar_box ">                        
                                                <div class="form-group block">
                                                    <input class="form-control" id="addbrandtf" style="display: inline-block" type="text">
                                                </div>
                                            </div>
                                            <div class="search_bar_box ">                        
                                                <button id="addbrandbtn" type="button" class="btn btn-danger">
                                                    <span class="glyphicon glyphicon-plus-sign"></span>
                                                </button>
                                            </div>
                                            <hr>
                                        </div>
                                        <script>
                                            function addbrand1() {

                                                var brndop = document.getElementById("selectbrand");
                                                var brand = brndop.options[brndop.selectedIndex].value;
                                                if (brand == "15000") {
                                                    $('#addbrndarea').show();
                                                } else {
                                                    $('#addbrndarea').hide();
                                                }
                                            }
                                            document.getElementById("addbrandbtn").addEventListener("click", function () {

                                                var newbrand;
                                                newbrand = document.getElementById("addbrandtf").value;
                                                $.post(
                                                        "Admin_add_new_brand",
                                                        {brand: newbrand},
                                                function (result, status) {

                                                    var brandid = result;
                                                    var selectobject = document.getElementById("selectbrand");
                                                    for (var i = 0; i < selectobject.length; i++) {
                                                        if (selectobject.options[i].value == "15000") {
                                                            selectobject.remove(i);
                                                        }
                                                    }

                                                    var select, option;
                                                    select = document.getElementById('selectbrand');
                                                    option = document.createElement('option');
                                                    option.value = brandid;
                                                    option.text = newbrand;
                                                    select.add(option);
                                                    option = document.createElement('option');
                                                    option.value = 15000;
                                                    option.text = "Add new";
                                                    option.style.color = 'white';
                                                    option.style.backgroundColor = '#269abc';
                                                    select.add(option);
                                                    $('#addbrndarea').hide();
                                                });
                                            });</script>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Model</td>
                                    <td>
                                        <input type="text" id="phnmodel" class="form-control " placeholder="Model number">
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row" rowspan="4">Network</th>
                                    <td>Networks</td>
                                    <td>
                                        <label class="checkbox-inline"><input id="tgbox" onchange="tgf()" type="checkbox" name="network" value="1">2G  </label>
                                        <input type="text" id="2gdetais" class="form-control  hideitems" placeholder="2G Bands">
                                        <label class="checkbox-inline"><input id="thgbox" onchange="thgf()" type="checkbox" name="network" value="2">3G  </label>
                                        <input type="text"  id="3gdetais" class="form-control  hideitems" placeholder="3G Bands">
                                        <label class="checkbox-inline"><input id="fgbox" onchange="fgf()" type="checkbox" name="network" value="3">4G  </label>
                                        <input type="text" id="4gdetais" class="form-control  hideitems" placeholder="4G Bands">
                                        <script>

                                            function tgf() {
                                                if (document.getElementById('tgbox').checked) {
                                                    $('#2gdetais').show();
                                                } else {
                                                    $('#2gdetais').hide();
                                                }
                                            }
                                            function thgf() {
                                                if (document.getElementById('thgbox').checked) {
                                                    $('#3gdetais').show();
                                                } else {
                                                    $('#3gdetais').hide();
                                                }
                                            }
                                            function fgf() {
                                                if (document.getElementById('fgbox').checked) {
                                                    $('#4gdetais').show();
                                                } else {
                                                    $('#4gdetais').hide();
                                                }
                                            }
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Speed</td>
                                    <td><input type="text" id="newtworkspeed" class="form-control " placeholder="Input speeds here"></td>
                                </tr>
                                <tr>
                                    <td>GPRS</td>
                                    <td>
                                        <label class="radio-inline"><input  type="radio" name="gprs" value="1">Yes</label>
                                        <label class="radio-inline"><input type="radio" name="gprs" value="2">no</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>EDGE</td>
                                    <td>
                                        <label class="radio-inline"><input type="radio" name="edge" value="1">Yes</label>
                                        <label class="radio-inline"><input type="radio" name="edge" value="2">no</label>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row" rowspan="2">Launch</th>
                                    <td >Announced</td>
                                    <td > 

                                        <input id="date" class="form-control date-picker" type="text">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>
                                        <label class="radio-inline"><input type="radio" name="status" value="1">Coming soon</label>
                                        <label class="radio-inline"><input type="radio" name="status" value="2">Available</label>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row" rowspan="3">Body</th>
                                    <td>Dimensions</td>
                                    <td>
                                        <input type="text" id="width" class="form-control  " placeholder="Width">
                                        <input type="text" id="height" class="form-control " placeholder="Height">
                                        <input type="text" id="depth" class="form-control " placeholder="Depth">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Weight</td>
                                    <td><input type="text" id="weight" class="form-control " placeholder="Weight(grams)"></td>
                                </tr>
                                <tr>
                                    <td>SIM</td>
                                    <td>
                                        <label class="radio-inline"><input type="radio" name="simsize" value="1">Mini SIM</label>
                                        <label class="radio-inline"><input type="radio" name="simsize" value="2">Micro SIM</label>
                                        <label class="radio-inline"><input type="radio" name="simsize" value="3">Nano SIM</label></br></br>
                                        <hr>

                                        <label class="radio-inline"><input type="radio" name="sim" value="1">Single SIM</label>
                                        <label class="radio-inline"><input type="radio" name="sim" value="2">Dual SIM</label>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row" rowspan="7">Display</th>
                                    <td>Type</td>
                                    <td>

                                        <select class=" form-control " onchange="displaytypecombo()" id="displaytype">

                                            <%
                                                Criteria criteria3 = ses.createCriteria(DB.ScrnType.class);
                                                criteria3.addOrder(Order.asc("id"));
                                                List<DB.ScrnType> scrntypes = criteria3.list();

                                                for (DB.ScrnType scnt : scrntypes) {

                                            %>
                                            <option value="<%=scnt.getId()%>"><%=scnt.getScrntype()%></option>
                                            <%                                        }

                                            %>
                                        </select>
                                        <script>
                                            function displaytypecombo() {
                                                var displaytype3 = document.getElementById('displaytype');
                                                var displaytype2 = displaytype3.options[displaytype3.selectedIndex].value;
                                                if (displaytype2 != 1) {
                                                    $('#touchtypehide').show();
                                                } else {
                                                    $('#touchtypehide').hide();
                                                }
                                            }

                                        </script>

                                        <p id="touchtypehide">
                                            <label class="radio-inline"><input type="radio" name="touchtypera" value="1">Non-touch</label>
                                            <label class="radio-inline"><input type="radio" name="touchtypera" value="2">Resistive</label>
                                            <label class="radio-inline"><input type="radio" name="touchtypera" value="3">Capacitive</label> 
                                        </p>


                                    </td>
                                </tr><tr>
                                    <td>Size</td>
                                    <td>
                                        Size
                                        <input type="text" id="scrnsize" class="form-control " placeholder="Size(Inches)">
                                        Ratio
                                        <input type="text" id="scrnbratio" class="form-control " placeholder="Screen to body Ration">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Resolution</td>
                                    <td>
                                        Width
                                        <input type="text" id="reswidth" class="form-control " placeholder="Width">
                                        Height
                                        <input type="text" id="resheight" class="form-control " placeholder="Height">
                                    </td>
                                </tr>
                                <tr>
                                    <td >Pixel density</td>
                                    <td>
                                        <input type="text" id="ppi" class="form-control " placeholder="Pixel dencity">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Multi touch</td>
                                    <td>
                                        <label class="radio-inline"><input type="radio" name="multouch" value="1">Yes</label>
                                        <label class="radio-inline"><input type="radio" name="multouch" value="2">No</label>

                                    </td>
                                </tr>

                                <tr>
                                    <td>Protection</td>
                                    <td><input type="text" id="Protection" class="form-control " placeholder="Protection"></td>
                                </tr>
                                <tr>
                                    <td>Other</td>
                                    <td>
                                        <input type="text" id="otherscrndetails" class="form-control " placeholder="Other details">
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row" rowspan="4">Platform</th>
                                    <td class="ttl">OS</td>
                                    <td class="nfo">
                                        <div>

                                            <select class=" form-control " onchange="loadosversionlist()" id="osversions" >
                                                <%                                            Criteria c4 = ses.createCriteria(DB.OsList.class);
                                                    c4.addOrder(Order.asc("id"));
                                                    List<DB.OsList> osl = c4.list();
                                                    for (DB.OsList ol : osl) {
                                                %>
                                                <option value="<%=ol.getId()%>"><%=ol.getOses()%></option>
                                                <%
                                                    }
                                                %>
                                                <option style="background-color: #269abc; font-size: 14px; color: black;" value="10000">Add new</option>
                                            </select>

                                            <div id="hiddenados" class="hideitems">
                                                <div class="search_bar_box ">                        
                                                    <div class="form-group block">
                                                        <input class="form-control" id="newostf" style="display: inline-block" type="text">
                                                    </div>
                                                </div>
                                                <div class="search_bar_box ">                        
                                                    <button id="addosbtn" type="button" class="btn btn-danger">
                                                        <span class="glyphicon glyphicon-plus-sign"></span>
                                                    </button>
                                                </div>
                                                <hr>
                                            </div>
                                        </div><br>

                                        <script>

                                            function loadosversionlist() {
                                                var os1 = document.getElementById('osversions');
                                                var osid = os1.options[os1.selectedIndex].value;
                                                if (osid != 1) {
                                                    if (osid == "10000") {
                                                        $('#hiddenados').show();
                                                        $('#jsonselect').hide();
                                                    } else {
                                                        $('#hidenversion').hide();
                                                        $('#hiddenados').hide();
                                                        $('#jsonselect').show();
                                                        $.post(
                                                                "Admin_os_list",
                                                                {osid: osid}, //meaasge you want to send

                                                        function (result) {

                                                            var i;
                                                            var selectbox = document.getElementById('jsonselect');
                                                            for (i = selectbox.options.length - 1; i >= 0; i--)
                                                            {
                                                                selectbox.remove(i);
                                                            }

                                                            var json = JSON.parse(result);
                                                            var lastKey;
                                                            for (var key in json) {
                                                                if (json.hasOwnProperty(key)) {
                                                                    lastKey = key;
                                                                }
                                                            }
                                                            var first;
                                                            for (var fi in json) {
                                                                if (json.hasOwnProperty(fi) && typeof (fi) !== 'function') {
                                                                    first = fi;
                                                                    break;
                                                                }
                                                            }

                                                            var count = lastKey;
                                                            var select, i, option;
                                                            select = document.getElementById('jsonselect');
                                                            for (i = first; i <= lastKey; ++i) {
                                                                if (typeof (json[i]) != "undefined") {
                                                                    option = document.createElement('option');
                                                                    option.value = i;
                                                                    option.text = json[i];
                                                                    select.add(option);
                                                                }
                                                            }
                                                            option = document.createElement('option');
                                                            option.value = 45666;
                                                            option.text = "ADD NEW"
                                                            option.style.color = 'black';
                                                            option.style.backgroundColor = '#269abc';
                                                            select.add(option);
                                                            /*var jsonselectaaa = document.getElementById('jsonselect');
                                                             var jsonselectbbb = jsonselectaaa.options[jsonselectaaa.selectedIndex].value;
                                                             alert(jsonselectbbb);*/

                                                            /* var json = JSON.parse(result);
                                                             alert(json["Name"]);
                                                             var names = result;
                                                             $('#hshshs').html(names);*/
                                                        });
                                                    }

                                                } else {
                                                    $('#jsonselect').hide();
                                                    $('#hiddenados').hide();
                                                }
                                            }
                                            document.getElementById("addosbtn").addEventListener("click", function () {
                                                var newos = document.getElementById('newostf').value;
                                                if (newos != "") {
                                                    $.post(
                                                            "Admin_addos",
                                                            {nwos: newos},
                                                    function (result, status) {
                                                        var getosid = result;
                                                        var selectobject = document.getElementById("osversions");
                                                        for (var i = 0; i < selectobject.length; i++) {
                                                            if (selectobject.options[i].value == "10000") {
                                                                alert(i);
                                                                selectobject.remove(i);
                                                            }
                                                        }
                                                        var select, option;
                                                        select = document.getElementById('osversions');
                                                        option = document.createElement('option');
                                                        option.value = getosid;
                                                        option.text = newos;
                                                        select.add(option);
                                                        option = document.createElement('option');
                                                        option.value = 10000;
                                                        option.text = "ADD NEW"
                                                        option.style.color = 'white';
                                                        option.style.backgroundColor = '#269abc';
                                                        select.add(option);
                                                        $('#hiddenados').hide();
                                                    }
                                                    );
                                                }
                                            });</script>

                                        <select id="jsonselect" onclick="addocversion()" class=" form-control hideitems">

                                        </select>
                                        <div id="hidenversion" class="hideitems">
                                            <div class="search_bar_box ">                        
                                                <div class="form-group block">
                                                    <input class="form-control" id="newosversiontf" style="display: inline-block" type="text">
                                                </div>
                                            </div>
                                            <div class="search_bar_box ">                        
                                                <button id="newverbt" type="button" class="btn btn-danger">
                                                    <span class="glyphicon glyphicon-plus-sign"></span>
                                                </button>
                                            </div>
                                            <hr>
                                        </div>
                                        <script>
                                            function addocversion() {
                                                var ver = document.getElementById("jsonselect");
                                                var vers = ver.options[ver.selectedIndex].value;
                                                if (vers == "45666") {
                                                    $('#hidenversion').show();
                                                } else {
                                                    $('#hidenversion').hide();
                                                }
                                            }
                                            document.getElementById("newverbt").addEventListener("click", function () {
                                                var osid;
                                                var newvers;
                                                var osid1 = document.getElementById("osversions");
                                                osid = osid1.options[osid1.selectedIndex].value;
                                                newvers = document.getElementById("newosversiontf").value;
                                                if (newvers != "") {
                                                    $.post(
                                                            "Admin_add_os_version",
                                                            {os: osid, ver: newvers},
                                                    function (result, status) {

                                                        var verid = result;
                                                        var selectobject = document.getElementById("jsonselect");
                                                        for (var i = 0; i < selectobject.length; i++) {
                                                            if (selectobject.options[i].value == "45666") {

                                                                selectobject.remove(i);
                                                            }
                                                        }

                                                        var select, option;
                                                        select = document.getElementById('jsonselect');
                                                        option = document.createElement('option');
                                                        option.value = verid;
                                                        option.text = newvers;
                                                        select.add(option);
                                                        option = document.createElement('option');
                                                        option.value = 45666;
                                                        option.text = "ADD NEW";
                                                        option.style.color = 'white';
                                                        option.style.backgroundColor = '#269abc';
                                                        select.add(option);
                                                        $('#hidenversion').hide();
                                                    });
                                                }
                                            });</script>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ttl">Chipset</td>
                                    <td class="nfo"><input type="text" id="chipset" class="form-control " placeholder="Chipset"></td>
                                </tr>
                                <tr>
                                    <td class="ttl">CPU</td>
                                    <td class="nfo"><input type="text" id="cpu" class="form-control " placeholder="CPU"></td>
                                </tr>
                                <tr>
                                    <td class="ttl">GPU</td>
                                    <td class="nfo"><input type="text" id="gpu" class="form-control " placeholder="GPU"></td>
                                </tr>
                                <tr>
                                    <th scope="row" rowspan="3">Memory</th>
                                    <td>Card slot</td>
                                    <td>
                                        <label class="radio-inline"><input type="radio" onclick="upto1()" name="cardslot" value="1">Yes</label>
                                        <label class="radio-inline"><input type="radio" onclick="upto2()" name="cardslot" value="2">No</label>
                                        <hr>
                                        <input type="text" id="card_up_to" class="form-control  hideitems" placeholder="Card Up to">
                                        <script>
                                            function upto1() {
                                                $('#card_up_to').show();
                                            }
                                            function upto2() {
                                                $('#card_up_to').hide();
                                                $('#card_up_to').val("");

                                            }
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td>RAM</td>
                                    <td>
                                        <input type="text" id="ram" class="form-control" placeholder=RAM>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Internal memory</td>
                                    <td>
                                        <input type="text" id="intmemory" class="form-control" placeholder="Internal memory">
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row" rowspan="4">Camera</th>
                                    <td class="ttl">Primary</td>
                                    <td class="ttl">
                                        Pixels
                                        <input type="text" id="Pixels" class="form-control " placeholder="Pixel">
                                        <hr>
                                        Resolution
                                        <input type="text" id="camwidth" class="form-control" placeholder="width">
                                        <input type="text" id="camheight" class="form-control" placeholder="height">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Features</td>
                                    <td>
                                        <textarea class="form-control" id="camfeatures"></textarea>    
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ttl">Video</td>
                                    <td class="nfo"><input type="text" id="video" class="form-control small" placeholder="Video camera details"></td>
                                </tr>
                                <tr>
                                    <td class="ttl">Secondary</td>
                                    <td class="nfo">
                                        <input type="text" id="seccampixel" class="form-control" placeholder="Pixels"><br>
                                        <input type="text" id="seccaminfo" class="form-control" placeholder="Other Info">
                                    </td>

                                </tr>                                               
                                <tr>
                                    <th scope="row" rowspan="4">Sound</th>
                                    <td class="ttl">Alert types</td>
                                    <td class="nfo">
                                        <label class="checkbox-inline"><input type="checkbox" name="alerttypes" value="1">Vibration  </label>
                                        <label class="checkbox-inline"><input type="checkbox" name="alerttypes" value="2">MP3  </label>
                                        <label class="checkbox-inline"><input type="checkbox" name="alerttypes" value="3">WAV </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ttl">Loudspeaker </td>
                                    <td class="nfo">
                                        <label class="radio-inline"><input type="radio" name="loudspeaker" value="1">Yes</label>
                                        <label class="radio-inline"><input type="radio" name="loudspeaker" value="2">No</label>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="ttl">3.5mm jack </td>
                                    <td class="nfo">
                                        <label class="radio-inline"><input type="radio" name="threefivejack" value="1">Yes</label>
                                        <label class="radio-inline"><input type="radio" name="threefivejack" value="2">No</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ttl">Other </td>
                                    <td class="nfo">
                                        <input type="text" id="soundotherspec" class="form-control" placeholder="Other Info">
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row" rowspan="7">COMMS</th>  
                                    <td class="ttl">WLAN</td>
                                    <td class="nfo">
                                        <input type="text" id="WLAN" class="form-control" placeholder="Wifi details">
                                    </td>

                                </tr>
                                <tr>
                                    <td class="ttl">Bluetooth</td>
                                    <td class="nfo"><input type="text" id="Bluetooth" class="form-control" placeholder="BLuetooth details"></td>
                                </tr>
                                <tr>
                                    <td class="ttl">GPS</td>

                                    <td class="nfo">
                                        <label class="radio-inline"><input type="radio" onclick="gps1()" name="gps" value="1">Yes</label>
                                        <label class="radio-inline"><input type="radio" onclick="gps2()" name="gps" value="2">No</label>
                                        <hr>
                                        <input type="text" id="gps_details"  class="form-control hideitems" placeholder="GPS details">
                                        <script>
                                            function gps1() {
                                                $('#gps_details').show();
                                            }
                                            function gps2() {
                                                $('#gps_details').hide();
                                                $('#gps_details').val("");
                                            }
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ttl">NFC</td>
                                    <td class="nfo">
                                        <label class="radio-inline"><input type="radio" name="nfc"  value="1">Yes</label>
                                        <label class="radio-inline"><input type="radio" name="nfc"  value="2">No</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ttl">Radio</td>
                                    <td class="nfo">
                                        <label class="radio-inline"><input type="radio" onclick="rad1()" name="radi" value="1">Yes</label>
                                        <label class="radio-inline"><input type="radio" onclick="rad2()" name="radi" value="2">No</label>
                                        <hr>
                                        <input type="text" id="radio_details" class="form-control hideitems" placeholder="GPS details">
                                        <script>
                                            function rad1() {
                                                $('#radio_details').show();
                                            }
                                            function rad2() {
                                                $('#radio_details').hide();
                                                $('#radio_details').val("");
                                            }
                                        </script>

                                    </td>
                                </tr>   
                                <tr>
                                    <td class="ttl">USB</td>
                                    <td class="nfo">
                                        <input type="text" id="USB" class="form-control" placeholder="USB details">
                                    </td>
                                </tr>
                                <tr>
                                </tr>


                                <tr>
                                    <th scope="row" rowspan="5">Features</th>
                                    <td class="ttl">Sensors</td>
                                    <td class="nfo">
                                        <%
                                            Criteria c5 = ses.createCriteria(DB.Sensors.class);
                                            c5.addOrder(Order.asc("sensor"));
                                            List<DB.Sensors> sen = c5.list();

                                            for (DB.Sensors se : sen) {
                                                out.print(" <label class='checkbox-inline'><input type='checkbox' name='sensor' value='" + se.getId() + "']>" + se.getSensor() + " </label>");
                                            }
                                            System.gc();
                                        %>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="ttl">Messaging</td>
                                    <td class="nfo">
                                        <input type="text" id="Messaging" class="form-control" placeholder="Messeging">
                                    </td>
                                </tr><tr>
                                    <td class="ttl">Browser</td>
                                    <td class="nfo">
                                        <input type="text" id="Browser" class="form-control" placeholder="Browser">
                                    </td>
                                </tr>	
                                <tr>
                                    <td class="ttl">Java</td>
                                    <td class="nfo">
                                        <label class="radio-inline"><input type="radio" name="java" value="1">Yes</label>
                                        <label class="radio-inline"><input type="radio" name="java" value="2">No</label>

                                    </td>
                                </tr>

                                <tr>
                                    <td class="ttl">Other</td>
                                    <td class="nfo">
                                        <input type="text" id="Other" class="form-control" placeholder="Other details">
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row" rowspan="4">Battery</th>
                                    <td class="ttl">Name</td>
                                    <td class="nfo"> 

                                        <label class="radio-inline"><input type="radio" name="btrymovrble" value="1">Removable</label>
                                        <label class="radio-inline"><input type="radio" name="btrymovrble" value="2">Non-removable</label>
                                        <hr>
                                        <input type="text" id="btrysize" class="form-control" placeholder="Size">

                                    </td>
                                </tr>
                                <tr>
                                    <td class="ttl">Fast charge</td>
                                    <td class="nfo">
                                        <label class="radio-inline"><input type="radio" name="fstchrj" value="1">Yes</label>
                                        <label class="radio-inline"><input type="radio" name="fstchrj" value="2">No</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ttl">Stand-by</td>
                                    <td class="nfo"> <input type="text" id="Standby" class="form-control" placeholder="Standby time"></td>
                                </tr>
                                <tr>
                                    <td class="ttl">Call time</td>
                                    <td class="nfo"><input type="text" id="calltime" class="form-control" placeholder="Call time"></td>
                                </tr>

                                <tr>
                                    <th scope="row" rowspan="1">Misc</th>
                                    <td class="ttl">Colors</td>
                                    <td class="nfo">
                                        <input type="text" id="colors" class="form-control" placeholder="add colors">
                                    </td>
                                </tr>
                                <tr>
                                    <th colspan="3">
                                        <input type="button" class="form-control btn btn-default" style="background-color: tomato; " value="Save" onclick="Devicedetails()">

                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
            <div id="Accesoriespart" class="col-sm-12 hideitems" >
                <div class="col-sm-12">
                    <form >
                        <table class="" cellspacing="0">
                            <tbody>
                                <tr>
                                    <th scope="row" rowspan="3">Device details</th>
                                    <td>Brand</td>
                                    <td> 
                                        <select class=" form-control " onchange="addbrand2()" id="selectbrand2">
                                            <%
                                                Criteria c6 = ses.createCriteria(DB.Manufacturers.class);
                                                c6.addOrder(Order.asc("id"));
                                                List<DB.Manufacturers> list3 = c6.list();

                                                for (DB.Manufacturers manuf : list3) {
                                            %>
                                            <option value="<%=manuf.getId()%>"><%=manuf.getName()%></option>
                                            <%
                                                }
                                                System.gc();
                                                ses.close();
                                            %>
                                            <option style="background-color: #269abc; font-size: 14px; color: black;" value="15000">Add new</option>
                                        </select>
                                        <div id="addbrndarea2" class="hideitems">
                                            <div class="search_bar_box ">                        
                                                <div class="form-group block">
                                                    <input class="form-control" id="addbrandtf1" style="display: inline-block" type="text">
                                                </div>
                                            </div>
                                            <div class="search_bar_box ">                        
                                                <button id="addbrandbtn2" type="button" class="btn btn-danger">
                                                    <span class="glyphicon glyphicon-plus-sign"></span>
                                                </button>
                                            </div>
                                            <hr>
                                        </div>
                                        <script>
                                            function addbrand2() {
                                                var brndop = document.getElementById("selectbrand2");
                                                var brand = brndop.options[brndop.selectedIndex].value;
                                                if (brand == "15000") {
                                                    $('#addbrndarea2').show();
                                                } else {
                                                    $('#addbrndarea2').hide();
                                                }
                                            }
                                            document.getElementById("addbrandbtn2").addEventListener("click", function () {
                                                var newbrand;
                                                newbrand = document.getElementById("addbrandtf1").value;
                                                $.post(
                                                        "Admin_add_new_brand",
                                                        {brand: newbrand},
                                                function (result, status) {

                                                    var brandid = result;
                                                    var selectobject = document.getElementById("selectbrand2");
                                                    for (var i = 0; i < selectobject.length; i++) {
                                                        if (selectobject.options[i].value == "15000") {
                                                            selectobject.remove(i);
                                                        }
                                                    }
                                                    var selectobject = document.getElementById("selectbrand");
                                                    for (var i = 0; i < selectobject.length; i++) {
                                                        if (selectobject.options[i].value == "15000") {
                                                            selectobject.remove(i);
                                                        }
                                                    }

                                                    var select, option;
                                                    select = document.getElementById('selectbrand');
                                                    option = document.createElement('option');
                                                    option.value = brandid;
                                                    option.text = newbrand;
                                                    select.add(option);
                                                    option = document.createElement('option');
                                                    option.value = 15000;
                                                    option.text = "Add new";
                                                    option.style.color = 'white';
                                                    option.style.backgroundColor = '#269abc';
                                                    select.add(option);


                                                    var select1, option1;
                                                    select1 = document.getElementById('selectbrand2');
                                                    option1 = document.createElement('option');
                                                    option1.value = brandid;
                                                    option1.text = newbrand;
                                                    select1.add(option1);
                                                    option1 = document.createElement('option');
                                                    option1.value = 15000;
                                                    option1.text = "Add new";
                                                    option1.style.color = 'white';
                                                    option1.style.backgroundColor = '#269abc';
                                                    select1.add(option1);
                                                    $('#addbrndarea2').hide();
                                                });
                                            });</script>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ttl">Model</td>
                                    <td class="nfo"><input type="text" id="accesmodel" class="form-control" placeholder="Model"></td>
                                </tr>
                                <tr>
                                    <td class="ttl">Details</td>
                                    <td class="nfo"><textarea class="form-control" id="accesdetail" placeholder="Enter details here"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <th colspan="3">
                                        <input type="button" class="form-control btn btn-default" style="background-color: tomato; " value="Save" onclick="saveaccs()">
                                    </th>
                                </tr>
                            <script>
                                function saveaccs() {

                                    var selectaccbrand = document.getElementById('selectbrand2');
                                    var selectbrand2 = selectaccbrand.options[selectaccbrand.selectedIndex].value;


                                    var acmodel = document.getElementById('accesmodel').value;


                                    var acdetail = document.getElementById('accesdetail').value;

                                    var devtype1 = document.getElementById('devtype');
                                    devtype = devtype1.options[devtype1.selectedIndex].value;
                                    alert(devtype);


                                    price = document.getElementById("Price").value;
                                    alert("call");
                                    qty = document.getElementById("qty").value;
                                    $.post(
                                            "Admin_save_accesories",
                                            {
                                                brand: selectbrand2, model: acmodel, detail: acdetail, devtype: devtype, price: price, qty: qty
                                            },
                                    function (result, status) {
                                        alert("ret");
                                    });
                                }
                                $(document).ready(function () {
                                    $('#devtype option[value="0"]').attr('selected', 'selected');
                                });
                            </script>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>



    </body>
</html>
