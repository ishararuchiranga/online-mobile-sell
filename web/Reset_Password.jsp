<%-- 
    Document   : Reset Password
    Created on : Dec 29, 2015, 9:31:28 PM
    Author     : Ishara
--%>

<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <script src="js/jquery-2.1.4.min.js"></script>
        <link href="css/typicons.min.css" rel="stylesheet">
    </head>
    <body>
        <%
            String id = request.getParameter("sjdail");
            String as[] = id.split("!");

            Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
            Criteria c = ses.createCriteria(DB.UserRegistration.class);
            c.add(Restrictions.eq("id", Integer.parseInt(as[1])));
            c.add(Restrictions.eq("registerId", as[0]));
            DB.UserRegistration usr = (DB.UserRegistration) c.uniqueResult();
        %>
        <script>
            var id_frm_url =<%=as[1]%>;
        </script>
        <%

            if (usr != null) {

            } else {
                response.sendRedirect("something_wrong.jsp");
            }
        %>
        <script>
            function call() {
                pwrdma = false;
                pwrdle = false;
                var pwrd = document.getElementById("pwrd1").value;
                if (pwrd.length <= 4) {
                    $("#pwrd1").addClass("loginform-wrong-email");
                    $("#pwrd2").addClass("loginform-wrong-email");
                    $("#wrngpasslength").show();
                    pwrdle = false;
                } else {
                    $("#pwrd1").removeClass("loginform-wrong-email");
                    $("#pwrd2").removeClass("loginform-wrong-email");
                    $("#wrngpasslength").hide();
                    pwrdle = true;
                }

                var p1 = document.getElementById("pwrd1").value;
                var p2 = document.getElementById("pwrd2").value;
                if (p1 === p2) {
                    if (!(p1 === "" || p2 === "")) {
                        pwrdma = true;
                        if (pwrdma == true && pwrdle == true) {

                            callservlet();
                        }
                    } else {
                        pwrdma = false;
                        $("#pwrd1").addClass("loginform-wrong-email");
                        $("#pwrd2").addClass("loginform-wrong-email");
                        $("#passworddoesntmatch").html("<p class='typcn typcn-times wrongformfield'> Please fill password fields</p>");
                        $("#passworddoesntmatch").show();
                    }
                } else {
                    pwrdma = false;
                    $("#pwrd1").addClass("loginform-wrong-email");
                    $("#pwrd2").addClass("loginform-wrong-email");
                    $("#pwrd1").val("");
                    $("#pwrd2").val("");
                    $("#passworddoesntmatch").show();
                }
            }
            function callservlet() {

                $("#loading").show();
                var pwrdsend = $("#pwrd1").val();

                $.post(
                        "Forgot_password2",
                        {id: id_frm_url, pwrd: pwrdsend}, //meaasge you want to send
                function (result) {
                    alert("a");
                    locate();

                }
                );


            }
            function locate(){
                alert("awa");
                window.location.replace("http://localhost:8080/Phonehut.lk_2/products.jsp#opensignin");
            }

        </script>
        <div class="modalDialog4">
            <div id="forgotpwrdinside">
                <a href="#close" title="Close" onclick="hideloginwrongelements()" class="close">X</a>
                <div id="forgotpwrdfrm" class="signup-form"><!--sign up form-->
                    <h2>Reset Password</h2>
                    <div id="loading" class="hideitems"><i class='fa fa-repeat fa-spin '></i> Please wait..</div>
                    <form>
                        <div id="loading" class="hideitems"><i class='fa fa-repeat fa-spin '></i> Please wait..</div>
                        <label id="wrngpasslength" class="hideitems"><p class="typcn typcn-times wrongformfield">Length must be more than 5 characters</p></label>
                        <label id="passworddoesntmatch" class="hideitems"><p class="typcn typcn-times wrongformfield">Passwords doesn't</p></label>
                        <label id="invalidfrgtemail" class="hideitems"><p class="typcn typcn-times wrongformfield">Invalid email</p></label>

                        <input type="password" placeholder="New password" name="pwrd" class="form-control " id="pwrd1"/>
                        <input type="password" placeholder="Retype new password" name="pwrd2" class="form-control " id="pwrd2"/>
                        <label>(Password must have more than 5 characters)</label>

                        <div id="Removebtnsetwait"><button type="button" id="frgtpwrdbtn"  class="btn btn-default" onclick="call()">Submit</button></div>

                    </form>
                </div>	
            </div>
        </div>
    </body>
</html>
