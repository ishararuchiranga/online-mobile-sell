<%-- 
    Document   : Compare
    Created on : Feb 13, 2016, 2:02:47 PM
    Author     : Ishara
--%>

<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <link rel="shortcut icon" type="image/x-icon" href="images/home/Slider1.jpg" />
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="css/navibar.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/typicons.min.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/price-range.css" rel="stylesheet">

        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/script.js"></script>
        <link rel="stylesheet" type="text/css" href="css/table2.css">
        <script src="js/jquery.js"></script>



    </head>
    <body>
        <%@include file="Header2.jsp" %>

        <script type="text/javascript">
            $(document).ready(function () {
                // clear text from input
                $("#searchdebox").click(function () {
                    if ($("#searchdebox").val() == "Search")
                    {
                        $("#searchdebox").val("");
                    }
                });
                // check if key has been pressed
                $("#searchdebox").keyup(function (event) {
                    if ($("#searchdebox").val() != "")
                    {
                        var key = $("#searchdebox").val();
                        $.post(
                                "Compare_search_devices",
                                {keyword: key}, //meaasge you want to send
                        function (result) {
                            var json = JSON.parse(result);
                            for (i in json) {
                                $(".searchterm").append(' <a onclick="addToCompare(' + i + ')">' + json[i] + '</a><br>');
                            }
                        });
                        $(".searchterm").text($("#searchdebox").val());
                        $(".searchterm").append('<br>');
                        $("#suggestions").css('visibility', 'visible');
                        $("#suggestions").hide();
                        $("#suggestions").fadeIn('slow');
                    } else {
                        // hide suggestions
                        $("#suggestions").fadeOut('slow', function () {
                            $("#suggestions").css('visibility', 'hidden');
                        });
                    }
                });
//                $("#searchdebox").focusout(function () {
//                    $("#suggestions").css('visibility', 'hidden');
//                });

            });
            function addToCompare(devid) {
                alert(devid);
                $.post(
                        "Add_to_compare",
                        {id: devid}, //meaasge you want to send
                function (result) {
                    window.location.replace("Compare.jsp");
                });
            }
        </script>

        <div id="comptaple_caomapre" style="min-height: 500px">
            <%                //
                HttpSession hs = request.getSession();
                Java_classes.Compare com8 = (Java_classes.Compare) hs.getAttribute("mycompare");

                if (com8 != null) {
                    List<Java_classes.NewCompareItems> n3 = com8.getArrayData();
                    String id = "";
                    String details = "";
                    for (Java_classes.NewCompareItems ni : n3) {
                        id += ni.getProid() + ",";
                        System.out.print(id);
                    }

//
                    String comproids = id;

                    String comproidsarr[] = comproids.split(",");
                    if (comproidsarr.length < 2) {
            %>
            <div style="padding-top: 10px;padding-bottom: 10px;background-color: greenyellow; margin-bottom: 20px">
                <label  style="font-weight: normal;padding-right: 10px">Search for compare:</label>
                <input style="display: inline-block; width: 140px" type="text" name="suggestions" class="form-control" id="searchdebox"  placeholder="keyword"/>

                <div id="suggestions" class="suggetionsbox">
                    Suggestions for <span class="searchterm"></span>
                </div><br>
            </div>
            <%//          
                    }
//
                }
            %>



            <%  //     

                if (com8 != null) {
            %>
            <div class="whole">
                <div class="plan">
                    <div class="content">
                        <ul class="uls">
                            <li id="catnet">Network</li>
                            <li>Speed</li>
                            <li>GPRS</li>
                            <li>Edge</li>
                            <li>Anounce date</li>
                            <li>Launch status</li>
                            <li>Dimension</li>
                            <li>Weight</li>
                            <li>Sim</li>
                            <li>Screen type</li>
                            <li>Screen body resolution</li>
                            <li>Resolution</li>
                            <li>Pixel density</li>
                            <li>Multi touch</li>
                            <li>Protection</li>
                            <li>Other</li>
                            <li>Platform</li>
                            <li>CPU</li>
                            <li id="catChipset">Chip set</li>
                            <li>GPU</li>
                            <li>Card slot</li>
                            <li>RAM</li>
                            <li>Internal memory</li>
                            <li id="maincamcat">Main camera</li>
                            <li id="videocat">Video </li>
                            <li>Secondary camera</li>
                            <li id="soucat">Sound</li>
                            <li>Loud speakers</li>
                            <li>3.5mm jack</li>
                            <li>Other</li>
                            <li id="catwifi">WLAN</li>
                            <li>Blutooth</li>
                            <li>GFC</li>
                            <li>NFC</li>
                            <li>Radio</li>
                            <li>USB</li>
                            <li id="catsensors">Sensors</li>
                            <li id="catmsg">Other</li>
                            <li>Browser</li>
                            <li>Java Support</li>
                            <li id="catother">Other</li>
                            <li>Battery</li>
                            <li>Fast Charging</li>
                            <li>Stanby Time</li>
                            <li>Call time</li>
                            <li>Color</li>
                        </ul>
                    </div>
                </div>
            </div>
            <%
                List<Java_classes.NewCompareItems> n2 = com8.getArrayData();
                String id = "";
                String details = "";
                for (Java_classes.NewCompareItems ni : n2) {
                    id += ni.getProid() + ",";
                    System.out.print(id);
                }

//
                String comproids = id;

                String comproidsarr[] = comproids.split(",");

                System.out.print("menn id" + id);
                Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();

                if (comproidsarr.length == 2) {
                    hs.removeAttribute("mycompare");
                }

                for (int i = 0; i < comproidsarr.length; ++i) {
                    Criteria ccom = ses.createCriteria(DB.Products.class);
                    ccom.add(Restrictions.eq("id", Integer.parseInt(comproidsarr[i])));
                    DB.Products pr = (DB.Products) ccom.uniqueResult();
            %>
            <div class="whole">
                <div id="model<%=i%>" class="type standard">
                    <p><%=pr.getModel().getManufacturers().getName()%>  <%=pr.getModel().getModel()%></p>
                </div>
                <div class="plan">

                    <div class="content">
                        <ul class="uls">
                            <li id="catimg<%=i%>"><img src="<%=pr.getModel().getImgSrc()%>" width="150px"/><br>
                                <span>Rs</span><%=pr.getPrice()%>.00</li>
                            <li id="networks<%=i%>"><%
                                Integer netid = pr.getModel().getSpecification().getNetwork().getId();
                                DB.Network netlo = (DB.Network) ses.load(DB.Network.class, netid);
                                Criteria crnet = ses.createCriteria(DB.BandsHasNetwork.class);
                                crnet.add(Restrictions.eq("network", netlo));
                                List<DB.BandsHasNetwork> b = crnet.list();
                                for (DB.BandsHasNetwork bn : b) {
                                    DB.NetworkTypes nettylo = (DB.NetworkTypes) ses.load(DB.NetworkTypes.class, bn.getNetworkTypes().getId());
                                    out.print(nettylo.getBands() + "-");
                                    out.print(bn.getBands());
                                    out.print("<br>");
                                }

                                %></li>
                            <li><%=pr.getModel().getSpecification().getNetwork().getSpeed()%></li>
                            <li><%
                                if (pr.getModel().getSpecification().getNetwork().getGprs().equals("1")) {
                                    out.print("Yes");
                                } else {
                                    out.print("no");
                                }
                                %></li>
                            <li><%
                                if (pr.getModel().getSpecification().getNetwork().getEdge().equals("1")) {
                                    out.print("Yes");
                                } else {
                                    out.print("no");
                                }

                                %></li>
                            <li><%=pr.getModel().getSpecification().getLaunch().getAnnouncedDate()%></li>
                            <li><%=pr.getModel().getSpecification().getLaunch().getLaunchStatus().getStat()%></li>
                            <li><%=pr.getModel().getSpecification().getBody().getDimension()%></li>
                            <li><%=pr.getModel().getSpecification().getBody().getWeight()%>g</li>

                            <li><%=pr.getModel().getSpecification().getBody().getSim().getSimType()%>  
                                (<%=pr.getModel().getSpecification().getBody().getSimSize().getSize()%>)</li>
                            <li><%=pr.getModel().getSpecification().getDisplay().getScrnType().getScrntype()%>
                                <%
                                    if (pr.getModel().getSpecification().getDisplay().getTouchType().getTouchType().equals("Non touch")) {
                                        out.print("Non touch");
                                    } else {
                                        out.print(pr.getModel().getSpecification().getDisplay().getTouchType().getTouchType() + " " + "Touch screen");
                                    }
                                    // %>
                            </li>
                            <li><%=pr.getModel().getSpecification().getDisplay().getSize()%> 
                                (<%=pr.getModel().getSpecification().getDisplay().getScrnBodyRatio()%>)</li>
                            <li><%=pr.getModel().getSpecification().getDisplay().getResolution()%></li>
                            <li>(<%=pr.getModel().getSpecification().getDisplay().getPixelDensity()%>ppi)</li>

                            <li><%=pr.getModel().getSpecification().getDisplay().getMultiTouch().getMultiTouch()%></li>
                            <li><%=pr.getModel().getSpecification().getDisplay().getProtection()%></li>
                            <li><%=pr.getModel().getSpecification().getDisplay().getOther()%></li>
                            <li><%=pr.getModel().getSpecification().getPlatformsUse().getVersionUse().getOsList().getOses()%>
                                <%=pr.getModel().getSpecification().getPlatformsUse().getVersionUse().getVersionsUse()%></li>
                            <li><%=pr.getModel().getSpecification().getPlatformsUse().getChipetUse()%></li>

                            <li id="cpu<%=i%>"><%=pr.getModel().getSpecification().getPlatformsUse().getCpuUse()%></li>
                            <li><%=pr.getModel().getSpecification().getPlatformsUse().getGpuUse()%></li>
                            <li><%

                                if (pr.getModel().getSpecification().getMemory().getCardSlot().getId().equals(1)) {
                                    out.print(pr.getModel().getSpecification().getMemory().getCardSlot().getSlot() + " (Up to "
                                            + pr.getModel().getSpecification().getMemory().getCardSlotUpto() + "GB)");
                                } else {
                                    out.print("No");
                                }
                                %></li>
                            <li><%=pr.getModel().getSpecification().getMemory().getRam()%>GB</li>
                            <li><%=pr.getModel().getSpecification().getMemory().getInternel()%>GB</li>

                            <li id="cam<%=i%>"> <p><%=pr.getModel().getSpecification().getCamera().getPixels()%>mp</p>
                                <p>(<%=pr.getModel().getSpecification().getCamera().getFeatures()%>)</p></li>

                            <li id="videocam<%=i%>"><%=pr.getModel().getSpecification().getCamera().getVideo()%></li>
                            <li><%=pr.getModel().getSpecification().getCamera().getSecondery()%></li>
                            <li id="sound<%=i%>"><%

                                Integer souid = pr.getModel().getSpecification().getSound().getId();
                                DB.Sound so = (DB.Sound) ses.load(DB.Sound.class, souid);

                                Criteria cso1 = ses.createCriteria(DB.AlertTypesHasSound.class);
                                cso1.add(Restrictions.eq("sound", so));

                                List<DB.AlertTypesHasSound> alttypelsit = cso1.list();

                                for (DB.AlertTypesHasSound alertTypesHasSound : alttypelsit) {
                                    DB.AlertTypes alt = (DB.AlertTypes) ses.load(DB.AlertTypes.class, alertTypesHasSound.getAlertTypes().getId());
                                    out.print(alt.getAlert());
                                    out.print("<br>");
                                }
                                %></li>

                            <li><%=pr.getModel().getSpecification().getSound().getLoudSpeaker().getType()%></li>
                            <li><%=pr.getModel().getSpecification().getSound().getThreefivveJack().getStatus()%></li>
                            <li><%=pr.getModel().getSpecification().getSound().getOther()%></li>
                            <li id="wifi<%=i%>"><%=pr.getModel().getSpecification().getCommunication().getWlan()%></li>
                            <li><%=pr.getModel().getSpecification().getCommunication().getBluetooth()%></li>

                            <li><%=pr.getModel().getSpecification().getCommunication().getGps()%></li>
                            <li><%=pr.getModel().getSpecification().getCommunication().getNfc().getStatus()%></li>
                            <li><%
                                if (pr.getModel().getSpecification().getCommunication().getRadio().equals("0")) {
                                    out.print("np");
                                } else {
                                    out.print(pr.getModel().getSpecification().getCommunication().getRadio());
                                }
                                %></li>
                            <li><%=pr.getModel().getSpecification().getCommunication().getUsb()%></li>


                            <li id="sensors<%=i%>"><%

                                Integer othf = pr.getModel().getSpecification().getOtherFeatures().getId();
                                DB.OtherFeatures ot = (DB.OtherFeatures) ses.load(DB.OtherFeatures.class, othf);

                                Criteria cothfe = ses.createCriteria(DB.OtherFeaturesHasSensors.class);
                                cothfe.add(Restrictions.eq("otherFeatures", ot));

                                List<DB.OtherFeaturesHasSensors> otherflist = cothfe.list();

                                for (DB.OtherFeaturesHasSensors o : otherflist) {
                                    DB.Sensors sen = (DB.Sensors) ses.load(DB.Sensors.class, o.getSensors().getId());
                                    out.print(sen.getSensor() + ",");

                                }
                                %></li>

                            <li id="msg<%=i%>"><%=pr.getModel().getSpecification().getOtherFeatures().getMesseging()%></li>

                            <li><%=pr.getModel().getSpecification().getOtherFeatures().getBrowser()%></li>
                            <li><%=pr.getModel().getSpecification().getOtherFeatures().getJavaSupport().getStatus()%></li>
                            <li id="otherf<%=i%>"><%=pr.getModel().getSpecification().getOtherFeatures().getOther()%></li>
                            <li><%=pr.getModel().getSpecification().getBattery().getSize()%>(
                                <%=pr.getModel().getSpecification().getBattery().getRemoveble().getStatus()%>)</li>

                            <li><%=pr.getModel().getSpecification().getBattery().getFastChrj().getStat()%></li>
                            <li><%=pr.getModel().getSpecification().getBattery().getStandyTime()%></li>
                            <li><%=pr.getModel().getSpecification().getBattery().getCallTime()%></li>
                            <li><%=pr.getModel().getSpecification().getColor().getColor()%></li>

                        </ul>
                    </div>

                </div>
            </div>
            <%   //

                }
            } else {
            %>
            <div style="padding-top: 10px;padding-bottom: 10px;background-color: greenyellow">
                <label  style="font-weight: normal;padding-right: 10px">Search for compare:</label>
                <input style="display: inline-block; width: 140px" type="text" name="suggestions" class="form-control" id="searchdebox"  placeholder="keyword"/>

                <div id="suggestions" class="suggetionsbox">
                    Suggestions for <span class="searchterm"></span>
                </div><br>
            </div>
            <%
                }
            %>

        </div>
        <script>
            $(document).ready(function () {
//catimg
                if ($('#catimg0').height() > $('#catimg1').height()) {
                    $('#catimg1').height($('#catimg0').height());

                } else {
                    $('#catimg0').height($('#catimg1').height());
                }

//heading
                if ($('#model0').height() > $('#model1').height()) {
                    $('#model1').height($('#model0').height());
                    $('#cat_header').height($('#model0').height());
                } else {
                    $('#model0').height($('#model1').height());
                    $('#cat_header').height($('#model1').height());
                }
//network
                if ($('#networks0').height() > $('#networks1').height()) {
                    $('#networks1').height($('#networks0').height());
                    $('#catnet').height($('#networks0').height());
                } else {
                    $('#networks0').height($('#networks1').height());
                    $('#catnet').height($('#networks1').height());
                }
//chipset
                if ($('#cpu0').height() > $('#cpu1').height()) {
                    $('#cpu1').height($('#cpu0').height());
                    $('#catChipset').height($('#cpu0').height());
                } else {
                    $('#cpu0').height($('#cpu1').height());
                    $('#catChipset').height($('#cpu1').height());
                }
//cam
                if ($('#cam0').height() > $('#cam1').height()) {
                    $('#cam1').height($('#cam0').height());
                    $('#maincamcat').height($('#cam0').height());
                } else {
                    $('#cam0').height($('#cam1').height());
                    $('#maincamcat').height($('#cam1').height());
                }
//videocam
                if ($('#videocam0').height() > $('#videocam1').height()) {
                    $('#videocam1').height($('#videocam0').height());
                    $('#videocat').height($('#videocam0').height());
                } else {
                    $('#videocam0').height($('#videocam1').height());
                    $('#videocat').height($('#videocam1').height());
                }
//sound
                if ($('#sound0').height() > $('#sound1').height()) {
                    $('#sound1').height($('#sound0').height());
                    $('#soucat').height($('#sound0').height());
                } else {
                    $('#sound0').height($('#sound1').height());
                    $('#soucat').height($('#sound1').height());
                }
//wifi
                if ($('#wifi0').height() > $('#wifi1').height()) {
                    $('#wifi1').height($('#wifi0').height());
                    $('#catwifi').height($('#wifi0').height());
                } else {
                    $('#wifi0').height($('#wifi1').height());
                    $('#catwifi').height($('#wifi1').height());
                }
//sensors
                if ($('#sensors0').height() > $('#sensors1').height()) {
                    $('#sensors1').height($('#sensors0').height());
                    $('#catsensors').height($('#sensors0').height());
                } else {
                    $('#sensors0').height($('#sensors1').height());
                    $('#catsensors').height($('#sensors1').height());
                }
//msg
                if ($('#msg0').height() > $('#msg1').height()) {
                    $('#msg1').height($('#msg0').height());
                    $('#catmsg').height($('#msg0').height());
                } else {
                    $('#msg0').height($('#msg1').height());
                    $('#catmsg').height($('#msg1').height());
                }
//otherf
                if ($('#otherf0').height() > $('#otherf1').height()) {
                    $('#otherf1').height($('#otherf0').height());
                    $('#catother').height($('#otherf0').height());
                } else {
                    $('#otherf0').height($('#otherf1').height());
                    $('#catother').height($('#otherf1').height());
                }
            });
        </script>
        <%@include file="Footer.jsp" %>
    </body>
</html>
