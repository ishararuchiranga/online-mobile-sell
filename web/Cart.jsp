<%-- 
    Document   : Cart
    Created on : Dec 15, 2015, 10:52:21 PM
    Author     : Ishara
--%>
<%@page import="java.util.Collections"%>
<script>
    window.onpageshow = function (evt) {
        // If persisted then it is in the page cache, force a reload of the page.
        if (evt.persisted) {
            document.body.style.display = "none";
            location.reload();
        }
    };
    var url = window.location.href;
    var urlparts = url.split('#');
    var b;
    if (urlparts[1] == b) {
    } else {
        console.log("history back");
        history.back();
    }
</script>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache">

        <link rel="shortcut icon" type="image/x-icon" href="images/home/Slider1.jpg" />
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="css/navibar.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/typicons.min.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/price-range.css" rel="stylesheet">

        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/script.js"></script>
    </head>
    <body>
        <style>
            body{
                background-image: url("images/geometry2.png");
            }
        </style>
        <%@include file="Header2.jsp" %>
        <div class="container bodycont" style="min-height: 450px; padding-top: 15px;">
            <section id="cart_items">
                <div >

                    <%                        double tot = 0.0;
                        DecimalFormat df = new DecimalFormat("0.00");

                        if (seshead.getAttribute("mycart") != null) {

                            Java_classes.Cart c = (Java_classes.Cart) seshead.getAttribute("mycart");
                            List<Java_classes.NewCartItems> n = c.getArrayData();

                            if (!(n.isEmpty())) {

                    %>
                    <div class="table-responsive cart_info">

                        <table id="table" class="table table-condensed">
                            <thead>
                                <tr class="cart_menu">
                                    <td class="image" style="border-bottom-left-radius: 20px;
                                        border-color: black;
                                        border-top-left-radius: 20px;">Item</td>
                                    <td class="description"></td>
                                    <td class="price">Price</td>
                                    <td class="quantity">Quantity</td>
                                    <td class="total">Total</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <%//
                                    for (Java_classes.NewCartItems ni : n) {

                                        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
                                        Criteria cri = ses.createCriteria(DB.Products.class);
                                        cri.add(Restrictions.eq("id", ni.getProid()));
                                        DB.Products pr = (DB.Products) cri.uniqueResult();
                                        tot += ni.getProQty() * pr.getPrice();

                                %>

                                <tr id="<%=ni.getProid()%>">
                                    <td class="cart_product">
                                        <a href=""><img width="100px" src="<%=pr.getModel().getImgSrc()%>" alt=""></a>
                                    </td>
                                    <td class="cart_description">
                                        <h4><a href="Product_details.jsp?id=<%=ni.getProid()%>"><%=pr.getModel().getManufacturers().getName()%> <%=pr.getModel().getModel()%></a></h4>
                                        <p><%
                                            if (pr.getModel().getDeviceType().getId() == 1) {
                                            %> <p class="typcn typcn-device-phone">Smart phone</p><%
                                            } else if (pr.getModel().getDeviceType().getId() == 2) {
                                        %><p class="typcn typcn-device-tablet">Tabs</p> <%
                                        } else if (pr.getModel().getDeviceType().getId() == 3) {
                                        %><p class="typcn typcn-headphones">Accesories</p> <%
                                        } else {
                                        %><p class="typcn typcn-time"> Smart watches</p> <%
                                            }
                                        %></p>
                                    </td>
                                    <td class="cart_price">
                                        <p>Rs.<%
                                            out.print(df.format(pr.getPrice()));
                                            %></p>
                                    </td>
                                    <td class="cart_quantity">
                                        <div class="cart_quantity_button">
                                            <a class="cart_quantity_up" onclick="addqty('<%=pr.getId()%>')"> + </a>
                                            <input id="qtybox<%=pr.getId()%>" disabled="" class="cart_quantity_input" name="quantity" value="<%=ni.getProQty()%>" autocomplete="off" size="2" type="text">
                                            <a class="cart_quantity_down" onclick="decreaseqty('<%=pr.getId()%>')"> - </a>
                                            <br>
                                            <br>
                                        </div>
                                        <span class="hideitems" id="cantincqty<%=pr.getId()%>" style="color: red;">Can't add more. Stock is low.</span>
                                        <span class="hideitems" id="cantdecqty<%=pr.getId()%>" style="color: red;">Quantity can't be 0</span>
                                    </td>

                                    <td class="cart_total">
                                        <input type="hidden" id="price0" value="<%=ni.getProQty() * pr.getPrice()%>">
                                        <p class="cart_total_price" id="price<%=ni.getProid()%>">Rs.<%
                                            out.print(df.format(ni.getProQty() * pr.getPrice()));
                                            %></p>
                                    </td>
                                    <td class="cart_delete">
                                        <a class="cart_quantity_delete" onclick="remo('<%=ni.getProid()%>')" ><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                <%          //
                                    }
                                %>
                            </tbody>
                        </table>
                        <script>
                            function remo(a) {
                                var avilcartcou = $('#cartcount').html();
                                if (avilcartcou.toString() == "1") {
                                    $('#cartcount').addClass("hideitems");
                                    $('#checkoutbtn').addClass("disableda");
                                } else {
                                    var newcartcou = Number(avilcartcou);
                                    var lastcou = newcartcou - 1;
                                    $('#cartcount').html(lastcou);
                                }

                                $('#' + a).hide();
//                                var np1 = 0;
//                                np1 = document.getElementById('price').value;
//                                var np2 = 0;
//                                np2 = document.getElementById('price1').value;
//                                np3 = np2 - np1;
//
//
//                                document.getElementById('price1set').innerHTML = "Rs." + np3 + "00";
//                                document.getElementById('price2set').innerHTML = "Rs." + np3 + "00";



                                $.post(
                                        "Remove_from_cart",
                                        {
                                            id: a
                                        },
                                function (result) {
                                    var out = result.split(",");
                                    $('#price1set').html("Rs." + out[1]);
                                    $('#price2set').html("Rs." + out[1]);
                                });
                            }
                            function addqty(id) {
                                $.post(
                                        "increase_cart_qty",
                                        {
                                            id: id
                                        },
                                function (res, ass) {
                                    var out = res.split(",");

                                    if (out[0] == "1") {
                                        $('#cantincqty' + id).show();
                                        setTimeout(
                                                function ()
                                                {
                                                    $('#cantincqty' + id).hide();
                                                }, 4000);
                                    } else {
                                        var avail = $('#qtybox' + id).val();
                                        var newq = parseInt(avail) + 1;
                                        $('#qtybox' + id).val(newq);
                                        $('#price' + id).html("Rs." + out[1]);
                                        $('#price1set').html("Rs." + out[2]);
                                        $('#price2set').html("Rs." + out[2]);

                                    }

                                });
                            }
                            function decreaseqty(id) {
                                var nowqty = $('#qtybox' + id).val();
//                                alert(nowqty);
                                if (parseInt(nowqty) !== 1) {
                                    $.post(
                                            "decrease_cart_qty",
                                            {
                                                id: id
                                            },
                                    function (res) {
                                        var out = res.split(",");

                                        if (out[0] == "1") {
                                            $('#cantdecqty' + id).show();
                                            setTimeout(
                                                    function () {
                                                        $('#cantdecqty' + id).hide();
                                                    },
                                                    2000);
                                        } else {
                                            var avail = $('#qtybox' + id).val();
                                            var newq = parseInt(avail) - 1;
                                            $('#qtybox' + id).val(newq);


                                            $('#price' + id).html("Rs." + out[1]);
                                            $('#price1set').html("Rs." + out[2]);
                                            $('#price2set').html("Rs." + out[2]);
                                        }
                                    });
                                } else {
                                    $('#cantdecqty' + id).show();
                                    setTimeout(
                                            function () {
                                                $('#cantdecqty' + id).hide();
                                            },
                                            2000);
                                }

                            }
                        </script>
                    </div>
                    <%
                    } else {
                    %>
                    <tr>
                        <td colspan='5'>
                            <div class='nothingfound'>
                                <h3>cart empty</h3>
                            </div>
                        </td>
                    </tr>
                    <%
                        }
                    } else {

                    %>
                    <tr>
                        <td colspan='5'>
                            <div class='nothingfound'>
                                <h3>cart empty</h3>
                            </div>
                        </td>
                    </tr>
                    <%                    }
                        System.gc();
                    %>

                </div>
            </section>
            <section id="do_action">
                <div class="container">
                    <!--                    <div class="heading">
                                            <h3>What would you like to do next?</h3>
                                            <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
                                        </div>-->
                    <div class="row">
                        <div class="col-sm-6 floatright cartpricebox">
                            <div class="total_area">
                                <ul>

                                    <input type="hidden" id='price1' value="<%=tot%>">
                                    <li>
                                        Cart Sub Total
                                        <span id="price1set">Rs.
                                            <%
                                                out.print(df.format(tot));
                                            %>
                                        </span>
                                    </li>
                                    <li>Promotions<span>-</span></li>
                                    <li>Total <span id="price2set">Rs.<%
                                        out.print(df.format(tot));
                                            %></span></li>
                                </ul>
                                <div class="textalignright">
                                    <a class="update" href="products.jsp">Update</a>
                                    <%
                                        if (seshead.getAttribute("Login_object") == null) {
                                    %>
                                    <a class="check_out" href="#opensignin" id="checkobtn" ><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;&nbsp;Check Out</a>
                                    <%
                                    } else {
                                    %>
                                    <a id="checkoutbtn" class="check_out" href="Checkout.jsp">Check Out</a>
                                    <%
                                        }
                                    %>
                                    <script>
                                        
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

        </div>
        <%@include file="Footer.jsp" %>
    </body>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>  
</html>
