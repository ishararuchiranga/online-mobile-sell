<%-- 
    Document   : Admin_shipping4
    Created on : Jul 8, 2016, 3:29:33 PM
    Author     : Ishara
--%>

<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/Adminmain.css" rel="stylesheet">
        <script src="js/jquery-2.1.4.min.js"></script>
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
            Criteria cr = ses.createCriteria(DB.District.class);
            cr.addOrder(Order.asc("id"));
            List<DB.District> dis = cr.list();

            Criteria cr2 = ses.createCriteria(DB.CostType.class);
            cr2.addOrder(Order.asc("id"));
            List<DB.CostType> cos = cr2.list();
        %>
        <table class="table-fill">
            <thead>
                <tr>
                    <th class="text-left"></th>
                    <th class="text-left">District</th>
                    <th class="text-left">Cost type</th>
                    <th class="text-left">Cost</th>
                    <!--<th class="text-left"></th>-->
                </tr>
            </thead>
            <tbody class="table-hover">
                <%                    for (DB.District d : dis) {
                %>
                <tr>
                    <td class="text-left"><%=d.getId()%></td>
                    <td class="text-left"><%=d.getDistrict()%></td>
                    <td class="text-left costypecol">
                        <%
                            if (d.getCostType().getId() == 1) {

                        %>
                        <p style="color: green; float: left" id="cost<%=d.getId()%>" class="pmargin"><%=d.getCostType().getCostType()%></p>

                        <%
                        } else {

                        %>
                        <p style="color: red; float: left" id="cost<%=d.getId()%>" class="pmargin"><%=d.getCostType().getCostType()%></p>
                        <%
                            }

                        %>

                        <select id="costtypes<%=d.getId()%>" class="hiden">
                            <%
                                for (DB.CostType co : cos) {
                            %>
                            <option value="<%=co.getId()%>"><%=co.getCostType()%></option>
                            <%
                                }
                            %>
                        </select>

                        <div class="edit_options">
                            <a id="editcosttype<%=d.getId()%>" class="" onclick="editcosttype(<%=d.getId()%>)"><span class="fa fa-edit"></span></a>
                            <a id="savecosttype<%=d.getId()%>" class="hiden edit_optionsa3" onclick="savecosttype(<%=d.getId()%>)"><span class="fa fa-save"></span></a>
                            <a id="cancelcosttype<%=d.getId()%>" class="hiden edit_optionsa2" onclick="cancelcosttype(<%=d.getId()%>)"><span class="fa fa-times-circle-o"></span></a>
                        </div>
                    </td>
                    <td class="text-left" style="width: 22%">
                        <p id="costp<%=d.getId()%>" style="float: left"><%
                            if (d.getCostType().getId() == 2) {
                                out.print("-");
                            } else {
                                out.print(d.getCost() + ".00");
                            }
                            %></p>

                        <%
                            if (d.getCostType().getId() == 2) {
                        %>
                        <input id="costtf<%=d.getId()%>" onkeydown="return isNumber(event)" class="hiden new-cost-input" type="text" placeholder="New cost">
                        <div id="editdiv<%=d.getId()%>" class="edit_options2 hiden">
                            <a id="editcost<%=d.getId()%>" class="" onclick="editcost(<%=d.getId()%>)"><span class="fa fa-edit"></span></a>
                            <a id="savecost<%=d.getId()%>" class="hiden edit_optionsa3" onclick="savecostandtype(<%=d.getId()%>)"><span class="fa fa-save"></span></a>
                            <a id="cancelcost<%=d.getId()%>" class="hiden edit_optionsa2" onclick="cancelcost2(<%=d.getId()%>)"><span class="fa fa-times-circle-o"></span></a>
                        </div>
                        <%
                        } else {
                        %>
                        <input id="costtf<%=d.getId()%>" onkeydown="return isNumber(event)" class="hiden new-cost-input" type="text" placeholder="New cost">
                        <div class="edit_options2">
                            <a id="editcost<%=d.getId()%>" class="" onclick="editcost(<%=d.getId()%>)"><span class="fa fa-edit"></span></a>
                            <a id="savecost<%=d.getId()%>" class="hiden edit_optionsa3" onclick="savecost(<%=d.getId()%>)"><span class="fa fa-save"></span></a>
                            <a id="cancelcost<%=d.getId()%>" class="hiden edit_optionsa2" onclick="cancelcost(<%=d.getId()%>)"><span class="fa fa-times-circle-o"></span></a>
                        </div>
                        <%
                            }
                        %>

                        <!--<a style="float: right; margin-right: 74%" href=""><span class="fa fa-edit"></span></a>-->
                    </td>

                </tr>
                <%
                    }
                %>
            </tbody>
        </table>
        <script>
            function editcost(id) {
                $('#costp' + id).hide();
                $('#costtf' + id).show();
                $('#editcost' + id).hide();
                $('#savecost' + id).show();
                $('#cancelcost' + id).show();
            }
            function cancelcost(id) {
                $('#costp' + id).show();
                $('#costtf' + id).hide();
                $('#editcost' + id).show();
                $('#savecost' + id).hide();
                $('#cancelcost' + id).hide();
            }
            function savecost(id) {
                var newcost = $('#costtf' + id).val();
                $.post(
                        "Admin_shipping_change_cost",
                        {id: id, cost: newcost},
                function (result) {
                    $('#costp' + id).html(result+".00");
                    $('#costp' + id).show();
                    $('#costtf' + id).hide();
                    $('#editcost' + id).show();
                    $('#savecost' + id).hide();
                    $('#cancelcost' + id).hide();
                });
            }
            function editcosttype(id) {
                $('#cost' + id).hide();
                $('#costtypes' + id).show();
                $('#editcosttype' + id).hide();
                $('#savecosttype' + id).show();
                $('#cancelcosttype' + id).show();
            }
            function cancelcosttype(id) {
                $('#cost' + id).show();
                $('#costtypes' + id).hide();
                $('#editcosttype' + id).show();
                $('#savecosttype' + id).hide();
                $('#cancelcosttype' + id).hide();
            }
            function savecosttype(id) {
                var type = $('#costtypes' + id).find(":selected").val();
                if (type == "1") {
                    $('#savecosttype' + id).hide();
                    $('#cancelcosttype' + id).hide();
                    $('#costp' + id).hide();
                    $('#costtf' + id).show();
                    $('#editdiv' + id).show();
                    $('#editcost' + id).hide();
                    $('#savecost' + id).show();
                    $('#cancelcost' + id).show();

                } else {
                    $.post(
                            "Admin_shipping_change_cost_type",
                            {id: id, type: type},
                    function (result) {
                        location.reload();
                    });
                }
            }
            function savecostandtype(id) {
                var type = $('#costtypes' + id).find(":selected").val();
                var newcost = $('#costtf' + id).val();
                $.post(
                        "Admin_shipping_free_to_nofree",
                        {id: id, type: type, cost: newcost},
                function (result) {
                    if (result == 1) {
                        location.reload();
                    }
//                    $('#costp' + id).html(result);
//                    $('#costp' + id).show();
//                    $('#costtf' + id).hide();
//                    $('#editcost' + id).show();
//                    $('#savecost' + id).hide();
//                    $('#cancelcost' + id).hide();
                });

            }
            function cancelcost2(id) {
                $('#cost' + id).show();
                $('#costtypes' + id).hide();
                $('#editcosttype' + id).show();
                $('#savecosttype' + id).hide();
                $('#cancelcosttype' + id).hide();


                $('#costp' + id).show();
                $('#costtf' + id).hide();
                $('#editdiv' + id).hide();
                $('#editcost' + id).show();
                $('#savecost' + id).hide();
                $('#cancelcost' + id).hide();
            }
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 90 || charCode > 105)) {
                    return false;
                }
                return true;
            }

        </script>
    </body>
</html>
