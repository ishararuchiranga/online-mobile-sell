<%-- 
    Document   : logintest.jsp
    Created on : Jun 22, 2016, 5:41:42 PM
    Author     : Ishara
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="shortcut icon" type="image/x-icon" href="images/home/Slider1.jpg" />
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="css/navibar.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/typicons.min.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/price-range.css" rel="stylesheet">


        <link href="css/admin.css" rel="stylesheet">
        <link href="css/style_1.css" rel="stylesheet">
        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/script.js"></script>
        <script src="js/main.js"></script>
    </head>
    <body >
        <%@include file="Header2.jsp" %>
    <body>
        <style>

        </style>

        <div id="wrapper"  class="loginf">
            <div class="join">Hey Welcome Back</div>
            <div class="lock"></div>
            <div class="clr"></div>

            <div class="clr"><hr></div>
            <div class="mail-text">Sign in using your email</div>
            <div class="forms">
                <form action="" method="get" name="register">
                    <input name="email" placeholder="Enter your email address..." size="70" onclick="border: 1px solid #30a8da;" id="mail" type="text">
                    <input name="password" placeholder="Enter your password..." size="70" onclick="border: 1px solid #30a8da;" id="password" type="password">
                </form>
            </div>
            <input class="showpasswordcheckbox"  type="checkbox">
            <label class="keepmelog">Keep me login</label>
            <a  class="log-btn">LOGIN</a>

            <div class="alternatelogin">
                <a id="opensigup" onclick="opensignup()" class="btn btn-link addblackcolor" >Sign up</a>
                <label class="or-label">or</label>
                <a href="#fgtpwrd" class="btn btn-link addRedcolor">Forgot Password</a>    
            </div>

        </div>
        <div id="wrapper1"  style="display: none"  class="signupf">
            <div class="join">Sign up with us</div>
            <div class="lock"></div>
            <div class="clr"></div>

            <div class="clr"><hr></div>
            <div class="mail-text">Sign up using your email address..</div>
            <div class="forms">
                <form action="" method="get" name="register">
                    <input name="fullname" placeholder="Enter your user name"  size="70" onclick="border: 1px solid #30a8da;" id="username" type="text">
                    <input name="email" placeholder="Enter your email address"  size="70" onclick="border: 1px solid #30a8da;" id="mail" type="text">
                    <input name="password" placeholder="Enter your password"  size="70" onclick="border: 1px solid #30a8da;" id="password" type="password">
                    <input name="password" placeholder="Retype your password"  size="70" onclick="border: 1px solid #30a8da;" id="password" type="password">
                </form>
            </div>
            <input name="submit" value="Log in" type="button">
            <div class="alternatelogin2">
                <a id="opensigin" onclick="opensignin()" class="btn btn-link addblackcolor" >Signin</a>
                <label class="or-label">or</label>
                <a href="#fgtpwrd" class="btn btn-link addRedcolor">Forgot Password</a>    
            </div>
        </div>

        <script>
                    function opensignup(){
                    $('#wrapper').hide();
                            $('#wrapper1').animate({ "opacity": "show", top:"100"}, 500);
                    }
            function opensignin(){
            $('#wrapper1').hide();
                    $('#wrapper1')..animate({ "opacity": "show", top:"100"}, 500);
            }

        </script>


        <%@include file="Footer.jsp" %>
    </body>
    <script src = "js/jquery.js" ></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script> 
</html>
