<%-- 
    Document   : Checkout
    Created on : Jan 26, 2016, 10:32:26 PM
    Author     : Ishara
--%>

<%@page import="java.text.DecimalFormat"%>
<%
    Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
    HttpSession htses = request.getSession();
    System.out.print("err1");
    if (htses.getAttribute("Login_object") != null) {
        System.out.print("err2");
        DB.UserRegistration ure = (DB.UserRegistration) htses.getAttribute("Login_object");

        DB.UserRegistration ure2 = (DB.UserRegistration) ses.load(DB.UserRegistration.class, ure.getId());
        if (ure2.getUserDetails() != null) {

            if (ure2.getUserDetails().getAdLine1() == null || ure2.getUserDetails().getAdLine1() == "") {
                htses.setAttribute("plzcomplete", "1");
                out.print("sss");
                response.sendRedirect("Profile_Account_details.jsp");
            } else if (ure2.getUserDetails().getFname() == null || ure2.getUserDetails().getFname() == "") {
                htses.setAttribute("plzcomplete", "1");
                out.print("sss");
                response.sendRedirect("Profile_Account_details.jsp");
            } else if (ure2.getUserDetails().getPhone() == null || ure2.getUserDetails().getPhone() == "") {
                htses.setAttribute("plzcomplete", "1");
                out.print("sss");
                response.sendRedirect("Profile_Account_details.jsp");
            } else {
%>
<%
            }
        }
    }


%>
<script>
    window.onpageshow = function (evt) {
        // If persisted then it is in the page cache, force a reload of the page.
        if (evt.persisted) {
            document.body.style.display = "none";
            location.reload();
        }
    };
    var url = window.location.href;
    var urlparts = url.split('#');
    var b;
    if (urlparts[1] == b) {
    } else {
        console.log("history back");
        history.back();
    }
</script>

<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache">

        <link rel="shortcut icon" type="image/x-icon" href="images/home/Slider1.jpg" />
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="css/navibar.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/typicons.min.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/price-range.css" rel="stylesheet">


        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/script.js"></script>
    </head>
    <body onload="loadshipdetails()">
        <style>
            body{
                background-image: url("images/geometry2.png");
            }
        </style>
        <%@include file="Header2.jsp" %>
        <div style="min-height: 400px;">

            <%//
                DecimalFormat df = new DecimalFormat("0.00");
                if (htses.getAttribute("mycart") != null) {
                    boolean checkdetails = false;
                    if (htses.getAttribute("Login_object") != null) {
            %>


            <section id="cart_items">
                <div class="container bodycont">
                    <div class="breadcrumbs">
                        <ol class="breadcrumb">
                            <li><a href="index.jsp">Home</a></li>
                            <li class="active">Check out</li>
                        </ol>
                    </div><!--/breadcrums-->               



                    <%             //
                        DB.UserRegistration urx = (DB.UserRegistration) htses.getAttribute("Login_object");
                        DB.UserRegistration urx2 = (DB.UserRegistration) ses.load(DB.UserRegistration.class, urx.getId());

                        if (urx2.getUserDetails() != null) {
                            checkdetails = true;
                        } else {
                            System.out.print("err3");
                            checkdetails = false;
                    %>
                    <div id="deliverinfo" class="">
                        <div class="clearfix ">
                            <div class="bill-to">
                                <div class="form-one" style=" width: 55%;text-align: center;margin-left: auto;margin-right: auto;float: none;">
                                    <p class="color-red" style="color: red">Please Enter Your details before continue</p>

                                    <form id="form1" action="Save_user_details" method="post">
                                        <input type="hidden" value="<%=urx2.getId()%>" name="id">
                                        <input placeholder="Email" value="<%=urx2.getEmail()%>" readonly="readonly" type="text">
                                        <input name="fname" placeholder="First Name *" value="" type="text" id="sid">
                                        <input name="lname" placeholder="Last Name *" type="text" value="" id="slname">
                                        <input name="phn" placeholder="Mobile number*" type="text" value="" id="sphn">
                                        <input name="l1" placeholder="Line 1*" type="text"  value="" id="sl1">
                                        <input name="l2" placeholder="Line 2*" type="text" id="sla2">
                                        <input name="city" placeholder="City*" type="text" id="scity">
                                        <select id="sprovl">
                                            <option value="0">Please select a province</option>
                                            <%
                                                Criteria dis1 = ses.createCriteria(DB.District.class);
                                                dis1.addOrder(Order.asc("id"));
                                                List<DB.District> dislist = dis1.list();
                                                for (DB.District d : dislist) {
                                            %>
                                            <option  value="<%=d.getId()%>"><%=d.getDistrict()%></option>
                                            <%
                                                }

                                            %>
                                            <script>
                                                $(document).ready(function () {
                                                    $('#anotherdislist option[value="0"]').attr('selected', 'selected');
                                                });</script>

                                        </select>
                                        <!--                                            <input name="province" placeholder="Province*" type="text" id="sprov">-->
                                    </form>
                                    <p id="swarn" style="color: red" class="hideitems">Please Enter all details before continue</p>
                                    <input type="button" onclick="saveuserdetails()" form="form1" value="Save" class="btn btn-default checkoutsave align-right">
                                    <script>
                                        function saveuserdetails() {
                                            var fname = $('#sid').val();
                                            var lname = $('#slname').val();
                                            var phn = $('#sphn').val();
                                            var l1 = $('#sl1').val();
                                            var l2 = $('#sla2').val();
                                            var city = $('#scity').val();
                                            var dis = $('#sprovl').find(":selected").val();
                                            if (dis != 0) {
                                                if (fname == "" || lname == "" || phn == "" || l1 == "" || l2 == "" || city == "" || dis == "") {
                                                    $('#swarn').show();
                                                } else {
                                                    $.post(
                                                            "Save_user_details",
                                                            {
                                                                fname: fname,
                                                                lname: lname,
                                                                l1: l1,
                                                                l2: l2,
                                                                city: city,
                                                                dis: dis,
                                                                phn: phn
                                                            },
                                                    function (result) {
                                                        window.location.replace('Checkout.jsp');
                                                    });
                                                }
                                            } else {
                                                $('#swarn').show();
                                            }
                                        }
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%                        }

                        if (checkdetails) {
                    %>


                    <div id="tablec" class="table-responsive cart_info">
                        <table class="table table-condensed">
                            <thead>
                                <tr class="cart_menu">
                                    <td class="image">Item</td>
                                    <td class="description"></td>
                                    <td class="price">Price</td>
                                    <td class="quantity">Quantity</td>
                                    <td class="total">Total</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <% double tot = 0;
                                    if (htses.getAttribute("mycart") != null) {

                                        Java_classes.Cart c = (Java_classes.Cart) htses.getAttribute("mycart");
                                        List<Java_classes.NewCartItems> n = c.getArrayData();

                                        for (Java_classes.NewCartItems ni : n) {

                                            Criteria cri = ses.createCriteria(DB.Products.class);
                                            cri.add(Restrictions.eq("id", ni.getProid()));
                                            DB.Products pr = (DB.Products) cri.uniqueResult();
                                            tot += ni.getProQty() * pr.getPrice();
                                %>
                                <tr id="tr<%=pr.getId()%>">

                                    <td class="cart_product">
                                        <a href=""><img width="60px" src="<%=pr.getModel().getImgSrc()%>" alt=""></a>
                                    </td>
                                    <td class="cart_description">
                                        <h4><a href="Product_details.jsp?id=<%=pr.getId()%>"><%=pr.getModel().getManufacturers().getName()%>
                                                <%=pr.getModel().getModel()%></a></h4>
                                        <p>Web ID: 1089772</p>
                                    </td>
                                    <td class="cart_price">
                                        <p>Rs.<% out.print(df.format(pr.getPrice()));%></p>
                                    </td>
                                    <td class="cart_quantity" id="wat<%=pr.getId()%>">
                                        <div class="cart_quantity_button">
                                            <p><%=ni.getProQty()%></p>
                                        </div>
                                    </td>
                                    <td class="cart_total">
                                        <input type="hidden" value="<%=ni.getProQty() * pr.getPrice()%>" id="price0">
                                        <p class="cart_total_price">Rs.<% out.print(df.format(ni.getProQty() * pr.getPrice()));%></p>
                                    </td>
                                    <td class="cart_delete">
                                        <a class="cart_quantity_delete" onclick="remo('<%=ni.getProid()%>')" ><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>

                                <%                                    }


                                %>
                                <tr>
                                    <td colspan="3">&nbsp;

                                        <table>
                                            <tr id="emailtpechoose"> 
                                                <td style="width: 75px"><h4>Ship to</h4></td>
                                                <td><label class="radio-inline"><input id="radio1"  type="radio" name="gprs" value="1" onchange="loadshipdetails()">To me</label></td>
                                                <td><label class="radio-inline"><input id="radio2" type="radio" name="gprs" value="2" onchange="loadshipdetails2()">To another</label></td>

                                            </tr>
                                        </table>

                                        <%                  //
                                            DB.UserRegistration ur = (DB.UserRegistration) htses.getAttribute("Login_object");
                                            DB.UserRegistration ur2 = (DB.UserRegistration) ses.load(DB.UserRegistration.class, ur.getId());

                                            if (ur2.getUserDetails() != null) {
                                                //error found
%>
                                        <div id="deliverinfo" class=" hideitems">
                                            <div class="clearfix ">
                                                <div class="bill-to">
                                                    <div class="form-one "style=" width: 55%;text-align: center;margin-left: auto;margin-right: auto;float: none; ">
                                                        <form>
                                                            <input disabled name="fname" id="fname1" placeholder="First Name *" value="<%=ur2.getUserDetails().getFname()%>" type="text">
                                                            <input disabled name="lname" id="lname1" placeholder="Last Name *" type="text" value="<%=ur2.getUserDetails().getLname()%>">
                                                            <input disabled name="phn" id="phn1" placeholder="Mobile number*" type="text" value="<%=ur2.getUserDetails().getPhone()%>">
                                                            <input disabled name="l1" id="l11" placeholder="Line 1*" type="text"  value="<%=ur2.getUserDetails().getAdLine1()%>">
                                                            <input disabled name="l2" id="l21" placeholder="Line 2*" type="text" value="<%=ur2.getUserDetails().getAdLine2()%>">
                                                            <input disabled name="city" id="city1" placeholder="City*" type="text" value="<%=ur2.getUserDetails().getCity()%>">
                                                            <input disabled name="province" id="provshow" placeholder="Province*" type="text" value="<%if (ur2.getUserDetails().getDistrict() != null) {
                                                                    out.print(ur2.getUserDetails().getDistrict().getDistrict());
                                                                }%>">
                                                            <input disabled name="province" id="prov1" placeholder="Province*" type="hidden" value="<%
                                                                if (ur2.getUserDetails().getDistrict() != null) {
                                                                    out.print(ur2.getUserDetails().getDistrict().getId());
                                                                }

                                                                   %>">
                                                        </form>
                                                        <p id="warn" style="color: red" class="hideitems">Please Enter all details before continue</p>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <%                        } else {


                                        %>

                                        <%                            }
                                        %>
                                        <div id="deliverinfo2" class=" hideitems">
                                            <div class="clearfix ">
                                                <div class="bill-to">
                                                    <div class="form-one" style=" width: 55%;text-align: center;margin-left: auto;margin-right: auto; float: none;">
                                                        <%
                                                            Criteria c2 = ses.createCriteria(DB.Delivery.class);
                                                            c2.add(Restrictions.eq("userRegistration", ur));
                                                            c2.addOrder(Order.desc("id"));
                                                            List<DB.Delivery> dl = c2.list();
                                                            if (!(dl.isEmpty())) {
                                                                outer:
                                                                for (DB.Delivery dd : dl) {
                                                        %>
                                                        <form>
                                                            <input id="sfname" placeholder="First Name *" value="<%=dd.getFname()%>" type="text">
                                                            <input id="slname" placeholder="Last Name *" type="text" value="<%=dd.getLname()%>">
                                                            <input id="sphn" placeholder="Mobile number*" type="text" value="<%=dd.getPhn()%>">
                                                            <input id="sadl1" placeholder="Line 1*" type="text"  value="<%=dd.getAd1()%>">
                                                            <input id="sadl2" placeholder="Line 2*" type="text" value="<%=dd.getAd2()%>">
                                                            <input id="scity" placeholder="City*" type="text" value="<%=dd.getCity()%>">

                                                            <select id="anotherdislist">
                                                                <%
                                                                    Criteria dis1 = ses.createCriteria(DB.District.class);
                                                                    dis1.addOrder(Order.asc("id"));
                                                                    List<DB.District> dislist = dis1.list();
                                                                    for (DB.District d : dislist) {
                                                                %>
                                                                <option  value="<%=d.getId()%>"><%=d.getDistrict()%></option>
                                                                <%
                                                                    }

                                                                %>
                                                                <script>
                                                                    $(document).ready(function () {
                                                                        $('#anotherdislist option[value="<%=dd.getDistrict().getId()%>"]').attr('selected', 'selected');
                                                                    });</script>

                                                            </select>

                                            <!--<input id="sprov" placeholder="District*" type="text" value="<%=dd.getDistrict().getDistrict()%>">-->


                                                            <p id="warn2" style="color: red" class="hideitems">Please Enter all details before continue</p>
                                                        </form>
                                                        <%
                                                                break outer;
                                                            }

                                                        } else {
                                                        %>
                                                        <form>
                                                            <input id="sfname" name="fname" placeholder="First Name *" value="" type="text">
                                                            <input id="slname" placeholder="Last Name *" type="text" value="">
                                                            <input id="sphn" placeholder="Mobile number*" type="text" value="">
                                                            <input id="sadl1" name="l1" placeholder="Line 1*" type="text"  value="">
                                                            <input id="sadl2" name="l2" placeholder="Line 2*" type="text" value="">
                                                            <input id="scity" name="city" placeholder="City*" type="text" value="">
                                                            <select id="anotherdislist">
                                                                <option value="0">Select a district</option>
                                                                <%
                                                                    Criteria dis1 = ses.createCriteria(DB.District.class);
                                                                    dis1.addOrder(Order.asc("id"));
                                                                    List<DB.District> dislist = dis1.list();
                                                                    for (DB.District d : dislist) {
                                                                %>
                                                                <option  value="<%=d.getId()%>"><%=d.getDistrict()%></option>
                                                                <%
                                                                    }
                                                                %>
                                                                <script>
                                                                    $(document).ready(function () {
                                                                        $('#anotherdislist option[value="0"]').attr('selected', 'selected');
                                                                    });</script>

                                                            </select>
                                                            <p id="warn2" style="color: red" class="hideitems">Please Enter all details before continue</p>
                                                        </form>
                                                        <%                                            }
                                                        %>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="2">
                                        <table class="table table-condensed total-result">
                                            <tbody><tr>
                                                    <td>Cart Sub Total</td>
                                                    <td id="price2set">Rs.<% out.print(df.format(tot));%>
                                                        <input type="hidden" value="<%=tot%>" id="price1">
                                                    </td>
                                                </tr>

                                                <%
                                                    if (urx2.getUserDetails() != null) {
                                                        if (urx2.getUserDetails().getDistrict() != null) {

                                                            if (urx2.getUserDetails().getDistrict().getCostType().getId() == 1) {
                                                                tot += urx2.getUserDetails().getDistrict().getCost();

                                                %>
                                                <tr class="shipping-cost">
                                                    <td>Shipping Cost</td>
                                                    <td id="shiipigfee">Rs.<%out.print(df.format(urx2.getUserDetails().getDistrict().getCost())); %></td>										
                                                </tr>
                                                <%                                    } else {
                                                %>
                                                <tr class="shipping-cost">
                                                    <td>Shipping Cost</td>
                                                    <td id="shiipigfee">Free</td>										
                                                </tr>
                                                <%
                                                            }
                                                        }
                                                    }
                                                %>


                                                <tr>
                                                    <td>Total</td>
                                                    <td><span id="price1set"> Rs.<%out.print(df.format(tot));%></span></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <button class="confirmaddressbtn" onclick="confirmaddress()">Confirm Delivery Address</button>
                                                        <button id="buybtn" type='button' style="margin-left: 0px;" onclick='buyproduct()' class='btn btn-fefault cart hideitems'> Buy&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-shopping-cart buybtnin'></i>&nbsp;&nbsp;</button>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                                <%
                                    } else {
                                        out.print("empty");
                                    }
                                %>


                            </tbody>


                        </table>
                    </div>
                    <%
                        }
                        //end
                    %>
                </div>
            </section>
            <%
            } else {
                htses.setAttribute("requestfrom", "Checkout.jsp");
            %>
            <script>
                window.location.replace("index.jsp#opensignin");</script>
                <%
                    }

                } else {
                %>
            <div class="nothingfound" style="width: 90%;margin-left: auto;margin-right: auto;">
                <h3>Checkout empty</h3>
            </div>
            <%
                }

                System.gc();
            %>
            <script>
                function loadshipdetails() {
                    $('#radio1').prop("checked", true);
                    $('#deliverinfo').show();
                    $('#deliverinfo2').hide();
                    var dis = $('#prov1').val();
                    var dis2;
                    if (parseInt(dis)) {
                        if (dis != 0) {

                            $.post(
                                    "Checkout_check_disprice",
                                    {
                                        id: dis
                                    },
                            function (result) {
                                var out = result.split(",");
                                if (out[0] == 1) {
                                    $('#shiipigfee').html("Rs." + out[1]);
                                    $('#price1set').html("Rs." + out[2]);
                                } else {
                                    $('#shiipigfee').html("Free");
                                    $('#price1set').html("Rs." + out[1]);
                                }
                            });
                        }
                    }
                }
                function loadshipdetails2() {
                    $('#deliverinfo').hide();
                    $('#deliverinfo2').show();
                    var dis = $('#anotherdislist').find(":selected").val();
                    if (dis != 0) {
                        $.post(
                                "Checkout_check_disprice",
                                {
                                    id: dis
                                },
                        function (result) {
                            var out = result.split(",");
                            if (out[0] == 1) {
                                $('#shiipigfee').html("Rs." + out[1]);
                                $('#price1set').html("Rs." + out[2]);
                            } else {
                                $('#shiipigfee').html("Free");
                                $('#price1set').html("Rs." + out[1]);
                            }
                        });
                    }
                }
                $('#anotherdislist').change(function () {
                    var dis = $('#anotherdislist').find(":selected").val();
                    if (dis != 0) {
                        $.post(
                                "Checkout_check_disprice",
                                {
                                    id: dis
                                },
                        function (result) {
                            var out = result.split(",");
                            if (out[0] == 1) {
                                $('#shiipigfee').html("Rs." + out[1]);
                                $('#price1set').html("Rs." + out[2]);
                            } else {
                                $('#shiipigfee').html("Free");
                                $('#price1set').html("Rs." + out[1]);
                            }
                        });
                    }
                });
                function confirmaddress() {

                    $('#swarn').hide();
                    $('#warn').hide();
                    $('#warn1').hide();
                    $('#warn2').hide();
                    var fname;
                    var lname;
                    var sphn;
                    var sadl1;
                    var sadl2;
                    var scity;
                    var sprov;
                    var flagchecknull = false;
                    if ($('#radio1').prop("checked")) {
                        fname = $('#fname1').val();
                        lname = $('#lname1').val();
                        sphn = $('#phn1').val();
                        sadl1 = $('#l11').val();
                        sadl2 = $('#l21').val();
                        scity = $('#city1').val();
                        sprov = $('#prov1').val();
                        if (fname == "" || lname == "" || sphn == "" || sadl1 == "" || sadl2 == "" || scity == "" || sprov == "") {
                            $('#warn').show();
                        } else {
                            flagchecknull = true;
                            $('#l11').prop("style", "background: #DFFFD9;");
                            $('#l21').prop("style", "background: #DFFFD9;");
                            $('#city1').prop("style", "background: #DFFFD9;");
                            $('#provshow').prop("style", "background: #DFFFD9;");
                        }
                    } else {

                        fname = document.getElementById('sfname').value;
                        lname = document.getElementById('slname').value;
                        sphn = document.getElementById('sphn').value;
                        sadl1 = document.getElementById('sadl1').value;
                        sadl2 = document.getElementById('sadl2').value;
                        scity = document.getElementById('scity').value;
                        sprov = $('#anotherdislist').find(":selected").val();
                        if (fname == "" || lname == "" || sphn == "" || sadl1 == "" || sadl2 == "" || scity == "" || sprov == "" || sprov == "0") {
                            $('#warn2').show();
                        } else {
                            $('#sadl1').prop("style", "background: #DFFFD9;");
                            $('#sadl2').prop("style", "background: #DFFFD9;");
                            $('#scity').prop("style", "background: #DFFFD9;");
                            $('#anotherdislist').prop("style", "background: #DFFFD9;");
                            flagchecknull = true;
                        }
                    }
                    if (flagchecknull) {
                        $('.confirmaddressbtn').hide();
                        $('#emailtpechoose').hide();
                        $('#buybtn').show();
                    }
                }


                function buyproduct() {

                    var fname;
                    var lname;
                    var sphn;
                    var sadl1;
                    var sadl2;
                    var scity;
                    var sprov;
                    var flagchecknull = false;
                    if ($('#radio1').prop("checked")) {
                        fname = $('#fname1').val();
                        lname = $('#lname1').val();
                        sphn = $('#phn1').val();
                        sadl1 = $('#l11').val();
                        sadl2 = $('#l21').val();
                        scity = $('#city1').val();
                        sprov = $('#prov1').val();
                        alert("/" + sprov + '/');
                        if (fname == "" || lname == "" || sphn == "" || sadl1 == "" || sadl2 == "" || scity == "" || sprov == "") {
                            $('#warn').show();
                            alert("Fill all fields");
                        } else {
                            flagchecknull = true;
                        }
                    } else {

                        fname = document.getElementById('sfname').value;
                        lname = document.getElementById('slname').value;
                        sphn = document.getElementById('sphn').value;
                        sadl1 = document.getElementById('sadl1').value;
                        sadl2 = document.getElementById('sadl2').value;
                        scity = document.getElementById('scity').value;
                        sprov = $('#anotherdislist').find(":selected").val();
                        if (fname == "" || lname == "" || sphn == "" || sadl1 == "" || sadl2 == "" || scity == "" || sprov == "") {
                            $('#warn2').show();
                            alert("Fill all fields");
                        } else {
                            flagchecknull = true;
                        }
                    }
                    alert(flagchecknull);
                    if (flagchecknull) {

                        $.post(
                                "Buy_product",
                                {
                                    id: "cart",
                                    fname: fname,
                                    lname: lname,
                                    phn: sphn,
                                    adl1: sadl1,
                                    adl2: sadl2,
                                    city: scity,
                                    prov: sprov
                                },
                        function (result) {
                            alert(result);
                            var out = result.split(",");
                            if (out[0] == "fail") {
                                $('#tr' + out[1]).addClass('loginform-wrong-email');
                                $('#wat' + out[1]).html("<h4 class='color-red'> Already sold. please remove from cart.</h4>");
                                $('html, body').animate({
                                    scrollTop: $('#tr' + out[1]).offset().top
                                }, 200);
                            } else if (out[0] == "fail2") {
                                $('#tr' + out[1]).addClass('loginform-wrong-email');
                                $('#wat' + out[1]).html("<h4 class='color-red'> Product unavailable. please remove.</h4>");
                                $('html, body').animate({
                                    scrollTop: $('#tr' + out[1]).offset().top
                                }, 200);
                            } else if (out[0] == "fail3") {
                                $('#tr' + out[1]).addClass('loginform-wrong-email');
                                $('#wat' + out[1]).html("<h4 class='color-red'>Stock Low</h4>");
                                $('html, body').animate({
                                    scrollTop: $('#tr' + out[1]).offset().top
                                }, 200);
                            } else if (out[0] == "pass") {



                                var popup = window.open('Invoice.jsp', "mywindow", "status=1,width=860,height=700");
                                popup.onload = function () {
                                    window.location.replace("index.jsp");
                                };
                                window.location.replace("index.jsp");
//                                w.onload = function () {
//                                    setInterval(function () {
//                                       window.location.replace("index.jsp");
//                                    }, 1);
//                                };


                            } else {
                                window.location.replace("index.jsp#opensignin");
                            }
//                            var a = result.toString();
//                            var b = a.split(",");
//                            alert(b[0]);
//                            if (b[0] === "fail") {
//                                $('#tr' + b[1]).addClass('loginform-wrong-email');
//                                $('#wat' + b[1]).html("<h4 class='color-red'> Already sold. please remove from cart.</h4>");
//                                $('html, body').animate({
//                                    scrollTop: $('#tr' + b[1]).offset().top
//                                }, 200);
//                            } else if (b[0] === "redirect") {
//                                window.location.replace("index.jsp#opensignin");
//                            } else {
//                                alert("as");
//                                window.open('Invoice.jsp', "mywindow", "status=1,width=860,height=700");
//                                window.location.replace("index.jsp");
//                            }
//                            
                        });
                    }

                }
                function remo(a) {
                    $('#tr' + a).hide();
//                    var np1 = 0;
//                    np1 = document.getElementById('price0').value;
//                    alert(np1);
//                    var np2 = 0;
//                    np2 = document.getElementById('price1').value;
//                    alert(np2);
//                    np3 = np2 - np1;
//                    //alert(np3);
//                    document.getElementById('price1set').innerHTML = "Rs." + np3 + "00";
//                    document.getElementById('price2set').innerHTML = "Rs." + np3 + "00";
                    $.post(
                            "Remove_from_cart",
                            {
                                id: a
                            },
                    function (result) {

                        var out = result.split(",");
                        if (out[0] == 1) {


                            $('#price2set').html("Rs." + out[1]);
                            var ship = $('#shiipigfee').html();
                            if (ship == "Free") {
                                $('#price1set').html(out[1]);
                            } else {
                                var shiparray = ship.split(".");
                                var shipprice = parseFloat(shiparray[1]);
                                var tot = shipprice + parseFloat(out[1]);
                                $('#price1set').html("Rs." + tot + ".00");
                            }
                            $('#price2set').val();
                        } else {
                            alert("2");
                            window.location.reload();
                        }

                    });
                }

            </script>

        </div>
        <%@include file="Footer.jsp" %>
    </body>
    <script  src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>  

</html>
