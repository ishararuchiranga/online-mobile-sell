<%-- 
    Document   : Profile
    Created on : Feb 12, 2016, 2:39:27 PM
    Author     : Ishara
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/typicons.min.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">

        <link href="css/price-range.css" rel="stylesheet">

        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/script.js"></script>
        <link href="css/navibar.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">

    </head>
    <body>
        <%@include file="Header2.jsp" %>

        <div  class="content_tabs" style="padding-top: 20px; padding-bottom: 20px; padding-right: 20px; ">
            <div id="tab-container">
                <ul>
                    <li class="selected"><a>Purchase History</a></li>
                    <li><a href="http://localhost:8080/Phonehut.lk_2/products.jsp">Account</a></li>

                </ul>
            </div>
            <div id="main-container">
            </div>
        </div>
        

        <%@include file="Footer.jsp" %>
    </body>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</html>