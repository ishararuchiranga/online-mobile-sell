<%-- 
    Document   : Admin_addProducts
    Created on : Mar 16, 2016, 6:29:26 PM
    Author     : Ishara
--%>

<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/Adminmain.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css" />
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/admin.css" rel="stylesheet">
        <script src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/calendar.js"></script>
        <script src="js/jquery.ajaxfileupload.js"></script>
        <script type="text/javascript" src="js/calendar.js"></script>




    </head>
    <body >
        <%
            int toshipcou = 0;
            Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
            Criteria crship = ses.createCriteria(DB.Invoice.class);

            DB.DeleverStatus dels = (DB.DeleverStatus) ses.load(DB.DeleverStatus.class, 1);

            crship.add(Restrictions.eq("deleverStatus", dels));
            crship.addOrder(Order.asc("id"));
            List<DB.Invoice> liin = crship.list();

            if (!(liin.isEmpty())) {
                toshipcou = liin.size();
            }

        %>

        <div>
            <style>

                .dropbtn {
                    background-color: #ffffff;
                    color: black;
                    padding: 8px;
                    // margin:6px;
                    font-size: 16px;
                    border: none;
                    cursor: pointer;
                    border: none;
                    height: 50px;

                }

                .dropdown {
                    position: relative;
                    display: inline-block;
                    float: right;
                    margin-right: 30px;
                }

                .dropdown-content {
                    display: none;
                    position: absolute;
                    min-width: 160px;
                    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                }

                .dropdown-content a {
                    color: black;
                    padding: 12px 16px;
                    text-decoration: none;
                    display: block;
                }

                .dropdown-content a:hover {
                    background-color: #f1f1f1
                }

                .dropdown:hover .dropdown-content {
                    display: block;
                    z-index: 1;
                }

                .dropdown:hover .dropbtn {
                    background-color: #D8D7D7;
                }

                //
                .topbaradmin{
                    position: fixed;
                    z-index: 1;
                    background-color: #F8F8F8; 
                    margin: 0px;
                }
                .logoadmin{
                    display: inline-block
                }
                .logoadmin img{
                    margin: 6px;
                }
                .sidebaradmin li a{
                    margin:  0px;
                }
                .active-menu{
                    background-color: #E5580C;
                    color: white;
                }
                .well{
                    min-height: 20px;
                    padding: 19px;
                    margin-bottom: 20px;
                    background-color: #F5F5F5;
                    border: 1px solid #E3E3E3;
                    border-radius: 4px;
                    box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.05) inset;
                }
                .quick-btn {
                    position: relative;
                    display: inline-block;
                    width: 90px;
                    height: 80px;
                    padding-top: 16px;
                    margin: 10px;
                    color: #444;
                    text-align: center;
                    text-decoration: none;
                    text-shadow: 0px 1px 0px rgba(255, 255, 255, 0.6);
                    box-shadow: 0px 0px 0px 1px #F8F8F8 inset, 0px 0px 0px 1px #CCC;
                }
                .quick-btn .label {
                    position: absolute;
                    top: -5px;
                    right: -5px;
                }
                .quick-btn span {
                    display: block;
                }
                .label-danger {
                    background-color: #D9534F;
                }
                .label {
                    display: inline;
                    padding: 0.2em 0.6em 0.3em;
                    font-size: 75%;
                    font-weight: bold;
                    line-height: 1;
                    color: #FFF;
                    text-align: center;
                    white-space: nowrap;
                    vertical-align: baseline;
                    border-radius: 0.25em;
                }
                .adminiconsize{
                    font-size: 35px;
                }
            </style>
            <div class="topbaradmin">
                <div class="logoadmin">
                    <img src="images/home/Phone.PNG">
                </div>
                <div class="dropdown" >
                    <button class="dropbtn"><img style="height: 28px" src="images/orange.png"> Admin Ishara</button>
                    <div class="dropdown-content">
                        <a href="#">Logout</a>
                        <a href="#">Settings</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-2" style=" padding: 0px;position: relative; box-sizing: border-box; float: left; height:3400px; background-color: #F8F8F8 ;border: 1px solid #D8D7D7;">
                <nav class="navbar-default navbar-side" role="navigation">
                    <div class="sidebar-collapse">
                        <ul class="nav sidebaradmin" id="main-menu">
                            <li>
                                <a  href="AdminPanel1.jsp"><i class="fa fa-dashboard"></i> Dashboard</a>
                            </li>
                            <li>
                                <a  href="Admin_addProducts.jsp"><i class="fa fa-plus-square"></i> Addproduct</a>
                            </li>
                            <li>
                                <a href="Admin_product_details.jsp"><i class="fa fa-check-square-o"></i> Product details</a>
                            </li>
                            <li>
                                <a href="Admin_user_management.jsp"><i class="fa fa-users"></i> User management</a>
                            </li>
                            <li>
                                <%                              //
                                    if (toshipcou != 0) {
                                %>
                                <a class="active-menu"><i class="fa fa-truck"></i> Delivery (<%=toshipcou%>)</a>
                                <%
                                } else {
                                %>
                                <a class="active-menu"><i class="fa fa-truck"></i> Delivery</a>
                                <%
                                    }
                                %>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="col-sm-10"  style="padding: 5px;float: left;  margin: 0px; box-sizing: border-box; height: 3400px; background-color: #F8F8F8;border: 1px solid #D8D7D7;">
                <div>
                    <h4>
                        Shipping
                    </h4>

                </div>
                <div>
                    &nbsp;Status:<select id="odstat" onchange="loadIframe()" style="width: 15%">
                        <option value="1">Ordered</option>
                        <option value="2">Shipped</option>
                        <option value="3">Delivered</option>
                    </select>
                    <style>

                    </style>
                    <a href="#delse" class="settiinga"><span class="fa fa-gear settingspan"></span> <span class="settingspan2">Settings</span></a>
                    <!--      
                    &nbsp;&nbsp;&nbsp;&nbsp;
                                        Date-Range:<input id="date1" placeholder="Start date"  class="date-picker" type="text">
                                        to <input id="date2" placeholder="end date" class="date-picker" type="text">
                                        <input type="button" onclick="loadIframe()" value="Search">-->
                </div>
                <iframe id="if" name="ifr" width="100%" height="99%" scrolling="no" border="0" src="Admin_shipping2.jsp?type=1" frameborder="no">Your browser not support this feature</iframe>
            </div>


        </div>
        <div id="delse" class="delsetting">
            <div id="forgotpwrdinside">
                <a href="#close" title="Close" onclick="reloadupdatepr2()"  class="close">X</a>
                <div id="forgotpwrdfrm" class="delsetting-form"><!--sign up form-->
                    <iframe class="pro-edit-iframe" id="prodetail-if"  border="0" src="Admin_shipping4.jsp" frameborder="no" height="100%" width="100%">Your browser not support this feature</iframe>
                </div>	
            </div>
        </div>
        <script>
            function loadIframe() {

                var type = $('#odstat').find(":selected").val();
                var date1 = $('#date1').val();
                var date2 = $('#date2').val();
                if (date1 == "") {
                    date1 = null;
                }
                if (date2 == "") {
                    date2 = null;
                }
                var $iframe = $('#if');
                $iframe.attr('src', "Admin_shipping2.jsp?type=" + type + "&date1=" + date1 + "&date2=" + date2 + "");

            }
        </script>
    </body>

</html>
