<%-- 
    Document   : Profile
    Created on : Feb 12, 2016, 2:39:27 PM
    Author     : Ishara
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/typicons.min.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">

        <link href="css/price-range.css" rel="stylesheet">

        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="js/script.js"></script>
        <link href="css/navibar.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">

    </head>
    <body>
        <%@include file="Header2.jsp" %>

        <div  class="content_tabs" style="padding-top: 20px; padding-bottom: 20px; padding-right: 20px; ">
            <div id="tab-container">
                <ul>
                    <li><a href="Profile_Purchase_History.jsp">Purchase History</a></li>
                    <li class="selected"><a>Account</a></li>
                </ul>
            </div>
            <div id="main-container">
                <div class="prof_topBar">
                    <div style="float: left" >
                        <h4 class="prof_topBar_header">Profile</h4>
                    </div>
                </div>
                <style>
                    .pleasfill{
                        border: 1px solid rgb(206, 0, 0);
                    }
                </style>
                <div style="border: 1px solid rgb(203, 203, 201); padding: 20px;">
                    <%                       //
                        HttpSession purhisSes = request.getSession();
                        int i = 0;
                        if (purhisSes.getAttribute("Login_object") != null) {
                            Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
                            DB.UserRegistration ure = (DB.UserRegistration) purhisSes.getAttribute("Login_object");
                            DB.UserRegistration ur = (DB.UserRegistration) ses.load(DB.UserRegistration.class, ure.getId());

                            if (purhisSes.getAttribute("plzcomplete") != null) {
                                purhisSes.removeAttribute("plzcomplete");
                                if (ur.getUserDetails() != null) {

                                    if (ur.getUserDetails().getAdLine1() == null) {
                    %>
                    <script>
                        $(document).ready(function () {
                            $('#adddiv').addClass('pleasfill');
                        });


                    </script>
                    <%
                        }
                        if (ur.getUserDetails().getFname() == null) {
                    %>
                    <script>
                        $(document).ready(function () {
                            $('#name').addClass('pleasfill');
                        });
                    </script>
                    <%
                        }
                        if (ur.getUserDetails().getPhone() == null) {
                    %>
                    <script>
                        $(document).ready(function () {
                            $('#phndiv').addClass('pleasfill');
                        });
                    </script>
                    <%
                            }
                        }
                    %>

                    <div class="alertprof">
                        <span class="closebtn" onclick="this.parentElement.style.display = 'none';">&times;</span> 
                        <strong>Please complete your profile details!!!</strong>
                    </div>
                    <%
                        }
                    %>


                    <div>
                        <ul style="text-align: center; text-align: center; padding: 0px; width: 400px;">
                            <li style="display: inline-block; float: left;">Emaill</li>
                            <li style="display: inline-block;"><%=ur.getEmail()%></li>
                            <li style="display: inline-block; float: right;"><a style="color: rgb(209, 206, 201);" >Change</a></li>
                        </ul>
                    </div>
                    <div>
                        <ul style="text-align: center; text-align: center; padding: 0px; width: 400px;">
                            <li style="display: inline-block; float: left;">User name</li>
                            <li style="display: inline-block;"><%=ur.getUserName()%></li>
                            <li style="display: inline-block; float: right;"><a style="color: rgb(209, 206, 201);">Change</a></li>
                        </ul>
                    </div>
                    <div>
                        <div>
                            <ul style="text-align: center; text-align: center; padding: 0px; width: 400px; display: inline-block">
                                <li style="display: inline-block; float: left;">Password</li>
                                <li   id="passwordli" style="display: inline-block;">
                                    <%
                                        String pwrd = ur.getPassword();
                                        for (i = 0; i < pwrd.length(); ++i) {
                                            out.print("*");
                                        }
                                    %>
                                </li>
                                <li style="display: inline-block; float: right;" id="passchangeli"><a id="passchange">Change</a></li>
                                <li style="display: inline-block; float: right; display: none" id="passchangeli2"><a id="passcancel">cancel</a></li>
                                <li style="display: inline-block; float: right; display: none" id="successfullch"><a style="color: greenyellow">Successfull</a></li>
                            </ul>
                        </div>
                        <div id="passchngepart" style="padding-left: 120px; display: none; ">
                            <div style="display: inline-block; ">
                                <input id="mainpass" style="margin-bottom: 3px;" type="password" placeholder="Current password"><br>
                                <input id="passch1" style="margin-bottom: 3px;" type="password" placeholder="New password"><br>
                                <input id="passch2" style="margin-bottom: 3px;" type="password" placeholder="Retype password"><br>
                                <p style="display: none; color: red" id="wrngpasslength2">*password length must be more 4</p>
                                <p style="display: none;color: red" id="passworddoesntmatch2">**password's does not match</p>
                                <p style="display: none;color: red" id="wrngoldpwrd">***wrong old password</p>
                                <input style="float: right" type="button" id="chngepwrdbtn" value="Save">
                                <div id="waitprof1" class="hideitems"><i class='fa fa-repeat fa-spin '></i> Please wait..</div>
                            </div>
                        </div>
                    </div>

                    <script>
                        var id =<%=ur.getId().toString()%>;
                        $('#passchange').click(function () {
                            $('#passchangeli').hide();
                            $('#passwordli').hide();
                            $('#passchangeli2').show();
                            $('#passwordli2').show();
                            $('#passchngepart').show();

                        });
                        $('#passcancel').click(function () {
                            $('#passchangeli').show();
                            $('#passwordli').show();
                            $('#passchangeli2').hide();
                            $('#passwordli2').hide();
                            $('#passchngepart').hide();

                        });

                        $('#chngepwrdbtn').click(function () {
                            var pwrdle = true;
                            var pwrdma = true;
                            var passch1 = $('#passch1').val();
                            var passch2 = $('#passch2').val();

                            if (passch1.length <= 4) {

                                $("#wrngpasslength2").show();
                                pwrdle = false;
                            } else {
                                $("#wrngpasslength2").hide();
                                pwrdle = true;
                                var mainpass = $("#mainpass").val();
                                $('#chngepwrdbtn').hide();
                                $('#waitprof1').show();
                                $.post(
                                        "Profile_password_change",
                                        {pwrd: mainpass, pwrd2: passch1}, //meaasge you want to send
                                function (result) {
                                    var out = result.toString();
                                    if (out === "1") {
                                        $("#wrngoldpwrd").show();
                                        $('#chngepwrdbtn').show();
                                        $('#waitprof1').hide();
                                    } else {
                                        $('#passchngepart').hide();
                                        $('#passchange').hide();
                                        $('#passcancel').hide();
                                        $('#passwordli').show();
                                        $('#successfullch').show();
                                    }
                                });

                            }
                            if (passch1 === passch2) {
                                if (!(passch1 === "" || passch2 === "")) {
                                    pwrdma = true;
                                    if (pwrdle) {

                                        $("#passworddoesntmatch2").hide();
                                        $("#wrngpasslength2").hide();

                                    }
                                } else {
                                    pwrdma = false;
                                    $("#passworddoesntmatch2").show();
                                }
                            } else {
                                pwrdma = false;

                                $("#passch1").val("");
                                $("#passch2").val("");
                                $("#passworddoesntmatch2").show();
                            }

                        });

                    </script>
                    <%
                        if (ur.getUserDetails() != null) {
                    %>
                    <div>
                        <ul id="name" style="text-align: center; text-align: center; padding: 0px; width: 400px;">
                            <li style="display: inline-block; float: left;">Name</li>
                            <li id="oldname" style="display: inline-block;"><%if (ur.getUserDetails().getFname() == null) {
                                    out.print("-");
                                } else {

                                    out.print(ur.getUserDetails().getFname());
                                    out.print(" ");
                                    out.print(ur.getUserDetails().getLname());
                                } %></li>
                                <%
                                    if (ur.getUserDetails().getFname() == null) {
                                %>
                            <li id="namechngeli" style="display: inline-block; float: right;"><a id="namechng">Change</a></li>
                                <%
                                } else {
                                %>
                            <li style="display: inline-block; float: right;"><a style="color: rgb(209, 206, 201);">Change</a></li>    
                                <%                                }
                                %>
                            <li id="namecanel" style="display: inline-block; float: right; display: none"><a id="phnchng2">Cancel</a></li>
                        </ul>
                        <div id="namechgeprt" style="padding-left: 120px; display: none; ">
                            <div style="display: inline-block;">
                                <input id="newname" style="margin-bottom: 3px;" value="" type="text" placeholder="First name"><br>
                                <input id="newname2" style="margin-bottom: 3px;" value="" type="text" placeholder="Last name"><br>
                                <input style="float: right" type="button" id="chngename" value="Save">
                                <div id="waitname" class="hideitems"><i class='fa fa-repeat fa-spin '></i> Please wait..</div>
                            </div>
                        </div>

                        <script>
                            $('#namechng').click(function () {
                                $('#namechgeprt').show();
                                $('#namecanel').show();
                                $('#oldname').hide();
                                $('#namechngeli').hide();
                            });
                            $('#phnchng2').click(function () {
                                $('#namechgeprt').hide();
                                $('#namecanel').hide();
                                $('#oldname').show();
                                $('#namechngeli').show();
                            });
                            $('#chngename').click(function () {
                                var name1 = $('#newname').val();
                                var name2 = $('#newname2').val();
                                $.post(
                                        "Profile_add_name",
                                        {name1: name1, name2: name2}, //meaasge you want to send
                                function (result) {
                                    $('#namechgeprt').hide();
                                    $('#namecanel').hide();
                                    $('#oldname').show();
                                    $('#namechngeli').show();
                                    $('#oldname').html(name1 + ' ' + name2);
                                    $('#namechng').replaceWith("<a style='color: rgb(209, 206, 201);'>Change</a>");

                                });
                            });
                        </script>
                    </div>
                    <div >
                        <ul id="phndiv" style="text-align: center; text-align: center; padding: 0px; width: 400px;">
                            <li style="display: inline-block; float: left;">Phone</li>
                            <li id="oldphoneli" style="display: inline-block;"><%if (ur.getUserDetails().getPhone() == null) {
                                    out.print("-");
                                } else {
                                    out.print(ur.getUserDetails().getPhone());
                                } %></li>
                            <li id="phnchngeli" style="display: inline-block; float: right;"><a id="phnchng">Change</a></li>
                            <li id="phncancelli" style="display: inline-block; float: right; display: none"><a id="phnchng12">Cancel</a></li>
                        </ul>
                        <div id="phonechgeprt" style="padding-left: 120px; display: none; ">
                            <div style="display: inline-block;">
                                <input id="newphone" style="margin-bottom: 3px;" value="<%if (ur.getUserDetails().getPhone() == null) {
                                        out.print("");
                                    } else {
                                        out.print(ur.getUserDetails().getPhone());
                                    } %>" type="text" placeholder="New number"><br>
                                <input style="float: right" type="button" id="chngephnno" value="Save">
                                <div id="waitphoneno" class="hideitems"><i class='fa fa-repeat fa-spin '></i> Please wait..</div>
                            </div>
                        </div>

                        <script>
                            $('#phnchng').click(function () {
                                $('#phonechgeprt').show();
                                $('#phncancelli').show();
                                $('#oldphoneli').hide();
                                $('#phnchngeli').hide();
                            });
                            $('#phnchng12').click(function () {
                                $('#phonechgeprt').hide();
                                $('#phncancelli').hide();
                                $('#oldphoneli').show();
                                $('#phnchngeli').show();
                            });
                            $('#chngephnno').click(function () {
                                var phnnew = $('#newphone').val();
                                $.post(
                                        "Profile_chgePhoneNo",
                                        {num: phnnew}, //meaasge you want to send
                                function (result) {
                                    $('#phonechgeprt').hide();
                                    $('#phncancelli').hide();
                                    $('#oldphoneli').empty()
                                    $('#oldphoneli').append(phnnew);
                                    $('#oldphoneli').show();
                                    $('#phnchngeli').show();
                                });
                            });
                        </script>
                    </div>
                    <div>
                        <ul id="adddiv" style="text-align: center; text-align: center; padding: 0px; width: 400px;">
                            <li style="display: inline-block; float: left;">Address</li>
                            <li id="oldaddli" style="display: inline-block;">
                                <% if (ur.getUserDetails().getAdLine1() != null) {%>
                                <%=ur.getUserDetails().getAdLine1()%><br>
                                <%=ur.getUserDetails().getAdLine2()%><br>
                                <%=ur.getUserDetails().getCity()%><br>
                                <%=ur.getUserDetails().getDistrict().getDistrict()%><br>
                                <% } else {
                                        out.print("None");
                                    } %>

                            </li>
                            <li id="addchngeli" style="display: inline-block; float: right;"><a id="addchnge">Change</a></li>
                            <li id="addcancelli" style="display: inline-block; float: right; display: none;"><a id="addcancel">cancel</a></li>
                        </ul>
                        <div id="addrchngepart" style="padding-left: 120px; display: none; ">
                            <div style="display: inline-block; ">
                                <input id="adl1" style="margin-bottom: 3px;" type="text" placeholder="Address line 1"><br>
                                <input id="adl2" style="margin-bottom: 3px;" type="text" placeholder="Address line 2"><br>
                                <input id="adcity" style="margin-bottom: 3px;" type="text" placeholder="City"><br>

                                <select id="adprovince">
                                    <option value="0">Select your district</option>
                                    <%
                                        Criteria crpro = ses.createCriteria(DB.District.class);
                                        crpro.addOrder(Order.asc("id"));
                                        List<DB.District> listdis = crpro.list();
                                        for (DB.District dis : listdis) {
                                    %>
                                    <option value="<%=dis.getId()%>"><%=dis.getDistrict()%></option>
                                    <%
                                        }
                                    %>
                                </select>
                                <!--<input id="adprovince" style="margin-bottom: 3px;" type="text" placeholder="Province">-->
                                <br>
                                <p style="display: none;color: red" id="wrngaddress">*Please fill all fields</p>
                                <input style="float: right" type="button" id="chngeaddrbtn" value="Save">
                                <div id="waitadd" class="hideitems"><i class='fa fa-repeat fa-spin '></i> Please wait..</div>
                            </div>
                        </div>
                        <script>
                            $('#addchnge').click(function () {
                                $('#addrchngepart').show();
                                $('#addcancelli').show();
                                $('#addchngeli').hide();
                                $('#oldaddli').hide();
                            });
                            $('#addcancel').click(function () {
                                $('#addrchngepart').hide();
                                $('#addchngeli').show();
                                $('#oldaddli').show();
                                $('#addcancelli').hide();
                            });
                            $('#chngeaddrbtn').click(function () {
                                var ad1 = $('#adl1').val();
                                var ad2 = $('#adl2').val();
                                var city = $('#adcity').val();
//                                var province = $('#adprovince').val();
                                var distric = $('#adprovince').find(":selected").val();
                                var distric2 = $('#adprovince').find(":selected").html();

                                if (distric != 0) {
                                    if (ad1 != "" && ad2 != "" && city != "" && distric != "") {

                                        $('#waitadd').show();
                                        $.post(
                                                "Profile_chngeAddress",
                                                {line1: ad1, line2: ad2, city: city, province: distric}, //meaasge you want to send
                                        function (result) {
                                            $('#addrchngepart').hide();
                                            $('#addchngeli').show();
                                            $('#oldaddli').empty();
                                            $('#oldaddli').append(ad1 + '<br>' + ad2 + '<br>' + city + '<br>' + distric2);
                                            $('#oldaddli').show();
                                            $('#addcancelli').hide();
                                            $('#waitadd').hide();
                                        });
                                    } else {
                                        $('#wrngaddress').show();
                                    }
                                } else {
                                    $('#wrngaddress').show();
                                }

                            });
                        </script>

                    </div>
                    <%                    } else {
                    %>
                    <div >
                        <ul id="name" style="text-align: center; text-align: center; padding: 0px; width: 400px;">
                            <li style="display: inline-block; float: left;">Name</li>
                            <li id="oldname" style="display: inline-block;">None</li>

                            <li id="namechngeli" style="display: inline-block; float: right;"><a id="namechng">Change</a></li>
                            <li id="namecanel" style="display: inline-block; float: right; display: none"><a id="phnchng2">Cancel</a></li>
                        </ul>
                        <div id="namechgeprt" style="padding-left: 120px; display: none; ">
                            <div style="display: inline-block;">
                                <input id="newname" style="margin-bottom: 3px;" value="" type="text" placeholder="First name"><br>
                                <input id="newname2" style="margin-bottom: 3px;" value="" type="text" placeholder="Last name"><br>
                                <input style="float: right" type="button" id="chngename" value="Save">
                                <div id="waitname" class="hideitems"><i class='fa fa-repeat fa-spin '></i> Please wait..</div>
                            </div>
                        </div>

                        <script>
                            $('#namechng').click(function () {
                                $('#namechgeprt').show();
                                $('#namecanel').show();
                                $('#oldname').hide();
                                $('#namechngeli').hide();
                            });
                            $('#phnchng2').click(function () {
                                $('#namechgeprt').hide();
                                $('#namecanel').hide();
                                $('#oldname').show();
                                $('#namechngeli').show();
                            });
                            $('#chngename').click(function () {
                                var name1 = $('#newname').val();
                                var name2 = $('#newname2').val();
                                $.post(
                                        "Profile_add_name",
                                        {name1: name1, name2: name2}, //meaasge you want to send
                                function (result) {
                                    $('#namechgeprt').hide();
                                    $('#namecanel').hide();
                                    $('#oldname').show();
                                    $('#namechngeli').show();
                                    $('#oldname').html(name1 + ' ' + name2);
                                    $('#namechng').replaceWith("<a style='color: rgb(209, 206, 201);'>Change</a>");

                                });
                            });
                        </script>
                    </div>

                    <div>
                        <ul style="text-align: center; text-align: center; padding: 0px; width: 400px;">
                            <li style="display: inline-block; float: left;">Phone</li>
                            <li id="phnnoneli" style="display: inline-block;">None</li>
                            <li id="addphnli" style="display: inline-block; float: right;"><a id="addphn">add</a></li>
                            <li id="addcancelphnli" style="display: inline-block; float: right; display: none;"><a id="addcancelphn">cancel</a></li>
                        </ul>
                        <div id="addphnprt" style="padding-left: 120px; display: none; ">
                            <div style="display: inline-block; ">
                                <input id="phnnum" style="margin-bottom: 3px;" type="text" placeholder="Phone number"><br>
                                <p style="display: none;color: red" id="addphnnumerror">*please fill the field</p>
                                <input style="float: right" type="button" id="addphnnumbtn" value="Save">
                                <div id="waitaddphnno" class="hideitems"><i class='fa fa-repeat fa-spin '></i> Please wait..</div>
                            </div>
                        </div>
                        <script>
                            $('#addphn').click(function () {
                                $('#addphnprt').show();
                                $('#addcancelphnli').show();
                                $('#addphnli').hide();
                                $('#phnnoneli').hide();
                            });

                            $('#addcancelphn').click(function () {
                                $('#addphnprt').hide();
                                $('#addcancelphnli').hide();
                                $('#addphnli').show();
                                $('#phnnoneli').show();
                            });
                            $('#addphnnumbtn').click(function () {
                                var phnnum = $('#phnnum').val();
                                $('#waitaddphnno').show();
                                if (phnnum != "") {
                                    $.post(
                                            "Profile_addPhnNo",
                                            {num: phnnum}, //meaasge you want to send
                                    function (result) {
                                        $('#waitaddphnno').hide();
                                        $('#addphnprt').hide();
                                        $('#addcancelphnli').hide();
                                        $('#addphnli').show();
                                        $('#phnnoneli').show();
                                        $('#phnnoneli').empty();
                                        $('#phnnoneli').append(phnnum);
                                    });
                                } else {
                                    $('addphnnumerror').show();
                                }
                            });
                        </script>
                    </div>
                    <div>
                        <ul style="text-align: center; text-align: center; padding: 0px; width: 400px;">
                            <li style="display: inline-block; float: left;">Address</li>
                            <li id="addrnone" style="display: inline-block;">
                                None
                            </li>
                            <li id="addaddrli" style="display: inline-block; float: right;"><a id="addaddr">Add</a></li>
                            <li id="canceladdrli" style="display: inline-block; float: right; display: none;"><a id="canceladdr">Cancel</a></li>
                        </ul>
                        <div id="addressprt" style="padding-left: 120px; display: none; ">
                            <div style="display: inline-block; ">
                                <input id="addl1" style="margin-bottom: 3px;" type="text" placeholder="Address line 1"><br>
                                <input id="addl2" style="margin-bottom: 3px;" type="text" placeholder="Address line 2"><br>
                                <input id="addcity" style="margin-bottom: 3px;" type="text" placeholder="City"><br>
                                <select id="addprovince">
                                    <option value="0">Select your district</option>
                                    <%
                                        Criteria crpro = ses.createCriteria(DB.District.class);
                                        crpro.addOrder(Order.asc("id"));
                                        List<DB.District> listdis = crpro.list();
                                        for (DB.District dis : listdis) {
                                    %>
                                    <option value="<%=dis.getId()%>"><%=dis.getDistrict()%></option>
                                    <%
                                        }
                                    %>
                                </select>
                                <p style="display: none;color: red" id="wrngaddress2">*Please fill all fields</p>
                                <input style="float: right" type="button" id="addaddrbtn" value="Save">
                                <div id="waitadadd" class="hideitems"><i class='fa fa-repeat fa-spin '></i> Please wait..</div>
                            </div>
                        </div>
                        <script>
                            $('#addaddr').click(function () {
                                $('#addressprt').show();
                                $('#canceladdrli').show();
                                $('#addaddrli').hide();
                                $('#addrnone').hide();
                            });
                            $('#canceladdr').click(function () {
                                $('#addressprt').hide();
                                $('#canceladdrli').hide();
                                $('#addaddrli').show();
                                $('#addrnone').show();
                            });
                            $('#addaddrbtn').click(function () {
                                var add1 = $('#addl1').val();
                                var add2 = $('#addl2').val();
                                var addcity = $('#addcity').val();
                                var distric = $('#addprovince').find(":selected").val();
                                var distric2 = $('#addprovince').find(":selected").html();
                                if (distric != 0) {
                                    if (add1 != "" && add2 != "" && addcity != "" && distric != "") {
                                        $('#waitadadd').show();
                                        $.post(
                                                "Profile_addAddress",
                                                {line1: add1, line2: add2, city: addcity, province: distric}, //meaasge you want to send
                                        function (result) {
                                            $('#addressprt').hide();
                                            $('#addaddrli').show();
                                            $('#addrnone').empty();
                                            $('#addrnone').append(add1 + '<br>' + add2 + '<br>' + addcity + '<br>' + distric2);
                                            $('#addrnone').show();
                                            $('#canceladdrli').hide();
                                            $('#waitadadd').hide();
                                        });
                                    } else {
                                        $('#wrngaddress2').show();
                                    }
                                } else {
                                    $('#wrngaddress2').show();
                                }

                            });
                        </script>
                    </div>
                    <%
                            }
                        }
                    %>
                </div>
                <div></div>
                <div></div>
            </div>
        </div>


        <%@include file="Footer.jsp" %>
    </body>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</html>