<%-- 
    Document   : Admin_update_product3
    Created on : May 16, 2016, 3:32:53 PM
    Author     : Ishara
--%>

<%@page import="org.hibernate.criterion.Order"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css" />
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <!--<link href="css/admin.css" rel="stylesheet">-->
        <script src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/calendar.js"></script>
        <script src="js/jquery.ajaxfileupload.js"></script>
        <link href="css/main.css" rel="stylesheet">
        <style>
            body{
                background: rgb(248, 248, 248);
            }
            .adp3-div1{
                float: left;
                margin-right: 520px;
            }
            .adp3-div2{
                float: left;
                padding-top: 25px;
            }
            h2{
                /*padding-left: 20px*/
                margin: 0px;
            }
            table{
                width: 300px;
                /*border: solid black;*/
            }
            td{
                padding: 5px;
                /*border: solid black;*/
            }
            a{
                font-size: 9px;
            }
            .tdmiddle{
                text-align: center;
            }
        </style>
    </head>
    <body>
        <%
            String prid = request.getParameter("pid");

            HttpSession ses_update_pr = request.getSession();

            if (ses_update_pr.getAttribute("updateproid") == null) {
                ses_update_pr.setAttribute("updateproid", prid);
            } else {
                ses_update_pr.removeAttribute("updateproid");
                ses_update_pr.setAttribute("updateproid", prid);
            }

            Session sesadp3 = conn.NewHibernateUtil.getSessionFactory().openSession();
            DB.Products pradp3 = (DB.Products) sesadp3.load(DB.Products.class, Integer.parseInt(prid));

            Criteria cradp3 = sesadp3.createCriteria(DB.InvoiceRegistry.class);
            cradp3.add(Restrictions.eq("products", pradp3));
            List<DB.InvoiceRegistry> invrlist = cradp3.list();

            int soldqty = 0;
            if (!(invrlist.isEmpty())) {
                for (DB.InvoiceRegistry in : invrlist) {
                    soldqty += Integer.parseInt(in.getQty());
                }
            }
        %>
        <h2><%=pradp3.getModel().getManufacturers().getName()%> <%=pradp3.getModel().getModel()%></h2>
        <div class="adp3-div1">
            <div class="prodetails-img" id="preview" >
                <img id="imageas"  style="margin-left: auto; margin-right: auto; height: 100%; display: block; border: 5px solid black;" src="<%=pradp3.getModel().getImgSrc()%>" />
                <a href="#" id="up" style="position: inherit; right: 70px; top: 289px;" class="upload-document-image">Change Image</a>
                <input type="file" accept="image/*" style="display: none" id="the-photo-file-field" name="datafile" onchange="handleFiles(this)" />
            </div>‪
        </div>
        <div class="adp3-div2">
            <div>
                <table>
                    <tbody>
                        <tr>
                            <td>ID</td>
                            <td class="tdmiddle"><%=pradp3.getId()%></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Model</td>
                            <td class="tdmiddle"><input type="hidden" id="brandid" value="<%=pradp3.getModel().getManufacturers().getId()%>"><%=pradp3.getModel().getManufacturers().getName()%> <%=pradp3.getModel().getModel()%></td>
                            <td ><a class="edittd" id="chngemodel-btn">Change</a></td>
                        </tr>

                        <tr>
                            <td>Added date</td>
                            <td class="tdmiddle"><%=pradp3.getAddedDate()%> <%=pradp3.getAddedTime()%></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Available qty</td>
                            <td class="tdmiddle"><%=pradp3.getQty()%></td>
                            <td><a id="addqty-btn">Add qty</a></td>
                        </tr>
                        <tr>
                            <td>Sold qty</td>
                            <td class="tdmiddle"><%=soldqty%></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Price</td>
                            <td class="tdmiddle">Rs.<%=pradp3.getPrice()%>.00</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td class="tdmiddle" id="statdiv"><%=pradp3.getProStatus().getState()%></td>
                            <td><a id="chngestat-btn">

                                    <%
                                        if (pradp3.getProStatus().getState().equals("Active")) {
                                            out.print("Inactive");
                                        } else {
                                            out.print("Active");
                                        }
                                    %>

                                </a></td>
                        </tr>
                        <tr>
                            <td>Product type</td>
                            <td class="tdmiddle" id="typeid"><%=pradp3.getModel().getDeviceType().getType()%></td>
                            <td>
                                <%
                                    if (!(pradp3.getModel().getDeviceType().getId().equals(3))) {
                                %>
                                <a id="changeprtype">Change</a>
                                <%
                                    }
                                %>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <span>  
                </span>

                <script>

                    $(function () {
                        $('.upload-document-image').click(function () {
                            $(this).next('input[type="file"]').trigger('click');
                            return false;
                        });
                    });
                    function handleFiles(fileInput) {
                        var files = fileInput.files;
                        for (var i = 0; i < files.length; i++) {
                            var file = files[i];
                            var imageType = /image.*/;
                            if (!file.type.match(imageType)) {
                                continue;
                            }

                            var img = document.getElementById("imageas");
                            // img.classList.add("obj");
                            //img.file = file;
                            //  $(fileInput).after(img);

                            var reader = new FileReader();
                            reader.onload = (function (aImg) {
                                return function (e) {
                                    aImg.src = e.target.result;
                                    aImg.style = "margin-left: auto; margin-right: auto; height: 100%; display: block; border: 5px solid black;";
                                };
                            })(img);
                            reader.readAsDataURL(file);
                        }
                    }
                    //upload
                    $(document).ready(function () {
//                        $('#up').click(function () {
                        $('input[type="file"]').ajaxfileupload({
                            'action': 'Admin_Change_image',
                            'onComplete': function (response) {
                                $('#upload').hide();
                            },
                            'onStart': function () {
                                $('#upload').show();
                            }
                        });
//                        });
                    });
                                    </script>


                <!--update product name -->
                <div id="chagemodel" class="chagemodel-model">
                    <!-- Modal content -->
                    <div class="chagemodel-content">
                        <div class="chagemodel-header">
                            <span class="chagemodel-close">X</span>
                        </div>
                        <div class="chagemodel-body">
                            <table>

                                <tr>
                                    <td>Select brand:</td>
                                    <td>  
                                        <select  onchange="validateselect()" id="brands">
                                            <%                                                Criteria criteria = sesadp3.createCriteria(DB.Manufacturers.class);
                                                criteria.addOrder(Order.asc("id"));
                                                List<DB.Manufacturers> list = criteria.list();
                                                for (DB.Manufacturers man : list) {
                                            %>
                                            <option value="<%=man.getId()%>"><%=man.getName()%></option>    
                                            <%
                                                }
                                            %>   
                                            <option class="addnew-select" value="addnew">Add new</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="newbrandtr" class="hideitems">
                                    <td>New brand:</td>
                                    <td><input type="text" id="addbrandtf" ></td>
                                </tr>
                                <tr id="newbrandtr2" class="hideitems">
                                    <td></td>
                                    <td><input type="button" class="updateprname-btn" value="Add" onclick="newbrand()"></td>
                                </tr>


                                <tr id="newname" class="hideitems">
                                    <td>Name:</td>
                                    <td><input type="text" id="newbrandname"></td>
                                </tr>
                                <tr id="btnpos" class="hideitems">
                                    <td></td>
                                    <td><input type="button" onclick="updatename()" class="updateprname-btn" value="save"></td>
                                </tr>

                            </table>

                        </div>
                    </div>
                </div>
                <script>
                    var modal = document.getElementById('chagemodel');
                    var btn = document.getElementById("chngemodel-btn");
                    var span = document.getElementsByClassName("chagemodel-close")[0];
                    btn.onclick = function () {
                        modal.style.display = "block";
                    }
                    span.onclick = function () {
                        modal.style.display = "none";
                        $('#newname').hide();
                        $('#btnpos').hide();
                        $('#newbrandtr').hide();
                        $('#newbrandtr2').hide();
                    }
                    window.onclick = function (event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    }
                    $(document).ready(function () {
                        var brandid = $('#brandid').val();
                        $('#brands option[value="' + brandid + '"]').attr('selected', 'selected');
                        $('#brands option[value="' + brandid + '"]').addClass('own-brand');
                    });
                    function validateselect() {
                        var brandid = $('#brandid').val();
                        var brndop = document.getElementById("brands");
                        var value = brndop.options[brndop.selectedIndex].value;
                        if (value == "addnew") {
                            $('#newbrandtr').show(200);
                            $('#newbrandtr2').show(200);
                            $('#newname').hide();
                            $('#btnpos').hide();
                        } else if (value == brandid) {
                            $('#newname').hide();
                            $('#btnpos').hide();
                            $('#newbrandtr').hide();
                            $('#newbrandtr2').hide();
                        } else {
                            $('#newbrandtr').hide();
                            $('#newbrandtr2').hide();
                            $('#newname').show(200);
                            $('#btnpos').show(200);
                        }
                    }
                    function newbrand() {

                        var newbrand;
                        newbrand = document.getElementById("addbrandtf").value;
                        alert("awa");
                        $.post(
                                "Admin_add_new_brand",
                                {brand: newbrand},
                        function (result, status) {
                            var brandid = result;
                            var selectobject = document.getElementById("brands");
                            for (var i = 0; i < selectobject.length; i++) {
                                if (selectobject.options[i].value == "addnew") {
                                    selectobject.remove(i);
                                }
                            }

                            var select, option;
                            select = document.getElementById('brands');
                            option = document.createElement('option');
                            option.value = brandid;
                            option.text = newbrand;
                            select.add(option);
                            option = document.createElement('option');
                            option.value = "addnew";
                            option.text = "Add new";
                            option.style.color = 'white';
                            option.style.backgroundColor = '#269abc';
                            select.add(option);
                            $('#newbrandtr').hide();
                            $('#newbrandtr2').hide();
                        });
                    }

                    function updatename() {

                        var modid = '<%=pradp3.getModel().getId()%>';
                        var newname = $('#newbrandname').val();
                        var brndop = document.getElementById("brands");
                        var value = brndop.options[brndop.selectedIndex].value;
                        if (newname != "") {
                            $.post(
                                    "Admin_update_product_model_name",
                                    {modid: modid, name: newname, newbrandid: value},
                            function (result) {
                                $('#newname').hide();
                                $('#btnpos').hide();
                                $('#newbrandtr').hide();
                                $('#newbrandtr2').hide();
                                $('#newbrandname').val("");
                                modal.style.display = "none";
                                location.reload(true);
                            });
                        } else {
                            $('#newbrandname').addClass('wrong-textbox');
                        }
                    }
                    $('#newbrandname').keydown(function () {
                        $('#newbrandname').removeClass('wrong-textbox');
                    });</script>



                <!--update qty-->

                <div id="addqty" class="addqty-model">
                    Modal content 
                    <div class="addqty-content">
                        <div class="addqty-header">
                            <span class="addqty-close">X</span>
                        </div>
                        <div class="addqty-body">
                            <table>
                                <tr>
                                    <td>Add more qty: </td>
                                    <td> <%=pradp3.getQty()%> + <input id="moretqy" class="width-add-qty" placeholder="more"></td>
                                    <td><input type="button" onclick="addqty()" value="Add"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div><!--
                -->               
                <script>
                    var modaladdqty = document.getElementById('addqty');
                    var btnaddqty = document.getElementById("addqty-btn");
                    var closeaddqty = document.getElementsByClassName("addqty-close")[0];
                    btnaddqty.onclick = function () {
                        modaladdqty.style.display = "block";
                    }
                    closeaddqty.onclick = function () {
                        modaladdqty.style.display = "none";
                    }
                    window.onclick = function (event) {
                        if (event.target == modaladdqty) {
                            modaladdqty.style.display = "none";
                        }
                    }
                    function addqty() {
                        var more = $('#moretqy').val();
                        $.post(
                                "Admin_add_qty",
                                {moreval: more},
                        function (result) {
                            modaladdqty.style.display = "none";
                            location.reload(true);
                        });
                    }
                </script>

                <script>
                    $('#chngestat-btn').click(function () {

                        var stat = $('#statdiv').text();
                        if (stat == "Active") {
                            $('#statdiv').html('Inactive');
                            $('#chngestat-btn').html('Active');
                            chngestat("2");
                        } else {
                            $('#statdiv').html('Active');
                            $('#chngestat-btn').html('Inactive');
                            chngestat("1");
                        }
                    });
                    function chngestat(stati) {
                        $.post(
                                "Admin_update_status",
                                {stid: stati},
                        function (result) {

                        });
                    }
                </script>

                <script>
                    $('#changeprtype').click(function () {
                        var prtype = $('#typeid').text();
                        if (prtype == "Smart phones") {
                            $('#typeid').html("Tabs");
                            changedevtype(2);
                        } else if (prtype == "Tabs") {
                            $('#typeid').html("Smart Watches");
                            changedevtype(4);
                        } else {
                            $('#typeid').html("Smart phones");
                            changedevtype(1);
                        }
                    });

                    function changedevtype(prtype) {
                        $.post(
                                "Admin_update_producttype",
                                {prtype: prtype},
                        function (result) {
//                           
                        });
                    }
                </script>
            </div>
        </div>
    </body>
</html>
