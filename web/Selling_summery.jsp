<%-- 
    Document   : Selling_summery
    Created on : Mar 26, 2016, 6:02:56 PM
    Author     : Ishara
--%>

<%@page import="org.apache.commons.lang3.time.DateUtils"%>
<%@page import="org.hibernate.criterion.Projections"%>
<%@page import="org.hibernate.criterion.Projection"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="org.hibernate.Criteria"%>
<%@page import="org.hibernate.Session"%>
<%@page import="java.util.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/main.css" rel="stylesheet" media="all">
        <script src="js/jquery-2.1.4.min.js"></script>
        <style>
            body {
                padding: 50px;
            }
            .label
            {
                font-size: 10pt;
                font-weight: bold;
                font-family: Arial;
            }
            .contents
            {
                border: 1px dotted black;
                padding: 5px;
                width: 300px;
            }
            .name
            {
                color: #18B5F0;
            }
            .left
            {
                float: left;
                width: 50px;
                height: 50px;
            }
            .right
            {
                margin-left: 60px;
                line-height:50px;
            }
            .clear
            {
                clear: both;
            }



        </style>
    </head>
    <body>
        <div id="mydiv">


            <%

                if (request.getParameter("type") != null) {

                    Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();

                    String type = request.getParameter("type");
                    if (type.equals("1")) {
            %>
            <div>
                <table id="q-graph">

                    <tbody>            
                        <%
                            int maxdaysell = 0;
                            int dateslimit = 7;
                            for (int ix = 0; ix < dateslimit; ++ix) {

                                Calendar cal = GregorianCalendar.getInstance();
                                cal.add(Calendar.DAY_OF_YEAR, -ix);
                                Date sevendaysago = cal.getTime();
                                String date = new SimpleDateFormat("yyyy-MM-dd").format(sevendaysago);
                                //String today = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
                                request.getParameter("id");

                                Criteria crd = ses.createCriteria(DB.Invoice.class);
                                crd.add(Restrictions.eq("date", date));
                                //crd.add(Restrictions.le("date", today));
                                List<DB.Invoice> in = crd.list();

                                int fulltotperday = 0;
                                for (DB.Invoice i : in) {
                                    out.print("asa");
                                    Criteria cr2 = ses.createCriteria(DB.InvoiceRegistry.class);
                                    cr2.add(Restrictions.eq("invoice", i));
                                    List<DB.InvoiceRegistry> invrlist = cr2.list();
                                    int invoicetot = 0;
                                    for (DB.InvoiceRegistry ir : invrlist) {
                                        double singleitemprice =ir.getProducts().getPrice();
                                        int itemq = Integer.parseInt(ir.getQty());
                                        invoicetot += singleitemprice * itemq;
                                    }
                                    fulltotperday += invoicetot;
                                }
                                if (maxdaysell < fulltotperday) {
                                    maxdaysell = fulltotperday;
                                }

                            }
                            int roundedNumber = ((maxdaysell + 99999) / 100000) * 100000;
                            if (roundedNumber != 0) {
                                int daycou = 1;
                                for (int ix = 0; ix < dateslimit; ++ix) {

                                    Calendar cal = GregorianCalendar.getInstance();
                                    cal.add(Calendar.DAY_OF_YEAR, -ix);
                                    Date sevendaysago = cal.getTime();
                                    String date = new SimpleDateFormat("yyyy-MM-dd").format(sevendaysago);
                                    //String today = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
                                    request.getParameter("id");
                                    Criteria crd = ses.createCriteria(DB.Invoice.class);
                                    crd.add(Restrictions.eq("date", date));
                                    //crd.add(Restrictions.le("date", today));
                                    List<DB.Invoice> in = crd.list();

                                    int fulltotperday = 0;
                                    for (DB.Invoice i : in) {
                                        Criteria cr2 = ses.createCriteria(DB.InvoiceRegistry.class);
                                        cr2.add(Restrictions.eq("invoice", i));
                                        List<DB.InvoiceRegistry> invrlist = cr2.list();
                                        int invoicetot = 0;
                                        for (DB.InvoiceRegistry ir : invrlist) {
                                            double singleitemprice = ir.getProducts().getPrice();
                                            int itemq = Integer.parseInt(ir.getQty());
                                            invoicetot += singleitemprice * itemq;
                                        }
                                        fulltotperday += invoicetot;
                                    }

                                    int colheight = (fulltotperday * 300) / roundedNumber;


                        %>

                    <style>
                        #q-graph #q<%=daycou%>{

                            <%
                                int left = (daycou - 1) * 150;
                            %>

                            left: <%=left%>px;

                        }

                    </style>

                    <tr class="qtr" id="q<%=daycou%>">
                        <th scope="row"><%=date%></th>
                        <td class="sent bar" style="height: <%=colheight%>px;"><p>Rs.<%=fulltotperday%></p></td>
                    </tr>


                    <%
                            ++daycou;

                        }

                        //            ses.createCriteria()
                    %>
                    </tbody>
                </table>

                <div id="ticks">
                    <%                   //
                        for (int i = 5; i > 0; --i) {
                            int a = (roundedNumber / 5) * i;

                    %>
                    <div class="tick" style="height: 59px;"><p><%=a%></p></div>
                            <%
                                }

                            %>
                </div>
            </div>


            <div id="product_details">
                <table id="product_details_table">
                    <tr>
                        <th></th>
                        <th>Date</th>
                        <th>Invoices count</th>
                        <th>Total sale</th>
                    </tr>
                    <%                        int tablenum = 0;
                        for (int ix = 0; ix < dateslimit; ++ix) {
                            ++tablenum;

                            Calendar cal = GregorianCalendar.getInstance();
                            cal.add(Calendar.DAY_OF_YEAR, -ix);
                            Date sevendaysago = cal.getTime();
                            String date = new SimpleDateFormat("yyyy-MM-dd").format(sevendaysago);
                            //String today = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
                            request.getParameter("id");
                            Criteria crd = ses.createCriteria(DB.Invoice.class);
                            crd.add(Restrictions.eq("date", date));
                            //crd.add(Restrictions.le("date", today));
                            List<DB.Invoice> in = crd.list();

                            int fulltotperday = 0;
                            int invcount = 0;
                            for (DB.Invoice i : in) {

                                Criteria cr2 = ses.createCriteria(DB.InvoiceRegistry.class);
                                cr2.add(Restrictions.eq("invoice", i));
                                List<DB.InvoiceRegistry> invrlist = cr2.list();
                                int invoicetot = 0;
                                for (DB.InvoiceRegistry ir : invrlist) {
                                    ++invcount;
                                    double singleitemprice = ir.getProducts().getPrice();
                                    int itemq = Integer.parseInt(ir.getQty());
                                    invoicetot += singleitemprice * itemq;
                                }
                                fulltotperday += invoicetot;
                            }

                    %>
                    <tr>
                        <td><%=tablenum%></td>
                        <td><%=date%></td>
                        <td><%=invcount%></td>
                        <td>b</td>
                    </tr>
                    <%

                        }
                    } else {

                    %>

                    <h2>Nothing sold in this range</h2>

                    <%                        }
                    %>

                </table>
            </div>




            <%                } else if (type.equals("2")) {

            %>
            <div id="as" id="daily">
                <table id="q-graph">

                    <tbody> 
                        <%                int daysperweek = 7;
                            int weeksnumber = 8;
                            int maxpperwk = 0;
                            for (int i = 0; i < 49; ++i) {

                                double daynum = i % 7;

                                if (daynum == 0) {
                                    Calendar cal = GregorianCalendar.getInstance();
                                    cal.add(Calendar.DAY_OF_YEAR, -i);
                                    Date sevendaysago = cal.getTime();

                                    int maxdaysell = 0;
                                    int fulltotoperweek = 0;
                                    for (int x = 0; x < 7; ++x) {
                                        Date dif = new Date(sevendaysago.getTime() - x * 24 * 3600 * 1000l);
                                        String dif2 = new SimpleDateFormat("yyyy-MM-dd").format(dif);
                                        Criteria crd = ses.createCriteria(DB.Invoice.class);
                                        crd.add(Restrictions.eq("date", dif2));
                                        //crd.add(Restrictions.le("date", today));
                                        List<DB.Invoice> ins = crd.list();
                                        int fulltotperday = 0;
                                        for (DB.Invoice in : ins) {
                                            Criteria cr2 = ses.createCriteria(DB.InvoiceRegistry.class);
                                            cr2.add(Restrictions.eq("invoice", in));
                                            List<DB.InvoiceRegistry> invrlist = cr2.list();
                                            int invoicetot = 0;
                                            for (DB.InvoiceRegistry ir : invrlist) {
                                                double singleitemprice = ir.getProducts().getPrice();
                                                int itemq = Integer.parseInt(ir.getQty());
                                                invoicetot += singleitemprice * itemq;
                                            }
                                            fulltotperday += invoicetot;
                                        }
                                        fulltotoperweek += fulltotperday;
                                    }

                                    if (fulltotoperweek > maxpperwk) {
                                        maxpperwk = fulltotoperweek;
                                    }

                                    //                            int colheight = (fulltotperday * 300) / roundedNumber;
                                    //                            System.out.print(fulltotperday);
                                    //                            System.out.print(roundedNumber);
                                    //                            System.out.print(colheight);
                                }

                            }
                            int roundedNumber = ((maxpperwk + 99999) / 100000) * 100000;
                            if (roundedNumber != 0) {

                                int wkcou = 0;
                                for (int i = 0; i < 49; ++i) {

                                    double daynum = i % 7;

                                    if (daynum == 0) {
                                        ++wkcou;
                                        Calendar cal = GregorianCalendar.getInstance();
                                        cal.add(Calendar.DAY_OF_YEAR, -i);
                                        Date sevendaysago = cal.getTime();
                                        String sevendaysago2 = new SimpleDateFormat("yyyy-MM-dd").format(sevendaysago);

                                        int maxdaysell = 0;
                                        int fulltotoperweek = 0;
                                        String dif2 = null;
                                        for (int x = 0; x < 7; ++x) {
                                            Date dif = new Date(sevendaysago.getTime() - x * 24 * 3600 * 1000l);
                                            dif2 = new SimpleDateFormat("yyyy-MM-dd").format(dif);
                                            Criteria crd = ses.createCriteria(DB.Invoice.class);
                                            crd.add(Restrictions.eq("date", dif2));
                                            //crd.add(Restrictions.le("date", today));
                                            List<DB.Invoice> ins = crd.list();
                                            int fulltotperday = 0;
                                            for (DB.Invoice in : ins) {
                                                Criteria cr2 = ses.createCriteria(DB.InvoiceRegistry.class);
                                                cr2.add(Restrictions.eq("invoice", in));
                                                List<DB.InvoiceRegistry> invrlist = cr2.list();
                                                int invoicetot = 0;
                                                for (DB.InvoiceRegistry ir : invrlist) {
                                                    double singleitemprice = ir.getProducts().getPrice();
                                                    int itemq = Integer.parseInt(ir.getQty());
                                                    invoicetot += singleitemprice * itemq;
                                                }
                                                fulltotperday += invoicetot;
                                            }
                                            fulltotoperweek += fulltotperday;
                                        }

                                        int colheight = (fulltotoperweek * 300) / roundedNumber;

            //                            int colheight = (fulltotperday * 300) / roundedNumber;
                                        //                            System.out.print(fulltotperday);
                                        //                            System.out.print(roundedNumber);
                                        //                            System.out.print(colheight);

                        %>



                    <style>
                        #q-graph #q<%=wkcou%>{

                            <%
                                int left = (wkcou - 1) * 150;

                            %>

                            left: <%=left%>px;

                        }

                    </style>

                    <tr class="qtr" id="q<%=wkcou%>">
                        <th scope="row"><%= sevendaysago2%> to <%=dif2%></th>
                        <td class="sent bar" style="height: <%=colheight%>px;"><p>Rs.<%=fulltotoperweek%></p></td>
                    </tr>



                    <%                        }

                        }
                    %>
                    </tbody>
                </table>

                <div id="ticks">
                    <%                   //
                        for (int i = 5; i > 0; --i) {
                            int a = (roundedNumber / 5) * i;

                    %>
                    <div class="tick" style="height: 59px;"><p><%=a%></p></div>
                            <%
                                }

                            %>
                </div>
            </div>

            <div id="product_details">
                <table id="product_details_table">
                    <tr>
                        <th></th>
                        <th>Date</th>
                        <th>Invoices count</th>
                        <th>Total sale</th>
                    </tr>
                    <%                        {
                            int wkcount = 0;
                            for (int i = 0; i < 49; ++i) {

                                double daynum = i % 7;

                                if (daynum == 0) {
                                    ++wkcount;

                                    Calendar cal = GregorianCalendar.getInstance();
                                    cal.add(Calendar.DAY_OF_YEAR, -i);
                                    Date sevendaysago = cal.getTime();
                                    String sevendaysago2 = new SimpleDateFormat("yyyy-MM-dd").format(sevendaysago);

                                    int maxdaysell = 0;
                                    int fulltotoperweek = 0;
                                    String dif2 = null;
                                    int invcountperwk = 0;
                                    for (int x = 0; x < 7; ++x) {
                                        Date dif = new Date(sevendaysago.getTime() - x * 24 * 3600 * 1000l);
                                        dif2 = new SimpleDateFormat("yyyy-MM-dd").format(dif);
                                        Criteria crd = ses.createCriteria(DB.Invoice.class);
                                        crd.add(Restrictions.eq("date", dif2));
                                        //crd.add(Restrictions.le("date", today));
                                        List<DB.Invoice> ins = crd.list();
                                        int fulltotperday = 0;
                                        for (DB.Invoice in : ins) {
                                            ++invcountperwk;
                                            Criteria cr2 = ses.createCriteria(DB.InvoiceRegistry.class);
                                            cr2.add(Restrictions.eq("invoice", in));
                                            List<DB.InvoiceRegistry> invrlist = cr2.list();
                                            int invoicetot = 0;
                                            for (DB.InvoiceRegistry ir : invrlist) {
                                                double singleitemprice =ir.getProducts().getPrice();
                                        int itemq = Integer.parseInt(ir.getQty());
                                                invoicetot += singleitemprice * itemq;
                                            }
                                            fulltotperday += invoicetot;

                                        }
                                        fulltotoperweek += fulltotperday;

                                    }
                    %>
                    <tr>
                        <td><%=wkcount%></td>
                        <td><%= sevendaysago2%> to <%=dif2%></td>
                        <td><%=invcountperwk%></td>
                        <td>Rs. <%=fulltotoperweek%>.00</td>
                    </tr>
                    <%
                                }

                            }
                        }
                    %>




                </table>
            </div>
        </div>


        <%                    } else {
                out.print("empty");
            }

        %>

        <%                        //
                } else if (type.equals("3")) {

                }
            }%>







        <button onclick="PrintElem('#mydiv')" value="aasa">adsf</button>

    </body>
    <script type="text/javascript">
//        $(function () {
//            $("#btnPrint").click(function () {
//                alert("a");
//                var myDiv = document.getElementById('mydiv');
//                var newWindow = window.open('', 'SecondWindow', 'toolbar=0,stat=0');
//                var style = newWindow.document.createElement('link');
//                style.type = "text/css";
//                style.rel = "stylesheet";
//                style.href = "css/main.css";
//                style.media = "all";
//                newWindow.document.write("<html><body " +
//                        "class='responsive light2012-home-switcher home switcher' " +
//                        " önload='window.print()'>" +
//                        myDiv.innerHTML +
//                        "</body></html>");
//                newWindow.document.getElementsByTagName("head")[0].appendChild(style);
//                newWindow.document.close();
//            });
//        });

//        function printContent(div_id)
//
//        {
//
//            var DocumentContainer = document.getElementById(div_id);
//
//            var html = '<html><head>' +
//                    '<link href="css/main.css" rel="stylesheet" type="text/css" />' +
//                    '</head><body style="background:#ffffff;">' +
//                    DocumentContainer.innerHTML +
//                    '</body></html>';
//
//
//
//            var WindowObject = window.open("", "PrintWindow",
//                    "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
//
//            WindowObject.document.writeln(html);
//
//            WindowObject.document.close();
//
//            WindowObject.focus();
//
//            WindowObject.print();
//
//            WindowObject.close();
//
//            document.getElementById('print_link').style.display = 'block';
//
//        }



        function PrintElem(elem)
        {
            Popup($('<div/>').append($(elem).clone()).html());
//            Popup($(elem).html());
        }

        function Popup(data)
        {
            var mywindow = window.open('', 'my div', 'height=400,width=600');
            mywindow.document.write('<html><head><title>my div</title>');
            /*optional stylesheet*/
            mywindow.document.write('<link rel="stylesheet" href="css/main.css" type="text/css" />');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10

            mywindow.print();
            // mywindow.close();

            return true;
        }
        //
        //        function printdiv(printdivname)
        //        {
        //            var headstr = "<html><head><title>Booking Details</title></head><body>";
        //            var footstr = "</body>";
        //            var newstr = document.getElementById('mydiv').innerHTML;
        //            var oldstr = document.body.innerHTML;
        //            document.body.innerHTML = headstr + newstr + footstr;
        //            window.print();
        //            document.body.innerHTML = oldstr;
        //            return false;
        //        }


    </script>
</html>
