package DB;
// Generated Jul 12, 2016 11:10:29 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * LoudSpeaker generated by hbm2java
 */
public class LoudSpeaker  implements java.io.Serializable {


     private Integer id;
     private String type;
     private Set sounds = new HashSet(0);

    public LoudSpeaker() {
    }

    public LoudSpeaker(String type, Set sounds) {
       this.type = type;
       this.sounds = sounds;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    public Set getSounds() {
        return this.sounds;
    }
    
    public void setSounds(Set sounds) {
        this.sounds = sounds;
    }




}


