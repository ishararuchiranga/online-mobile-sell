package DB;
// Generated Jul 12, 2016 11:10:29 PM by Hibernate Tools 4.3.1



/**
 * InvoiceRegistry generated by hbm2java
 */
public class InvoiceRegistry  implements java.io.Serializable {


     private Integer id;
     private Invoice invoice;
     private Products products;
     private String qty;

    public InvoiceRegistry() {
    }

	
    public InvoiceRegistry(Invoice invoice, Products products) {
        this.invoice = invoice;
        this.products = products;
    }
    public InvoiceRegistry(Invoice invoice, Products products, String qty) {
       this.invoice = invoice;
       this.products = products;
       this.qty = qty;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public Invoice getInvoice() {
        return this.invoice;
    }
    
    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
    public Products getProducts() {
        return this.products;
    }
    
    public void setProducts(Products products) {
        this.products = products;
    }
    public String getQty() {
        return this.qty;
    }
    
    public void setQty(String qty) {
        this.qty = qty;
    }




}


