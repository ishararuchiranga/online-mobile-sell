package DB;
// Generated Jul 12, 2016 11:10:29 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * AccesoriesAndParts generated by hbm2java
 */
public class AccesoriesAndParts  implements java.io.Serializable {


     private Integer id;
     private String name;
     private String description;
     private Set models = new HashSet(0);

    public AccesoriesAndParts() {
    }

    public AccesoriesAndParts(String name, String description, Set models) {
       this.name = name;
       this.description = description;
       this.models = models;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    public Set getModels() {
        return this.models;
    }
    
    public void setModels(Set models) {
        this.models = models;
    }




}


