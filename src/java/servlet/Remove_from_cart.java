/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import Java_classes.Cart;
import Java_classes.NewCartItems;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Remove_from_cart", urlPatterns = {"/Remove_from_cart"})
public class Remove_from_cart extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String id = request.getParameter("id");
            
            NewCartItems p = new NewCartItems();
            p.setProid(Integer.parseInt(id));
            
            HttpSession hs = request.getSession();
            Cart c = (Cart) hs.getAttribute("mycart");
            c.removepro(p);
            Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
            
            if (hs.getAttribute("Login_object") != null) {
                Transaction tr = ses.beginTransaction();
                Criteria cr = ses.createCriteria(DB.Cart.class);
                DB.Products pr = (DB.Products) ses.load(DB.Products.class, Integer.parseInt(id));
                cr.add(Restrictions.eq("products", pr));
                
                DB.UserRegistration ur = (DB.UserRegistration) hs.getAttribute("Login_object");
                
                cr.add(Restrictions.eq("userRegistration", ur));
                DB.Cart car = (DB.Cart) cr.uniqueResult();
                ses.delete(car);
                tr.commit();
            }
            
            hs.setAttribute("mycart", c);
            
            Java_classes.Cart c3 = (Java_classes.Cart) hs.getAttribute("mycart");
            List<Java_classes.NewCartItems> nnew3 = c3.getArrayData();
            double newtotprice = 0;
            System.out.println("listsize-" + nnew3.size());
            if (nnew3.size() != 0) {
                for (Java_classes.NewCartItems nci : nnew3) {
                    double itemsprice;
                    DB.Products pro = (DB.Products) ses.load(DB.Products.class, nci.getProid());
                    itemsprice = nci.getProQty() * pro.getPrice();
                    newtotprice += itemsprice;
                }
                
                DecimalFormat df = new DecimalFormat("0.00");
                if (newtotprice == 0) {
                    out.print("2," + 0);
                } else {
                    out.print("1," + df.format(newtotprice));
                }
            } else {
                out.print("2," + 0);
                hs.removeAttribute("mycart");
            }
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
