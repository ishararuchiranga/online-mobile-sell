/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Forgot_password", urlPatterns = {"/Forgot_password"})
public class Forgot_password extends HttpServlet {

    Integer addcustomerid;
    private String host;
    private String port;
    private String user;
    private String pass;

    public void init() {
        ServletContext context = getServletContext();
        host = context.getInitParameter("host");
        port = context.getInitParameter("port");
        user = context.getInitParameter("user");
        pass = context.getInitParameter("pass");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("awane bn");

        String text = "<br>Save Successfull<br>"; //message you will recieve 
        String regid = UUID.randomUUID().toString();

        String email = request.getParameter("email");

        //Check user is available in db
        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        Criteria c = ses.createCriteria(DB.UserRegistration.class);
        c.add(Restrictions.eq("email", email));
        DB.UserRegistration usr = (DB.UserRegistration) c.uniqueResult();
        PrintWriter out = response.getWriter();

        if (usr != null) {

            Transaction tx = null;
            tx = ses.beginTransaction();
            DB.UserRegistration ur = (DB.UserRegistration) ses.get(DB.UserRegistration.class, usr.getId());
            ur.setRegisterId(regid);
            ses.update(ur);
            tx.commit();

            String url = "http://localhost:8080/Phonehut.lk_2/Reset_Password.jsp?sjdail=" + regid + "!" + usr.getId() + "";
            try {
                Java_classes.EmailUtility.sendEmail(host, port, user, pass, "ishara.ira@gmail.com", "password reset",
                        url);
            } catch (MessagingException ex) {
                ex.printStackTrace();
            }

            out.println("1");
            System.out.println("1");
        } else {
            System.out.println("2");
            out.println("2");
        }
    }

}
