/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import Java_classes.Cart;
import Java_classes.NewCartItems;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "increase_cart_qty", urlPatterns = {"/increase_cart_qty"})
public class increase_cart_qty extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession hs = request.getSession();
        PrintWriter out = response.getWriter();

        String id, qty;
        int avqty = 0;
        id = request.getParameter("id");

        Cart c = (Cart) hs.getAttribute("mycart");
        List<NewCartItems> nc = c.getArrayData();

        forloop:
        for (NewCartItems n : nc) {
            if (n.getProid() == Integer.parseInt(id)) {
                avqty = n.getProQty();

                qty = "1";
                Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
                DB.Products pr = (DB.Products) ses.load(DB.Products.class, Integer.parseInt(id));
                System.out.println(Integer.parseInt(pr.getQty()));
                System.out.println(avqty + 1);
                if (avqty + 1 > Integer.parseInt(pr.getQty())) {
                    System.out.println("Aulak na");
                    out.print("1,");
                } else {

                    System.out.println("Aul");
                    Java_classes.NewCartItems nc2 = new Java_classes.NewCartItems();

                    nc2.setProid(Integer.parseInt(id));
                    nc2.setProQty(Integer.parseInt(qty));

                    Cart c2 = null;
                    if (hs.getAttribute("mycart") == null) {
                        c2 = new Cart();
                    } else {
                        c2 = (Cart) hs.getAttribute("mycart");
                    }
                    c.addProductToCart(nc2);
                    hs.setAttribute("mycart", c2);

                    if (hs.getAttribute("Login_object") != null) {
                        Transaction tr = ses.beginTransaction();
                        Criteria cr = ses.createCriteria(DB.Cart.class);
                        DB.Products probj = (DB.Products) ses.load(DB.Products.class, Integer.parseInt(id));
                        cr.add(Restrictions.eq("products", probj));

                        DB.UserRegistration ur = (DB.UserRegistration) hs.getAttribute("Login_object");

                        cr.add(Restrictions.eq("userRegistration", ur));
                        DB.Cart car = (DB.Cart) cr.uniqueResult();
                        if (car == null) {
                            DB.Cart crtsave = new DB.Cart(probj, ur, qty);
                            ses.save(crtsave);
                        } else {
                            car.setQty((Integer.parseInt(car.getQty()) + Integer.parseInt(qty)) + "");
                            ses.save(car);
                        }
                        tr.commit();

                    }
                    Java_classes.Cart c3 = (Java_classes.Cart) hs.getAttribute("mycart");
                    List<Java_classes.NewCartItems> nnew3 = c3.getArrayData();

                    double newtotprice = 0;
                    for (Java_classes.NewCartItems nci : nnew3) {
                        double itemsprice;
                        DB.Products pro = (DB.Products) ses.load(DB.Products.class, nci.getProid());
                        itemsprice = nci.getProQty() * pro.getPrice();
                        newtotprice += itemsprice;
                    }

                    double newitemprice = pr.getPrice() * (avqty + 1);
                    DecimalFormat df = new DecimalFormat("0.00");
                    out.print("2," + df.format(newitemprice) + "," + df.format(newtotprice));
                    ses.close();
                }
                break forloop;
            }
        }

    }

}
