/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Admin_add_new_brand", urlPatterns = {"/Admin_add_new_brand"})
public class Admin_add_new_brand extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       String brand=request.getParameter("brand");
        System.out.println(brand);
         Session s = conn.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr = null;
        PrintWriter out=response.getWriter();
        try {
            tr = s.beginTransaction();
            
            DB.Manufacturers man=new DB.Manufacturers(brand, null);
            s.save(man);
            out.print(man.getId());
            
            tr.commit();
            s.close();
           
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.gc();
        
    }
}
