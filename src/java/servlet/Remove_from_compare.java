/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import Java_classes.Cart;
import Java_classes.NewCompareItems;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Remove_from_compare", urlPatterns = {"/Remove_from_compare"})
public class Remove_from_compare extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            String id = request.getParameter("id");
            System.out.println(id);

            Java_classes.NewCompareItems p = new NewCompareItems();
            p.setProid(id);

            HttpSession hs = request.getSession();
            Java_classes.Compare c = (Java_classes.Compare) hs.getAttribute("mycompare");

            c.removecompare(p);

            Java_classes.Compare com8 = (Java_classes.Compare) hs.getAttribute("mycompare");
            List<Java_classes.NewCompareItems> n2 = com8.getArrayData();

            for (Java_classes.NewCompareItems ni : n2) {
                System.out.println(ni.getProid()+"a");
            }

            hs.setAttribute("mycompare", c);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
