/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Admin_shipping_change_cost", urlPatterns = {"/Admin_shipping_change_cost"})
public class Admin_shipping_change_cost extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        String cost = request.getParameter("cost");
        System.out.println(cost);
        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        PrintWriter out=response.getWriter();

        DB.District dis = (DB.District) ses.load(DB.District.class, Integer.parseInt(id));
        if (Integer.parseInt(cost) <= 5000) {
            Transaction tr = ses.beginTransaction();
            dis.setCost(Double.parseDouble(cost));
            ses.update(dis);
            tr.commit();
            ses.close();
            out.print(cost);
        } else {
            out.print(dis.getCost());
        }
    }

}
