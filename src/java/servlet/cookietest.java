///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package servlet;
//
//import java.io.IOException;
//import java.io.PrintWriter;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//
///**
// *
// * @author Ishara
// */
//@WebServlet(name = "cookietest", urlPatterns = {"/cookietest"})
//public class cookietest extends HttpServlet {
//
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        try {
//            String filepath = "c:\\file.xml";
//            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
//            Document doc = docBuilder.parse(filepath);
//
//            // Get the root element
//            Node company = doc.getFirstChild();
//
//		// Get the staff element , it may not working if tag has spaces, or
//            // whatever weird characters in front...it's better to use
//            // getElementsByTagName() to get it directly.
//            // Node staff = company.getFirstChild();
//            // Get the staff element by tag name directly
//            Node staff = doc.getElementsByTagName("staff").item(0);
//
//            // update staff attribute
//            NamedNodeMap attr = staff.getAttributes();
//            Node nodeAttr = attr.getNamedItem("id");
//            nodeAttr.setTextContent("2");
//
//            // append a new node to staff
//            Element age = doc.createElement("age");
//            age.appendChild(doc.createTextNode("28"));
//            staff.appendChild(age);
//
//            // loop the staff child node
//            NodeList list = staff.getChildNodes();
//
//            for (int i = 0; i < list.getLength(); i++) {
//
//                Node node = list.item(i);
//
//                // get the salary element, and update the value
//                if ("salary".equals(node.getNodeName())) {
//                    node.setTextContent("2000000");
//                }
//
//                //remove firstname
//                if ("firstname".equals(node.getNodeName())) {
//                    staff.removeChild(node);
//                }
//
//            }
//
//            // write the content into xml file
//            TransformerFactory transformerFactory = TransformerFactory.newInstance();
//            Transformer transformer = transformerFactory.newTransformer();
//            DOMSource source = new DOMSource(doc);
//            StreamResult result = new StreamResult(new File(filepath));
//            transformer.transform(source, result);
//
//            System.out.println("Done");
//
//        } catch (ParserConfigurationException pce) {
//            pce.printStackTrace();
//        } catch (TransformerException tfe) {
//            tfe.printStackTrace();
//        } catch (IOException ioe) {
//            ioe.printStackTrace();
//        } catch (SAXException sae) {
//            sae.printStackTrace();
//        }
//    }
//
//}
