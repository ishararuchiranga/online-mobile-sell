/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Profile_addPhnNo", urlPatterns = {"/Profile_addPhnNo"})
public class Profile_addPhnNo extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession se = request.getSession();
        DB.UserRegistration ure = (DB.UserRegistration) se.getAttribute("Login_object");

        String id = ure.getId() + "";
        String num = request.getParameter("num");

        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        Criteria cr = ses.createCriteria(DB.UserRegistration.class);
        cr.add(Restrictions.eq("id", Integer.parseInt(id)));
        DB.UserRegistration ur = (DB.UserRegistration) cr.uniqueResult();

        if (ur.getUserDetails() != null) {
            Transaction tr = ses.beginTransaction();

            DB.UserDetails reg = (DB.UserDetails) ses.get(DB.UserDetails.class, ur.getUserDetails().getId());
            reg.setPhone(num);
            ses.update(reg);
            tr.commit();

        } else {
            Transaction tr2 = ses.beginTransaction();

            DB.UserDetails ud = new DB.UserDetails(null, null, null, num, null, null, null, null);
            ses.save(ud);
            tr2.commit();

            Transaction tr3 = ses.beginTransaction();
            DB.UserRegistration reg = (DB.UserRegistration) ses.get(DB.UserRegistration.class, ur.getId());
            reg.setUserDetails(ud);
            ses.update(reg);
            tr3.commit();
        }

        ses.close();
    }

}
