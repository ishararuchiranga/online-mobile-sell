/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import DB.OtherFeatures;
import DB.OtherFeaturesHasSensors;
import DB.PlatformsUse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Admin_save_Device", urlPatterns = {"/Admin_save_Device"})
public class Admin_save_Device extends HttpServlet {

    private Integer networkid;
    private Integer launchid;
    private Integer bodyid;
    private Integer displayid;
    private Integer platformsuseid;
    private Integer memoryid;
    private Integer cameraid;
    private Integer soundid;
    private Integer commiunicationid;
    private Integer otherfeaturesid;
    private Integer batteryid;
    private Integer colorid;
    private Integer specid;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String manufacturer = request.getParameter("brand");
        String devicetyp = request.getParameter("devicetyp");
        String model = request.getParameter("model");

        Session s = conn.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr = s.beginTransaction();

//        try {
//            DB.Manufacturers ma = (DB.Manufacturers) s.load(DB.Manufacturers.class, Integer.parseInt(manufacturer));
//            DB.DeviceType div = (DB.DeviceType) s.load(DB.DeviceType.class, Integer.parseInt(devicetyp));
//            DB.Model mo = new DB.Model(div, ma, null, model, null);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        /**
         * ******Network**********
         */
        String[] ntworks = request.getParameterValues("ntworks[]");
        String[] netbands = request.getParameterValues("netbands[]");
        String spd = request.getParameter("spd");
        String gprs = request.getParameter("gprs");
        String edge = request.getParameter("ed");
        try {
            DB.Network network = new DB.Network(spd, gprs, edge, null, null);
            s.save(network);
            networkid = network.getId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        DB.Network ntwrk = (DB.Network) s.load(DB.Network.class, networkid);
        for (int i = 0; i < ntworks.length; i++) {

            DB.NetworkTypes networktypes = (DB.NetworkTypes) s.load(DB.NetworkTypes.class, Integer.parseInt(ntworks[i]));
            DB.BandsHasNetwork bhn = new DB.BandsHasNetwork(ntwrk, networktypes, netbands[i]);
            s.save(bhn);
        }
        /**
         * *******Launch**********
         */
        String an_date = request.getParameter("an_date");
        String stat = request.getParameter("stat");

        try {
            DB.LaunchStatus launchstat = (DB.LaunchStatus) s.load(DB.LaunchStatus.class, Integer.parseInt(stat));
            DB.Launch launch = new DB.Launch(launchstat, an_date, null);
            s.save(launch);
            launchid = launch.getId();
        } catch (Exception e) {
//            e.printStackTrace();
        }
        /**
         * *******Body**********
         */
        String dimension = request.getParameter("dimension");
        String weight = request.getParameter("weight");
        String simsize = request.getParameter("simsize");
        String sim = request.getParameter("sim");

        try {
            DB.Sim sm = (DB.Sim) s.load(DB.Sim.class, Integer.parseInt(sim));
            DB.SimSize smsize = (DB.SimSize) s.load(DB.SimSize.class, Integer.parseInt(simsize));

            DB.Body body = new DB.Body(sm, smsize, dimension, weight, null);
            s.save(body);
            bodyid = body.getId();

        } catch (Exception e) {
            e.printStackTrace();
        }
        /**
         * *******Display**********
         */
        String scrntye = request.getParameter("scrntye");
        String touchtype = request.getParameter("touchtype");
        String scrnsize = request.getParameter("scrnsize");
        String scrnratio = request.getParameter("scrnratio");
        String res = request.getParameter("res");
        String ppi = request.getParameter("ppi");
        String multitouch = request.getParameter("multitouch");
        String scrnprotection = request.getParameter("scrnprotection");
        String scrnotherdetails = request.getParameter("otherscrnde");

        try {
            DB.MultiTouch multtch = (DB.MultiTouch) s.load(DB.MultiTouch.class, Integer.parseInt(multitouch));
            DB.TouchType tchtype = (DB.TouchType) s.load(DB.TouchType.class, Integer.parseInt(touchtype));
            DB.ScrnType scrntype = (DB.ScrnType) s.load(DB.ScrnType.class, Integer.parseInt(scrntye));

            DB.Display display = new DB.Display(multtch, scrntype, tchtype, scrnsize, res, scrnprotection, scrnratio, ppi, scrnotherdetails, null);
            s.save(display);
            displayid = display.getId();

        } catch (Exception e) {
            e.printStackTrace();
        }
        /**
         * *******Display**********
         */
        String os = request.getParameter("os");
        String chip = request.getParameter("chip");
        String cpu = request.getParameter("cpu");
        String gpu = request.getParameter("gpu");

        try {
            DB.VersionUse versionuse = (DB.VersionUse) s.load(DB.VersionUse.class, Integer.parseInt(os));
            DB.PlatformsUse platformsuse = new PlatformsUse(versionuse, chip, cpu, gpu, null);
            s.save(platformsuse);
            platformsuseid = platformsuse.getId();

        } catch (Exception e) {
            e.printStackTrace();
        }
        /**
         * *******Memory**********
         */
        String cardslot = request.getParameter("cardslot");
        String cardup = request.getParameter("cardup");
        String ram = request.getParameter("ram");
        String intmem = request.getParameter("intmem");

        try {

            DB.CardSlot crdslt = (DB.CardSlot) s.load(DB.CardSlot.class, Integer.parseInt(cardslot));
            DB.Memory memory = new DB.Memory(crdslt, intmem, ram, cardup, null);
            s.save(memory);
            memoryid = memory.getId();

        } catch (Exception e) {
        }
        /**
         * *******Memory**********
         */
        String pixels = request.getParameter("pixels");
        String camres = request.getParameter("camres");
        String camfeat = request.getParameter("camfeat");
        String video = request.getParameter("video");
        String seccampix = request.getParameter("seccampix");
        String seccaminf = request.getParameter("seccaminf");

        try {
            DB.Camera camera = new DB.Camera(pixels, video, seccampix + "" + seccaminf, camres, camfeat, null, null);
            s.save(camera);
            cameraid = camera.getId();

        } catch (Exception e) {
            e.printStackTrace();
        }
        /**
         * *******Sound**********
         */
        String[] alerttypes = request.getParameterValues("alerttypes[]");
        String loudspeaker = request.getParameter("loudspeaker");
        String threefive = request.getParameter("threefive");
        String souther = request.getParameter("souther");

        DB.ThreefivveJack thfivej = (DB.ThreefivveJack) s.load(DB.ThreefivveJack.class, Integer.parseInt(threefive));
        DB.LoudSpeaker loud = (DB.LoudSpeaker) s.load(DB.LoudSpeaker.class, Integer.parseInt(loudspeaker));

        DB.Sound sound = new DB.Sound(loud, thfivej, souther, null, null);
        s.save(sound);
        soundid = sound.getId();

        for (int i = 0; i < alerttypes.length; i++) {
            DB.AlertTypes alerttype = (DB.AlertTypes) s.load(DB.AlertTypes.class, Integer.parseInt(alerttypes[i]));
            DB.AlertTypesHasSound aths = new DB.AlertTypesHasSound(alerttype, sound);
            s.save(aths);
        }
        /**
         * *******Comms**********
         */
        String wl = request.getParameter("wl");
        String bluet = request.getParameter("bluet");
        String gps = request.getParameter("gps");
        String nfc = request.getParameter("nfc");
        String radio = request.getParameter("radio");
        String usb = request.getParameter("usb");

        DB.Nfc nf = (DB.Nfc) s.load(DB.Nfc.class, Integer.parseInt(nfc));

        DB.Communication commiunication = new DB.Communication(nf, wl, bluet, gps, usb, radio, null);
        s.save(commiunication);
        commiunicationid = commiunication.getId();

        /**
         * *******Features**********
         */
        String[] sensor = request.getParameterValues("sensor[]");
        String msg = request.getParameter("msg");
        String browser = request.getParameter("browser");
        String ja = request.getParameter("ja");
        String other = request.getParameter("other");

        DB.JavaSupport javasup = (DB.JavaSupport) s.load(DB.JavaSupport.class, Integer.parseInt(ja));

        DB.OtherFeatures otherfeatures = new OtherFeatures(javasup, browser, other, msg, null, null);
        s.save(otherfeatures);
        otherfeaturesid = otherfeatures.getId();

        for (int i = 0; i < sensor.length; i++) {
            DB.Sensors sens = (DB.Sensors) s.load(DB.Sensors.class, Integer.parseInt(sensor[i]));
            DB.OtherFeaturesHasSensors ofhs = new OtherFeaturesHasSensors(otherfeatures, sens);
            s.save(ofhs);

        }
        /**
         * *******Features**********
         */
        String btrymo = request.getParameter("btrymo");
        String btrysize = request.getParameter("btrysize");
        String fstchrj = request.getParameter("fstchrj");
        String standb = request.getParameter("standb");
        String calltime = request.getParameter("calltime");
        String color = request.getParameter("color");

        DB.FastChrj fstchr = (DB.FastChrj) s.load(DB.FastChrj.class, Integer.parseInt(fstchrj));
        DB.Removeble rem = (DB.Removeble) s.load(DB.Removeble.class, Integer.parseInt(btrymo));

        DB.Battery battery = new DB.Battery(fstchr, rem, btrysize, standb, calltime, null);
        s.save(battery);
        batteryid = battery.getId();

        /**
         * *******Color**********
         */
        DB.Color col = new DB.Color(color, null);
        s.save(col);
        colorid = col.getId();
        /**
         * *******Save spec table**********
         */

        try {

            DB.Network net = (DB.Network) s.load(DB.Network.class, networkid);
            DB.Launch lau = (DB.Launch) s.load(DB.Launch.class, launchid);
            DB.Body bod = (DB.Body) s.load(DB.Body.class, bodyid);
            DB.Display dis = (DB.Display) s.load(DB.Display.class, displayid);
            DB.PlatformsUse pla = (DB.PlatformsUse) s.load(DB.PlatformsUse.class, platformsuseid);
            DB.Memory mem = (DB.Memory) s.load(DB.Memory.class, memoryid);
            DB.Camera cam = (DB.Camera) s.load(DB.Camera.class, cameraid);
            DB.Sound sou = (DB.Sound) s.load(DB.Sound.class, soundid);
            DB.Communication com = (DB.Communication) s.load(DB.Communication.class, commiunicationid);
            DB.OtherFeatures oth = (DB.OtherFeatures) s.load(DB.OtherFeatures.class, otherfeaturesid);
            DB.Battery bat = (DB.Battery) s.load(DB.Battery.class, batteryid);
            DB.Color colo = (DB.Color) s.load(DB.Color.class, colorid);
            //
            DB.Specification spec = new DB.Specification(bat, bod, cam, colo, com, dis, lau, mem, net, oth, pla, sou);
            s.save(spec);
            specid = spec.getId();

        } catch (Exception e) {
            e.printStackTrace();
        }

        /**
         * *******Save model**********
         */
        //key
        String keywords = null;
        keywords = manufacturer+ " ";
        keywords += manufacturer + " " + devicetyp+ " ";
        keywords += manufacturer + " " + model+ " ";
        keywords += devicetyp + " " + manufacturer+ " ";
        keywords += model + " " + devicetyp+ " ";
        keywords += "," + color + "" + manufacturer+ " ";
        keywords += "," + manufacturer + "" + color+ " ";
        keywords += "," + os + " " + manufacturer+ " ";
        keywords += "," + manufacturer + "" + os;

        DB.Model mod = null;
        try {
            DB.Manufacturers ma = (DB.Manufacturers) s.load(DB.Manufacturers.class, Integer.parseInt(manufacturer));
            DB.DeviceType div = (DB.DeviceType) s.load(DB.DeviceType.class, Integer.parseInt(devicetyp));
            DB.Specification sp = (DB.Specification) s.load(DB.Specification.class, specid);

            HttpSession httpses = request.getSession();
            String imgpath = (String) httpses.getAttribute("path");

            // mod = new DB.Model(null, div, ma, sp, model, imgpath, null);
            mod = new DB.Model(null, div, ma, sp, model, imgpath, keywords, null);
            s.save(mod);

        } catch (Exception e) {
            e.printStackTrace();
        }
//save product
        String price = request.getParameter("price");
        String qty = request.getParameter("qty");

        String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        String time = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());

        DB.ProStatus prost=(DB.ProStatus) s.load(DB.ProStatus.class, Integer.parseInt("1"));
        
        DB.Products pro = new DB.Products(mod, prost,date, Double.parseDouble(price), qty, "", time, null, null,null);
        s.save(pro);

        tr.commit();
        s.close();
        System.gc();

        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        Criteria c = ses.createCriteria(DB.Products.class);
        c.add(Restrictions.eq("id", Integer.parseInt("7")));
        DB.Products pr = (DB.Products) c.uniqueResult();

        Set<DB.BandsHasNetwork> bandsHasNetworks = pr.getModel().getSpecification().getNetwork().getBandsHasNetworks();
        for (DB.BandsHasNetwork bandsHasNetwork : bandsHasNetworks) {
            bandsHasNetwork.getId();
        }
    }

}
