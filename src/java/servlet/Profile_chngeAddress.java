/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Profile_chngeAddress", urlPatterns = {"/Profile_chngeAddress"})
public class Profile_chngeAddress extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession se = request.getSession();
        DB.UserRegistration ure = (DB.UserRegistration) se.getAttribute("Login_object");

        String addressLine1 = request.getParameter("line1");
        String addressLine2 = request.getParameter("line2");
        String city = request.getParameter("city");
        String distri = request.getParameter("province");

        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        DB.UserRegistration ur = (DB.UserRegistration) ses.load(DB.UserRegistration.class, ure.getId());
        if (ur != null) {
            ur.getUserDetails().getId();
            Transaction tr = ses.beginTransaction();
            DB.UserDetails reg = (DB.UserDetails) ses.get(DB.UserDetails.class, ur.getUserDetails().getId());
            DB.District dis = (DB.District) ses.get(DB.District.class, Integer.parseInt(distri));
            reg.setDistrict(dis);
            reg.setAdLine1(addressLine1);
            reg.setAdLine2(addressLine2);
            reg.setCity(city);

            ses.save(reg);
            tr.commit();
            ses.close();
        }
    }

}
