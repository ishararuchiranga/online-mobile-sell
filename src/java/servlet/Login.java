/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import DB.Cart;
import DB.CookieTable;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.UUID;
import org.hibernate.Transaction;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String comefrom = request.getHeader("referer");

        boolean keepme = "true".equals(request.getParameter("keepme"));
        String email = request.getParameter("loginemailad");
        String password = request.getParameter("loginfrmpwrd");

        PrintWriter out = response.getWriter();

        SessionFactory ses = conn.NewHibernateUtil.getSessionFactory();
        Session session = ses.openSession();
        DB.UserRegistration ur = Java_classes.first.Userlogin(session, email, password);
        HttpSession httpses = request.getSession();
        if (ur != null) {
            String uname = ur.getUserName();
            String email_ad = ur.getEmail();
            int user_type = ur.getUserTypes().getId();
            int user_reg_status = ur.getStatus().getId();
            int uid = ur.getId();

            httpses.setAttribute("Login_object", ur);
//            httpses.setAttribute("uname", uname);
//            httpses.setAttribute("email", email);
//            httpses.setAttribute("utype", user_type);
//            httpses.setAttribute("urstatus", user_reg_status);
//            httpses.setAttribute("userid", uid);
            Transaction tr = session.beginTransaction();

            //cart check
            if (httpses.getAttribute("mycart") != null) {
                Java_classes.Cart c = (Java_classes.Cart) httpses.getAttribute("mycart");
                List<Java_classes.NewCartItems> n = c.getArrayData();

                for (Java_classes.NewCartItems ni : n) {
                    Criteria cri = session.createCriteria(DB.Products.class);
                    cri.add(Restrictions.eq("id", ni.getProid()));
                    DB.Products pr = (DB.Products) cri.uniqueResult();

                    //check, is there a cart in db for this user
                    System.out.println("empty");
                    Criteria crprsearch = session.createCriteria(DB.Cart.class);
                    DB.Products probj = (DB.Products) session.load(DB.Products.class, ni.getProid());
                    crprsearch.add(Restrictions.eq("userRegistration", ur));
                    crprsearch.add(Restrictions.eq("products", probj));
                    DB.Cart cartres = (DB.Cart) crprsearch.uniqueResult();
                    if (cartres != null) {
                        DB.Cart dbupdateqty = (DB.Cart) session.get(DB.Cart.class, cartres.getId());
                        dbupdateqty.setQty(ni.getProQty() + "");
                        session.update(probj);
                    } else {
                        DB.Cart cartobj = new Cart(pr, ur, ni.getProQty() + "");
                        session.save(cartobj);
                    }

                }
                httpses.removeAttribute("mycart");
                Criteria crmycart = session.createCriteria(DB.Cart.class);
                crmycart.add(Restrictions.eq("userRegistration", ur));
                List<DB.Cart> mycartlist = crmycart.list();
                for (Cart mycartlist1 : mycartlist) {
                    Java_classes.NewCartItems nc = new Java_classes.NewCartItems();

                    nc.setProid(mycartlist1.getProducts().getId());
                    nc.setProQty(Integer.parseInt(mycartlist1.getQty()));
//
                    HttpSession hs = request.getSession();

                    Java_classes.Cart crtcls = null;

                    if (hs.getAttribute("mycart") == null) {
                        crtcls = new Java_classes.Cart();
                    } else {
                        crtcls = (Java_classes.Cart) hs.getAttribute("mycart");
                    }

                    crtcls.addProductToCart(nc);

                    hs.setAttribute("mycart", crtcls);
                }
            } else {
                Criteria crmycart = session.createCriteria(DB.Cart.class);
                crmycart.add(Restrictions.eq("userRegistration", ur));
                List<DB.Cart> mycartlist = crmycart.list();
                for (Cart mycartlist1 : mycartlist) {
                    Java_classes.NewCartItems nc = new Java_classes.NewCartItems();

                    nc.setProid(mycartlist1.getProducts().getId());
                    nc.setProQty(Integer.parseInt(mycartlist1.getQty()));
//
                    HttpSession hs = request.getSession();

                    Java_classes.Cart crtcls = null;

                    if (hs.getAttribute("mycart") == null) {
                        crtcls = new Java_classes.Cart();
                    } else {
                        crtcls = (Java_classes.Cart) hs.getAttribute("mycart");
                    }

                    crtcls.addProductToCart(nc);

                    hs.setAttribute("mycart", crtcls);
                }

            }

            //keep me signin
            if (keepme) {
//                Criteria cravailcookie = session.createCriteria(DB.CookieTable.class);
//                cravailcookie.add(Restrictions.eq("userRegistration", ur));
//                List<DB.CookieTable> cookt = cravailcookie.list();
//                if (!(cookt.isEmpty())) {
//                    for (DB.CookieTable c : cookt) {
//                        session.delete(c);
//                    }
//                }
                String uuid = UUID.randomUUID().toString();
                DB.CookieTable ct = new CookieTable(uuid, ur);
                session.save(ct);
                Java_classes.cookie.addCookie(response, "phonehutloginquthentication", uuid, 864000);
            }
//            else {
//                Criteria cricook = session.createCriteria(DB.CookieTable.class);
//                cricook.add(Restrictions.eq("userRegistration", ur));
//
//                DB.CookieTable ck = (DB.CookieTable) cricook.uniqueResult();
//                if (ck != null) {
//                    session.delete(ck);
//                }
//                Java_classes.cookie.removeCookie(response, "phonehutloginquthentication");
//            }

            String from = (String) httpses.getAttribute("requestfrom");
            //redirect to certain jsp
            if (from != null) {
                httpses.removeAttribute("requestfrom");
                response.sendRedirect(from);
            } else {
                response.sendRedirect(comefrom);
            }
            tr.commit();

        } else {
            httpses.setAttribute("login_status", "2");
            response.sendRedirect("index.jsp#opensignin");
        }

        System.gc();
    }

}
