/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import DB.InvoiceRegistry;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Buy_product", urlPatterns = {"/Buy_product"})
public class Buy_product extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        HttpSession hs = request.getSession();

        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String phn = request.getParameter("phn");
        String adl1 = request.getParameter("adl1");
        String adl2 = request.getParameter("adl2");
        String city = request.getParameter("city");
        String dis = request.getParameter("prov");
        PrintWriter out = response.getWriter();

        DB.District dist = (DB.District) ses.load(DB.District.class, Integer.parseInt(dis));

        DB.UserRegistration ur = (DB.UserRegistration) hs.getAttribute("Login_object");
        DB.UserRegistration ure = (DB.UserRegistration) ses.load(DB.UserRegistration.class, ur.getId());

        if (ure.getStatus().getId() == Integer.parseInt("2")) {

            //send data to invoice
            hs.setAttribute("ship_fname", fname);
            hs.setAttribute("ship_lname", lname);
            hs.setAttribute("ship_phn", phn);
            hs.setAttribute("ship_adl1", adl1);
            hs.setAttribute("ship_adl2", adl2);
            hs.setAttribute("ship_city", city);
            hs.setAttribute("dist", dist.getDistrict());

            //
            

//            DB.Delivery del = new DB.Delivery(ure, fname, lname, phn, adl1, adl2, city, prov, null);
            DB.Delivery del = new DB.Delivery(dist, ure, fname, lname, phn, adl1, adl2, city, null);
            ses.save(del);
            //
            String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
            String time = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());

            hs.setAttribute("inv_date", date);
            hs.setAttribute("inv_time", time);
            //

            Java_classes.Cart c3 = (Java_classes.Cart) hs.getAttribute("mycart");
            List<Java_classes.NewCartItems> nnew3 = c3.getArrayData();
            double newtotprice = 0;
            for (Java_classes.NewCartItems nci : nnew3) {
                double itemsprice;
                DB.Products pro = (DB.Products) ses.load(DB.Products.class, nci.getProid());
                itemsprice = nci.getProQty() * pro.getPrice();
                newtotprice += itemsprice;
            }
            hs.setAttribute("subtot", newtotprice);

            if (dist.getCostType().getId() == 1) {
                newtotprice += dist.getCost();
                hs.setAttribute("ship", dist.getCost());
            } else {
                hs.setAttribute("ship", "x");
            }

            hs.setAttribute("tot", newtotprice);
            //
            DB.DeleverStatus dels = (DB.DeleverStatus) ses.load(DB.DeleverStatus.class, 1);
            // DB.Invoice in = new DB.Invoice(del, ur, date, null);
            DB.Invoice in = new DB.Invoice(dels, del, ure, date, "0", time, newtotprice, null);
            //

            ses.save(in);
            hs.setAttribute("inv_id", in.getId().toString());

            Java_classes.Cart c = (Java_classes.Cart) hs.getAttribute("mycart");

            hs.setAttribute("Invoice", c);

            List<Java_classes.NewCartItems> n = c.getArrayData();
            forloop1:
            for (Java_classes.NewCartItems ni : n) {
                DB.Products pr = (DB.Products) ses.get(DB.Products.class, ni.getProid());
                if (pr.getQty().equals("0")) {
                    out.print("fail," + ni.getProid());
                    break forloop1;
                } else if (pr.getProStatus().getId() == 2) {
                    out.print("fail2," + ni.getProid());
                    break forloop1;
                } else if (Integer.parseInt(pr.getQty()) < ni.getProQty()) {
                    out.print("fail3," + ni.getProid());
                    break forloop1;
                } else {
                    pr.setQty((Integer.parseInt(pr.getQty()) - ni.getProQty()) + "");
                    ses.update(pr);
                    //
                    DB.InvoiceRegistry ir = new InvoiceRegistry(in, pr, ni.getProQty() + "");
                    ses.save(ir);

                    //remove carts from db
                    Criteria crcarts = ses.createCriteria(DB.Cart.class);
                    crcarts.add(Restrictions.eq("userRegistration", ure));
                    List<DB.Cart> listcart = crcarts.list();
                    if (!(listcart.isEmpty())) {
                        for (DB.Cart crt : listcart) {
                            ses.delete(crt);
                        }
                    }
                    hs.removeAttribute("mycart");
                    
                    out.print("pass,");
                    Transaction tr = ses.beginTransaction();
                    tr.commit();
                }
            }

            System.gc();
        } else {
            request.getSession().invalidate();
            out.print("redirect,");
        }
        
        ses.close();
    }

}
