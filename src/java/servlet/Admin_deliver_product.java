/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author isharar
 */
@WebServlet(name = "Admin_deliver_product", urlPatterns = {"/Admin_deliver_product"})
public class Admin_deliver_product extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String invoiceid=request.getParameter("invid");
        Session ses=conn.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr=ses.beginTransaction();
        
        DB.Invoice in=(DB.Invoice) ses.load(DB.Invoice.class, Integer.parseInt(invoiceid));
        DB.DeleverStatus del=(DB.DeleverStatus) ses.load(DB.DeleverStatus.class, 2);
        in.setDeleverStatus(del);
        ses.update(in);
        tr.commit();
        ses.close();       
        
    }

}
