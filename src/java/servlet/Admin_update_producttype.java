/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Admin_update_producttype", urlPatterns = {"/Admin_update_producttype"})
public class Admin_update_producttype extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession ses = request.getSession();
        String prid = (String) ses.getAttribute("updateproid");
        String typeid = request.getParameter("prtype");

        Session sess = conn.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr = sess.beginTransaction();

        DB.DeviceType devt = (DB.DeviceType) sess.load(DB.DeviceType.class, Integer.parseInt(typeid));
        DB.Products pr = (DB.Products) sess.load(DB.Products.class, Integer.parseInt(prid));
        DB.Model mo = pr.getModel();
        mo.setDeviceType(devt);
        sess.update(mo);
        tr.commit();
        sess.close();
        
    }
}
