/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import com.mysql.jdbc.exceptions.MySQLSyntaxErrorException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Logout", urlPatterns = {"/Logout"})
public class Logout extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String comefrom = request.getHeader("referer");
        request.getSession().invalidate();

        String uuid = Java_classes.cookie.getCookieValue(request, "phonehutloginquthentication");
        if (uuid != null) {
            Java_classes.cookie.removeCookie(response, "phonehutloginquthentication");
            Session seshd = conn.NewHibernateUtil.getSessionFactory().openSession();
            Transaction tr = seshd.beginTransaction();
            DB.CookieTable cook = (DB.CookieTable) seshd.load(DB.CookieTable.class, uuid);
            if (cook != null) {
                seshd.delete(cook);
                tr.commit();
            }
        }

        response.sendRedirect(comefrom);
        System.gc();
    }

}
