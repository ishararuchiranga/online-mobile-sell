/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Rate_product2", urlPatterns = {"/Rate_product2"})
public class Rate_product2 extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        String review = request.getParameter("review");
        String rate = request.getParameter("rate");
        String name = request.getParameter("name");
        String email = request.getParameter("email");

        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr = ses.beginTransaction();
        DB.Products pr = (DB.Products) ses.load(DB.Products.class, Integer.parseInt(id));
        
         String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        String time = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());

        
        DB.Nonregistercomment nonre=new DB.Nonregistercomment(name, email, null);
        ses.save(nonre);
        
        DB.Rating ra=new DB.Rating(nonre, pr, null, rate, review, date, time);
        ses.save(ra);
        tr.commit();
        ses.close();
    }

}
