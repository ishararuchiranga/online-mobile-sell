/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Admin_Bannerurl_get", urlPatterns = {"/Admin_Bannerurl_get"})
public class Admin_Bannerurl_get extends HttpServlet {
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String id = request.getParameter("id");
        PrintWriter out = response.getWriter();
        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        Criteria crad1 = ses.createCriteria(DB.Banner.class);
        crad1.addOrder(Order.asc("id"));
        List<DB.Banner> ban = crad1.list();
        int bannerno = 1;
        if (!(ban.isEmpty())) {
            
            for (DB.Banner b : ban) {
                if (bannerno == Integer.parseInt(id)) {
                    out.print(b.getUrl());
                }
                ++bannerno;
            }
        }
    }
    
}
