/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Compare_search_devices", urlPatterns = {"/Compare_search_devices"})
public class Compare_search_devices extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        PrintWriter out = response.getWriter();

        String keyword = request.getParameter("keyword");

        if (keyword != null) {
            Criteria cr1 = ses.createCriteria(DB.Products.class);

            Criteria criteriatest = ses.createCriteria(DB.Model.class);
            criteriatest.add(Restrictions.like("keywords", "%" + keyword + "%"));
            List<DB.Model> mlist = criteriatest.list();

            cr1.add(Restrictions.in("model", mlist));

            List<DB.Products> plist = cr1.list();

            try {
                //create Json Object
                JSONObject json = new JSONObject();

                // put some value pairs into the JSON object .
                for (DB.Products pro : plist) {
                    json.put(pro.getId() + "", pro.getModel().getManufacturers().getName() + " " + pro.getModel().getModel());
                }

                for (int i = 0; i < json.length(); i++) {
                    System.out.println(json.length());
                }
                // finally output the json string
                out.print(json);
                System.out.println(json);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            ses.close();
        }

    }

}
