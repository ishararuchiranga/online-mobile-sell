/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Admin_update_user_status", urlPatterns = {"/Admin_update_user_status"})
public class Admin_update_user_status extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String uid = request.getParameter("uid");
        String stid = request.getParameter("stid");
        System.out.println(uid);
        System.out.println(stid);

        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr = ses.beginTransaction();
        DB.UserRegistration ur = (DB.UserRegistration) ses.load(DB.UserRegistration.class, Integer.parseInt(uid));
        DB.Status st = (DB.Status) ses.load(DB.Status.class, Integer.parseInt(stid));
        ur.setStatus(st);
        ses.update(ur);
        tr.commit();
        ses.close();

    }

}
