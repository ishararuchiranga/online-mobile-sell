/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Profile_deletePurchaseHistory", urlPatterns = {"/Profile_deletePurchaseHistory"})
public class Profile_deletePurchaseHistory extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String ids = request.getParameter("id");
        String idArray[] = ids.split(",");
        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr = ses.beginTransaction();
        for (int i = 0; i < idArray.length; i++) {
            System.out.println(idArray[i]);
            DB.Invoice inv = (DB.Invoice) ses.get(DB.Invoice.class, Integer.parseInt(idArray[i]));
            inv.setUserDeleted("1");
            ses.update(inv);
        }
        tr.commit();
        ses.close();

    }

}
