/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Admin_update_status", urlPatterns = {"/Admin_update_status"})
public class Admin_update_status extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesh = request.getSession();
        String prid = (String) sesh.getAttribute("updateproid");
        String statid=request.getParameter("stid");
        
        Session ses=conn.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr=ses.beginTransaction();
        DB.ProStatus prs=(DB.ProStatus) ses.load(DB.ProStatus.class, Integer.parseInt(statid));
        DB.Products pr=(DB.Products) ses.load(DB.Products.class, Integer.parseInt(prid));
        pr.setProStatus(prs);
        ses.update(pr);
        tr.commit();
        ses.close();
    }


}
