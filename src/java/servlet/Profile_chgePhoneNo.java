/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Profile_chgePhoneNo", urlPatterns = {"/Profile_chgePhoneNo"})
public class Profile_chgePhoneNo extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession se = request.getSession();
        DB.UserRegistration ure = (DB.UserRegistration) se.getAttribute("Login_object");
        String id = ure.getId()+"";
        String phn = request.getParameter("num");

        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        Criteria cr = ses.createCriteria(DB.UserRegistration.class);
        cr.add(Restrictions.eq("id", Integer.parseInt(id)));
        DB.UserRegistration ur = (DB.UserRegistration) cr.uniqueResult();

        if (ur != null) {
            ur.getUserDetails().getId();
            Transaction tr = ses.beginTransaction();
            DB.UserDetails reg = (DB.UserDetails) ses.get(DB.UserDetails.class, ur.getUserDetails().getId());
            reg.setPhone(phn);
            ses.save(reg);
            tr.commit();
            ses.close();
        }
    }

}
