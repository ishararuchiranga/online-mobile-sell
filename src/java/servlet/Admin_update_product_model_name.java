/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Admin_update_product_model_name", urlPatterns = {"/Admin_update_product_model_name"})
public class Admin_update_product_model_name extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String moid = request.getParameter("modid");
        String name = request.getParameter("name");
        String newbrand = request.getParameter("newbrandid");

        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr = ses.beginTransaction();
        DB.Model mod = (DB.Model) ses.load(DB.Model.class, Integer.parseInt(moid));
        mod.setModel(name);
        DB.Manufacturers man = (DB.Manufacturers) ses.load(DB.Manufacturers.class, Integer.parseInt(newbrand));
        mod.setManufacturers(man);
        
        String manufacturer=man.getName();
        String devicetyp=mod.getDeviceType().getType();
        String color=mod.getSpecification().getColor().getColor();
        String os=mod.getSpecification().getPlatformsUse().getVersionUse().getOsList().getOses();

        String keywords = null;
        keywords += manufacturer;
        keywords += manufacturer + " " + devicetyp;
        keywords += manufacturer + " " + name;
        keywords += devicetyp + " " + manufacturer;
        keywords += name + " " + devicetyp;
        keywords += "," + color + "" + manufacturer;
        keywords += "," + manufacturer + "" + color;
        keywords += "," + os + " " + manufacturer;
        keywords += "," + manufacturer + "" + os;
   
        mod.setKeywords(keywords);

        ses.update(mod);
        tr.commit();
        ses.close();
    }
}