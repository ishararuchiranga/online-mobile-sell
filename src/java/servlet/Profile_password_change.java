/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Profile_password_change", urlPatterns = {"/Profile_password_change"})
public class Profile_password_change extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession se = request.getSession();
        DB.UserRegistration ure = (DB.UserRegistration) se.getAttribute("Login_object");

        PrintWriter out = response.getWriter();
        String p1 = request.getParameter("pwrd");
        String p2 = request.getParameter("pwrd2");
        String id = ure.getId() + "";
        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        Criteria cr = ses.createCriteria(DB.UserRegistration.class);
        cr.add(Restrictions.eq("id", Integer.parseInt(id)));
        cr.add(Restrictions.eq("password", p1));
        DB.UserRegistration ur = (DB.UserRegistration) cr.uniqueResult();
        if (ur != null) {
            Transaction tr = ses.beginTransaction();
            DB.UserRegistration reg = (DB.UserRegistration) ses.get(DB.UserRegistration.class, ur.getId());
            reg.setPassword(p2);
            ses.save(reg);
            tr.commit();
            ses.close();
        } else {
            System.out.println("wrong pass bitch");
            out.print("1");
        }

    }

}
