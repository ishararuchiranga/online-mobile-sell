/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Profile_add_name", urlPatterns = {"/Profile_add_name"})
public class Profile_add_name extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession s = request.getSession();

        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        DB.UserRegistration ur = (DB.UserRegistration) s.getAttribute("Login_object");
        DB.UserRegistration ure = (DB.UserRegistration) ses.load(DB.UserRegistration.class, ur.getId());
        if (ure.getUserDetails() != null) {
            DB.UserDetails ud = (DB.UserDetails) ses.load(DB.UserDetails.class, ure.getUserDetails().getId());
            Transaction tr = ses.beginTransaction();
            ud.setFname(request.getParameter("name1"));
            ud.setLname(request.getParameter("name2"));
            ses.update(ud);
            tr.commit();
            ses.close();
        } else {
            Transaction tr = ses.beginTransaction();
            DB.UserDetails ud = new DB.UserDetails(null, request.getParameter("name1"), request.getParameter("name2"), null, null, null, null, null);
            ses.save(ud);
            tr.commit();
            Transaction tr1 = ses.beginTransaction();
            ure.setUserDetails(ud);
            ses.update(ure);
            tr1.commit();
            ses.close();
        }

    }

}
