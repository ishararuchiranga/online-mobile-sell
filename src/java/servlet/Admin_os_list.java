/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Admin_os_list", urlPatterns = {"/Admin_os_list"})
public class Admin_os_list extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("osid");

        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        Criteria c = ses.createCriteria(DB.VersionUse.class);
        DB.OsList osid = (DB.OsList) ses.load(DB.OsList.class, Integer.parseInt(id));
        c.add(Restrictions.eq("osList", osid));
        List<DB.VersionUse> ver = c.list();

        
        try {
            PrintWriter out = response.getWriter();
            //create Json Object
            JSONObject json = new JSONObject();

            // put some value pairs into the JSON object .
            for (DB.VersionUse v : ver) {
                json.put(v.getId() + "", v.getVersionsUse());
                System.out.println(v.getId() + ","+ v.getVersionsUse());
            }
            
            for (int i = 0; i < json.length(); i++) {
                System.out.println(json.length());
            }

            // finally output the json string
            out.print(json);
            
        } catch (JSONException ex) {
            Logger.getLogger(Admin_os_list.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.gc();
    }

}
