/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import DB.UserRegistration;
import Java_classes.first;
import static Java_classes.first.serch_userstatus;
import static Java_classes.first.serch_usertype;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "User_registration", urlPatterns = {"/User_registration"})
public class User_registration extends HttpServlet {

    Integer addcustomerid;
    private String host;
    private String port;
    private String user;
    private String pass;

    public void init() {
        ServletContext context = getServletContext();
        host = context.getInitParameter("host");
        port = context.getInitParameter("port");
        user = context.getInitParameter("user");
        pass = context.getInitParameter("pass");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String text = "<br>Save Successfull<br>"; //message you will recieve 
        String regid = UUID.randomUUID().toString();

        PrintWriter out = response.getWriter();

        String email = request.getParameter("email");

        boolean mailer = false;

        String pwrd = request.getParameter("pwrd");
        String uname = request.getParameter("uname");

        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        Criteria cr = ses.createCriteria(DB.UserRegistration.class);
        cr.add(Restrictions.eq("email", email));
        DB.UserRegistration checku = (DB.UserRegistration) cr.uniqueResult();
        if (checku == null) {
            Transaction tr = ses.beginTransaction();
            DB.UserTypes usertype = (DB.UserTypes) ses.load(DB.UserTypes.class, 1);
            DB.Status status = (DB.Status) ses.load(DB.Status.class, 1);
            String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
            //mail
            String recipient = "ishara.ira@gmail.com";
            String subject = "Verification email";
            String content = "http://localhost:8080/Phonehut.lk_2/User_active?kfghfjghfkd=" + regid;
            try {
                Java_classes.EmailUtility.sendEmail(host, port, user, pass, recipient, subject, content);
                DB.UserRegistration ur = new UserRegistration(status, null, usertype, email, uname, pwrd, regid,date, null, null, null, null, null,null);
                ses.save(ur);
                tr.commit();
            } catch (Exception ex) {
                ex.printStackTrace();
                mailer = true;
                out.print("3");
            }
            System.gc();
            if (!mailer) {
                out.print("1");
            }
        } else {
            out.print("2");
        }
//        addcustomerid = Java_classes.first.addcustomer(email, pwrd, regid, uname);
//
    }

}
