/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Admin_user_chnge", urlPatterns = {"/Admin_user_chnge"})
public class Admin_user_chnge extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("Id");
        String stid = request.getParameter("stid");
        String utid = request.getParameter("utid");
        System.out.println(id);
        System.out.println(stid);
        System.out.println(utid);

        if (stid.equals("nothing")) {
            Session s = conn.NewHibernateUtil.getSessionFactory().openSession();
            Criteria c = s.createCriteria(DB.UserRegistration.class);
            c.add(Restrictions.eq("id", Integer.parseInt(id)));

            DB.UserRegistration user = (DB.UserRegistration) c.uniqueResult();

            if (user != null) {
                Transaction tx = null;
                try {

                    tx = s.beginTransaction();

                    DB.UserTypes usertype = Java_classes.first.serch_usertype(Integer.parseInt(utid), s);
                    DB.UserRegistration ur = (DB.UserRegistration) s.get(DB.UserRegistration.class, Integer.parseInt(id));

                    ur.setUserTypes(usertype);
                    s.update(ur);
                    tx.commit();
                } catch (HibernateException e) {
                    if (tx != null) {
                        tx.rollback();
                    }
                    e.printStackTrace();
                } finally {
                    s.close();
                }

                System.out.println("done");
            } else {
//            response.sendRedirect("Login.jsp?msg=User name or password does not correct");
//            out.print(request.getParameter("uname" + "1"));

                System.out.println("error");
            }
        } else if (utid.equals("nothing")) {
            Session s = conn.NewHibernateUtil.getSessionFactory().openSession();
            Criteria c = s.createCriteria(DB.UserRegistration.class);
            c.add(Restrictions.eq("id", Integer.parseInt(id)));

            DB.UserRegistration user = (DB.UserRegistration) c.uniqueResult();

            if (user != null) {

                Transaction tx = null;
                try {

                    tx = s.beginTransaction();
                    DB.Status status = Java_classes.first.serch_userstatus(Integer.parseInt(stid), s);
                    DB.UserRegistration ur = (DB.UserRegistration) s.get(DB.UserRegistration.class, Integer.parseInt(id));

                    ur.setStatus(status);

                    s.update(ur);
                    tx.commit();
                } catch (HibernateException e) {
                    if (tx != null) {
                        tx.rollback();
                    }
                    e.printStackTrace();
                } finally {
                    s.close();
                }

                System.out.println("done");
            } else {
//            response.sendRedirect("Login.jsp?msg=User name or password does not correct");
//            out.print(request.getParameter("uname" + "1"));

                System.out.println("error");
            }
        } else {
            Session s = conn.NewHibernateUtil.getSessionFactory().openSession();
            Criteria c = s.createCriteria(DB.UserRegistration.class);
            c.add(Restrictions.eq("id", Integer.parseInt(id)));

            DB.UserRegistration user = (DB.UserRegistration) c.uniqueResult();

            if (user != null) {

                Transaction tx = null;
                try {

                    tx = s.beginTransaction();
                    DB.Status status = Java_classes.first.serch_userstatus(Integer.parseInt(stid), s);
                    DB.UserTypes usertype = Java_classes.first.serch_usertype(Integer.parseInt(utid), s);
                    DB.UserRegistration ur = (DB.UserRegistration) s.get(DB.UserRegistration.class, Integer.parseInt(id));

                    ur.setStatus(status);
                    ur.setUserTypes(usertype);
                    s.update(ur);
                    tx.commit();
                } catch (HibernateException e) {
                    if (tx != null) {
                        tx.rollback();
                    }
                    e.printStackTrace();
                } finally {
                    s.close();
                }

                System.out.println("done");
            } else {
//            response.sendRedirect("Login.jsp?msg=User name or password does not correct");
//            out.print(request.getParameter("uname" + "1"));

                System.out.println("error");
            }
        }
System.gc();
    }

}
