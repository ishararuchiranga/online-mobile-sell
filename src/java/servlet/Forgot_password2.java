/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Forgot_password2", urlPatterns = {"/Forgot_password2"})
public class Forgot_password2 extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        String pwrd = request.getParameter("pwrd");

        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        Criteria c = ses.createCriteria(DB.UserRegistration.class);
        c.add(Restrictions.eq("id", Integer.parseInt(id)));
        DB.UserRegistration ur = (DB.UserRegistration) c.uniqueResult();

//      
        if (ur != null){
            PrintWriter out = response.getWriter();
            out.print("success");
            Transaction tr = ses.beginTransaction();
            DB.UserRegistration usr = (DB.UserRegistration) ses.get(DB.UserRegistration.class, Integer.parseInt(id));
            usr.setPassword(pwrd);
            usr.setRegisterId("0");
            ses.update(usr);
            tr.commit();
            ses.close();
        }
        System.gc();
    }

}
