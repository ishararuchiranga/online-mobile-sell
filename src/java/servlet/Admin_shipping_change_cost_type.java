/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Admin_shipping_change_cost_type", urlPatterns = {"/Admin_shipping_change_cost_type"})
public class Admin_shipping_change_cost_type extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        String type = request.getParameter("type");

        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        PrintWriter out = response.getWriter();

        DB.District dis = (DB.District) ses.load(DB.District.class, Integer.parseInt(id));
        try {
            if (Integer.parseInt(type) == 1 || Integer.parseInt(type) == 2) {
                Transaction tr = ses.beginTransaction();
                DB.CostType cost = (DB.CostType) ses.load(DB.CostType.class, Integer.parseInt(type));
                dis.setCostType(cost);
                ses.update(dis);
                tr.commit();
                ses.close();
            } else {
                out.print(dis.getCostType().getId());
            }
        } catch (NumberFormatException e) {
            out.print(dis.getCostType().getId());
        }

    }

}
