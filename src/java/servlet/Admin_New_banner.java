/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import DB.SiteData;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.validator.UrlValidator;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Admin_New_banner", urlPatterns = {"/Admin_New_banner"})
public class Admin_New_banner extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String url = request.getParameter("url");

        if (url.isEmpty()) {
            url = "#";
        }

        UrlValidator urlValidator = new UrlValidator();
        if (urlValidator.isValid(url)) {
            System.out.println("val");
        } else {
            System.out.println("inv");
        }

        PrintWriter out = response.getWriter();
        HttpSession ses = request.getSession();
        String imgpath = (String) ses.getAttribute("path");
        String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        String time = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());

        Session se = conn.NewHibernateUtil.getSessionFactory().openSession();
        Criteria cr = se.createCriteria(DB.SiteData.class);

        DB.UserRegistration ur = (DB.UserRegistration) ses.getAttribute("Login_object");

        List<DB.SiteData> si = cr.list();
        if (si.isEmpty()) {
            Criteria cr2 = se.createCriteria(DB.Banner.class);
            List<DB.Banner> ba = cr2.list();
            if (ba.isEmpty()) {
                Transaction tr = se.beginTransaction();
                DB.SiteData site = new SiteData(ur, date, time, null);
                se.save(site);
                DB.Banner ban = new DB.Banner(site, imgpath, url);
                se.save(ban);
                tr.commit();
                se.close();
            }
        } else {

            Transaction tr = se.beginTransaction();
            DB.SiteData site = new SiteData(ur, date, time, null);
            se.save(site);
            DB.Banner ban = new DB.Banner(site, imgpath, url);
            se.save(ban);
            tr.commit();
            se.close();
        }
    }

}
