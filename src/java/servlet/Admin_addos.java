/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import DB.OsList;
import DB.UserRegistration;
import static Java_classes.first.serch_userstatus;
import static Java_classes.first.serch_usertype;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Admin_addos", urlPatterns = {"/Admin_addos"})
public class Admin_addos extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String os = request.getParameter("nwos");
        PrintWriter out=response.getWriter();
        System.out.println(os);

        Session s = conn.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr = null;
        try {
            tr = s.beginTransaction();
            
            DB.OsList oll=new DB.OsList(os, null);
            s.save(oll);
            out.print(oll.getId());
            
            tr.commit();
            s.close();
           
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.gc();
    }
}
