/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Admin_add_os_version", urlPatterns = {"/Admin_add_os_version"})
public class Admin_add_os_version extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String os=request.getParameter("os");
        String ver=request.getParameter("ver");
        
        PrintWriter out=response.getWriter();
        
        Session s=conn.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr=null;
        try {
            tr=s.beginTransaction();
            
            DB.OsList ol=(DB.OsList) s.load(DB.OsList.class, Integer.parseInt(os));
            
            DB.VersionUse ve=new DB.VersionUse(ol, ver, null);
            s.save(ve);
            int id=ve.getId();
           
            tr.commit();
            s.close();
             out.print(id);
           
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.gc();
    }

}
