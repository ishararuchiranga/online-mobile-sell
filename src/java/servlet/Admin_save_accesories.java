/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Admin_save_accesories", urlPatterns = {"/Admin_save_accesories"})
public class Admin_save_accesories extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String brand = request.getParameter("brand");
        String model = request.getParameter("model");
        String detail = request.getParameter("detail");
        String devtype = request.getParameter("devtype");
        String keyword = "";

        Session s = conn.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr = s.beginTransaction();

        DB.AccesoriesAndParts pa = new DB.AccesoriesAndParts("", detail, null);
        s.save(pa);

        DB.Manufacturers ma = (DB.Manufacturers) s.load(DB.Manufacturers.class, Integer.parseInt(brand));
        DB.DeviceType dt = (DB.DeviceType) s.load(DB.DeviceType.class, Integer.parseInt(devtype));

        HttpSession ses = request.getSession();
        String path = (String) ses.getAttribute("path");

        keyword += ma.getName() + ",";
        keyword += model + ",";
        keyword += detail + ",";
        keyword += ma.getName() + " " + model + ",";
        keyword += model + " " + ma.getName() + ",";

        DB.Model mo = new DB.Model(pa, dt, ma, null, model, path, keyword, null);
        s.save(mo);

        String price = request.getParameter("price");
        String qty = request.getParameter("qty");

        String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        String time = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());

        DB.ProStatus prost = (DB.ProStatus) s.load(DB.ProStatus.class, Integer.parseInt("1"));

        DB.Products pro = new DB.Products(mo, prost,date, Double.parseDouble(price), qty, "", time, null,null, null);
        s.save(pro);

        tr.commit();
        s.close();
    }

}
