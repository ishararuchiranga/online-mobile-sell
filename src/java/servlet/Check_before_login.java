/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Check_before_login", urlPatterns = {"/Check_before_login"})
public class Check_before_login extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String email = request.getParameter("email");
        String pword = request.getParameter("pword");

        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        Criteria cr = ses.createCriteria(DB.UserRegistration.class);
        cr.add(Restrictions.eq("email", email));
        cr.add(Restrictions.eq("password", pword));
        DB.UserRegistration ur = (DB.UserRegistration) cr.uniqueResult();
        if (ur != null) {
            if (ur.getStatus().getId() == 2) {
                out.print("4");
            } else if (ur.getStatus().getId() == 1) {
                out.print("2");
            } else {
                out.print("3");
            }
        } else {
            out.print("1");
        }
    }

}
