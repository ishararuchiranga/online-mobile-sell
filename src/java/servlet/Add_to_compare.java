/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import Java_classes.Cart;
import Java_classes.Compare;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Add_to_compare", urlPatterns = {"/Add_to_compare"})
public class Add_to_compare extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id;

        id = request.getParameter("id");

        System.out.println(id);
        Java_classes.NewCompareItems nc = new Java_classes.NewCompareItems();

        nc.setProid(id);

//
        HttpSession hs = request.getSession();

        Java_classes.Compare c = null;

        if (hs.getAttribute("mycompare") == null) {
            c = new Compare();
        } else {
            c = (Compare) hs.getAttribute("mycompare");
        }

        c.addcompare(nc);

        hs.setAttribute("mycompare", c);

    }

}
