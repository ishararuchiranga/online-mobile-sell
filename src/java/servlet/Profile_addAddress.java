/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Profile_addAddress", urlPatterns = {"/Profile_addAddress"})
public class Profile_addAddress extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession se = request.getSession();
        DB.UserRegistration ure = (DB.UserRegistration) se.getAttribute("Login_object");

        String addressLine1 = request.getParameter("line1");
        String addressLine2 = request.getParameter("line2");
        String city = request.getParameter("city");
        String district = request.getParameter("province");

        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        DB.UserRegistration ur = (DB.UserRegistration) ses.load(DB.UserRegistration.class, ure.getId());

        if (ur.getUserDetails() != null) {
//           
            Transaction tr = ses.beginTransaction();
            DB.UserDetails reg = (DB.UserDetails) ses.get(DB.UserDetails.class, ur.getUserDetails().getId());
            DB.District dis = (DB.District) ses.get(DB.District.class, Integer.parseInt(district));
            reg.setAdLine1(addressLine1);
            reg.setAdLine2(addressLine2);
            reg.setCity(city);
            reg.setDistrict(dis);
            ses.update(reg);
            tr.commit();
        } else {
            Transaction tr2 = ses.beginTransaction();
            DB.District dis = (DB.District) ses.get(DB.District.class, Integer.parseInt(district));
            DB.UserDetails ud = new DB.UserDetails(dis, null, null, null, addressLine1, addressLine2, city, null);
            ses.save(ud);
            tr2.commit();
            Transaction tr3 = ses.beginTransaction();
            DB.UserRegistration reg = (DB.UserRegistration) ses.get(DB.UserRegistration.class, ur.getId());
            reg.setUserDetails(ud);
            ses.update(reg);
            tr3.commit();
        }
    }
}
