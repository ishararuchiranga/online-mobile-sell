/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Admin_shipping_free_to_nofree", urlPatterns = {"/Admin_shipping_free_to_nofree"})
public class Admin_shipping_free_to_nofree extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String id = request.getParameter("id");
        String cost = request.getParameter("cost");
        String type = request.getParameter("type");

        if (Integer.parseInt(id)<=5000) {
            Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
            DB.District d = (DB.District) ses.load(DB.District.class, Integer.parseInt(id));
            if (d != null) {
                Transaction tr = ses.beginTransaction();
                DB.CostType ct = (DB.CostType) ses.load(DB.CostType.class, 1);
                d.setCost(Double.parseDouble(cost));
                d.setCostType(ct);
                ses.save(d);
                tr.commit();
                ses.close();
                out.print("1");
            }
        }else{
            out.print("2");
        }
    }
}
