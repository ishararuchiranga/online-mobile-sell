package servlet;

import Java_classes.Cart;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Add_to_cart", urlPatterns = {"/Add_to_cart"})
public class Add_to_cart extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        PrintWriter out = response.getWriter();

        String id, qty;
        id = request.getParameter("id");
        qty = request.getParameter("qty");

        DB.Products pro = (DB.Products) ses.load(DB.Products.class, Integer.parseInt(id));

        try {
            if (Integer.parseInt(pro.getQty()) >= Integer.parseInt(qty)) {
                Java_classes.NewCartItems nc = new Java_classes.NewCartItems();

                nc.setProid(Integer.parseInt(id));
                nc.setProQty(Integer.parseInt(qty));
//
                HttpSession hs = request.getSession();

                Cart c = null;

                if (hs.getAttribute("mycart") == null) {
                    c = new Cart();
                } else {
                    c = (Cart) hs.getAttribute("mycart");
                }

                c.addProductToCart(nc);

                hs.setAttribute("mycart", c);

                if (hs.getAttribute("Login_object") != null) {
                    Transaction tr = ses.beginTransaction();
                    Criteria cr = ses.createCriteria(DB.Cart.class);
                    DB.Products probj = (DB.Products) ses.load(DB.Products.class, Integer.parseInt(id));
                    cr.add(Restrictions.eq("products", probj));

                    DB.UserRegistration ur = (DB.UserRegistration) hs.getAttribute("Login_object");

                    cr.add(Restrictions.eq("userRegistration", ur));
                    DB.Cart car = (DB.Cart) cr.uniqueResult();
                    if (car == null) {
                        DB.Cart crtsave = new DB.Cart(probj, ur, qty);
                        ses.save(crtsave);
                    } else {
                        car.setQty((Integer.parseInt(car.getQty()) + Integer.parseInt(qty)) + "");
                        ses.save(car);
                    }
                    tr.commit();
                    ses.close();
                }
                response.sendRedirect("index.jsp");
            } else {
                out.print("1");
            }
        } catch (NumberFormatException e) {
            out.print("2");
        }
    }
}
