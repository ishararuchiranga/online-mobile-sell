/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Checkout_check_disprice", urlPatterns = {"/Checkout_check_disprice"})
public class Checkout_check_disprice extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String id = request.getParameter("id");

        HttpSession hs = request.getSession();
        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();

        DB.District dis = (DB.District) ses.load(DB.District.class, Integer.parseInt(id));

        Java_classes.Cart c3 = (Java_classes.Cart) hs.getAttribute("mycart");
        List<Java_classes.NewCartItems> nnew3 = c3.getArrayData();
        double newtotprice = 0;
        for (Java_classes.NewCartItems nci : nnew3) {
            double itemsprice;
            DB.Products pro = (DB.Products) ses.load(DB.Products.class, nci.getProid());
            itemsprice = nci.getProQty() * pro.getPrice();
            newtotprice += itemsprice;

        }
         DecimalFormat df = new DecimalFormat("0.00");

        if (dis.getCostType().getId() == 1) {
            newtotprice += dis.getCost();
            out.print("1," + df.format(dis.getCost()) + "," + df.format(newtotprice));
        } else {
            out.print("2," + df.format(newtotprice));
        }

    }

}
