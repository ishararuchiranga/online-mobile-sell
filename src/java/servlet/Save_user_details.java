/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Ishara
 */
@WebServlet(name = "Save_user_details", urlPatterns = {"/Save_user_details"})
public class Save_user_details extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String phone = "";

        HttpSession sesht = request.getSession();
        DB.UserRegistration ur = (DB.UserRegistration) sesht.getAttribute("Login_object");
        int id = ur.getId();

        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String l1 = request.getParameter("l1");
        String l2 = request.getParameter("l2");
        String city = request.getParameter("city");
        String dis = request.getParameter("dis");
        phone = request.getParameter("phn");

        Session ses = conn.NewHibernateUtil.getSessionFactory().openSession();
        DB.District dist = (DB.District) ses.load(DB.District.class, Integer.parseInt(dis));

        Transaction tr = ses.beginTransaction();
        DB.UserDetails ud = new DB.UserDetails(dist, fname, lname, phone, l1, l2, city, null);
        ses.save(ud);
        tr.commit();

        Transaction tr2 = ses.beginTransaction();
        DB.UserRegistration ur2 = (DB.UserRegistration) ses.load(DB.UserRegistration.class, ur.getId());
        ur2.setUserDetails(ud);
        ses.update(ur2);
        tr2.commit();
    }

}
