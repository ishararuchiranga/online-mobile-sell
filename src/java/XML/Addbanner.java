/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package XML;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Ishara
 */
public class Addbanner {
    public  void addimage(String url) {
        try {
            DocumentBuilderFactory dbFactory
                    = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder
                    = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            // root element
            Element rootElement = doc.createElement("Home");
            doc.appendChild(rootElement);

            Element supercar = doc.createElement("Banner");
            rootElement.appendChild(supercar);

            Element image = doc.createElement("image");
            Attr attrType = doc.createAttribute("type");
            attrType.setValue("image");
            image.setAttributeNode(attrType);
            image.appendChild(doc.createTextNode("image1"));
            supercar.appendChild(image);
            //
            Element urlxml = doc.createElement("image");
            Attr attrType2 = doc.createAttribute("type");
            attrType2.setValue("image");
            urlxml.setAttributeNode(attrType2);
            urlxml.appendChild(doc.createTextNode(url));
            supercar.appendChild(urlxml);
//
//            Element carname1 = doc.createElement("image");
//            Attr attrType1 = doc.createAttribute("type");
//            attrType1.setValue("image2");
//            carname1.setAttributeNode(attrType1);
//            carname1.appendChild(doc.createTextNode("url2"));
//            supercar.appendChild(carname1);

            // write the content into xml file
            String filepath = "src\\java\\xmlwebinfo\\visitor.xml";
            
            TransformerFactory transformerFactory
                    = TransformerFactory.newInstance();
            Transformer transformer
                    = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result
                    = new StreamResult(new File("src\\java\\XML\\websitedata.xml"));
            transformer.transform(source, result);
            // Output to console for testing
            StreamResult consoleResult
                    = new StreamResult(System.out);
            transformer.transform(source, consoleResult);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

 
}
