package XML;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.File;

public class CreateXmlFileDemo {

    public static void main(String argv[]) {

        try {
            DocumentBuilderFactory dbFactory
                    = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder
                    = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            // root element
            Element rootElement = doc.createElement("Home");
            doc.appendChild(rootElement);

            //  supercars element
            Element supercar = doc.createElement("Banner");
            rootElement.appendChild(supercar);
    
            // carname element
            Element carname = doc.createElement("image");
            Attr attrType = doc.createAttribute("type");
            attrType.setValue("image");
            carname.setAttributeNode(attrType);
            carname.appendChild(doc.createTextNode("ull1"));
            supercar.appendChild(carname);
//
//            Element carname1 = doc.createElement("image");
//            Attr attrType1 = doc.createAttribute("type");
//            attrType1.setValue("image2");
//            carname1.setAttributeNode(attrType1);
//            carname1.appendChild(doc.createTextNode("url2"));
//            supercar.appendChild(carname1);
            Element supercar2 = doc.createElement("Banner");
            rootElement.appendChild(supercar2);

            Element carname2 = doc.createElement("image");
            Attr attrType2 = doc.createAttribute("type");
            attrType2.setValue("image");
            carname2.setAttributeNode(attrType2);
            carname2.appendChild(doc.createTextNode("ull2"));
            supercar2.appendChild(carname2);

            // write the content into xml file
            TransformerFactory transformerFactory
                    = TransformerFactory.newInstance();
            Transformer transformer
                    = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result
                    = new StreamResult(new File("src\\java\\XML\\websitedata.xml"));
            transformer.transform(source, result);
            // Output to console for testing
            StreamResult consoleResult
                    = new StreamResult(System.out);
            transformer.transform(source, consoleResult);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
