/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Java_classes;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ishara
 */
public class Compare {

    private List<NewCompareItems> arrayData;

    public Compare() {
        arrayData = new ArrayList<NewCompareItems>();
    }

    /**
     * @return the arrayData
     */
    public List<NewCompareItems> getArrayData() {
        return arrayData;
    }

    public void addcompare(NewCompareItems ni) {
        int qty = 0;
        arrayData.add(ni);
    }

    public void removecompare(NewCompareItems nc) {
        for (int i = 0; i < arrayData.size(); i++) {
            NewCompareItems elem = arrayData.get(i);
            if (elem.getProid().equals(nc.getProid())) {
                arrayData.remove(elem);
            }
        }
    }
}
