package Java_classes;

/**
 * @author Ishara
 */
public class NewCartItems {

    private int proid;

    private int proQty;

    /**
     * @return the proid
     */
    public int getProid() {
        return proid;
    }

    /**
     * @param proid the proid to set
     */
    public void setProid(int proid) {
        this.proid = proid;
    }

    /**
     * @return the proQty
     */
    public int getProQty() {
        return proQty;
    }

    /**
     * @param proQty the proQty to set
     */
    public void setProQty(int proQty) {
        this.proQty = proQty;
    }

}
