/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Java_classes;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ishara
 */
public class Cart {

    private List<NewCartItems> arrayData;

    public Cart() {
        arrayData = new ArrayList<NewCartItems>();
    }

    /**
     * @return the arrayData
     */
    public List<NewCartItems> getArrayData() {
        return arrayData;
    }

    public void addProductToCart(NewCartItems ni) {
        int qty = 0;
        for (int i = 0; i < arrayData.size(); i++) {
            NewCartItems old = arrayData.get(i);

            if (old.getProid() == ni.getProid()) {
                qty = old.getProQty() + ni.getProQty();
                arrayData.remove(old);
                ni.setProQty(qty);
            }
        }
        arrayData.add(ni);
    }

    public void removepro(NewCartItems nc) {
        for (int i = 0; i < arrayData.size(); i++) {
            NewCartItems elem = arrayData.get(i);
            if (elem.getProid() == nc.getProid()) {
                arrayData.remove(elem);
            }
        }
    }

}
