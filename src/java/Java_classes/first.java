/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Java_classes;

import DB.UserRegistration;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ishara
 */
public class first {

    //Save user
    private static Integer Cusid;

    public static Integer addcustomer(String email, String password, String regid, String uname) {
        Session s = conn.NewHibernateUtil.getSessionFactory().openSession();
        Transaction tr = null;
        try {
            tr = s.beginTransaction();
            DB.UserTypes usertype = serch_usertype(Integer.parseInt("1"), s);
            DB.Status status = serch_userstatus(Integer.parseInt("1"), s);
String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
            DB.UserRegistration ur = new UserRegistration(status, null, usertype, email, uname, password, regid,date, null, null,null,null,null,null);
            s.save(ur);
            Cusid = ur.getId();
            tr.commit();
            s.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Cusid;
    }

    public static DB.UserTypes serch_usertype(Integer iduserType, Session ses) {
        DB.UserTypes serch_UserType = (DB.UserTypes) ses.load(DB.UserTypes.class, iduserType);
        return serch_UserType;
    }

    public static DB.Status serch_userstatus(Integer statusid, Session ses) {
        DB.Status stat = (DB.Status) ses.load(DB.Status.class, statusid);
        return stat;
    }

    //Login user
    public static DB.UserRegistration Userlogin(Session ses, String email, String password) {
        Criteria c = ses.createCriteria(DB.UserRegistration.class);
        c.add(Restrictions.eq("email", email));
        c.add(Restrictions.eq("password", password));
        return (UserRegistration) c.uniqueResult();
    }
}
